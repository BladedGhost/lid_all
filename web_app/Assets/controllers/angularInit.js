﻿
ngCommon.controller('DashStaffOnShift', function ($scope, $http) {
    var url = '/Services/Ajaxcalls.asmx/ngStaffOnShift';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })
})

ngCommon.controller('DashBdays', function ($scope, $http) {
    $scope._id = function () {
        var url = '/Services/Ajaxcalls.asmx/ngDashBdays?day' ;
        $http.get(url)
        .success(function (data) {
            var myjson = JSON.parse(data);
            $scope.data = JSON.parse(myjson);
        })
    }
})

//applaodStudents.controller('StudentMarksController', function ($scope, $http) {
//    $scope.loadStudents = function (idClass, idExam, idSubject) {
//        var url = '/Service/AjaxData.asmx/getStudentsMarks?Class=' + idClass + '&Exam=' + idExam + '&Subject=' + idSubject;
//        $http.get(url)
//        .success(function (data) {
//            var myjson = JSON.parse(data);
//            $scope.data = JSON.parse(myjson);
//        })
//    };



ngCommon.controller('DashTodaysShift', function ($scope, $http) {
    var url = '/Services/Ajaxcalls.asmx/ngTodaysShift';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })
})

ngCommon.controller('DashAnnouncement', function ($scope, $http) {
    var url = '/Services/Ajaxcalls.asmx/ngAnnouncement';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })
})

ngCommon.controller('StaffCountController', function ($scope, $http) {
    var url = '/Services/Ajaxcalls.asmx/ngStaffCount';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })
})

ngCommon.controller('dashStudentsController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getStudent_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number Of Students Per Class'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

ngCommon.controller('dashTeachersController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getTeacher_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number Of Teacher Per Class'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

ngCommon.controller('dashClassController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getClasses_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Class Capacity vs Number Of Students'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

ngCommon.controller('MyClassController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getStudent_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Students Grades Capacity'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})





























//appSubject.controller('SubjectsController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getSubjects';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})


//appClass.controller('ClassController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getClass';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appStudents.controller('StudentsController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getStudents';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appTeachers.controller('TeachersController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getTeachers';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appParents.controller('ParentsController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getParents';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appExam.controller('ExamsController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getExams';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appBook.controller('BooksController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getBooks';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appTransaport.controller('TransaportController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getTransport';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appRoom.controller('RoomsController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getRooms';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appRoutine.controller('RoutineController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getRoutine';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appClassRoutine.controller('ClassRoutineController', function ($scope, $http) {
//    $scope.loadClassRoutine = function (IDClass) {
//        var url = '/Service/AjaxData.asmx/getClassRoutine?IDClass=' + IDClass;
//        $http.get(url)
//        .success(function (data) {
//            var myjson = JSON.parse(data);
//            $scope.data = JSON.parse(myjson);
//        })
//    };
//})

//appDays.controller('WeekDaysController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getClassRoutine';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appSlots.controller('SlotsController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getClassRoutine';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})



//appLog.controller('LoginHistoryController', function ($scope, $http) {
//    var url = '/Service/AjaxData.asmx/getLoginHistory?IDMembership=1';
//    $http.get(url)
//    .success(function (data) {
//        var myjson = JSON.parse(data);
//        $scope.data = JSON.parse(myjson);
//    })
//})

//appStudentPayment.controller('StudentPaymentController', function ($scope, $http) {
//    $scope.loadStudentPayment = function (IDStudent) {
//        var url = '/Service/AjaxData.asmx/getStudentPaymentHistory?IDStudent=' + IDStudent;
//        $http.get(url)
//        .success(function (data) {
//            var myjson = JSON.parse(data);
//            $scope.data = JSON.parse(myjson);
//        })
//    };
//})

//appStudentReport.controller('StudentReportController', function ($scope, $http) {
//    $scope.loadStudentReport = function (IDStudent) {
//        var url = '/Service/AjaxData.asmx/getStudentReports?IDStudent=' + IDStudent;
//        $http.get(url)
//        .success(function (data) {
//            var myjson = JSON.parse(data);
//            $scope.data = JSON.parse(myjson);
//        })
//    };
//})

//applaodStudents.controller('StudentMarksController', function ($scope, $http) {
//    $scope.loadStudents = function (idClass, idExam, idSubject) {
//        var url = '/Service/AjaxData.asmx/getStudentsMarks?Class=' + idClass + '&Exam=' + idExam + '&Subject=' + idSubject;
//        $http.get(url)
//        .success(function (data) {
//            var myjson = JSON.parse(data);
//            $scope.data = JSON.parse(myjson);
//        })
//    };

//    $scope.updateMarks = function (Mark,Student,Exam,Subject) {
//        var url = '/Service/AjaxData.asmx/updStudentsMarks?Mark=' + Mark + '&Student=' + Student + '&Exam=' + Exam + '&Subject=' + Subject;
//        $http.get(url)
//        .success(function (data) {
//            //var myjson = JSON.parse(data);
//            //$scope.data = JSON.parse(myjson);
//        })
//    };
//});

