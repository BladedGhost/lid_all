﻿
var app = angular.module('Subjects', []);

app.controller('SubjectsController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getSubjects';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })
})

