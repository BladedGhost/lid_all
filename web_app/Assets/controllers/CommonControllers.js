﻿
var appStudents = angular.module('dashStudents', []);
var appTeachers = angular.module('dashTeachers', []);
var appMarks = angular.module('dashMarks', []);
var appLibrary = angular.module('dashLibrary', []);
var appClass = angular.module('dashClass', []);
var appPerfomance = angular.module('dashPerfomance', []);
var appSchool = angular.module('dashSchool', []);

appStudents.controller('dashStudentsController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getStudent_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Students Grades Capacity'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

appTeachers.controller('dashTeachersController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getStudent_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Students Grades Capacity'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

appMarks.controller('dashMarksController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getStudent_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Students Grades Capacity'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

appLibrary.controller('dashLibraryController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getStudent_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Students Grades Capacity'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

appClass.controller('dashClassController', function ($scope, $http) {
    var url = '/Service/AjaxData.asmx/getStudent_dash';
    $http.get(url)
    .success(function (data) {
        var myjson = JSON.parse(data);
        $scope.data = JSON.parse(myjson);
    })

    setTimeout(function () {
        $('#Chart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Students Grades Capacity'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Count'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})

appPerfomance.controller('PerfomanceController', function ($scope, $http) {
        var url = '/Service/AjaxData.asmx/getStudentPerfomance';
        $http.get(url)
        .success(function (data) {
            var myjson = JSON.parse(data);
            $scope.data = JSON.parse(myjson);
        })

    setTimeout(function () {
        $('#dashPerfomanceChart').highcharts({
            data: {
                table: document.getElementById('DataTable')
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Students Perfomance'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Marks'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.y + ' ' + this.x.toLowerCase();
                }
            }
        });
    }, 1000);
})



