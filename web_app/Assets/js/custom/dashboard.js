jQuery(document).ready(function () {
    jQuery('#tabs').tabs();

    jQuery(".datepicker").datepicker();



    jQuery('#overviewselect, input:checkbox').uniform();

    ///// DATE PICKER /////
    jQuery("#datepickfrom, #datepickto").datepicker();

    ///// SLIM SCROLL /////
    jQuery('#scroll1').slimscroll({
        color: '#666',
        size: '10px',
        width: 'auto',
        height: '175px'
    });

    ///// ACCORDION /////
    jQuery('#accordion').accordion({ autoHeight: false });

    ///// SIMPLE CHART /////
    var flash = [[0, 2], [1, 6], [2, 3], [3, 8], [4, 5], [5, 13], [6, 8]];
    var html5 = [[0, 5], [1, 4], [2, 4], [3, 1], [4, 9], [5, 10], [6, 13]];

    function showTooltip(x, y, contents) {
        jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5
        }).appendTo("body").fadeIn(200);
    }


    //var plot = jQuery.plot(jQuery("#chartplace"),
    //	   [ { data: flash, label: "Flash(x)", color: "#069"}, { data: html5, label: "HTML5(x)", color: "#FF6600"} ], {
    //		   series: {
    //			   lines: { show: true, fill: true, fillColor: { colors: [ { opacity: 0.05 }, { opacity: 0.15 } ] } },
    //			   points: { show: true }
    //		   },
    //		   legend: { position: 'nw'},
    //		   grid: { hoverable: true, clickable: true, borderColor: '#ccc', borderWidth: 1, labelMargin: 10 },
    //		   yaxis: { min: 0, max: 15 }
    //		 });

    //var previousPoint = null;
    //jQuery("#chartplace").bind("plothover", function (event, pos, item) {
    //	jQuery("#x").text(pos.x.toFixed(2));
    //	jQuery("#y").text(pos.y.toFixed(2));

    //	if(item) {
    //		if (previousPoint != item.dataIndex) {
    //			previousPoint = item.dataIndex;

    //			jQuery("#tooltip").remove();
    //			var x = item.datapoint[0].toFixed(2),
    //			y = item.datapoint[1].toFixed(2);

    //			showTooltip(item.pageX, item.pageY,
    //							item.series.label + " of " + x + " = " + y);
    //		}

    //	} else {
    //	   jQuery("#tooltip").remove();
    //	   previousPoint = null;            
    //	}

    //});

    //jQuery("#chartplace").bind("plotclick", function (event, pos, item) {
    //	if (item) {
    //		jQuery("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
    //		plot.highlight(item.series, item.datapoint);
    //	}
    //});


    ///// SWITCHING LIST FROM 3 COLUMNS TO 2 COLUMN LIST /////
    function rearrangeShortcuts() {
        if (jQuery(window).width() < 430) {
            if (jQuery('.shortcuts li.one_half').length == 0) {
                var count = 0;
                jQuery('.shortcuts li').removeAttr('class');
                jQuery('.shortcuts li').each(function () {
                    jQuery(this).addClass('one_half');
                    if (count % 2 != 0) jQuery(this).addClass('last');
                    count++;
                });
            }
        } else {
            if (jQuery('.shortcuts li.one_half').length > 0) {
                jQuery('.shortcuts li').removeAttr('class');
            }
        }
    }
    jQuery('#calendar').fullCalendar({
        //events: source,
        header: {
            left: 'month', //,agendaWeek,agendaDay',
            center: 'title',
            right: 'today, prev, next'
        },
        buttonText: {
            prev: '&laquo;',
            next: '&raquo;',
            prevYear: '&nbsp;&lt;&lt;&nbsp;',
            nextYear: '&nbsp;&gt;&gt;&nbsp;',
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
        },
        //eventClick:  function(event, jsEvent, view) {
        //    //set the values and open the modal
        //    $("#eventInfo").html(event.description);
        //    $("#eventLink").attr('href', event.url);
        //    $("#eventContent").dialog({ modal: true, title: event.title });
        //},
        dayClick: function (date, jsEvent, view) {
            var dt = date.format("dd MMM yyyy");
            //alert($("#hCalendar").text());
            //document.getElementById("bday").innerText = dt;
            window.radopen("/Auth/frm/frmCalendar.aspx?I=" + dt, "Add_win");
            //jQuery.ajax({
            //    type: "GET",
            //    url: '/Services/Ajaxcalls.asmx/ngDashBdays',
            //    dataType: "json",
            //    data: {dob:dt},
            //    success: function (data) {
            //        var objdata = jQuery.parseJSON(data);
            //        jQuery('#dob-list li').each(function () {
            //            jQuery(this).remove();
            //        });
            //        for (var count in objdata) {
            //            jQuery("#dob-list").append('' +
            //               ' <li>'+
            //               '    <div>'+
            //               '         <span class="three_fourth">'+
            //               '             <span class="left">'+
            //               '                 <span class="title"><a href="#">' + objdata[count].fullname + '</a></span>' +
            //               '                 <span class="desc">' + objdata[count].dept + ' [' + objdata[count].position_name + ']</span>' +
            //               '             </span>'+
            //               '         </span>'+
            //               '         <span class="one_fourth last">'+
            //               '             <span class="right">'+
            //               '                 <span class="h3">'+
            //               '                     <img src="/Assets/images/thumbs/avatar.png" alt="" style="width: 16px; height: 16px; padding-bottom: 2px 0px" /><br />' +
            //               '                 </span>'+
            //               '             </span>'+
            //               '        </span>'+
            //               '         <br clear="all" />'+
            //               '     </div>'+
            //               ' </li>');
            //        }
            //    },
            //    error: function (XMLHttpRequest, textStatus, errorThrown) {
            //            debugger;
            //        }
            //});


            //jQuery.ajax({
            //    type: "GET",
            //    url: '/Services/Ajaxcalls.asmx/ngDashStaffOnLeave',
            //    dataType: "json",
            //    data: { dob: dt },
            //    success: function (data) {
            //        var objdata = jQuery.parseJSON(data);
            //        jQuery('#leave-list li').each(function () {
            //            jQuery(this).remove();
            //        });
            //        for (var count in objdata) {
            //            jQuery("#leave-list").append('' +
            //               ' <li>' +
            //               '    <div>' +
            //               '         <span class="three_fourth">' +
            //               '             <span class="left">' +
            //               '                 <span class="title"><a href="#">' + objdata[count].fullname + '</a><small>' + objdata[count].dept + ' [' + objdata[count].position_name + ']</small> </span>' +
            //               '                 <span class="desc">' + objdata[count].type + ' [' + objdata[count].period + ']</span>' +
            //               '             </span>' +
            //               '         </span>' +
            //               '     </div>' +
            //               ' </li>');
            //        }
            //    },
            //    error: function (XMLHttpRequest, textStatus, errorThrown) {
            //        debugger;
            //    }
            //});
        },
        editable: true,
        droppable: false, // this allows things to be dropped onto the calendar !!!

    });
    function reposTitle() {
        if (jQuery(window).width() < 450) {
            if (!jQuery('.fc-header-title').is(':visible')) {
                if (jQuery('h3.calTitle').length == 0) {
                    var m = jQuery('.fc-header-title h2').text();
                    jQuery('<h3 class="calTitle">' + m + '</h3>').insertBefore('#calendar table.fc-header');
                }
            }
        } else {
            jQuery('h3.calTitle').remove();
        }
    }
    reposTitle();

    rearrangeShortcuts();

    ///// ON RESIZE WINDOW /////
    jQuery(window).resize(function () {
        rearrangeShortcuts();
        reposTitle();
    });


		
});
