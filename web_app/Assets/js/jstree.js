﻿
//Primary byteFile Class (Singleton)
var byteFile = {

    ajax: {
        loader: null,
        modal: null,

        init: function () {
            byteFile.ajax.loader = jQuery('<div id="fleeMate_loader" class="wrap"><div class="bg"><div class="loading"><span style="top="-5px" margin-left: 0px; id="loader_message" class="title">' + byteFile.title + '</span><span class="text">' + byteFile.locale.loader.message + '</span> </div></div></div>').appendTo(document.body);
            byteFile.ajax.modal = jQuery('<div id="byteFile_loader_modal" class="wrap_modal"></div>').appendTo(document.body);
        }
    },

    load: function (active, element, top, left, message) {
        var where;

        try {
            if (typeof message != 'undefined' && message.length > 0) {
                jQuery('#loader_message').text(message);
            }

            byteFile.loader.hide();

            if (!active) {
                jQuery('#loader_message').text(byteFile.locale.loader.message);

                byteFile.ajax.modal.hide();
                byteFile.ajax.loader.hide();

                jQuery(document.body).css('cursor', '');

                return;
            }

            if (byteFile.ajax.modal != null) {
                jQuery(byteFile.ajax.modal).height(jQuery(document).height());
            }

            if (byteFile.base != undefined && byteFile.base != null) {
                where = 'BASE';

                byteFile.base.events.window.resize.create({
                    key: 'byteFile_loader',
                    event: function () {
                        try {
                            byteFile.ajax.loader.center();
                            byteFile.ajax.modal.center();
                        }
                        catch (e) {
                            byteFile.error('FRAMEWORK :: byteFile.load() > Error on Resize: ' + e.message + '...');
                        }
                    }
                });


                if (active) {
                    jQuery(document.body).css('cursor', 'wait');

                    byteFile.toast.create(
                        byteFile.toast.modes.info,
                        byteFile.title,
                        byteFile.locale.loader.message
                    );

                    if (element && element.length > 0) {
                        byteFile.ajax.modal.show();
                        byteFile.ajax.modal.center();
                        byteFile.ajax.loader.show();
                        byteFile.ajax.loader.center();
                    }
                }
            }
            else {
                where = 'NO BASE';

                if (active) {
                    jQuery(document.body).css('cursor', 'wait');

                    byteFile.toast.create(
                        byteFile.toast.modes.info,
                        byteFile.title,
                        byteFile.locale.loader.message
                    );

                    if (element && element.length > 0 && byteFile.ajax.loader != undefined) {
                        byteFile.ajax.modal.show();
                        byteFile.ajax.modal.center();
                        byteFile.ajax.loader.show();
                        byteFile.ajax.loader.center();
                    }
                }
                else {
                    byteFile.ajax.modal.hide();
                    byteFile.ajax.loader.hide();

                    jQuery(document.body).css('cursor', '');
                }
            }
        }
        catch (e) {
            byteFile.error('FRAMEWORK :: byteFile.load() > Error @ ' + where + ': ' + e.message + '...');
        }
    },

    bind: function (element, model) {
        kendo.bind(element, kendo.observable(model));
    },

    noCache: function () {
        return 'nocache=' + new Date().getTime();
    },
    
    queries: {
        category: null,
        tree: null,
        drop: null,
        position: null
    },
    
    init: function () {
        jQuery('html').click(function () {
            //Implemented e.stopPropagation() in input event handlers first for drop down
            jQuery('div[data-tree="items"]:visible').hide();
        });

        jQuery.validator.addMethod(
            'regex',
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            'Please enter alphabetical characters only.'
        );

        kendo.data.binders.placeholder = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['placeholder'].get();

                if (value) {
                    jQuery(this.element).attr('placeholder', value);
                }
            }
        });

        kendo.data.binders.class = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['class'].get();

                if (value) {
                    jQuery(this.element).attr('class', value);
                }
            }
        });

        kendo.data.binders.id = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['id'].get();

                if (value) {
                    jQuery(this.element).attr('id', value);
                }
            }
        });

        kendo.data.binders.href = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['href'].get();

                if (value) {
                    jQuery(this.element).attr('href', value);
                }
            }
        });

        kendo.data.binders.src = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['src'].get();

                if (value) {
                    jQuery(this.element).attr('src', value);
                }
            }
        });

        kendo.data.binders.explain = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['explain'].get();

                if (value) {
                    jQuery(this.element).data('explain', value);
                }
            }
        });

        kendo.data.binders.alert = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['alert'].get();

                if (value) {
                    jQuery(this.element).data('alert', value);
                }
            }
        });

        kendo.data.binders.title = kendo.data.Binder.extend({
            refresh: function () {
                var value = this.bindings['title'].get();

                if (value) {
                    jQuery(this.element).data('title', value);
                }
            }
        });

        //JavaScript string formatter
        if (!String.prototype.format) {
            String.prototype.format = function () {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function (match, number) {
                    return typeof args[number] != 'undefined'
                      ? args[number]
                      : match
                    ;
                });
            };
        }

        if (!String.prototype.jsDate) {
            String.prototype.jsDate = function () {
                var value = this.toString();

                var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)(?:([\+-])(\d{2})\:(\d{2}))?Z?jQuery/.exec(value);

                if (a) {
                    var utcMilliseconds = Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]);
                    return new Date(utcMilliseconds);
                }

                return value;
            }
        }

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", Math.max(0, ((jQuery(window).height() - jQuery(this).outerHeight()) / 2) + jQuery(window).scrollTop()) + "px");
            this.css("left", Math.max(0, ((jQuery(window).width() - jQuery(this).outerWidth()) / 2) + jQuery(window).scrollLeft()) + "px");

            return this;
        }

        //Create Numeric Hashcode from string value
        String.prototype.hashCode = function () {
            var hash = 0, i, chr, len;

            if (this.length == 0) return hash;

            for (i = 0, len = this.length; i < len; i++) {
                chr = this.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }

            return hash;
        };

        String.prototype.capitalise = function () {
            return this.charAt(0).toUpperCase() + this.slice(1);
        };

        jQuery.fn.outerHTML = function () {
            return jQuery('<div />').append(this.eq(0).clone()).html();
        };

        jQuery.fn.tree = function (options, load, expand) {
            try {
                var control = this;
                control.attr('data-handler', 'tree');

                //Defaults
                load = (load) ? load : false;

                if (options.lolz) {
                    //byte-File.log(load + ': ' + JSON.stringify(options, null, 4));
                }

                options.key = (options.key) ? options.key : 0;
                options.parent = (options.parent) ? options.parent : '';
                options.take = (options.take) ? options.take : 20;
                options.page = (options.page) ? options.page : 0;
                options.count = (options.count) ? options.count : 0;
                options.placeholder = (options.placeholder) ? options.placeholder : '';
                options.width = (options.width) ? options.width : 250;
                options.highlight = (options.highlight) ? options.highlight : 250;
                options.multi = (options.multi) ? options.multi : false;
                options.all = (options.all) ? options.all : false;
                options.drop = (options.drop) ? options.drop : false;
                options.include = (options.include) ? options.include : [];
                options.search = (options.search) ? options.search : '';
                options.crawl = (options.crawl) ? options.crawl : false;
                options.categories = (options.categories) ? options.categories : '';
                options.verbage = (options.verbage) ? options.verbage : '';
                options.memory = (options.memory) ? options.memory : false;
                options.bank = (options.bank) ? options.bank : [];

                options.first = (options.first) ? options.first : false;

                options.edit = (options.edit) ? options.edit : function () { };
                options.editConfirm = (options.editConfirm) ? options.editConfirm : function () { };
                options.editCancel = (options.editCancel) ? options.editCancel : function () { };
                options.select = (options.select) ? options.select : function () { };
                options.clear = (options.clear) ? options.clear : function () { };
                //Defaults

                var editor = (control.find('#tree_editor').length == 0) ? jQuery('<div id="tree_editor" style="padding-left: 14px !important; background-color: #e5e5e5; padding: 2px; border-left: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5; border-right: 1px solid #e5e5e5; cursor: pointer !important; cursor: hand !important;"></div>') : control.find('#tree_editor');
                var all = (control.find('#tree_all').length == 0) ? jQuery('<i id="tree_all" class="fa fa-sort-amount-asc"></i>') : control.find('#tree_all');
                var clear = (control.find('#tree_clear').length == 0) ? jQuery('<i id="tree_clear" class="fa fa-minus-square-o"></i>') : control.find('#tree_clear');
                var bank = (control.find('#tree_bank').length == 0) ? jQuery('<div id="tree_bank" style="display: none;"><h5>Click an item to remove it</h5></div>') : control.find('#tree_bank');

                if (options.memory) {
                    if (options.bank.length == 0) editor.text('No items selected.');

                    editor.unbind('click');
                    editor.click(function (e) {
                        e.stopPropagation();

                        if (options.bank.length == 0 || editor.text().indexOf('All') > -1) {
                            byteFile.toast.create(
                                byteFile.toast.modes.info,
                                byteFile.title,
                                'You can only edit items when they are manually selected.'
                            );

                            return;
                        }

                        bank.find('div').remove();

                        for (var i = 0; i < options.bank.length; i++) {
                            var opt = options.bank[i];

                            bank.append('<div id="' + opt.value + '">' + opt.text + '</div>');
                        }

                        byteFile.modal.create(
                            byteFile.modal.modes.confirm,
                            byteFile.title,
                            bank.html(),
                            function (content) {
                                //Confirm

                                jQuery.each(items.find('input[type="checkbox"]'), function () {
                                    var checkbox = jQuery(this);
                                    checkbox.removeAttr('checked');
                                });

                                options.bank = [];

                                jQuery.each(content.find('div'), function () {
                                    var opt = jQuery(this);

                                    options.bank.push({
                                        value: opt.attr('id'),
                                        text: opt.text()
                                    });

                                    items.find('input[value="' + opt.attr('id') + '"]').attr('checked', 'checked');
                                });

                                var selected = jQuery.map(options.bank, function (n, i) {
                                    return n.value;
                                }).join(',');

                                var list = selected.split(',');
                                var feedback = '';

                                if (options.bank.length > 0) {
                                    feedback = (options.bank.length == 1) ? 'One item selected.' : 'Multiple items selected.';
                                    editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                }
                                else {
                                    feedback = 'No items selected.';
                                    editor.text('No items selected.');
                                }

                                control.find('#tree_search').val(feedback);

                                control.data('tree', {
                                    list: list,
                                    value: selected,
                                    text: feedback,
                                    data: null,
                                    handle: control,
                                    options: options,
                                    load: load
                                });

                                if (options.changed && typeof options.changed == 'function') {
                                    options.changed({
                                        list: list,
                                        value: selected,
                                        text: feedback,
                                        data: null,
                                        handle: control,
                                        options: options,
                                        load: load
                                    });
                                }
                            },
                            function (content) {
                                //Close
                            },
                            false,
                            byteFile.modal.size.large,
                            function (content) {
                                //Bound

                                jQuery.each(content.find('div'), function () {
                                    var opt = jQuery(this);

                                    opt.css('cursor', 'pointer');

                                    opt.click(function (e) {
                                        e.stopPropagation();

                                        opt.remove();
                                    });
                                });
                            });
                    });

                    all.unbind('click');
                    all
                        .mouseover(function () {
                            if (editor.find('div').length > 0) {
                                jQuery.each(editor.find('div'), function () {
                                    jQuery(this).remove();
                                });
                            }

                            jQuery('<div>Select All Items</div>').prependTo(editor);
                        })
                        .mouseout(function () {
                            jQuery.each(editor.find('div'), function () {
                                jQuery(this).remove();
                            });
                        })
                        .click(function (e) {
                            e.stopPropagation();

                            jQuery.each(items.find('input[type="checkbox"]'), function () {
                                var checkbox = jQuery(this);
                                checkbox.attr('checked', 'checked');
                            });

                            options.bank = ['All'];

                            var selected = 'All';
                            var list = ['All'];
                            var feedback = 'All items selected.';

                            control.data('tree', {
                                list: list,
                                value: selected,
                                text: feedback,
                                data: null,
                                handle: control,
                                options: options,
                                load: load
                            });

                            if (options.changed && typeof options.changed == 'function') {
                                options.changed({
                                    list: list,
                                    value: selected,
                                    text: feedback,
                                    data: null,
                                    handle: control,
                                    options: options,
                                    load: load
                                });
                            }

                            editor.text(feedback);
                            input.val(feedback);

                            items.hide();

                            options.select();
                        });

                    clear.unbind('click');
                    clear
                        .mouseover(function () {
                            if (editor.find('div').length > 0) {
                                jQuery.each(editor.find('div'), function () {
                                    jQuery(this).remove();
                                });
                            }

                            jQuery('<div>Clear Selection</div>').prependTo(editor);
                        })
                        .mouseout(function () {
                            jQuery.each(editor.find('div'), function () {
                                jQuery(this).remove();
                            });
                        })
                        .click(function (e) {
                            e.stopPropagation();

                            jQuery.each(items.find('input[type="checkbox"]'), function () {
                                var checkbox = jQuery(this);
                                checkbox.removeAttr('checked');
                            });

                            options.bank = [];

                            var selected = '';
                            var list = [];
                            var feedback = '';

                            feedback = 'No items selected.';

                            editor.text(feedback);
                            input.val('');

                            items.find('div').remove();
                            items.hide();

                            options.search = '';
                            options.key = 0;
                            options.page = 0;
                            options.crawl = false;

                            if (options.changed && typeof options.changed == 'function') {
                                control.data('tree', {
                                    list: [],
                                    value: 0,
                                    text: '',
                                    data: null,
                                    handle: control,
                                    options: options,
                                    load: load
                                });

                                options.changed(control.data('tree'));
                            }

                            control.tree(options, false);
                        });
                }
                else {
                    editor.hide();
                    all.hide();
                    clear.hide();
                }

                var input = null;
                var items = null;
                var container = null;
                var loader = null;
                var wrapper = null;
                var icons_left = null;
                var icons_right = null;

                control
                    .css('width', options.width + 'px')
                    .css('z-index', '100000000');

                if (control.find('#item_container').length == 0) {
                    control.data('tree', {
                        list: [],
                        value: 0,
                        text: '',
                        data: null,
                        handle: control,
                        options: options,
                        load: load
                    });
                    items = jQuery('<div data-tree="items" autocomplete="off" id="item_container" style="' + ((options.drop) ? 'position: absolute !important; z-index: 999999; height: 250px;' : '') + ' overflow-y: scroll; overflow-x: hidden; background-color: #fff; display: none; width: ' + (options.width + 39) + 'px;  padding: 5px;"></div>');
                }
                else {
                    items = control.find('#item_container');
                }

                if (control.find('div:first').length && !load) {
                    //if(byteFile.queries.tree != null) byteFile.queries.tree.abort();
                    items.empty();
                }

                if (options.page > 0 && load) {
                    items = control.find('#item_container');
                }

                control.data('events', {
                    fire: function (event) {
                        switch (event) {
                            case 'change':
                                options.key = control.data('select');
                                control.tree(options, true);
                                break;
                            default:
                                break;
                        }
                    }
                });

                if (control.find('#tree_search').length) {
                    input = control.find('#tree_search');

                    //if (input.val().length > 0) {
                    //    byteFile.highlight.element = input;
                    //    byteFile.highlight.apply(input);
                    //}

                    container = control.find('#items');
                    loader = control.find('#loader');
                    wrapper = control.find('#grouper');
                    icons_left = control.find('#icons_container_left');
                    icons_right = control.find('#icon_container_right');

                    if (options.crawl) {
                        control.find('#loader').attr('class', 'fa fa-refresh fa-spin');

                        byteFile.toast.create(
                            byteFile.toast.modes.info,
                            byteFile.title,
                            //'savantCUBE Reporting',
                            'We are searching your organisation hierarchy, please be patient...'
                        );

                        jQuery.ajax({
                            url: options.categories,
                            type: 'GET',
                            dataType: 'json',
                            success: function (data) {
                                //byte-File.log(JSON.stringify(data, null, 4));

                                var searchCategories = [];

                                jQuery.each(data, function () {
                                    var category = this;

                                    jQuery.each(options.include, function () {
                                        var include = this;

                                        if (category.english.toLowerCase() == include.toLowerCase()) {
                                            searchCategories.push(category.id);
                                        }
                                    });
                                });

                                ////byte-File.log(searchCategories.join(','));

                                //SEARCH!!
                                var start = new Date().getTime();
                                var end = new Date().getTime();
                                var time = end - start;

                                //byte-File.log('byteFile.tree() > Data retrieval for search started...');

                                jQuery.ajax({
                                    url: options.source + '&total=0&page=0&take=0&search=' + options.search + '&key=0&parent=0&category=' + searchCategories.join(','),
                                    type: 'GET',
                                    dataType: 'json',
                                    success: function (data) {
                                        end = new Date().getTime();
                                        time = end - start;

                                        //byte-File.log('byteFile.tree() > Data retrieval for search took (' + (+time / 1000 % 60).toFixed(2) + ') seconds...');

                                        //byte-File.log(JSON.stringify(data, null, 4));

                                        try {
                                            if (data.length <= 0) {
                                                byteFile.toast.create(
                                                    byteFile.toast.modes.warning,
                                                    byteFile.title,
                                                    //'savantCUBE Reporting',
                                                    'No results matching your search criteria could be located...'
                                                );

                                                return;
                                            }

                                            start = new Date().getTime();

                                            var parents = byteFile.filter.from(data)
                                                .equals(options.parent, -1)
                                                .sort(options.text)
                                                .select();

                                            end = new Date().getTime();
                                            time = end - start;

                                            //byte-File.log('byteFile.tree() > Finding parents took (' + (+time / 1000 % 60).toFixed(2) + ') seconds...');

                                            start = new Date().getTime();

                                            var distinct = byteFile.filter.from(parents).distinct(options.value);

                                            end = new Date().getTime();
                                            time = end - start;

                                            //byte-File.log('byteFile.tree() > Finding distinct parents took (' + (+time / 1000 % 60).toFixed(2) + ') seconds...');

                                            var primaryColor = '#fff';
                                            var secondaryColor = '#EFF2EF';
                                            var currentColor = 0;

                                            start = new Date().getTime();

                                            //byte-File.log('byteFile.tree() > Iterating entire hierarchy @ ' + start.toString() + '...');

                                            jQuery.each(distinct, function () {
                                                var parent = this;

                                                var row = byteFile.filter.from(data)
                                                    .equals(options.value, parseFloat(parent))
                                                    .select()[0];

                                                if (currentColor == primaryColor) {
                                                    currentColor = secondaryColor;
                                                }
                                                else {
                                                    currentColor = primaryColor;
                                                }

                                                if (items.find('[data-value="' + row[options.value] + '"]').length <= 0) {
                                                    var option =
                                                        jQuery('<div data-iscategory="false" data-loaded="false" data-options="false" style="width: 100%; cursor: pointer; cursor: hand; margin-left: 20px; background-color: ' + currentColor + ';" data-value="' + row[options.value] + '" data-text="' + row[options.text] + '"> ' + ((row.hasChildren && row.hasChildren.toString().toLowerCase() == 'true') ? ' <i class="fa fa-plus-square"></i>&nbsp;' : '&nbsp;&nbsp;') + ((options.multi) ? ' <input type="checkbox" data-text="' + row[options.text] + '" value="' + row[options.value] + '" ' + ((options.all) ? 'checked="checked"' : '') + ' /> ' : '<input type="radio"  data-text="' + row[options.text] + '" name="org_hierarchy" value="' + row[options.value] + '" /> ') + ' ' + row[options.text] + '</div>')
                                                            .appendTo(items)
                                                            .mouseover(function () {
                                                                jQuery(this)
                                                                    .data('currentColor', jQuery(this).css('background'))
                                                                    .css('background', options.highlight)
                                                                    .css('cursor', 'pointer');
                                                            })
                                                            .mouseout(function () {
                                                                jQuery(this).css('background', jQuery(this).data('currentColor'));
                                                            })
                                                            .data('text', row[options.text])
                                                            .data('value', row[options.value])
                                                            .click(function (e) {
                                                                e.stopPropagation();

                                                                var item = jQuery(this);

                                                                if (options.multi) {
                                                                    var checkbox = jQuery(this).find('input[type="checkbox"]:first');

                                                                    if (checkbox.is(':checked')) {
                                                                        checkbox.removeAttr('checked');

                                                                        if (options.memory) {
                                                                            options.bank = jQuery.grep(options.bank, function (x) {
                                                                                return x.value != item.data('value');
                                                                            });

                                                                            if (options.bank.length > 0) {
                                                                                editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                                                            }
                                                                            else {
                                                                                editor.text('No items selected.');
                                                                            }
                                                                        }

                                                                        //byte-File.log('selected');
                                                                    }
                                                                    else {
                                                                        checkbox.attr('checked', 'checked');

                                                                        if (options.memory) {
                                                                            options.bank.push({
                                                                                text: item.data('text'),
                                                                                value: item.data('value')
                                                                            });

                                                                            if (options.bank.length > 0) {
                                                                                editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                                                            }
                                                                            else {
                                                                                editor.text('No items selected.');
                                                                            }
                                                                        }

                                                                        //byte-File.log('de-selected');
                                                                    }

                                                                    if (options.memory) {
                                                                        //byte-File.log(JSON.stringify(options.bank, null, 4));
                                                                    }

                                                                    var selected = '';

                                                                    if (options.memory) {
                                                                        selected = jQuery.map(options.bank, function (n, i) {
                                                                            return n.value;
                                                                        }).join(',');
                                                                    }
                                                                    else {
                                                                        selected = jQuery.map(control.find('input[type="checkbox"]:checked'), function (n, i) {
                                                                            return n.value;
                                                                        }).join(',');
                                                                    }

                                                                    //selected = selected.replace(/,\s*jQuery/, ''); //removes last comma from string if any
                                                                    var list = selected.split(',');
                                                                    var feedback = '';

                                                                    if (options.memory) {
                                                                        if (options.bank.length > 0) {
                                                                            feedback = (options.bank.length == 1) ? 'One item selected.' : 'Multiple items selected.';
                                                                        }
                                                                        else {
                                                                            feedback = 'No items selected.';
                                                                        }
                                                                    }
                                                                    else {
                                                                        feedback = (selected.replace('[""]', '').length == 0) ? '' : '(' + list.length + ') items selected.';
                                                                    }

                                                                    input.val(feedback);

                                                                    control.data('tree', {
                                                                        list: list,
                                                                        value: selected,
                                                                        text: feedback,
                                                                        data: null,
                                                                        handle: control,
                                                                        options: options,
                                                                        load: load
                                                                    });

                                                                    if (options.changed && typeof options.changed == 'function') {
                                                                        options.changed({
                                                                            list: list,
                                                                            value: selected,
                                                                            text: feedback,
                                                                            data: null,
                                                                            handle: control,
                                                                            options: options,
                                                                            load: load
                                                                        });
                                                                    }

                                                                    options.search = '';

                                                                    if (selected.replace('[""]', '').length == 0) {
                                                                        items.hide();
                                                                    }
                                                                }
                                                                else {
                                                                    var radiobutton = jQuery(this).find('input[type="radio"]:first');

                                                                    if (radiobutton.is(':checked')) {
                                                                        radiobutton.removeAttr('checked');
                                                                    }
                                                                    else {
                                                                        radiobutton.attr('checked', 'checked');
                                                                    }

                                                                    input.val(jQuery(this).data('text'));

                                                                    control.data('tree', {
                                                                        list: jQuery(this).data('value'),
                                                                        value: jQuery(this).data('value'),
                                                                        text: jQuery(this).data('text'),
                                                                        data: null,
                                                                        handle: control,
                                                                        options: options,
                                                                        load: load
                                                                    });

                                                                    if (options.changed && typeof options.changed == 'function') {
                                                                        options.changed({
                                                                            list: jQuery(this).data('value'),
                                                                            value: jQuery(this).data('value'),
                                                                            text: jQuery(this).data('text'),
                                                                            data: null,
                                                                            handle: control,
                                                                            options: options,
                                                                            load: load
                                                                        });
                                                                    }

                                                                    options.search = '';

                                                                    items.hide();
                                                                }

                                                                e.stopPropagation();
                                                                e.preventDefault();
                                                            });

                                                    option.find('i').click(function (e) {
                                                        try {
                                                            e.stopPropagation();

                                                            //byte-File.log('click option');

                                                            if (!option.data('loaded')) {
                                                                //byte-File.log('data not loaded');

                                                                option
                                                                    .data('loaded', true)
                                                                    .data('key', row[options.value]);

                                                                if (typeof expand != 'undefined') {
                                                                    option.data('category', expand.data('category'));
                                                                }
                                                                else {
                                                                    option.data('category', searchCategories.join(','));
                                                                }

                                                                if (!option.data('options')) {
                                                                    //byte-File.log('no options saved');

                                                                    options.search = '';
                                                                    options.crawl = false;

                                                                    option.data('options', options);
                                                                }
                                                                else {
                                                                    //byte-File.log('options already saved');

                                                                    var config = option.data('options');
                                                                    config.page++;

                                                                    option.data('options', config);
                                                                }

                                                                //byte-File.log('loading tree hierarchy for this option');

                                                                options = option.data('options');

                                                                options.lolz = true;

                                                                //byte-File.log(JSON.stringify(options, null, 4));

                                                                control.tree(options, true, option);
                                                            }
                                                            else {
                                                                //byte-File.log('data already loaded');

                                                                if (option.data('loaded') && option.find('div:first').is(':visible')) {
                                                                    option.find('div').hide();
                                                                }
                                                                else {
                                                                    option.find('div').show();
                                                                }
                                                            }

                                                            //control.tree(options, true, option);
                                                            e.stopPropagation();
                                                            e.preventDefault();
                                                        }
                                                        catch (e) {
                                                            byteFile.error('FRAMEWORK :: byteFile.extend() > Tree Control > Error: ' + e.message + '...');
                                                        }
                                                    });

                                                    //var count = byteFile.filter.from(data)
                                                    //    .equals(options.parent, row[options.value])
                                                    //    .count();

                                                    ////byte-File.log(count + ': ' + row[options.value]);

                                                    //if (count > 0) {
                                                    //    byteFile.hierarchy(data, row, options.text, options.value, options.parent, option, options, control, input, items, load, editor, all, clear, bank);
                                                    //}
                                                }
                                            });

                                            end = new Date().getTime();
                                            time = end - start;

                                            //byte-File.log('byteFile.tree() > Iterating entire hierarchy took (' + (+time / 1000 % 60).toFixed(2) + ') seconds @ ' + start.toString() + '...');

                                            control.find('#loader').attr('class', 'fa fa-sort-desc');
                                        }
                                        catch (e) {
                                            byteFile.error('FRAMEWORK :: byteFile.extend() > tree Error @ Search Success: ' + e.message + '...');
                                        }
                                    },
                                    error: function (e) {
                                        byteFile.error('FRAMEWORK :: byteFile.extend() > tree Error @ Search Fail: ' + e.message + '...');
                                    }
                                });
                            }
                        });

                        return;
                    }

                    if (options.key && options.key > 0) {
                        control.find('#loader').attr('class', 'fa fa-refresh fa-spin');

                        jQuery.ajax({
                            url: options.categories,
                            type: 'GET',
                            dataType: 'json',
                            success: function (data) {
                                //byte-File.log(JSON.stringify(data, null, 4));

                                var searchCategories = [];

                                jQuery.each(data, function () {
                                    var category = this;

                                    jQuery.each(options.include, function () {
                                        var include = this;

                                        if (category.english.toLowerCase() == include.toLowerCase()) {
                                            searchCategories.push(category.id);
                                        }
                                    });
                                });

                                jQuery.ajax({
                                    url: options.source + '&total=0&page=0&take=0&search=&key=' + options.key + '&parent=0&category=' + searchCategories.join(','),
                                    type: 'GET',
                                    dataType: 'json',
                                    success: function (data) {
                                        //byte-File.log(JSON.stringify(data, null, 4));

                                        try {
                                            if (data.length <= 0) return;

                                            //byte-File.log(JSON.stringify(data, null, 4));

                                            var row = data[0];

                                            input.val(row[options.text]);

                                            var option =
                                                jQuery('<div data-iscategory="false" data-loaded="false" data-options="false" style="width: 100%; cursor: pointer; cursor: hand; margin-left: 20px; background-color: #fff;" data-value="' + row[options.value] + '" data-text="' + row[options.text] + '">' + ((options.multi) ? '<input type="checkbox" data-text="' + row[options.text] + '" value="' + row[options.value] + '" ' + ((options.all) ? 'checked="checked"' : '') + ' /> ' : '<input type="radio"  data-text="' + row[options.text] + '" name="org_hierarchy" value="' + row[options.value] + '" /> ') + ' ' + row[options.text] + '</div>')
                                                    .appendTo(items)
                                                    .data('text', row[options.text])
                                                    .data('value', row[options.value]);

                                            control.data('tree', {
                                                list: option.data('value'),
                                                value: option.data('value'),
                                                text: option.data('text'),
                                                data: null,
                                                handle: control,
                                                options: options,
                                                load: load
                                            });

                                            if (options.changed && typeof options.changed == 'function') {
                                                options.changed({
                                                    list: option.data('value'),
                                                    value: option.data('value'),
                                                    text: option.data('text'),
                                                    data: null,
                                                    handle: control,
                                                    options: options,
                                                    load: load
                                                });
                                            }

                                            options.key = 0;

                                            items.hide();

                                            control.find('#loader').attr('class', 'fa fa-sort-desc');
                                        }
                                        catch (e) {
                                            byteFile.error('FRAMEWORK :: byteFile.extend() > tree Error @ Search Success: ' + e.message + '...');
                                        }
                                    },
                                    error: function (e) {
                                        byteFile.error('FRAMEWORK :: byteFile.extend() > tree Error @ Search Fail: ' + e.message + '...');
                                    }
                                });
                            }
                        });

                        return;
                    }
                }
                else {
                    control.data('tree', {
                        list: [],
                        value: '',
                        text: '',
                        data: null,
                        handle: control,
                        options: options,
                        load: load
                    });
                    if (options.verbage.length > 0) {
                        control.append('<div>' + options.verbage + '</div>');
                    }

                    wrapper = jQuery('<div class="input-group"  id="grouper" style="cursor: pointer !important; cursor: hand !important;"></div>').appendTo(control);
                    container = jQuery('<div id="items" style="position:relative"></div>').appendTo(control);
                    icons_left = jQuery('<span id="icons_container_left" class="input-group-addon"></span>').appendTo(wrapper);

                    if (!options.memory) icons_left.hide();

                    //editor.appendTo(control);
                    //all.appendTo(control);
                    //clear.appendTo(control);
                    //bank.appendTo(control);

                    all.appendTo(icons_left);
                    clear.appendTo(icons_left);
                    editor.appendTo(control);
                    bank.appendTo(control);
                 
                    input = jQuery('<input data-tree="search" autocomplete="off" class="form-control" id="tree_search" type="text" ' + ((options.width) ? 'style="width: ' + options.width + 'px;"' : '') + ' />')
                        .appendTo(wrapper)
                        .data('handle', control)
                        .click(function (e) {
                            e.stopPropagation();

                            if (items.is(':hidden')) {
                                jQuery('[data-tree="items"]').each(function () {
                                    jQuery(this).hide();
                                });

                                items.show();
                            }
                            else {
                                items.hide();
                            }

                            e.stopPropagation();
                            e.preventDefault();
                        })
                        .blur(function (e) {
                            e.stopPropagation();
                            e.preventDefault();

                            if (options.memory && options.bank.length > 0) {
                                return;
                            }

                            if (input.val().length <= 0) {
                                control.data('tree', {
                                    list: [],
                                    value: 0,
                                    text: '',
                                    data: null,
                                    handle: control,
                                    options: options,
                                    load: load
                                });
                            }
                        })
                        .keyup(function (e) {
                            e.stopPropagation();
                            e.preventDefault();

                            var element = jQuery(this);
                            var items = control.find('#item_container');

                            if (e.which == 13) {
                                items.show();

                                if (!options.memory || options.bank.length == 0) {
                                    control.data('tree', {
                                        list: [],
                                        value: '',
                                        text: '',
                                        data: null,
                                        handle: control,
                                        options: options,
                                        load: load
                                    });
                                }

                                if (element.val().length > 4) {
                                    options.search = element.val(),
                                    options.key = 0;
                                    options.page = 0;
                                    options.crawl = true;

                                    control.tree(options, false);
                                }

                                if (element.val().length > 0 && element.val().length < 5) {
                                    byteFile.toast.create(
                                        byteFile.toast.modes.warning,
                                        byteFile.title,
                                        //'savantCUBE Reporting',
                                        'Please enter at least 5 characters...'
                                    );
                                }
                            }

                            if (element.val().length == 0) {
                                options.search = '';
                                options.key = 0;
                                options.page = 0;
                                options.crawl = false;

                                if (options.changed && typeof options.changed == 'function') {
                                    control.data('tree', {
                                        list: [],
                                        value: '',
                                        text: '',
                                        data: null,
                                        handle: control,
                                        options: options,
                                        load: load
                                    });

                                    options.changed(control.data('tree'));
                                }

                                //if (options.memory && options.bank.length == 0) {
                                //    control.data('tree', {
                                //        list: [],
                                //        value: '',
                                //        text: '',
                                //        data: null,
                                //        handle: control,
                                //        options: options,
                                //        load: load
                                //    });
                                //}

                                control.tree(options, false);
                            }
                        });

                    icons_right = jQuery('<span id="icon_container_right" class="input-group-addon"><i id="loader" class="fa fa-sort-desc"></i>').appendTo(wrapper);

                    control.find('#loader').click(function (e) {
                        e.stopPropagation();

                        if (items.is(':hidden')) {
                            byteFile.highlight.apply(input);
                            items.show();
                        }
                        else {
                            items.hide();
                            byteFile.highlight.remove();
                        }

                        e.stopPropagation();
                        e.preventDefault();
                    });

                    input.attr('placeholder', options.placeholder);

                    //items.css('top', input.css('top'));
                    items.css('left', input.css('left'));
                }

                control.data('options', options);

                control.find('#loader').attr('class', 'fa fa-refresh fa-spin');

                if (options.categories.length > 0 && !load) {
                    //jQuery.ajax({
                    //    url: options.categories,
                    //    type: 'GET',
                    //    dataType: 'json',
                    //    success: function (data) {
                            //byte-File.log(JSON.stringify(data, null, 4));

                            try {
                                var primaryColor = '#fff';
                                var secondaryColor = '#EFF2EF';
                                var currentColor = 0;
                                jQuery.each(options.include, function () {
                                    var include = this;
                                    jQuery.each(options.categories, function () {
                                        var row = this;

                                        //if (row[options.text].replace(" ", "") != include.replace(" ", "")) {
                                        //    return true;
                                        //}

                                        if (currentColor == primaryColor) {
                                            currentColor = secondaryColor;
                                        }
                                        else {
                                            currentColor = primaryColor;
                                        }

                                        var option =
                                            jQuery('<div data-iscategory="true" style="width:100%; cursor: pointer; cursor: hand; background-color: ' + currentColor + ';" data-value="' + row + '" data-text="' + row + '"><i class="fa fa-plus-square"></i> &nbsp;' + row + '</div>')
                                                .appendTo(items)
                                                .data('text', row)//[options.text])
                                                .data('value', row) //[options.value])
                                                .click(function (e) {
                                                    e.stopPropagation();

                                                    try {
                                                        var item = jQuery(this);

                                                        if (!item.data('loaded')) {
                                                            item.data('loaded', true);
                                                            item.data('category', row);//[options.value]);

                                                            item.data('key', 0);
                                                            control.tree(options, true, item);
                                                            return;
                                                        }

                                                        if (item.data('loaded') && item.find('div:first').is(':visible')) {
                                                            item.find('div').hide();
                                                        }
                                                        else {
                                                            item.find('div').show();
                                                        }
                                                    }
                                                    catch (e) {
                                                        byteFile.error('FRAMEWORK :: byteFile.tree() > Category Click Error: ' + e.message + '...');
                                                    }

                                                    e.stopPropagation();
                                                    e.preventDefault();
                                                });
                                    });
                                });
                                items.appendTo(container);

                                if (options.bound && typeof options.bound == 'function') {
                                   // options.bound(data);
                                }

                                control.find('#loader').attr('class', 'fa fa-sort-desc');

                                //if (options.key && options.key > 0) {
                                //    control.tree(options, true, option);
                                //}
                            }
                            catch (e) {
                                byteFile.error('FRAMEWORK :: byteFile.extend() > tree Error @ Populate Categories Success: ' + e.message + '...');
                            }
                   //     }
                   // });
                }

                if (options.categories.length > 0 && load) {
                    var curl = '';

                    curl = options.source + '?search=' + options.search + '&parent=' + ((expand.data('key') > 0) ? expand.data('key') : '0') + '&category=' + expand.data('category');

                    //byte-File.log(curl);
                    //byte-File.log(curl);

                    jQuery.ajax({
                        url: curl,
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            data = JSON.parse(data);
                            var where = 'got data';

                            try {
                                where = 'log data';
                                //byte-File.log(JSON.stringify(data, null, 4));

                                if (data.length <= 0) {
                                    control.find('#loader').attr('class', 'fa fa-sort-desc');

                                    if (options.bound && typeof options.bound == 'function') {
                                        options.bound(data);
                                    }

                                    control.find('#loader').attr('class', 'fa fa-sort-desc');

                                    if (expand.find('#expander_' + expand.data('key') + '_' + expand.data('category')).length) {
                                        expand.find('#expander_' + expand.data('key') + '_' + expand.data('category')).empty();
                                        expand.find('#expander_' + expand.data('key') + '_' + expand.data('category')).text('No more data to load...');
                                    }

                                    return;
                                }

                                var primaryColor = '#fff';
                                var secondaryColor = '#EFF2EF';
                                var currentColor = 0;

                                where = 'data loop start';

                                jQuery.each(data, function () {
                                    var row = this;

                                    if (currentColor == primaryColor) {
                                        currentColor = secondaryColor;
                                    }
                                    else {
                                        currentColor = primaryColor;
                                    }

                                    var option =
                                        jQuery('<div data-iscategory="false" data-loaded="false" data-options="false" style="width: 100%; cursor: pointer; cursor: hand; margin-left: 20px; background-color: ' + currentColor + ';" data-value="' + row[options.value] + '" data-text="' + row[options.text] + '"> ' + ((row.HasChildrens == '1') ? '<i class="fa fa-plus-square"></i>&nbsp;' : '&nbsp;&nbsp;') + ((options.multi) ? '  <input type="checkbox" data-text="' + row[options.text] + '" value="' + row[options.value] + '" ' + ((options.all) ? 'checked="checked"' : '') + ' /> ' : '<input type="radio"  data-text="' + row[options.text] + '" name="org_hierarchy" value="' + row[options.value] + '" /> ') + ' ' + row[options.text] + '</div>')
                                            .appendTo(expand)
                                            .mouseover(function () {
                                                jQuery(this)
                                                    .data('currentColor', jQuery(this).css('background'))
                                                    .css('background', options.highlight)
                                                    .css('cursor', 'pointer');
                                            })
                                            .mouseout(function () {
                                                jQuery(this).css('background', jQuery(this).data('currentColor'));
                                            })
                                            .data('text', row[options.text])
                                            .data('value', row[options.value])
                                            .click(function (e) {
                                                e.stopPropagation();
                                                var item = jQuery(this);

                                                if (options.multi) {
                                                    var checkbox = jQuery(this).find('input[type="checkbox"]:first');

                                                    if (checkbox.is(':checked')) {
                                                        checkbox.removeAttr('checked');

                                                        if (options.memory) {
                                                            options.bank = jQuery.grep(options.bank, function (x) {
                                                                return x.value != item.data('value');
                                                            });

                                                            if (options.bank.length > 0) {
                                                                editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                                            }
                                                            else {
                                                                editor.text('No items selected.');
                                                            }
                                                        }

                                                        //byte-File.log('selected');
                                                    }
                                                    else {
                                                        checkbox.attr('checked', 'checked');

                                                        if (options.memory) {
                                                            options.bank.push({
                                                                text: item.data('text'),
                                                                value: item.data('value')
                                                            });

                                                            if (options.bank.length > 0) {
                                                                editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                                            }
                                                            else {
                                                                editor.text('No items selected.');
                                                            }
                                                        }

                                                        //byte-File.log('de-selected');
                                                    }

                                                    if (options.memory) {
                                                        //byte-File.log('01' + JSON.stringify(options.bank, null, 4));
                                                    }

                                                    var selected = '';

                                                    if (options.memory) {
                                                        selected = jQuery.map(options.bank, function (n, i) {
                                                            return n.value;
                                                        }).join(',');
                                                    }
                                                    else {
                                                        selected = jQuery.map(control.find('input[type="checkbox"]:checked'), function (n, i) {
                                                            return n.value;
                                                        }).join(',');
                                                    }

                                                    //selected = selected.replace(/,\s*jQuery/, ''); //removes last comma from string if any
                                                    var list = selected.split(',');
                                                    var feedback = '';

                                                    if (options.memory) {
                                                        if (options.bank.length > 0) {
                                                            feedback = (options.bank.length == 1) ? 'One item selected.' : 'Multiple items selected.';
                                                        }
                                                        else {
                                                            feedback = 'No items selected.';
                                                        }
                                                    }
                                                    else {
                                                        feedback = (selected.replace('[""]', '').length == 0) ? '' : '(' + list.length + ') items selected.';
                                                    }

                                                    input.val(feedback);

                                                    control.data('tree', {
                                                        list: list,
                                                        value: selected,
                                                        text: feedback,
                                                        data: null,
                                                        handle: control,
                                                        options: options,
                                                        load: load
                                                    });

                                                    if (options.changed && typeof options.changed == 'function') {
                                                        options.changed({
                                                            list: list,
                                                            value: selected,
                                                            text: feedback,
                                                            data: null,
                                                            handle: control,
                                                            options: options,
                                                            load: load
                                                        });
                                                    }

                                                    options.search = '';

                                                    if (selected.replace('[""]', '').length == 0) {
                                                        items.hide();
                                                    }
                                                }
                                                else {
                                                    //jQuery('input[type="radio"][name="org_hierarchy"]').each(function () {
                                                    //    //jQuery(this).find('input[type="radio"]:first').removeAttr('checked');
                                                    //    if (jQuery(this).is(':checked')) {
                                                    //        jQuery(this).removeAttr('checked');
                                                    //    }
                                                    //})
                                                    var radiobutton = jQuery(this).find('input[type="radio"]:first');

                                                    //if (radiobutton.is(':checked')) {
                                                    //    radiobutton.removeAttr('checked');
                                                    //}
                                                    //else {
                                                        radiobutton.attr('checked', 'checked');
                                                    //}

                                                    input.val(jQuery(this).data('text'));

                                                    control.data('tree', {
                                                        list: jQuery(this).data('value'),
                                                        value: jQuery(this).data('value'),
                                                        text: jQuery(this).data('text'),
                                                        data: null,
                                                        handle: control,
                                                        options: options,
                                                        load: load
                                                    });

                                                    if (options.changed && typeof options.changed == 'function') {
                                                        options.changed({
                                                            list: jQuery(this).data('value'),
                                                            value: jQuery(this).data('value'),
                                                            text: jQuery(this).data('text'),
                                                            data: null,
                                                            handle: control,
                                                            options: options,
                                                            load: load
                                                        });
                                                    }

                                                    options.search = '';

                                                    items.hide();
                                                }

                                                e.stopPropagation();
                                                e.preventDefault();
                                            });

                                    option.find('i').click(function (e) {
                                        e.stopPropagation();

                                        if (!option.data('loaded')) {
                                            option
                                                .data('loaded', true)
                                                .data('key', row[options.value])
                                                .data('category', expand.data('category'));

                                            if (!option.data('options')) {
                                                options.page = 0;
                                                option.data('options', options);
                                            }
                                            else {
                                                var config = option.data('options');
                                                config.page++;

                                                option.data('options', config);
                                            }

                                            options = option.data('options');
                                            //control.tree(options, true, jQuery(this));
                                            control.tree(options, true, option);

                                            //return;
                                        }
                                        else {
                                            if (option.data('loaded') && option.find('div:first').is(':visible')) {
                                                option.find('div').hide();
                                            }
                                            else {
                                                option.find('div').show();
                                            }
                                        }

                                        //control.tree(options, true, option);
                                        e.stopPropagation();
                                        e.preventDefault();
                                    });
                                });

                                where = 'data loop end';

                                where = 'expander';

                                if (expand.find('#expander_' + expand.data('key') + '_' + expand.data('category')).length) {
                                    expand.find('#expander_' + expand.data('key') + '_' + expand.data('category')).appendTo(expand);
                                }
                                else {
                                    if (data.length == options.take) {
                                        var option =
                                            jQuery('<div data-iscategory="false" data-options="false" id="expander_' + expand.data('key') + '_' + expand.data('category') + '" style="width: 100%; cursor: pointer; cursor: hand; border: 1px solid black; padding: 5px; margin-left: 20px; margin-top: 10px; margin-bottom: 10px; background-color: ' + options.highlight + ';" ><i class="fa fa-download"></i> Load more...</div>')
                                                .appendTo(expand)
                                                .click(function (e) {
                                                    e.stopPropagation();

                                                    var config = (jQuery(this).data('options')) ? jQuery.parseJSON(jQuery(this).data('options')) : options;

                                                    config.page++;

                                                    jQuery(this).data('options', JSON.stringify(config));

                                                    control.tree(config, true, expand);

                                                    e.stopPropagation();
                                                    e.preventDefault();
                                                });
                                    }
                                }

                                where = 'even firing';

                                if (options.bound && typeof options.bound == 'function') {
                                    options.bound(data);
                                }

                                control.find('#loader').attr('class', 'fa fa-sort-desc');
                            }
                            catch (e) {
                                byteFile.error('FRAMEWORK :: byteFile.extend() > Error @ "' + where + '" in Tree Populate Success: ' + e.message + '...');
                            }
                        },
                        error: function (e) {
                            byteFile.error('FRAMEWORK :: byteFile.extend() > tree Error @ Populate Error: ' + e.message + '...');
                        }
                    });
                }
            }
            catch (e) {
                byteFile.error('FRAMEWORK :: byteFile.extend() > tree Error @ General: ' + e.message + '...');
            }
        };

        jQuery.fn.drop = function (options, load) {
            try {
                var control = this;
                control.attr('data-handler', 'drop');

                var button = false;

                if (control.attr('data-command')) {
                    button = true;
                }

                //Defaults
                load = (load) ? load : false;

                options.key = (options.key) ? options.key : 0;
                options.take = (options.take) ? options.take : 20;
                options.page = (options.page) ? options.page : 0;
                options.count = (options.count) ? options.count : 0;
                options.placeholder = (options.placeholder) ? options.placeholder : '';
                options.width = (options.width) ? options.width : 250;
                options.highlight = (options.highlight) ? options.highlight : 250;
                options.multi = (options.multi) ? options.multi : false;
                options.loaded = (options.loaded) ? options.loaded : false;
                options.bound = (options.bound) ? options.bound : function () { };
                options.changed = (options.changed) ? options.changed : function () { };
                options.search = (options.search) ? options.search : '';
                options.position = (options.position) ? options.position : 0;
                options.searching = (options.searching) ? options.searching : false;
                options.memory = (options.memory) ? options.memory : false;
                options.bank = (options.bank) ? options.bank : [];

                options.first = (options.first) ? options.first : false;
                options.edit = (options.edit) ? options.edit : function () { };
                options.editConfirm = (options.editConfirm) ? options.editConfirm : function () { };
                options.editCancel = (options.editCancel) ? options.editCancel : function () { };
                options.all = (options.all) ? options.all : function () { };
                options.clear = (options.clear) ? options.clear : function () { };
                //Defaults

                options.width = +options.width - +26; //Compensate for loader icon width

                ////byte-File.log(options.width);

                var items = (control.find('#drop_item_container').length == 0) ? jQuery('<div data-tree="items" autocomplete="off" id="drop_item_container" style="color:black;background-color: #fff; z-index: 9999999; display: none; width: ' + (options.width + 39) + 'px; height: 200px; overflow: auto; padding: 5px; border: 1px solid black; margin-top: -1px;"></div>') : control.find('#drop_item_container');
                var container = (control.find('#drop_container').length == 0) ? jQuery('<div id="drop_container" style="position:absolute;z-index:99999999999999"></div>').appendTo(control) : control.find('#drop_container');
                var wrapper = (control.find('#drop_wrapper').length == 0) ? jQuery('<div class="input-group" id="drop_wrapper" style="cursor: pointer !important; cursor: hand !important;"><span id="drop_loader_container" class="input-group-addon" style="border-top: 1px solid rgb(204, 204, 204) !important; border-bottom: 1px solid rgb(204, 204, 204) !important; border-right: 1px solid rgb(204, 204, 204) !important; background-color: white !important; min-width: 26px; width: 26px !important; padding: 0px !important;"><i id="drop_loader" class="fa fa-sort-desc"></i></span></div>').appendTo(control) : control.find('#drop_wrapper');
                var icons_left = (control.find('#icons_container_left').length == 0) ? jQuery('<span id="icons_container_left" class="input-group-addon"></span>').prependTo(wrapper) : control.find('#icons_container_left');
                var loader = wrapper.find('#drop_loader');

                //Memory/Bank Controls
                var editor = (control.find('#drop_editor').length == 0) ? jQuery('<div id="drop_editor" style="padding-left: 14px !important; background-color: #e5e5e5; padding: 2px; border-left: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5; border-right: 1px solid #e5e5e5; cursor: pointer !important; cursor: hand !important;"></div>').appendTo(control) : control.find('#drop_editor');
                var all = (control.find('#drop_all').length == 0) ? jQuery('<i id="drop_all" style="margin-right:5px !important;" class="fa fa-sort-amount-asc"></i>').appendTo(icons_left) : control.find('#drop_all');
                var clear = (control.find('#drop_clear').length == 0) ? jQuery('<i id="drop_clear" class="fa fa-minus-square-o"></i>').appendTo(icons_left) : control.find('#drop_clear');
                var bank = (control.find('#drop_bank').length == 0) ? jQuery('<div id="drop_bank" style="display: none;"><h5>Click an item to remove it</h5></div>').appendTo(control) : control.find('#drop_bank');

                if (!options.memory) icons_left.hide();

                if (options.memory) {
                    if (options.bank.length == 0) editor.text('No items selected.');

                    editor.unbind('click');
                    editor.click(function (e) {
                        e.stopPropagation();

                        if (options.bank.length == 0 || editor.text().indexOf('All') > -1) {
                            byteFile.toast.create(
                                byteFile.toast.modes.info,
                                byteFile.title,
                                'You can only edit items when they are manually selected.'
                            );

                            return;
                        }

                        options.edit();

                        bank.find('div').remove();

                        for (var i = 0; i < options.bank.length; i++) {
                            var opt = options.bank[i];

                            bank.append('<div id="' + opt.value + '">' + opt.text + '</div>');
                        }

                        byteFile.modal.create(
                            byteFile.modal.modes.confirm,
                            byteFile.title,
                            bank.html(),
                            function (content) {
                                //Confirm

                                jQuery.each(items.find('input[type="checkbox"]'), function () {
                                    var checkbox = jQuery(this);
                                    checkbox.removeAttr('checked');
                                });

                                options.bank = [];

                                jQuery.each(content.find('div'), function () {
                                    var opt = jQuery(this);

                                    options.bank.push({
                                        value: opt.attr('id'),
                                        text: opt.text()
                                    });

                                    items.find('input[value="' + opt.attr('id') + '"]').attr('checked', 'checked');
                                });

                                var selected = jQuery.map(options.bank, function (n, i) {
                                    return n.value;
                                }).join(',');

                                var list = selected.split(',');
                                var feedback = '';

                                if (options.bank.length > 0) {
                                    feedback = (options.bank.length == 1) ? 'One item selected.' : 'Multiple items selected.';
                                    editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                }
                                else {
                                    feedback = 'No items selected.';
                                    editor.text('No items selected.');
                                }

                                control.find('#drop_input').val(feedback);

                                control.data('drop', {
                                    list: list,
                                    value: selected,
                                    text: feedback,
                                    data: null,
                                    handle: control,
                                    options: options,
                                    load: load
                                });

                                if (options.changed && typeof options.changed == 'function') {
                                    options.changed({
                                        list: list,
                                        value: selected,
                                        text: feedback,
                                        data: null,
                                        handle: control,
                                        options: options,
                                        load: load
                                    });
                                }

                                options.editConfirm();
                            },
                            function (content) {
                                options.editCancel();
                            },
                            false,
                            byteFile.modal.size.large,
                            function (content) {
                                //Bound

                                jQuery.each(content.find('div'), function () {
                                    var opt = jQuery(this);

                                    opt.css('cursor', 'pointer');

                                    opt.click(function (e) {
                                        e.stopPropagation();

                                        opt.remove();
                                    });
                                });
                            });
                    });

                    all.unbind('click');
                    all
                        .mouseover(function () {
                            if (editor.find('div').length > 0) {
                                jQuery.each(editor.find('div'), function () {
                                    jQuery(this).remove();
                                });
                            }

                            jQuery('<div>Select All Items</div>').prependTo(editor);
                        })
                        .mouseout(function () {
                            jQuery.each(editor.find('div'), function () {
                                jQuery(this).remove();
                            });
                        })
                        .click(function (e) {
                            e.stopPropagation();

                            jQuery.each(items.find('input[type="checkbox"]'), function () {
                                var checkbox = jQuery(this);
                                checkbox.attr('checked', 'checked');
                            });

                            options.bank = ['All'];

                            var selected = 'All';
                            var list = ['All'];
                            var feedback = 'All items selected.';

                            control.data('drop', {
                                list: list,
                                value: selected,
                                text: feedback,
                                data: null,
                                handle: control,
                                options: options,
                                load: load
                            });

                            if (options.changed && typeof options.changed == 'function') {
                                options.changed({
                                    list: list,
                                    value: selected,
                                    text: feedback,
                                    data: null,
                                    handle: control,
                                    options: options,
                                    load: load
                                });
                            }

                            editor.text(feedback);
                            input.val(feedback);

                            items.hide();

                            options.all();
                        });

                    clear.unbind('click');
                    clear
                        .mouseover(function () {
                            if (editor.find('div').length > 0) {
                                jQuery.each(editor.find('div'), function () {
                                    jQuery(this).remove();
                                });
                            }

                            jQuery('<div>Clear Selection</div>').prependTo(editor);
                        })
                        .mouseout(function () {
                            jQuery.each(editor.find('div'), function () {
                                jQuery(this).remove();
                            });
                        })
                        .click(function (e) {
                            e.stopPropagation();

                            jQuery.each(items.find('input[type="checkbox"]'), function () {
                                var checkbox = jQuery(this);
                                checkbox.removeAttr('checked');
                            });

                            options.bank = [];

                            var selected = '';
                            var list = [];
                            var feedback = '';

                            feedback = 'No items selected.';

                            editor.text(feedback);
                            input.val('');

                            items.find('div').remove();
                            items.hide();

                            options.search = '';
                            options.key = 0;
                            options.page = 0;

                            control.data('drop', {
                                list: [],
                                value: '',
                                text: '',
                                data: null,
                                handle: control,
                                options: options,
                                load: load
                            });

                            if (options.changed && typeof options.changed == 'function') {
                                control.data('drop', {
                                    list: [],
                                    value: '',
                                    text: '',
                                    data: null,
                                    handle: control,
                                    options: options,
                                    load: load
                                });

                                options.changed(control.data('drop'));
                            }

                            control.drop(options, false);

                            options.clear();
                        });
                }
                else {
                    editor.hide();
                    all.hide();
                    clear.hide();
                }

                //if (!options.bank.memory) {

                //}

                //if (button) {
                //    //byte-File.log('has button');

                //    control
                //        .closest('[data-morph="true"]')
                //        .css('position', 'absolute')
                //        .css('z-index', '99999')
                //        .css('top', '200px')
                //        .css('right', '200px');

                //    //byte-File.log('moved button');
                //}

                var input = (wrapper.find('#drop_input').length == 0)
                        ? jQuery('<input data-tree="search" id="drop_input" autocomplete="off" type="text" ' + ((options.width) ? 'style="color:black; height: 26px; padding: 3px; width: ' + options.width + 'px;"' : '') + ' />')
                        .insertAfter(icons_left)
                        .data('handle', control)
                        .click(function (e) {
                            e.stopPropagation();

                            if (items.is(':hidden')) {
                                jQuery('[data-tree="items"]').each(function () {
                                    jQuery(this).hide();
                                });

                                items.show();
                            }
                            else {
                                items.hide();
                            }

                            e.stopPropagation();
                            e.preventDefault();
                        })
                        .keyup(function (e) {
                            try {
                                e.stopPropagation();
                                e.preventDefault();

                                if (items.is(':hidden')) {
                                    jQuery('[data-tree="items"]').each(function () {
                                        jQuery(this).hide();
                                    });

                                    items.show();
                                }

                                var item = jQuery(this);


                                if (e.which == 35 || e.which == 16 || e.which == 17) {
                                    return;
                                }

                                if (e.which == 13 || (item.val().length == 0 || item.val().length > 1)) {
                                    items.scrollTop(0);

                                    //var item = jQuery(this);

                                    if (item.val().length > 1) {
                                        if (!options.searching) {
                                            //byteFile.toast.create(
                                            //    byteFile.toast.modes.info,
                                            //    byteFile.title,
                                            //    'Searching...'
                                            //);
                                        }

                                        options.search = item.val(),
                                        options.key = 0;
                                        options.page = 0;
                                        options.searching = true;

                                        control.drop(options, false);
                                    }

                                    if (item.val().length > 0 && item.val().length < 2) {
                                        byteFile.toast.create(
                                            byteFile.toast.modes.warning,
                                            byteFile.title,
                                            'Please enter at least 2 characters...'
                                        );
                                    }

                                    if (item.val().length == 0) {
                                        options.search = '';
                                        options.key = 0;
                                        options.page = 0;

                                        control.data('drop', {
                                            list: [],
                                            value: '',
                                            text: '',
                                            data: null,
                                            handle: control,
                                            options: options,
                                            load: load
                                        });

                                        if (options.changed && typeof options.changed == 'function') {
                                            control.data('drop', {
                                                list: [],
                                                value: '',
                                                text: '',
                                                data: null,
                                                handle: control,
                                                options: options,
                                                load: load
                                            });

                                            options.changed(control.data('drop'));
                                        }

                                        control.drop(options, false);
                                    }

                                    //control.drop(options, false);
                                }
                            }
                            catch (e) {
                                byteFile.error('FRAMEWORK :: byteFile.extend() > Error @ Drop: ' + e.message + '...');
                            }
                        }) : control.find('#drop_input');


                (function (wrapper, items) {
                    wrapper.unbind('click');

                    wrapper.click(function (e) {
                        e.stopPropagation();

                        if (items.is(':hidden')) {
                            jQuery('[data-tree="items"]').each(function () {
                                jQuery(this).hide();
                            });

                            items.show();
                        }
                        else {
                            items.hide();
                        }

                        e.stopPropagation();
                        e.preventDefault();
                    });
                })(wrapper, items);

                options.search = (input.val().length > 1) ? input.val() : '';

                if (!load) {
                    items.empty();
                }

                if (!options.loaded) { //Init (First Load)
                    control.data('drop', {
                        list: [],
                        value: 0,
                        text: '',
                        data: null,
                        handle: control,
                        options: options,
                        load: load
                    });

                    control.data('events', {
                        fire: function (event) {
                            switch (event) {
                                case 'change':
                                    options.key = control.find('input').val();
                                    control.drop(options, false);
                                    return;
                            }
                        }
                    });

                    control.data('options', options);
                    control.css('width', options.width + 'px');
                    items.css({
                        position: 'absolute',
                        top: '35px'

                    });

                    //items.unbind('scroll');

                    items.bind('scroll', function (e) {
                        e.stopPropagation();

                        if (jQuery(this).scrollTop() + jQuery(this).innerHeight() >= (this.scrollHeight - 350)) {
                            options.page++;
                            options.position = jQuery(this).scrollTop();

                            control.drop(options, true);
                        }
                    });

                    input.unbind('click');
                    input.click(function (e) {
                        e.stopPropagation();

                        if (items.is(':hidden')) {
                            jQuery('[data-tree="items"]').each(function () {
                                jQuery(this).hide();
                            });

                            items.show();

                        }
                        else {
                            items.hide();
                        }

                        e.stopPropagation();
                        e.preventDefault();
                    });

                    loader.unbind('click');
                    loader.click(function e() {
                        if (items.is(':hidden')) {
                            jQuery('[data-tree="items"]').each(function () {
                                jQuery(this).hide();
                            });

                            items.show();
                        }
                        else {
                            items.hide();
                        }

                        e.stopPropagation();
                        e.preventDefault();
                    });

                    input.attr('placeholder', options.placeholder);

                    options.loaded = true;
                }

                loader.attr('class', 'fa fa-refresh fa-spin');

                //byte-File.log(options.source + '&total=' + options.count + '&page=' + options.page + '&take=' + options.take + '&search=' + options.search + '&key=' + options.key);

                jQuery.ajax({
                    url: options.source + '&total=' + options.count + '&page=' + options.page + '&take=' + options.take + '&search=' + options.search + '&key=' + options.key,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        try {
                            if (data.length > 0) {
                                options.searching = false;

                                if (options.key > 0 && data.length == 1) {
                                    input.val(data[0][options.text]);

                                    control.data('drop', {
                                        list: [],
                                        value: data[0][options.value],
                                        text: data[0][options.text],
                                        data: null,
                                        handle: control,
                                        options: options,
                                        load: load
                                    });

                                    if (options.changed && typeof options.changed == 'function') {
                                        options.changed({
                                            value: data[0][options.value],
                                            text: data[0][options.text],
                                            data: null,
                                            handle: control,
                                            options: options,
                                            load: load
                                        });
                                    }

                                    options.key = 0;
                                }
                            }
                            else {
                                return;
                            }

                            var primaryColor = '#fff';
                            var secondaryColor = '#EFF2EF';
                            var currentColor = 0;

                            jQuery.each(data, function () {
                                var row = this;

                                if (currentColor == primaryColor) {
                                    currentColor = secondaryColor;
                                }
                                else {
                                    currentColor = primaryColor;
                                }

                                var matches = jQuery.grep(options.bank, function (x) {
                                    return x.value == row[options.value];
                                });

                                var select = (matches.length > 0) ? true : false;

                                if (items.find('[data-value="' + row[options.value] + '"]').length <= 0) {
                                    var option = jQuery('<div style="width: 100%; background-color: ' + currentColor + ';" data-value="' + row[options.value] + '" data-text="' + row[options.text] + '">' + ((options.multi) ? '<input type="checkbox" disabled="disabled" value="' + row[options.value] + '" ' + ((select) ? 'checked="checked"' : '') + ' />' : '') + ' ' + row[options.text] + '</div>')
                                        .appendTo(items)
                                        .mouseover(function () {
                                            ////byte-File.log('mouse over');

                                            var item = jQuery(this);

                                            item.data('currentColor', item.css('background'))
                                                .css('background', options.highlight)
                                                .css('cursor', 'pointer');
                                        })
                                        .mouseout(function () {
                                            var item = jQuery(this);
                                            item.css('background', item.data('currentColor'));
                                        })
                                        .mousedown(function (e) {
                                            //For when it's a kendo grid control in the create/change section of the wizard
                                            e.stopPropagation();

                                            if (options.framework) {
                                                ////byte-File.log('mouse down');

                                                var item = jQuery(this);

                                                if (options.multi) {
                                                    ////byte-File.log('multi select');

                                                    var checkbox = item.find('input[type="checkbox"]');

                                                    if (checkbox.is(':checked')) {
                                                        options.count--;
                                                        checkbox.removeAttr('checked');
                                                    }
                                                    else {
                                                        options.count++;
                                                        checkbox.attr('checked', 'checked');
                                                    }

                                                    var selected = jQuery.map(control.find('input[type="checkbox"]:checked'), function (n, i) {
                                                        return n.value;
                                                    }).join(',');

                                                    //selected = selected.replace(/,\s*jQuery/, ''); //removes last comma from string if any
                                                    var list = selected.split(',');
                                                    var feedback = (selected.replace('[""]', '').length == 0) ? '' : '(' + list.length + ') items selected...';

                                                    input.val(feedback);

                                                    control.data('drop', {
                                                        list: list,
                                                        value: selected,
                                                        text: feedback,
                                                        data: null,
                                                        handle: control,
                                                        options: options,
                                                        load: load
                                                    });

                                                    if (options.changed && typeof options.changed == 'function') {
                                                        options.changed({
                                                            list: list,
                                                            value: selected,
                                                            text: feedback,
                                                            data: null,
                                                            handle: control,
                                                            options: options,
                                                            load: load
                                                        });
                                                    }

                                                    if (selected.replace('[""]', '').length == 0) {
                                                        items.hide();
                                                    }
                                                }
                                                else {
                                                    ////byte-File.log('not multi select');

                                                    input.focus().val(item.data('text'));

                                                    control.data('drop', {
                                                        list: item.data('value'),
                                                        value: item.data('value'),
                                                        text: item.data('text'),
                                                        data: null,
                                                        handle: control,
                                                        options: options,
                                                        load: load
                                                    });

                                                    if (options.changed && typeof options.changed == 'function') {
                                                        ////byte-File.log('fire event');
                                                        ////byte-File.log(item.data('text') + ': ' + item.data('value'));

                                                        options.changed({
                                                            list: null,
                                                            value: item.data('value'),
                                                            text: item.data('text'),
                                                            data: null,
                                                            handle: control,
                                                            options: options,
                                                            load: load
                                                        });
                                                    }

                                                    items.hide();
                                                }
                                            }
                                        })
                                        .click(function (e) {
                                            e.stopPropagation();
                                            //This event will not fire when used as a kendo grid edit column control
                                            ////byte-File.log('click');

                                            var item = jQuery(this);

                                            if (options.multi) {
                                                //byte-File.log('is multi!');

                                                var checkbox = item.find('input[type="checkbox"]');

                                                if (checkbox.is(':checked')) {
                                                    options.count--;
                                                    checkbox.removeAttr('checked');

                                                    if (options.memory) {
                                                        options.bank = jQuery.grep(options.bank, function (x) {
                                                            return x.value != item.data('value');
                                                        });

                                                        if (options.bank.length > 0) {
                                                            editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                                        }
                                                        else {
                                                            editor.text('No items selected.');
                                                        }
                                                    }

                                                    //byte-File.log('selected');
                                                }
                                                else {
                                                    options.count++;
                                                    checkbox.attr('checked', 'checked');

                                                    if (options.memory) {
                                                        options.bank.push({
                                                            text: item.data('text'),
                                                            value: item.data('value')
                                                        });

                                                        if (options.bank.length > 0) {
                                                            editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                                        }
                                                        else {
                                                            editor.text('No items selected.');
                                                        }
                                                    }

                                                    //byte-File.log('de-selected');
                                                }

                                                if (options.memory) {
                                                    //byte-File.log(JSON.stringify(options.bank, null, 4));
                                                }

                                                var selected = '';

                                                if (options.memory) {
                                                    selected = jQuery.map(options.bank, function (n, i) {
                                                        return n.value;
                                                    }).join(',');
                                                }
                                                else {
                                                    selected = jQuery.map(control.find('input[type="checkbox"]:checked'), function (n, i) {
                                                        return n.value;
                                                    }).join(',');
                                                }

                                                //selected = selected.replace(/,\s*jQuery/, ''); //removes last comma from string if any
                                                var list = selected.split(',');
                                                var feedback = '';

                                                if (options.memory) {
                                                    if (options.bank.length > 0) {
                                                        feedback = (options.bank.length == 1) ? 'One item selected.' : 'Multiple items selected.';
                                                    }
                                                    else {
                                                        feedback = 'No items selected.';
                                                    }
                                                }
                                                else {
                                                    feedback = (selected.replace('[""]', '').length == 0) ? '' : '(' + list.length + ') items selected.';
                                                }

                                                input.val(feedback);

                                                control.data('drop', {
                                                    list: list,
                                                    value: selected,
                                                    text: feedback,
                                                    data: null,
                                                    handle: control,
                                                    options: options,
                                                    load: load
                                                });

                                                if (options.changed && typeof options.changed == 'function') {
                                                    options.changed({
                                                        list: list,
                                                        value: selected,
                                                        text: feedback,
                                                        data: null,
                                                        handle: control,
                                                        options: options,
                                                        load: load
                                                    });
                                                }

                                                if (selected.replace('[""]', '').length == 0) {
                                                    items.hide();
                                                }
                                            }
                                            else {
                                                ////byte-File.log('click');
                                                ////byte-File.log(item.data('text'));

                                                input.focus().val(item.data('text'));

                                                control.data('drop', {
                                                    list: item.data('value'),
                                                    value: item.data('value'),
                                                    text: item.data('text'),
                                                    data: null,
                                                    handle: control,
                                                    options: options,
                                                    load: load
                                                });

                                                if (options.changed && typeof options.changed == 'function') {
                                                    options.changed({
                                                        list: item.data('value'),
                                                        value: item.data('value'),
                                                        text: item.data('text'),
                                                        data: null,
                                                        handle: control,
                                                        options: options,
                                                        load: load
                                                    });
                                                }

                                                items.hide();
                                            }

                                            e.stopPropagation();
                                            e.preventDefault();
                                        });
                                }
                            });

                            items.appendTo(container);
                            items.scrollTop(options.position);

                            if (options.bound && typeof options.bound == 'function') {
                                options.bound(data);
                            }

                            loader.attr('class', 'fa fa-sort-desc');
                        }
                        catch (e) {
                            byteFile.error('FRAMEWORK :: byteFile.extend() > Drop Error @ Populate Success: ' + e.message + '...');
                        }
                    },
                    error: function (e) {
                        byteFile.error('FRAMEWORK :: byteFile.extend() > Drop Error @ Populate Error: ' + e.message + '...');
                    }
                });
            }
            catch (e) {
                byteFile.error('FRAMEWORK :: byteFile.extend() > Drop Error @ General: ' + e.message + '...');
            }
        };

        jQuery.fn.isBound = function (type, fn) {
            var data = this.data('events')[type];

            if (data === undefined || data.length === 0) {
                return false;
            }

            return (-1 !== jQuery.inArray(fn, data));
        };
    },

    highlight: {
        handle: null,
        element: null,
        apply: function (element) {
            byteFile.highlight.element = element;

            //byteFile.highlight.handle = new Hilitor();
            byteFile.highlight.handle.apply(byteFile.highlight.element.val());
        },
        remove: function () {
            byteFile.highlight.handle.remove();
        }
    },

    hierarchy: function (data, parent, sortField, childField, parentField, container, options, control, input, items, load, editor, all, clear, bank) {
        try {
            var primaryColor = '#fff';
            var secondaryColor = '#EFF2EF';
            var currentColor = 0;

            var start = new Date().getTime();

            var children = byteFile.filter.from(data)
                .equals(parentField, parent[childField])
                .sort(sortField)
                .select();

            var distinct = byteFile.filter.from(children).distinct(options.value);

            var end = new Date().getTime();
            var time = end - start;

            //byte-File.log('byteFile.hierarchy() > Getting children of "' + parent[childField] + ' took (' + (+time / 1000 % 60).toFixed(2) + ') seconds...');

            start = new Date().getTime();

            jQuery.each(distinct, function () {
                var child = this;

                var row = byteFile.filter.from(data)
                    .equals(options.value, parseFloat(child))
                    .select()[0];

                //var row = this;
                //byte-File.log('Test: ' + JSON.stringify(child, null, 4));

                //byte-File.log(row.organisation);

                //control.find('#loader').attr('class', 'fa-spin fa-spinner');

                if (currentColor == primaryColor) {
                    currentColor = secondaryColor;
                }
                else {
                    currentColor = primaryColor;
                }

                var option =
                    jQuery('<div data-iscategory="false" data-loaded="true" data-options="false" style="width: 100%; cursor: pointer; cursor: hand; margin-left: 20px; background-color: ' + currentColor + ';" data-value="' + row[options.value] + '" data-text="' + row[options.text] + '"> ' + ((row.HasChildrens == '1') ? ' <i class="fa fa-plus-square"></i>&nbsp;' : '&nbsp;&nbsp;') + ((options.multi) ? ' <input type="checkbox" data-text="' + row[options.text] + '" value="' + row[options.value] + '" ' + ((options.all) ? 'checked="checked"' : '') + ' /> ' : '<input type="radio"  data-text="' + row[options.text] + '" name="org_hierarchy" value="' + row[options.value] + '" /> ') + ' ' + row[options.text] + '</div>')
                        .appendTo(container)
                        .mouseover(function () {
                            jQuery(this)
                                .data('currentColor', jQuery(this).css('background'))
                                .css('background', options.highlight)
                                .css('cursor', 'pointer');
                        })
                        .mouseout(function () {
                            jQuery(this).css('background', jQuery(this).data('currentColor'));
                        })
                        .data('text', row[options.text])
                        .data('value', row[options.value])
                        .click(function (e) {
                            e.stopPropagation();

                            try {
                                var item = jQuery(this);

                                if (options.multi) {
                                    var checkbox = jQuery(this).find('input[type="checkbox"]:first');

                                    if (checkbox.is(':checked')) {
                                        checkbox.removeAttr('checked');

                                        if (options.memory) {
                                            options.bank = jQuery.grep(options.bank, function (x) {
                                                return x.value != item.data('value');
                                            });

                                            if (options.bank.length > 0) {
                                                editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                            }
                                            else {
                                                editor.text('No items selected.');
                                            }
                                        }

                                        //byte-File.log('selected');
                                    }
                                    else {
                                        checkbox.attr('checked', 'checked');

                                        if (options.memory) {
                                            options.bank.push({
                                                text: item.data('text'),
                                                value: item.data('value')
                                            });

                                            if (options.bank.length > 0) {
                                                editor.text('(' + options.bank.length + ') items selected. Click here to edit.');
                                            }
                                            else {
                                                editor.text('No items selected.');
                                            }
                                        }

                                        //byte-File.log('de-selected');
                                    }

                                    if (options.memory) {
                                        //byte-File.log(JSON.stringify(options.bank, null, 4));
                                    }

                                    var selected = '';

                                    if (options.memory) {
                                        selected = jQuery.map(options.bank, function (n, i) {
                                            return n.value;
                                        }).join(',');
                                    }
                                    else {
                                        selected = jQuery.map(control.find('input[type="checkbox"]:checked'), function (n, i) {
                                            return n.value;
                                        }).join(',');
                                    }

                                    //selected = selected.replace(/,\s*jQuery/, ''); //removes last comma from string if any
                                    var list = selected.split(',');
                                    var feedback = '';

                                    if (options.memory) {
                                        if (options.bank.length > 0) {
                                            feedback = (options.bank.length == 1) ? 'One item selected.' : 'Multiple items selected.';
                                        }
                                        else {
                                            feedback = 'No items selected.';
                                        }
                                    }
                                    else {
                                        feedback = (selected.replace('[""]', '').length == 0) ? '' : '(' + list.length + ') items selected.';
                                    }

                                    input.val(feedback);

                                    control.data('tree', {
                                        list: list,
                                        value: selected,
                                        text: feedback,
                                        data: null,
                                        handle: control,
                                        options: options,
                                        load: load
                                    });

                                    if (options.changed && typeof options.changed == 'function') {
                                        options.changed({
                                            list: list,
                                            value: selected,
                                            text: feedback,
                                            data: null,
                                            handle: control,
                                            options: options,
                                            load: load
                                        });
                                    }

                                    options.search = '';

                                    if (selected.replace('[""]', '').length == 0) {
                                        items.hide();
                                    }
                                }
                                else {
                                    var radiobutton = jQuery(this).find('input[type="radio"]:first');

                                    if (radiobutton.is(':checked')) {
                                        radiobutton.removeAttr('checked');
                                    }
                                    else {
                                        radiobutton.attr('checked', 'checked');
                                    }

                                    input.val(jQuery(this).data('text'));

                                    control.data('tree', {
                                        list: jQuery(this).data('value'),
                                        value: jQuery(this).data('value'),
                                        text: jQuery(this).data('text'),
                                        data: null,
                                        handle: control,
                                        options: options,
                                        load: load
                                    });

                                    if (options.changed && typeof options.changed == 'function') {
                                        options.changed({
                                            list: jQuery(this).data('value'),
                                            value: jQuery(this).data('value'),
                                            text: jQuery(this).data('text'),
                                            data: null,
                                            handle: control,
                                            options: options,
                                            load: load
                                        });
                                    }

                                    options.search = '';

                                    items.hide();
                                }

                                //if (!jQuery(this).data('loaded')) {
                                //    jQuery(this)
                                //        .data('loaded', true)
                                //        .data('key', row[options.value])
                                //        .data('category', expand.data('category'));

                                //    if (!jQuery(this).data('options')) {
                                //        options.page = 0;
                                //        jQuery(this).data('options', options);
                                //    }
                                //    else {
                                //        var config = jQuery(this).data('options');
                                //        config.page++;

                                //        jQuery(this).data('options', config);
                                //    }

                                //    options = jQuery(this).data('options');
                                //    //control.tree(options, true, jQuery(this));

                                //    return;
                                //}
                                //else {
                                //    if (jQuery(this).data('loaded') && jQuery(this).find('div:first').is(':visible')) {
                                //        jQuery(this).find('div').hide();
                                //    }
                                //    else {
                                //        jQuery(this).find('div').show();
                                //    }
                                //}

                                if (jQuery(this).data('loaded') && jQuery(this).find('div:first').is(':visible')) {
                                    jQuery(this).find('div').hide();
                                }
                                else {
                                    jQuery(this).find('div').show();
                                }
                            }
                            catch (e) {
                                byteFile.error('FRAMEWORK :: byteFile.hierarchy() > Error @ Hierarchy Item Click: ' + e.message + '...');
                            }

                            e.stopPropagation();
                            e.preventDefault();
                        });

                var count = byteFile.filter.from(data)
                    .equals(parentField, row[childField])
                    .count();

                //byte-File.log('byteFile.hierarchy() > Found (' + count + ') children of "[' + parentField + ':' + row[childField] + ']...');

                if (count > 0) {
                    byteFile.hierarchy(data, row, sortField, childField, parentField, option, options, control, input, items, load, editor, all, clear, bank);
                }
            });

            end = new Date().getTime();
            time = end - start;

            //byte-File.log('byteFile.hierarchy() > Populating children of "' + parent[childField] + ' took (' + (+time / 1000 % 60).toFixed(2) + ') seconds...');
        }
        catch (e) {
            byteFile.error('FRAMEWORK :: byteFile.hierarchy() > Error: ' + e.message + '...');
        }
    },
    
    helpers: {
        array: {
            contains: function (obj, list, prop) {
                try {
                    for (i = 0; i < list.length; i++) {
                        if (list[i][prop] == obj[prop]) {
                            return i;
                        }
                    }

                    return -1;
                }
                catch (e) {
                    //byte-File.log('byteFile.helpers.array.contains() > Error: ' + e.message + '...');
                }
            },
            get: function (list, prop, val) {
                try {
                    for (i = 0; i < list.length; i++) {
                        if (list[i][prop] == val) {
                            return list[i];
                        }
                    }

                    return {};
                }
                catch (e) {
                    //byte-File.log('byteFile.helpers.array.get() > Error: ' + e.message + '...');
                }
            }
        },
        file: {
            size: function (bytes) {
                if (bytes == 0) return '0 Byte';

                var k = 1000;
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
                var i = Math.floor(Math.log(bytes) / Math.log(k));

                return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
            },
            bytes: function (bytes) {
                return bytes * 1024 * 1024;
            },
            dataURItoBlob: function (dataURI, mimeType) {
                var binary = atob(dataURI.split(',')[1]);
                var array = [];

                for (var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }

                return new Blob([new Uint8Array(array)], { type: mimeType });
            },
        },
        types: {
            kpis: {
                colours: {
                    green: 'dashboard-stat green',
                    blue: 'dashboard-stat blue',
                    red: 'dashboard-stat red',
                    yellow: 'dashboard-stat yellow',
                    purple: 'dashboard-stat purple',
                },
            },
            indicators: {
                colours: {
                    green: 'dashboard-stat green',
                    blue: 'dashboard-stat blue',
                    red: 'dashboard-stat red',
                    yellow: 'dashboard-stat yellow',
                    purple: 'dashboard-stat purple',
                },
            },
            aggregates: {
                sum: 'sum',
                average: 'average',
                count: 'count',
                min: 'min',
                max: 'max',
            },
            colours: {
                blue: 'colour_dark_blue',//'blue',
                red: 'colour_dark_blue',//'red',
                green: 'colour_dark_blue',//'green',
                yellow: 'colour_dark_blue',//'yellow',
                purple: 'colour_dark_blue',
                orange: 'colour_dark_blue',//'orange',
            },
            imports: {
                colours: {
                    blue: 'blue',
                    red: 'red',
                    green: 'green',
                    yellow: 'yellow',
                    purple: 'colour_dark_blue',
                    orange: 'orange',
                },
            },
            charts: {
                area: 'area',
                bar: 'bar',
                column: 'column',
                boxplot: 'boxPlot',
                bubble: 'bubble',
                bullet: 'bullet',
                donut: 'donut',
                funnel: 'funnel',
                line: 'line',
                pie: 'pie',
                polar: 'polar',
                radar: 'radar',
                rangebar: 'rangeBar',
                waterfall: 'waterfall',
                scatter: 'scatter',
                sparklines: 'sparklines',
            },
            gauges: {
                linear: 'linear',
                radial: 'radial',
            },
            stats: {
                spark: 'spark',
                radial: 'radial',
            },
            lists: {
                list: 'list',
                grid: 'grid'
            },
            input: {
                hidden: 'hidden',
                checkbox: 'checkbox',
                colour: 'colour',
                numeric: {
                    number: 'number',
                    currency: 'currency',
                    weight: 'weight',
                    percentage: 'percentage',
                    whole: 'whole',
                },
                file: {
                    single: 'file_single',
                    multi: 'file_multi',
                },
                select: {
                    combo: 'combo',
                    multi: 'select_multi',
                    treeview: 'select_treeview',
                    drop: 'drop',
                    auto: 'auto',
                    //radio: 'radio',
                },
                text: {
                    single: 'text_single',
                    multi: 'text_multi',
                },
                date: {
                    date: 'date',
                    time: 'time',
                    datetime: 'datetime',
                },
            },
            icons: {
                automobile: 'fa fa-automobile',
                bank: 'fa fa-bank',
                behance: 'fa fa-behance',
                behance_square: 'fa fa-behance-square',
                bomb: 'fa fa-bomb',
                building: 'fa fa-building',
                cab: 'fa fa-cab',
                car: 'fa fa-car',
                child: 'fa fa-child',
                circle_o_notch: 'fa fa-circle-o-notch',
                circle_thin: 'fa fa-circle-thin',
                codepen: 'fa fa-codepen',
                cube: 'fa fa-cube',
                cubes: 'fa fa-cubes',
                database: 'fa fa-database',
                delicious: 'fa fa-delicious',
                deviantart: 'fa fa-deviantart',
                digg: 'fa fa-digg',
                drupal: 'fa fa-drupal',
                empire: 'fa fa-empire',
                envelope_square: 'fa fa-envelope-square',
                fax: 'fa fa-fax',
                file_archive_o: 'fa fa-file-archive-o',
                file_audio_o: 'fa fa-file-audio-o',
                file_code_o: 'fa fa-file-code-o',
                file_excel_o: 'fa fa-file-excel-o',
                file_image_o: 'fa fa-file-image-o',
                file_movie_o: 'fa fa-file-movie-o',
                file_pdf_o: 'fa fa-file-pdf-o',
                file_photo_o: 'fa fa-file-photo-o',
                file_picture_o: 'fa fa-file-picture-o',
                file_powerpoint_o: 'fa fa-file-powerpoint-o',
                file_sound_o: 'fa fa-file-sound-o',
                file_video_o: 'fa fa-file-video-o',
                file_word_o: 'fa fa-file-word-o',
                file_zip_o: 'fa fa-file-zip-o',
                ge: 'fa fa-ge',
                git: 'fa fa-git',
                git_square: 'fa fa-git-square',
                google: 'fa fa-google',
                graduation_cap: 'fa fa-graduation-cap',
                hacker_news: 'fa fa-hacker-news',
                header: 'fa fa-header',
                history: 'fa fa-history',
                institution: 'fa fa-institution',
                joomla: 'fa fa-joomla',
                jsfiddle: 'fa fa-jsfiddle',
                language: 'fa fa-language',
                life_bouy: 'fa fa-life-bouy',
                life_ring: 'fa fa-life-ring',
                life_saver: 'fa fa-life-saver',
                mortar_board: 'fa fa-mortar-board',
                openid: 'fa fa-openid',
                paper_plane: 'fa fa-paper-plane',
                paper_plane_o: 'fa fa-paper-plane-o',
                paragraph: 'fa fa-paragraph',
                paw: 'fa fa-paw',
                pied_piper: 'fa fa-pied-piper',
                pied_piper_alt: 'fa fa-pied-piper-alt',
                pied_piper_square: 'fa fa-pied-piper-square',
                qq: 'fa fa-qq',
                ra: 'fa fa-ra',
                rebel: 'fa fa-rebel',
                recycle: 'fa fa-recycle',
                reddit: 'fa fa-reddit',
                reddit_square: 'fa fa-reddit-square',
                send: 'fa fa-send',
                send_o: 'fa fa-send-o',
                share_alt: 'fa fa-share-alt',
                share_alt_square: 'fa fa-share-alt-square',
                slack: 'fa fa-slack',
                sliders: 'fa fa-sliders',
                soundcloud: 'fa fa-soundcloud',
                space_shuttle: 'fa fa-space-shuttle',
                spoon: 'fa fa-spoon',
                spotify: 'fa fa-spotify',
                steam: 'fa fa-steam',
                steam_square: 'fa fa-steam-square',
                stumbleupon: 'fa fa-stumbleupon',
                stumbleupon_circle: 'fa fa-stumbleupon-circle',
                support: 'fa fa-support',
                taxi: 'fa fa-taxi',
                tencent_weibo: 'fa fa-tencent-weibo',
                tree: 'fa fa-tree',
                university: 'fa fa-university',
                vine: 'fa fa-vine',
                wechat: 'fa fa-wechat',
                weixin: 'fa fa-weixin',
                wordpress: 'fa fa-wordpress',
                yahoo: 'fa fa-yahoo',
                adjust: 'fa fa-adjust',
                anchor: 'fa fa-anchor',
                archive: 'fa fa-archive',
                arrows: 'fa fa-arrows',
                arrows_h: 'fa fa-arrows-h',
                arrows_v: 'fa fa-arrows-v',
                asterisk: 'fa fa-asterisk',
                automobile: 'fa fa-automobile',
                ban: 'fa fa-ban',
                bank: 'fa fa-bank',
                bar_chart_o: 'fa fa-bar-chart-o',
                barcode: 'fa fa-barcode',
                bars: 'fa fa-bars',
                beer: 'fa fa-beer',
                bell: 'fa fa-bell',
                bell_o: 'fa fa-bell-o',
                bolt: 'fa fa-bolt',
                bomb: 'fa fa-bomb',
                book: 'fa fa-book',
                bookmark: 'fa fa-bookmark',
                bookmark_o: 'fa fa-bookmark-o',
                briefcase: 'fa fa-briefcase',
                bug: 'fa fa-bug',
                building: 'fa fa-building',
                building_o: 'fa fa-building-o',
                bullhorn: 'fa fa-bullhorn',
                bullseye: 'fa fa-bullseye',
                cab: 'fa fa-cab',
                calendar: 'fa fa-calendar',
                calendar_o: 'fa fa-calendar-o',
                camera: 'fa fa-camera',
                camera_retro: 'fa fa-camera-retro',
                car: 'fa fa-car',
                caret_square_o_down: 'fa fa-caret-square-o-down',
                caret_square_o_left: 'fa fa-caret-square-o-left',
                caret_square_o_right: 'fa fa-caret-square-o-right',
                caret_square_o_up: 'fa fa-caret-square-o-up',
                certificate: 'fa fa-certificate',
                check: 'fa fa-check',
                check_circle: 'fa fa-check-circle',
                check_circle_o: 'fa fa-check-circle-o',
                check_square: 'fa fa-check-square',
                check_square_o: 'fa fa-check-square-o',
                child: 'fa fa-child',
                circle: 'fa fa-circle',
                circle_o: 'fa fa-circle-o',
                circle_o_notch: 'fa fa-circle-o-notch',
                circle_thin: 'fa fa-circle-thin',
                clock_o: 'fa fa-clock-o',
                cloud: 'fa fa-cloud',
                cloud_download: 'fa fa-cloud-download',
                cloud_upload: 'fa fa-cloud-upload',
                code: 'fa fa-code',
                code_fork: 'fa fa-code-fork',
                coffee: 'fa fa-coffee',
                cog: 'fa fa-cog',
                cogs: 'fa fa-cogs',
                comment: 'fa fa-comment',
                comment_o: 'fa fa-comment-o',
                comments: 'fa fa-comments',
                comments_o: 'fa fa-comments-o',
                compass: 'fa fa-compass',
                credit_card: 'fa fa-credit-card',
                crop: 'fa fa-crop',
                crosshairs: 'fa fa-crosshairs',
                cube: 'fa fa-cube',
                cubes: 'fa fa-cubes',
                cutlery: 'fa fa-cutlery',
                dashboard: 'fa fa-dashboard',
                database: 'fa fa-database',
                desktop: 'fa fa-desktop',
                dot_circle_o: 'fa fa-dot-circle-o',
                download: 'fa fa-download',
                edit: 'fa fa-edit',
                ellipsis_h: 'fa fa-ellipsis-h',
                ellipsis_v: 'fa fa-ellipsis-v',
                envelope: 'fa fa-envelope',
                envelope_o: 'fa fa-envelope-o',
                envelope_square: 'fa fa-envelope-square',
                eraser: 'fa fa-eraser',
                exchange: 'fa fa-exchange',
                exclamation: 'fa fa-exclamation',
                exclamation_circle: 'fa fa-exclamation-circle',
                exclamation_triangle: 'fa fa-exclamation-triangle',
                external_link: 'fa fa-external-link',
                external_link_square: 'fa fa-external-link-square',
                eye: 'fa fa-eye',
                eye_slash: 'fa fa-eye-slash',
                fax: 'fa fa-fax',
                female: 'fa fa-female',
                fighter_jet: 'fa fa-fighter-jet',
                file_archive_o: 'fa fa-file-archive-o',
                file_audio_o: 'fa fa-file-audio-o',
                file_code_o: 'fa fa-file-code-o',
                file_excel_o: 'fa fa-file-excel-o',
                file_image_o: 'fa fa-file-image-o',
                file_movie_o: 'fa fa-file-movie-o',
                file_pdf_o: 'fa fa-file-pdf-o',
                file_photo_o: 'fa fa-file-photo-o',
                file_picture_o: 'fa fa-file-picture-o',
                file_powerpoint_o: 'fa fa-file-powerpoint-o',
                file_sound_o: 'fa fa-file-sound-o',
                file_video_o: 'fa fa-file-video-o',
                file_word_o: 'fa fa-file-word-o',
                file_zip_o: 'fa fa-file-zip-o',
                film: 'fa fa-film',
                filter: 'fa fa-filter',
                fire: 'fa fa-fire',
                fire_extinguisher: 'fa fa-fire-extinguisher',
                flag: 'fa fa-flag',
                flag_checkered: 'fa fa-flag-checkered',
                flag_o: 'fa fa-flag-o',
                flash: 'fa fa-flash',
                flask: 'fa fa-flask',
                folder: 'fa fa-folder',
                folder_o: 'fa fa-folder-o',
                folder_open: 'fa fa-folder-open',
                folder_open_o: 'fa fa-folder-open-o',
                frown_o: 'fa fa-frown-o',
                gamepad: 'fa fa-gamepad',
                gavel: 'fa fa-gavel',
                gear: 'fa fa-gear',
                gears: 'fa fa-gears',
                gift: 'fa fa-gift',
                glass: 'fa fa-glass',
                globe: 'fa fa-globe',
                graduation_cap: 'fa fa-graduation-cap',
                group: 'fa fa-group',
                hdd_o: 'fa fa-hdd-o',
                headphones: 'fa fa-headphones',
                heart: 'fa fa-heart',
                heart_o: 'fa fa-heart-o',
                history: 'fa fa-history',
                home: 'fa fa-home',
                image: 'fa fa-image',
                inbox: 'fa fa-inbox',
                info: 'fa fa-info',
                info_circle: 'fa fa-info-circle',
                institution: 'fa fa-institution',
                key: 'fa fa-key',
                keyboard_o: 'fa fa-keyboard-o',
                language: 'fa fa-language',
                laptop: 'fa fa-laptop',
                leaf: 'fa fa-leaf',
                legal: 'fa fa-legal',
                lemon_o: 'fa fa-lemon-o',
                level_down: 'fa fa-level-down',
                level_up: 'fa fa-level-up',
                life_bouy: 'fa fa-life-bouy',
                life_ring: 'fa fa-life-ring',
                life_saver: 'fa fa-life-saver',
                lightbulb_o: 'fa fa-lightbulb-o',
                location_arrow: 'fa fa-location-arrow',
                lock: 'fa fa-lock',
                magic: 'fa fa-magic',
                magnet: 'fa fa-magnet',
                mail_forward: 'fa fa-mail-forward',
                mail_reply: 'fa fa-mail-reply',
                mail_reply_all: 'fa fa-mail-reply-all',
                male: 'fa fa-male',
                map_marker: 'fa fa-map-marker',
                meh_o: 'fa fa-meh-o',
                microphone: 'fa fa-microphone',
                microphone_slash: 'fa fa-microphone-slash',
                minus: 'fa fa-minus',
                minus_circle: 'fa fa-minus-circle',
                minus_square: 'fa fa-minus-square',
                minus_square_o: 'fa fa-minus-square-o',
                mobile: 'fa fa-mobile',
                mobile_phone: 'fa fa-mobile-phone',
                money: 'fa fa-money',
                moon_o: 'fa fa-moon-o',
                mortar_board: 'fa fa-mortar-board',
                music: 'fa fa-music',
                navicon: 'fa fa-navicon',
                paper_plane: 'fa fa-paper-plane',
                paper_plane_o: 'fa fa-paper-plane-o',
                paw: 'fa fa-paw',
                pencil: 'fa fa-pencil',
                pencil_square: 'fa fa-pencil-square',
                pencil_square_o: 'fa fa-pencil-square-o',
                phone: 'fa fa-phone',
                phone_square: 'fa fa-phone-square',
                photo: 'fa fa-photo',
                picture_o: 'fa fa-picture-o',
                plane: 'fa fa-plane',
                plus: 'fa fa-plus',
                plus_circle: 'fa fa-plus-circle',
                plus_square: 'fa fa-plus-square',
                plus_square_o: 'fa fa-plus-square-o',
                power_off: 'fa fa-power-off',
                print: 'fa fa-print',
                puzzle_piece: 'fa fa-puzzle-piece',
                qrcode: 'fa fa-qrcode',
                question: 'fa fa-question',
                question_circle: 'fa fa-question-circle',
                quote_left: 'fa fa-quote-left',
                quote_right: 'fa fa-quote-right',
                random: 'fa fa-random',
                recycle: 'fa fa-recycle',
                refresh: 'fa fa-refresh',
                reorder: 'fa fa-reorder',
                reply: 'fa fa-reply',
                reply_all: 'fa fa-reply-all',
                retweet: 'fa fa-retweet',
                road: 'fa fa-road',
                rocket: 'fa fa-rocket',
                rss: 'fa fa-rss',
                rss_square: 'fa fa-rss-square',
                search: 'fa fa-search',
                search_minus: 'fa fa-search-minus',
                search_plus: 'fa fa-search-plus',
                send: 'fa fa-send',
                send_o: 'fa fa-send-o',
                share: 'fa fa-share',
                share_alt: 'fa fa-share-alt',
                share_alt_square: 'fa fa-share-alt-square',
                share_square: 'fa fa-share-square',
                share_square_o: 'fa fa-share-square-o',
                shield: 'fa fa-shield',
                shopping_cart: 'fa fa-shopping-cart',
                sign_in: 'fa fa-sign-in',
                sign_out: 'fa fa-sign-out',
                signal: 'fa fa-signal',
                sitemap: 'fa fa-sitemap',
                sliders: 'fa fa-sliders',
                smile_o: 'fa fa-smile-o',
                sort: 'fa fa-sort',
                sort_alpha_asc: 'fa fa-sort-alpha-asc',
                sort_alpha_desc: 'fa fa-sort-alpha-desc',
                sort_amount_asc: 'fa fa-sort-amount-asc',
                sort_amount_desc: 'fa fa-sort-amount-desc',
                sort_asc: 'fa fa-sort-asc',
                sort_desc: 'fa fa-sort-desc',
                sort_down: 'fa fa-sort-down',
                sort_numeric_asc: 'fa fa-sort-numeric-asc',
                sort_numeric_desc: 'fa fa-sort-numeric-desc',
                sort_up: 'fa fa-sort-up',
                space_shuttle: 'fa fa-space-shuttle',
                spinner: 'fa fa-spinner',
                spoon: 'fa fa-spoon',
                square: 'fa fa-square',
                square_o: 'fa fa-square-o',
                star: 'fa fa-star',
                star_half: 'fa fa-star-half',
                star_half_empty: 'fa fa-star-half-empty',
                star_half_full: 'fa fa-star-half-full',
                star_half_o: 'fa fa-star-half-o',
                star_o: 'fa fa-star-o',
                suitcase: 'fa fa-suitcase',
                sun_o: 'fa fa-sun-o',
                support: 'fa fa-support',
                tablet: 'fa fa-tablet',
                tachometer: 'fa fa-tachometer',
                tag: 'fa fa-tag',
                tags: 'fa fa-tags',
                tasks: 'fa fa-tasks',
                taxi: 'fa fa-taxi',
                terminal: 'fa fa-terminal',
                thumb_tack: 'fa fa-thumb-tack',
                thumbs_down: 'fa fa-thumbs-down',
                thumbs_o_down: 'fa fa-thumbs-o-down',
                thumbs_o_up: 'fa fa-thumbs-o-up',
                thumbs_up: 'fa fa-thumbs-up',
                ticket: 'fa fa-ticket',
                times: 'fa fa-times',
                times_circle: 'fa fa-times-circle',
                times_circle_o: 'fa fa-times-circle-o',
                tint: 'fa fa-tint',
                toggle_down: 'fa fa-toggle-down',
                toggle_left: 'fa fa-toggle-left',
                toggle_right: 'fa fa-toggle-right',
                toggle_up: 'fa fa-toggle-up',
                trash_o: 'fa fa-trash-o',
                tree: 'fa fa-tree',
                trophy: 'fa fa-trophy',
                truck: 'fa fa-truck',
                umbrella: 'fa fa-umbrella',
                university: 'fa fa-university',
                unlock: 'fa fa-unlock',
                unlock_alt: 'fa fa-unlock-alt',
                unsorted: 'fa fa-unsorted',
                upload: 'fa fa-upload',
                user: 'fa fa-user',
                users: 'fa fa-users',
                video_camera: 'fa fa-video-camera',
                volume_down: 'fa fa-volume-down',
                volume_off: 'fa fa-volume-off',
                volume_up: 'fa fa-volume-up',
                warning: 'fa fa-warning',
                wheelchair: 'fa fa-wheelchair',
                wrench: 'fa fa-wrench',
                file: 'fa fa-file',
                file_archive_o: 'fa fa-file-archive-o',
                file_audio_o: 'fa fa-file-audio-o',
                file_code_o: 'fa fa-file-code-o',
                file_excel_o: 'fa fa-file-excel-o',
                file_image_o: 'fa fa-file-image-o',
                file_movie_o: 'fa fa-file-movie-o',
                file_o: 'fa fa-file-o',
                file_pdf_o: 'fa fa-file-pdf-o',
                file_photo_o: 'fa fa-file-photo-o',
                file_picture_o: 'fa fa-file-picture-o',
                file_powerpoint_o: 'fa fa-file-powerpoint-o',
                file_sound_o: 'fa fa-file-sound-o',
                file_text: 'fa fa-file-text',
                file_text_o: 'fa fa-file-text-o',
                file_video_o: 'fa fa-file-video-o',
                file_word_o: 'fa fa-file-word-o',
                file_zip_o: 'fa fa-file-zip-o',
                circle_o_notch: 'fa fa-circle-o-notch',
                cog: 'fa fa-cog',
                gear: 'fa fa-gear',
                refresh: 'fa fa-refresh',
                spinner: 'fa fa-spinner',
                check_square: 'fa fa-check-square',
                check_square_o: 'fa fa-check-square-o',
                circle: 'fa fa-circle',
                circle_o: 'fa fa-circle-o',
                dot_circle_o: 'fa fa-dot-circle-o',
                minus_square: 'fa fa-minus-square',
                minus_square_o: 'fa fa-minus-square-o',
                plus_square: 'fa fa-plus-square',
                plus_square_o: 'fa fa-plus-square-o',
                square: 'fa fa-square',
                square_o: 'fa fa-square-o',
                bitcoin: 'fa fa-bitcoin',
                btc: 'fa fa-btc',
                cny: 'fa fa-cny',
                dollar: 'fa fa-dollar',
                eur: 'fa fa-eur',
                euro: 'fa fa-euro',
                gbp: 'fa fa-gbp',
                inr: 'fa fa-inr',
                jpy: 'fa fa-jpy',
                krw: 'fa fa-krw',
                money: 'fa fa-money',
                rmb: 'fa fa-rmb',
                rouble: 'fa fa-rouble',
                rub: 'fa fa-rub',
                ruble: 'fa fa-ruble',
                rupee: 'fa fa-rupee',
                attempt: 'fa fa-try',
                turkish_lira: 'fa fa-turkish-lira',
                usd: 'fa fa-usd',
                won: 'fa fa-won',
                yen: 'fa fa-yen',
                align_center: 'fa fa-align-center',
                align_justify: 'fa fa-align-justify',
                align_left: 'fa fa-align-left',
                align_right: 'fa fa-align-right',
                bold: 'fa fa-bold',
                chain: 'fa fa-chain',
                chain_broken: 'fa fa-chain-broken',
                clipboard: 'fa fa-clipboard',
                columns: 'fa fa-columns',
                copy: 'fa fa-copy',
                cut: 'fa fa-cut',
                dedent: 'fa fa-dedent',
                eraser: 'fa fa-eraser',
                file: 'fa fa-file',
                file_o: 'fa fa-file-o',
                file_text: 'fa fa-file-text',
                file_text_o: 'fa fa-file-text-o',
                files_o: 'fa fa-files-o',
                floppy_o: 'fa fa-floppy-o',
                font: 'fa fa-font',
                header: 'fa fa-header',
                indent: 'fa fa-indent',
                italic: 'fa fa-italic',
                link: 'fa fa-link',
                list: 'fa fa-list',
                list_alt: 'fa fa-list-alt',
                list_ol: 'fa fa-list-ol',
                list_ul: 'fa fa-list-ul',
                outdent: 'fa fa-outdent',
                paperclip: 'fa fa-paperclip',
                paragraph: 'fa fa-paragraph',
                paste: 'fa fa-paste',
                repeat: 'fa fa-repeat',
                rotate_left: 'fa fa-rotate-left',
                rotate_right: 'fa fa-rotate-right',
                save: 'fa fa-save',
                scissors: 'fa fa-scissors',
                strikethrough: 'fa fa-strikethrough',
                subscript: 'fa fa-subscript',
                superscript: 'fa fa-superscript',
                table: 'fa fa-table',
                text_height: 'fa fa-text-height',
                text_width: 'fa fa-text-width',
                th: 'fa fa-th',
                th_large: 'fa fa-th-large',
                th_list: 'fa fa-th-list',
                underline: 'fa fa-underline',
                undo: 'fa fa-undo',
                unlink: 'fa fa-unlink',
                angle_double_down: 'fa fa-angle-double-down',
                angle_double_left: 'fa fa-angle-double-left',
                angle_double_right: 'fa fa-angle-double-right',
                angle_double_up: 'fa fa-angle-double-up',
                angle_down: 'fa fa-angle-down',
                angle_left: 'fa fa-angle-left',
                angle_right: 'fa fa-angle-right',
                angle_up: 'fa fa-angle-up',
                arrow_circle_down: 'fa fa-arrow-circle-down',
                arrow_circle_left: 'fa fa-arrow-circle-left',
                arrow_circle_o_down: 'fa fa-arrow-circle-o-down',
                arrow_circle_o_left: 'fa fa-arrow-circle-o-left',
                arrow_circle_o_right: 'fa fa-arrow-circle-o-right',
                arrow_circle_o_up: 'fa fa-arrow-circle-o-up',
                arrow_circle_right: 'fa fa-arrow-circle-right',
                arrow_circle_up: 'fa fa-arrow-circle-up',
                arrow_down: 'fa fa-arrow-down',
                arrow_left: 'fa fa-arrow-left',
                arrow_right: 'fa fa-arrow-right',
                arrow_up: 'fa fa-arrow-up',
                arrows: 'fa fa-arrows',
                arrows_alt: 'fa fa-arrows-alt',
                arrows_h: 'fa fa-arrows-h',
                arrows_v: 'fa fa-arrows-v',
                caret_down: 'fa fa-caret-down',
                caret_left: 'fa fa-caret-left',
                caret_right: 'fa fa-caret-right',
                caret_square_o_down: 'fa fa-caret-square-o-down',
                caret_square_o_left: 'fa fa-caret-square-o-left',
                caret_square_o_right: 'fa fa-caret-square-o-right',
                caret_square_o_up: 'fa fa-caret-square-o-up',
                caret_up: 'fa fa-caret-up',
                chevron_circle_down: 'fa fa-chevron-circle-down',
                chevron_circle_left: 'fa fa-chevron-circle-left',
                chevron_circle_right: 'fa fa-chevron-circle-right',
                chevron_circle_up: 'fa fa-chevron-circle-up',
                chevron_down: 'fa fa-chevron-down',
                chevron_left: 'fa fa-chevron-left',
                chevron_right: 'fa fa-chevron-right',
                chevron_up: 'fa fa-chevron-up',
                hand_o_down: 'fa fa-hand-o-down',
                hand_o_left: 'fa fa-hand-o-left',
                hand_o_right: 'fa fa-hand-o-right',
                hand_o_up: 'fa fa-hand-o-up',
                long_arrow_down: 'fa fa-long-arrow-down',
                long_arrow_left: 'fa fa-long-arrow-left',
                long_arrow_right: 'fa fa-long-arrow-right',
                long_arrow_up: 'fa fa-long-arrow-up',
                toggle_down: 'fa fa-toggle-down',
                toggle_left: 'fa fa-toggle-left',
                toggle_right: 'fa fa-toggle-right',
                toggle_up: 'fa fa-toggle-up',
                arrows_alt: 'fa fa-arrows-alt',
                backward: 'fa fa-backward',
                compress: 'fa fa-compress',
                eject: 'fa fa-eject',
                expand: 'fa fa-expand',
                fast_backward: 'fa fa-fast-backward',
                fast_forward: 'fa fa-fast-forward',
                forward: 'fa fa-forward',
                pause: 'fa fa-pause',
                play: 'fa fa-play',
                play_circle: 'fa fa-play-circle',
                play_circle_o: 'fa fa-play-circle-o',
                step_backward: 'fa fa-step-backward',
                step_forward: 'fa fa-step-forward',
                stop: 'fa fa-stop',
                youtube_play: 'fa fa-youtube-play',
                adn: 'fa fa-adn',
                android: 'fa fa-android',
                apple: 'fa fa-apple',
                behance: 'fa fa-behance',
                behance_square: 'fa fa-behance-square',
                bitbucket: 'fa fa-bitbucket',
                bitbucket_square: 'fa fa-bitbucket-square',
                bitcoin: 'fa fa-bitcoin',
                btc: 'fa fa-btc',
                codepen: 'fa fa-codepen',
                css3: 'fa fa-css3',
                delicious: 'fa fa-delicious',
                deviantart: 'fa fa-deviantart',
                digg: 'fa fa-digg',
                dribbble: 'fa fa-dribbble',
                dropbox: 'fa fa-dropbox',
                drupal: 'fa fa-drupal',
                empire: 'fa fa-empire',
                facebook: 'fa fa-facebook',
                facebook_square: 'fa fa-facebook-square',
                flickr: 'fa fa-flickr',
                foursquare: 'fa fa-foursquare',
                ge: 'fa fa-ge',
                git: 'fa fa-git',
                git_square: 'fa fa-git-square',
                github: 'fa fa-github',
                github_alt: 'fa fa-github-alt',
                github_square: 'fa fa-github-square',
                gittip: 'fa fa-gittip',
                google: 'fa fa-google',
                google_plus: 'fa fa-google-plus',
                google_plus_square: 'fa fa-google-plus-square',
                hacker_news: 'fa fa-hacker-news',
                html5: 'fa fa-html5',
                instagram: 'fa fa-instagram',
                joomla: 'fa fa-joomla',
                jsfiddle: 'fa fa-jsfiddle',
                linkedin: 'fa fa-linkedin',
                linkedin_square: 'fa fa-linkedin-square',
                linux: 'fa fa-linux',
                maxcdn: 'fa fa-maxcdn',
                openid: 'fa fa-openid',
                pagelines: 'fa fa-pagelines',
                pied_piper: 'fa fa-pied-piper',
                pied_piper_alt: 'fa fa-pied-piper-alt',
                pied_piper_square: 'fa fa-pied-piper-square',
                pinterest: 'fa fa-pinterest',
                pinterest_square: 'fa fa-pinterest-square',
                qq: 'fa fa-qq',
                ra: 'fa fa-ra',
                rebel: 'fa fa-rebel',
                reddit: 'fa fa-reddit',
                reddit_square: 'fa fa-reddit-square',
                renren: 'fa fa-renren',
                share_alt: 'fa fa-share-alt',
                share_alt_square: 'fa fa-share-alt-square',
                skype: 'fa fa-skype',
                slack: 'fa fa-slack',
                soundcloud: 'fa fa-soundcloud',
                spotify: 'fa fa-spotify',
                stack_exchange: 'fa fa-stack-exchange',
                stack_overflow: 'fa fa-stack-overflow',
                steam: 'fa fa-steam',
                steam_square: 'fa fa-steam-square',
                stumbleupon: 'fa fa-stumbleupon',
                stumbleupon_circle: 'fa fa-stumbleupon-circle',
                tencent_weibo: 'fa fa-tencent-weibo',
                trello: 'fa fa-trello',
                tumblr: 'fa fa-tumblr',
                tumblr_square: 'fa fa-tumblr-square',
                twitter: 'fa fa-twitter',
                twitter_square: 'fa fa-twitter-square',
                vimeo_square: 'fa fa-vimeo-square',
                vine: 'fa fa-vine',
                vk: 'fa fa-vk',
                wechat: 'fa fa-wechat',
                weibo: 'fa fa-weibo',
                weixin: 'fa fa-weixin',
                windows: 'fa fa-windows',
                wordpress: 'fa fa-wordpress',
                xing: 'fa fa-xing',
                xing_square: 'fa fa-xing-square',
                yahoo: 'fa fa-yahoo',
                youtube: 'fa fa-youtube',
                youtube_play: 'fa fa-youtube-play',
                youtube_square: 'fa fa-youtube-square',
                ambulance: 'fa fa-ambulance',
                h_square: 'fa fa-h-square',
                hospital_o: 'fa fa-hospital-o',
                medkit: 'fa fa-medkit',
                plus_square: 'fa fa-plus-square',
                stethoscope: 'fa fa-stethoscope',
                user_md: 'fa fa-user-md',
                wheelchair: 'fa fa-wheelchair',
            },
        }
    },

    modal: {
        modes: {
            normal: 'normal',
            confirm: 'confirm'
        },
        size: {
            small: 'modal-small',
            large: 'modal-lg',
            full: 'modal-full',
            long: 'modal-long'
        },
        create: function (mode, title, content, confirm, close, wrap, size, bound) {
            //byte-File.log('byteFile.modal.create() > Modal created for "' + title + '" in "' + mode + '" mode...');

            if (size) {
                jQuery('#modal_size').removeClass(byteFile.modal.size.small);
                jQuery('#modal_size').removeClass(byteFile.modal.size.small);
                jQuery('#modal_size').removeClass(byteFile.modal.size.full);

                jQuery('#modal_size').addClass(size);
            }

            jQuery('#modal_confirm').hide();
            jQuery('#modal_default_body').html('');

            jQuery('#modal_default_title').html(title);

            if (wrap) {
                jQuery('#modal_default_body').append(jQuery('#' + content));
            }
            else {
                jQuery('#modal_default_body').html(content);
            }

            //Removes any previous bindings so duplicate effects don't occur
            jQuery('#modal_confirm').unbind('click');
            jQuery('#modal_close').unbind('click');

            if (mode == byteFile.modal.modes.confirm) {
                if (confirm) {
                    jQuery('#modal_confirm').click(function (e) {
                        //byte-File.log('byteFile.modal.create() > User clicked CONFIRM button of modal "' + title + '"...');
                        var result = confirm(jQuery('#modal_default_body'));

                        if (typeof result != 'undefined') {
                            if (!result) {
                                jQuery('#modal_confirm').attr('data-dismiss', '');
                            }
                            else {
                                jQuery('#modal_confirm').attr('data-dismiss', 'modal');
                            }
                        }
                    });
                }

                jQuery('#modal_confirm').show();
            }

            if (close) {
                jQuery('#modal_close').click(function () {
                    //byte-File.log('byteFile.modal.create() > User clicked CLOSE button of modal "' + title + '"...');
                    close(jQuery('#modal_default_body'));


                });
            }

            if (bound) {
                //byte-File.log('byteFile.modal.create() > Control fired bound event of modal "' + title + '"...');
                bound(jQuery('#modal_default_body'));
                jQuery('#labelname').detach();

            }

            jQuery('#modal_default').modal();
        },

        winform: function (mode, title, content, contentholder, confirm, close, wrap, size, bound) {
            //byte-File.log('byteFile.modal.create() > Modal created for "' + title + '" in "' + mode + '" mode...');

            var myWindow = jQuery(content);

            myWindow.kendoWindow({
                width: "600px",
                title: title,
                visible: false,
                modal: true,
                open: function (e) {
                    this.wrapper.css({ top: 100 });
                },
                animation: {
                    close: {
                        effects: "fade:out"
                    },
                    open: {
                        effects: "fade:in"
                    }
                },
                appendTo: contentholder //"#modal_default"
            }).data("kendoWindow").center().open();

            jQuery(myWindow).find('#modal_confirm').unbind('click');
            jQuery(myWindow).find('#modal_close').unbind('click');

            jQuery(myWindow).find('#remind_me_later').kendoDateTimePicker({
                value: new Date()
            });
            function onClose() {
                undo.fadeIn();
            }

            jQuery(myWindow).find('#modal_confirm').click(function (e) {
                //byte-File.log('byteFile.modal.create() > User clicked CONFIRM button of modal "' + title + '"...');

                var result = confirm(jQuery('#modal_default_body'));

                if (typeof result != 'undefined') {
                    if (!result) {
                        jQuery('#modal_confirm').attr('data-dismiss', '');
                    }
                    else {
                        jQuery('#modal_confirm').attr('data-dismiss', 'modal');
                    }
                }
                jQuery(this).closest("[data-role=window]").data("kendoWindow").close();
            });
          

            jQuery(myWindow).find('#modal_close').click(function () {
                //byte-File.log('byteFile.modal.create() > User clicked CLOSE button of modal "' + title + '"...');
                jQuery(this).closest("[data-role=window]").data("kendoWindow").close();
            });
        },

        bodyform: function (mode, title, content, confirm, close, wrap, size, bound) {
            //byte-File.log('byteFile.modal.create() > Modal created for "' + title + '" in "' + mode + '" mode...');

            var myWindow = jQuery(content);

            myWindow.kendoWindow({
                width: "600px",
                title: title,
                visible: false,
                modal: true,
                open: function (e) {
                    this.wrapper.css({ top: 100 });
                },
                animation: {
                    close: {
                        effects: "fade:out"
                    },
                    open: {
                        effects: "fade:in"
                    }
                },
                appendTo: "body"
                //actions: [
                ////    "Pin",
                ////    "Minimize",
                ////    "Maximize",
                //    "Close"
                //],
                //close: onClose
            }).data("kendoWindow").center().open();

            //Removes any previous bindings so duplicate effects don't occur
            jQuery(myWindow).find('#modal_confirm').unbind('click');
            jQuery(myWindow).find('#modal_close').unbind('click');

            jQuery(myWindow).find('#remind_me_later').kendoDateTimePicker({
                value: new Date()
            });
            function onClose() {
                undo.fadeIn();
            }

            //if (size) {
            //    jQuery('#modal_size').removeClass(byteFile.modal.size.small);
            //    jQuery('#modal_size').removeClass(byteFile.modal.size.small);
            //    jQuery('#modal_size').removeClass(byteFile.modal.size.full);

            //    jQuery('#modal_size').addClass(size);
            //}

            ////jQuery('#modal_confirm').hide();
            ////jQuery('#modal_default_body').html('');

            //jQuery('#modal_default_title').html(title);

            //if (wrap) {
            //    jQuery('#modal_default_body').append(jQuery('#' + content));
            //}
            //else {
            //    jQuery('#modal_default_body').html(content);
            //}

            //if (mode == byteFile.modal.modes.confirm) {
            //    if (confirm) {
            jQuery(myWindow).find('#modal_confirm').click(function (e) {
                //byte-File.log('byteFile.modal.create() > User clicked CONFIRM button of modal "' + title + '"...');

                var result = confirm(jQuery('#modal_default_body'));

                if (typeof result != 'undefined') {
                    if (!result) {
                        jQuery('#modal_confirm').attr('data-dismiss', '');
                    }
                    else {
                        jQuery('#modal_confirm').attr('data-dismiss', 'modal');
                    }
                }
                jQuery(this).closest("[data-role=window]").data("kendoWindow").close();
            });
            //    }

            //    jQuery('#modal_confirm').show();
            //}

            //if (close) {
            jQuery(myWindow).find('#modal_close').click(function () {
                //byte-File.log('byteFile.modal.create() > User clicked CLOSE button of modal "' + title + '"...');
                // close(jQuery('#modal_default_body'));
                // myWindow.fadeOut();
                jQuery(this).closest("[data-role=window]").data("kendoWindow").close();
            });
            //}

            //if (bound) {
            //    //byte-File.log('byteFile.modal.create() > Control fired bound event of modal "' + title + '"...');
            //    bound(jQuery('#modal_default_body'));
            //    jQuery('#labelname').detach();

            //}

            //jQuery('#modal_default').
            //    myWindow.fadeIn();
        },


    },

    toast: {
        modes: {
            info: 'info',
            warning: 'warning',
            success: 'success',
            error: 'error'
        },
        positions: {
            top_left: 'toast-top-left',
            top_center: 'toast-top-center',
            top_right: 'toast-top-right',
            bottom_left: 'toast-bottom-left',
            bottom_center: 'toast-bottom-center',
            bottom_right: 'toast-bottom-right',
        },
        create: function (mode, title, message, position) {
            try {
                if (position && position.length > 0) {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": position,
                        "onclick": null,
                        "showDuration": "3000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "show",
                        "hideMethod": "fadeOut"
                    }
                }
                else {
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "1000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "show",
                        "hideMethod": "fadeOut"
                    }
                }

                switch (mode) {
                    case byteFile.toast.modes.info:
                        toastr.info(message, title);
                        byteFile.sounds.play('info');
                        break;
                    case byteFile.toast.modes.warning:
                        byteFile.sounds.play('warning');
                        toastr.warning(message, title);
                        break;
                    case byteFile.toast.modes.success:
                        byteFile.sounds.play('success');
                        toastr.success(message, title);
                        break;
                    case byteFile.toast.modes.error:
                        if (message.indexOf('tick()') <= 0) {
                            byteFile.sounds.play('error');;
                            toastr.error(message, title);
                        }
                        break;
                    default:
                        toastr.info(message, title);
                        break;
                }
            }
            catch (e) {
                //byteFile.error('FRAMEWORK :: byteFile.toast.create() > Error: ' + e.message + '...');
            }
        }
    },
    
    filter: jlinq,
    
    script: function (path, func) {
        try {
            jQuery.getScript(path, function (data, textStatus, jqxhr) { //Main Language File
                try {
                    //byte-File.log('byteFile.script() > Loaded script file: ' + path + ', Status: ' + textStatus);
                    ////byte-File.log(data);

                    if (func) {
                        ////byte-File.log('byteFile.script() > Load function defined, firing!');
                        func(data);
                    }
                }
                catch (e) {
                    byteFile.error('FRAMEWORK :: byteFile.script() Load Error: ' + e.message);
                }

            })
            .fail(function (jqxhr, settings, exception) {
                alert(exception + ' - ' + exception.message);
                byteFile.error('FRAMEWORK :: byteFile.script() > Script error in file: "' + path + '": ' + exception + ' - ' + exception.message);
            });
        }
        catch (e) {
            byteFile.error('FRAMEWORK :: byteFile.script() > Script error in file: "' + path + '": ' + e.message);
        }
    },

    get: function (path, func) {
        jQuery('<div/>').load(path + '?' + byteFile.noCache(), function (data) {
            if (func) {
                func(data);
            }
        });
    },
    
}