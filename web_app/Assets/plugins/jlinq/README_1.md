# jlinq
## WHAT: 
A node-usable version of https://github.com/hugoware/jlinq-beta
If you can do it in LINQ, you *should* be able to do it in jlinq

Language Integrated Query (LINQ, pronounced "link") is a Microsoft 
.NET Framework component that adds native data querying capabilities
to .NET languages.  LINQ defines a set of method names (called standard 
query operators, or standard sequence operators), along with translation
rules from so-called query expressions to expressions using these method 
names, lambda expressions and anonymous types. These can, for example, 
be used to project and filter data into arrays, enumerable classes, XML 
(LINQ to XML), relational databases, and third party data sources. 

## WHO:
* Original Author: Hugo Bonacci - hugoware.com

* Modified for Node by: Nik Martin - nikmartin.com

It should work fine, report bugs on github.

