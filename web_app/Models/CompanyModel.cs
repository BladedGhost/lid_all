﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app.Models
{
    public class CompanyModel
    {
        public long company_id { get; set; }
        public long group_id { get; set; }
        public long company_type_id { get; set; }
        public string company_name { get; set; }
        public string company_reg { get; set; }
        public string company_trading { get; set; }
        public string company_email { get; set; }
        public string company_tel { get; set; }
        public string company_fax { get; set; }
        public string company_street { get; set; }
        public string company_building { get; set; }
        public string company_city { get; set; }
        public string company_pcode { get; set; }
        public string company_pobox { get; set; }
        public string company_pooffice { get; set; }
        public string company_pocity { get; set; }
        public string company_pocode { get; set; }
        public byte[] company_logo { get; set; }
        public string imgDataURL { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
    public class SiteModel
    {
        public long site_id { get; set; }
        public string site_name { get; set; }
    }
    public class CompanyInspector
    {
        public long inspector_id { get; set; }
        public long company_id { get; set; }
        public long certificate_id { get; set; }
        public string certification { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string idnumber { get; set; }
        public string registerno { get; set; }
        public byte[] signature_img { get; set; }
        public string imgDataURL { get; set; }
        public bool isactive { get; set; }
    }

    public class Tags
    {
        public long tag_id { get; set; }
        public long site_id { get; set; }
        public long item_id { get; set; }
        public string sn_no { get; set; }
        public string tag_barcode { get; set; }
        public string company_name { get; set; }
        public string tag_description { get; set; }
        public string site_description { get; set; }
        public string site_name { get; set; }
        public string barcode { get; set; }
        public string category_name { get; set; }
        public string asset_name { get; set; }
        public string asset_description { get; set; }
        public string item_name { get; set; }
        public string insp_cycle { get; set; }
        public string test_cycle { get; set; }
        public string certification { get; set; }
        public string last_inspection_date { get; set; }
        public string next_inspection_date { get; set; }
        public string size_ton { get; set; }
        public string size_meter { get; set; }
        public string size_mm { get; set; }
        public string std_code { get; set; }
        public string visual { get; set; }
        public string loadtest { get; set; }
        public string scrap { get; set; }
        public string recondition { get; set; }
        public string insp_comments { get; set; }
        public string insp_status { get; set; }
        public string inspe_photo { get; set; }
        public string insp_lat { get; set; }
        public string insp_lon { get; set; }
        public string insp_fullname { get; set; }
        public string insp_certificate { get; set; }
        public string insp_company { get; set; }
        public string scanin { get; set; }
        public string scanout { get; set; }
        public string linkatag { get; set; }
        public string handovertag { get; set; }
        public string tag_latitude { get; set; }
        public string tag_longitude { get; set; }
        public string man_name { get; set; }
    }

    public class CompanySites
    {
        public long site_id { get; set; }
        public string site_name { get; set; }
        public string site_description { get; set; }
        public string contact_person { get; set; }
        public string contact_email { get; set; }
        public string contact_tel { get; set; }
        public string address_street { get; set; }
        public string address_building { get; set; }
        public string address_city { get; set; }
        public string address_pcode { get; set; }
        public string company_id { get; set; }
        public string tag_count { get; set; }
    }
    public class CompanyAsset
    {
        public string std_code { get; set; }

        public long asset_id { get; set; }
        public string category_id { get; set; }
        public string item_id { get; set; }
        public long cert_id { get; set; }
        public long test_id { get; set; }
        public long insp_id { get; set; }
        public long ass_id { get; set; }
        public long man_id { get; set; }
        public string asset_name { get; set; }
        public string asset_description { get; set; }
        public string barcode { get; set; }
        public long inspection_cycle_id { get; set; }
        public long load_test_cycle_id { get; set; }
        public string category_name { get; set; }
        public string item_name { get; set; }
        public string site_name { get; set; }
        public string test_cycle { get; set; }
        public string insp_cycle { get; set; }
        public string certification { get; set; }
        public string company_name { get; set; }
        public string size_meter { get; set; }
        public string size_ton { get; set; }
        public string size_mm { get; set; }
        public string imgDataURL { get; set; }
        public byte[] image { get; set; }
        public bool ops_times { get; set; }
        public TimeSpan time_from { get; set; }
        public TimeSpan time_to { get; set; }
    }
}