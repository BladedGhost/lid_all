﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app.Models
{
    public class HomeModel
    {
    }

    public partial class AppointmentDiary
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int SomeImportantKey { get; set; }
        public System.DateTime DateTimeScheduled { get; set; }
        public int AppointmentLength { get; set; }
        public int StatusENUM { get; set; }
    }
    public partial class Events
    {
        public string event_id { get; set; }
        public string event_desc { get; set; }
        public bool? isActive { get; set; }
    }


    public class DashBoard
    {
        public string weekday { get; set; }


        public DashBoard()
        {
            this.list_sites = new List<CompanySites>();
            this.list_inspectors = new List<CompanyInspector>();
            this.list_inspection = new List<PassFailReportDTO>();
            this.list_upcoming = new List<MobiRecentInspections>();
        }

        public List<CompanySites> list_sites { get; set; }
        public List<CompanyInspector> list_inspectors { get; set; }
        public List<PassFailReportDTO> list_inspection { get; set; }
        public List<MobiRecentInspections> list_upcoming { get; set; }
    }
}