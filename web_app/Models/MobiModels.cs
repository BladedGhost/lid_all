﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app.Models
{

    public class MobiUser
    {
        public string inspector_id { get; set; }
        public string company_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
    public class MobiTag
    {
        public long category_id { get; set; }
        public long company_id { get; set; }
        public long item_id { get; set; }
        public long asset_id { get; set; }
        public long site_id { get; set; }
        public string serial_no { get; set; }
        public string asset_name { get; set; }
        public string asset_description { get; set; }
        public string image { get; set; }
        public string barcode { get; set; }
        public string tag_latitude { get; set; }
        public string tag_longitude { get; set; }
    }
    public class MobiSites
    {
        public long site_id { get; set; }
        public string site_name { get; set; }
        public int tag_count { get; set; }
    }
    public class MobiAssets
    {
        public long asset_id { get; set; }
        public string asset_name { get; set; }
    }
    public class MobiAsset
    {
        public long asset_id { get; set; }
        public long test_id { get; set; }
        public long item_id { get; set; }
        public string barcode { get; set; }
        public string test_description { get; set; }
        public string test_info { get; set; }
        public string discard { get; set; }
    }
    public class MobiRecentInspections
    {
        public string asset_name { get; set; }
        public string site_name { get; set; }
        public byte[] image { get; set; }
        public string imgDataURL { get; set; }
        public string inspection_date { get; set; }
        public string inspection_status { get; set; }
        public string comments { get; set; }
        public string fullname { get; set; }
        public string insp_status { get; set; }
        public string time_cycle { get; set; }
    }
    public class MobiInspection
    {
        public long inspector_id { get; set; }
        public long company_id { get; set; }
        public string barcode { get; set; }
        public string inspector { get; set; }
        public string comments { get; set; }
        public string status { get; set; }
        public string image { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public bool visual { get; set; }
        public bool loadtest { get; set; }
        public bool scrap { get; set; }
        public bool recondition { get; set; }

        public List<MobiInspectionItems> inspection_items { get; set; }

        public MobiInspection()
        {
            this.inspection_items = new List<MobiInspectionItems>();
        }
    }
    public class MobiInspectionItems
    {
        public long asset_id { get; set; }
        public long test_id { get; set; }
        public long item_id { get; set; }
        public string comments { get; set; }
        public string status { get; set; }
    }
}