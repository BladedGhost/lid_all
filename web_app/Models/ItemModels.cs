﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app.Models
{
    public class ItemModels
    {
    }

    public class ItemCategory
    {
        public long category_id { get; set; }
        public string category_name { get; set; }
        public long category_parent { get; set; }
    }
    public class TestModel
    {
        public long test_id { get; set; }
        public string test_description { get; set; }
        public string discard { get; set; }
        public string test_info { get; set; }
    }
    public class ItemCategorySelected
    {
        public bool selected { get; set; }
    }
    public class ToggleLinkModel
    {
        public long item_id { get; set; }
        public long test_id { get; set; }
    }
    public class ItemCategoryTree
    {
        public string category_id { get; set; }
        public string group_id { get; set; }
        public string id { get; set; }
        public string text { get; set; }
        public List<ItemCategoryTree> children { get; set; }
        public string icon { get; set; }
        public ItemCategorySelected state { get; set; }
        public string parent { get; set; }

        //public ItemCategoryTree()
        //{
        //    this.children = new List<ItemCategoryTree>();
        //}
    }

    public class Items
    {
        public long item_id { get; set; }
        public long parent_id { get; set; }
        public long links { get; set; }
        public string item_name { get; set; }
        public string std_code { get; set; }
        public string state { get; set; }
        public string icon { get; set; }
        public string imgDataURL { get; set; }
        public byte[] image { get; set; }
    }

    public class LinkTests
    {
        public string cat_id { get; set; }
        public string itm_id { get; set; }
        public string keyword { get; set; }
        public bool isadmin { get; set; }
        public List<ItemCategoryTree> cat_list { get; set; }
        public List<TestModel> tests_list { get; set; }
        public List<TestModel> linked_tests { get; set; }

        public LinkTests()
        {
            this.cat_list = new List<ItemCategoryTree>();
            this.tests_list = new List<TestModel>();
            this.linked_tests = new List<TestModel>();
        }
    }
}