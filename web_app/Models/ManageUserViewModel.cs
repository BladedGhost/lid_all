﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app.Models
{
    public class ManageUserViewModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}