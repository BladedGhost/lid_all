﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web_app.Models
{
    public class AdminModel
    {
    }

    public class ShiftManage
    {
        public SelectList ddl_shifts;

        public int week_inx { get; set; }
        public int dept_id { get; set; }
        public int pos_id { get; set; }
        public bool allowselectshift { get; set; }
        public bool allowadhoc { get; set; }
        public bool allowmultshifts { get; set; }

        public string keyword { get; set; }
        public string schedule_type { get; set; }
        public string schedule_id { get; set; }
        public string staff_id { get; set; }
        public string staff_fullname { get; set; }
        public string shift_name { get; set; }
        public string shift_period { get; set; }
        public string shift_id { get; set; }
        public string weekday { get; set; }
        public string photo { get; set; }

        public List<Staff> Staff_list { get; set; }
        public List<WeekDays> Week_days { get; set; }
        public List<Shift> shift_list { get; set; }
        public List<QuickShift> qshift_list { get; set; }
        public List<Position> position_list { get; set; }
        public List<ShiftComments> shiftcomments { get; set; }
        public List<Structure> levels { get; set; }
        public List<Dept> dept { get; set; }

        public List<StaffGroup> group_list { get; set; }
        public ShiftManage()
        {
            this.group_list = new List<StaffGroup>();
            this.levels = new List<Structure>();
            this.dept = new List<Dept>();
            this.Staff_list = new List<Staff>();
            this.Week_days = new List<WeekDays>();
            this.shift_list = new List<Shift>();
            this.qshift_list = new List<QuickShift>();
            this.position_list = new List<Position>();
            this.shiftcomments = new List<ShiftComments>();
        }

    }

    public class quickStaffStats
    {
        public string tot_hours { get; set; }
        public string worked_hours { get; set; }
        public string remaining_hours { get; set; }
        public string weekday { get; set; }
        public string dayname { get; set; }
        public DateTime day { get; set; }
        public string shift_name { get; set; }
        public string shift_from { get; set; }
        public string shift_to { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string worked { get; set; }
        public string staff_shortname { get; set; }
        public string position { get; set; }
        public string department { get; set; }
        public string staff_number { get; set; }
        public string status { get; set; }
        public long shift_id { get; set; }
        public long staff_id { get; set; }
        
    }

    public class StaffDetails
    {

        public string staff_id { get; set; }
        public string startday { get; set; }
        public string weekendday { get; set; }
        public Staff Staff { get; set; }
        public List<Shift> shift_list { get; set; }
        public StaffHours staff_hours { get; set; }

        public StaffDetails()
        {
            this.shift_list = new List<Shift>();
            this.staff_hours = new StaffHours();
        }
    }
    public class WeekDays
    {
        public string day_code { get; set; }
        public string day_name { get; set; }
    }
    public class StaffWeeklyStats
    {
        public List<quickStaffStats> quick_stats { get; set; }

        public StaffWeeklyStats()
        {
            this.quick_stats = new List<quickStaffStats>();
        }

        public string total_hrs { get; set; }
        public string remaining_hrs { get; set; }
        public string worked_hrs { get; set; }
        public string today_hrs { get; set; }

        public long staff_id { get; set; }
        public string staff_shortname { get; set; }
        public string lastdayname { get; set; }
        public string lastday { get; set; }
        public string firstday { get; set; }
        public string firstdayname { get; set; }
        public string workingdays { get; set; }
        public string workarea { get; set; }
    }
    public class ShiftComments
    {
        public string photo { get; set; }
        public string comm_id { get; set; }
        public string comm_by { get; set; }
        public string comm_date { get; set; }
        public string comm_desc { get; set; }
    }
    public class StaffHours
    {
        public string non_billable_hours { get; set; }
        public string tot_hours { get; set; }
        public string worked_hours { get; set; }
        public string remaining_hours { get; set; }
    }
    public class CodeTypes
    {
        public string code_id { get; set; }
        public string code_name { get; set; }
    }
    public class AjaxResults
    {
        public long id { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }
    public class SaveShiftResults
    {
        public string shift_id { get; set; }

        public string code { get; set; }
        public string message { get; set; }
        public string dayname { get; set; }
        public string staff_id { get; set; }
        public string dept_id { get; set; }
        public string startday { get; set; }
        public string dept_hours { get; set; }
        public string staff_hours { get; set; }
        public string schedule_type { get; set; }
        public string obj_id { get; set; }
    }
    public class ScatterChart
    {
        public string shift_from { get; set; }
        public string shift_to { get; set; }
    }
    public class ChartDataDTO
    {
        public string desc { get; set; }
        public string point { get; set; }
    }
    public class OrgStructure
    {
        public string id { get; set; }
        public string name { get; set; }
        public string parentId { get; set; }
        public string title { get; set; }
        public string phone { get; set; }
        public string mail { get; set; }
        public string image { get; set; }
        public string HasChildrens { get; set; }
    }
    public class Structure
    {
        public long structure_id { get; set; }
        public string parent_id { get; set; }
        public string structure_level { get; set; }
        public string structure_code { get; set; }
        public string structure_name { get; set; }
        public string staff_count { get; set; }
        public string min_hours { get; set; }
        public string max_hours { get; set; }
    }
    public class OrganisationStructure
    {
        public string id { get; set; }
        public string parentId { get; set; }
        public string title { get; set; }
        public string name { get; set; }
        public string min_hours { get; set; }
        public string max_hours { get; set; }
    }
    public class DashData
    {
        public string dept_code { get; set; }
        public string Mon { get; set; }
        public string Tue { get; set; }
        public string Wed { get; set; }
        public string Thu { get; set; }
        public string Fri { get; set; }
        public string Sat { get; set; }
        public string Sun { get; set; }
    }
    public class DashLabel
    {
        public string dept_code { get; set; }
    }
    public class StaffLite
    {
        public long staff_id { get; set; }
        public string staff_name { get; set; }
        public string staff_surname { get; set; }
        public string staff_number { get; set; }
        public string staff_fullname { get; set; }
        public string staff_desc { get; set; }
        public string department_id { get; set; }
        public string position_id { get; set; }
    }
    public class ShiftList
    {
        public string shift_id { get; set; }
        public string shift_name { get; set; }
    }
    public class AutoSchedulerData
    {
        public string Autoschedulerdata_id { get; set; }
        public string dept_id { get; set; }
        public string dept_name { get; set; }
        public string shift_id { get; set; }
        public string shift_name { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
        public string date_created { get; set; }
        public string created_by { get; set; }
        public int re_run { get; set; }
        public bool deleted { get; set; }
    }
    public class Staff
    {
        public string staff_id { get; set; }
        public long workarea_id { get; set; }
        public string staff_name { get; set; }
        public string staff_surname { get; set; }
        public string staff_number { get; set; }
        public string staff_shortname { get; set; }
        public string staff_longname { get; set; }
        public string staff_shortdept { get; set; }
        public string staff_longdept { get; set; }
        public string staff_idnumber { get; set; }
        public string age { get; set; }
        public string staff_mobile { get; set; }
        public string staff_telephone { get; set; }
        public string staff_telephonehome { get; set; }
        public string staff_email { get; set; }
        public string photo { get; set; }
        public System.DateTime emp_date { get; set; }
        public string min_hours { get; set; }
        public string max_hours { get; set; }
        public string dept_hours { get; set; }

        public string employee_type { get; set; }
        public string structure_level { get; set; }
        public string idtype { get; set; }
        public string country_of_issue { get; set; }
        public string gender { get; set; }
        public string race { get; set; }
        public string marital_status { get; set; }
        public string nationality { get; set; }
        public string qualification { get; set; }

        public string department_id { get; set; }
        public string position_id { get; set; }
        public string title_id { get; set; }
        public string emp_type_id { get; set; }
        public string department { get; set; }
        public string dept_code { get; set; }
        public string position { get; set; }
        public string title { get; set; }
        public string emp_type { get; set; }
        public string weekday { get; set; }
        public string curr_shift { get; set; }
        public long curr_shift_id { get; set; }
        public long schedule_id { get; set; }
        public string curr_shift_from { get; set; }
        public string curr_shift_to { get; set; }



            public long staff_title { get; set; }
            public System.DateTime date_created { get; set; }
            public bool isActive { get; set; }
            public bool isDeleted { get; set; }
            public long structure_id { get; set; }
            public string staff_initials { get; set; }
            public long idType_id { get; set; }
            public long counntyOfIssue_id { get; set; }
            public System.DateTime dob { get; set; }
            public long gender_id { get; set; }
            public long race_id { get; set; }
            public long maritalstatus_id { get; set; }
            public long nationality_id { get; set; }
            public long qualification_id { get; set; }
            public System.DateTime qualification_date { get; set; }
            public string staff_hometelephone { get; set; }
            public string staff_spousename { get; set; }
            public string staff_spousenumber { get; set; }
            public string staff_nextofkinname { get; set; }
            public string staff_nextofkinnumber { get; set; }
            public string staff_houseno { get; set; }
            public string staff_pobox { get; set; }
            public string staff_streetname { get; set; }
            public string staff_postoffice { get; set; }
            public string staff_addresssurburd { get; set; }
            public string staff_postsurburd { get; set; }
            public string staff_addresstown { get; set; }
            public string staff_posttown { get; set; }
            public string staff_addresscode { get; set; }
            public string staff_postcode { get; set; }
            public bool no_schedule { get; set; }
    }

    public class Title
    {
        public string title_id { get; set; }
        public string title_name { get; set; }
    }
    public class StaffWorkingDays
    {
        public long id { get; set; }
        public long staff_id { get; set; }
        public string weekday { get; set; }
        public bool is_active { get; set; }
    }
    public class StaffWeekDays
    {
        public bool mon { get; set; }
        public bool tue { get; set; }
        public bool wed { get; set; }
        public bool thu { get; set; }
        public bool fri { get; set; }
        public bool sat { get; set; }
        public bool sun { get; set; }
    }
    public class Dept
    {
        public string dept_id { get; set; }
        public string dept_code { get; set; }
        public string dept_name { get; set; }
    }
    public class Position
    {
        public string position_id { get; set; }
        public string position_code { get; set; }
        public string position_name { get; set; }
        public string min_hours { get; set; }
        public string max_hours { get; set; }
    }
    public class MonthWeek
    {
        public string week_no { get; set; }
        public string week_name { get; set; }
    }

    public class Experience
    {
        public string experience_id { get; set; }
        public string staff_id { get; set; }
        public string position { get; set; }
        public string insitution { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
        public string responsibilities { get; set; }
        public string skills { get; set; }
    }

    public class Qualification
    {
        public string qualification_id { get; set; }
        public string staff_id { get; set; }
        public string qualification_name { get; set; }
        public string insitution { get; set; }
        public string qualification_date { get; set; }
        public string description { get; set; }
    }
    public class Leave
    {
        public string leave_id { get; set; }
        public string staff_number { get; set; }
        public string employee_type { get; set; }
        public string staff_fullname { get; set; }
        public string staff_department { get; set; }
        public string staff_photo { get; set; }
        public string leave_type { get; set; }
        public bool leave_approved { get; set; }
        public DateTime leave_from { get; set; }
        public DateTime leave_to { get; set; }
        public string leave_days { get; set; }
        public DateTime datestamp { get; set; }
    }
    public class LeaveTypes
    {
        public string leavetype_id { get; set; }
        public string leavetype_code { get; set; }
        public string leavetype_name { get; set; }
    }
    public class WorkArea
    {
        public long id { get; set; }
        public long dept_id { get; set; }
        public string name { get; set; }
        public string structure_name { get; set; }
        public long noofstaff { get; set; }
    }
    public class ShiftTrade
    {
        public string trade_id { get; set; }
        public string staff_from { get; set; }
        public string staff_to { get; set; }
        public string shift_from { get; set; }
        public string shift_to { get; set; }
        public DateTime weekday_from { get; set; }
        public DateTime weekday_to { get; set; }
        public string trade_reason { get; set; }
        public bool? trade_approved { get; set; }
        public string approved_by { get; set; }
        public string approve_reason { get; set; }
    }

    public class ShiftLite
    {
        public string shift_id { get; set; }
        public string shift_code { get; set; }
        public string shift_name { get; set; }
        public int staff { get; set; }
        public string shift_from { get; set; }
        public string shift_to { get; set; }
    }
        public class Shift
    {
        public string dept_name;

        public bool is_adhoc { get; set; }
        public long dept_id { get; set; }
        public long schedule_id { get; set; }
        public long shift_id { get; set; }
        public string staff_id { get; set; }
        public string shift_code { get; set; }
        public string shift_name { get; set; }
        public string shift_from { get; set; }
        public string shift_to { get; set; }
        public string split_from { get; set; }
        public string weekday { get; set; }
        public string split_to { get; set; }
        public bool is_split { get; set; }
        public string minutes { get; set; }
        public string billable { get; set; }
        public string non_billable { get; set; }
        public string period { get; set; }
        public string period_sm { get; set; }
        public string tothours { get; set; }
        public bool staff_onleave { get; set; }
        public bool? shift_approved { get; set; }
        public string leave_name { get; set; }
        public string rej_reason { get; set; }
        public string rej_by { get; set; }
        public bool allday { get; set; }
        public bool repeated { get; set; }
        public int staff { get; set; }
        public string status { get; set; }
    }
    public class TradeReason
    {
        public string tradereason_id { get; set; }
        public string tradereason_code { get; set; }
        public string tradereason_name { get; set; }
    }
    public class Roles
    {
        public long role_id { get; set; }
        public string role_code { get; set; }
        public string role_name { get; set; }
        public string role_dept { get; set; }
        public string role_active { get; set; }
        public long role_deptid { get; set; }
        public bool role_isactive { get; set; }

        public List<Users> user_list { get; set; }

        public Roles()
        {
            this.user_list = new List<Users>();
        }
    }

    public class Menus
    {
        public long menu_id { get; set; }
        public string menu_code { get; set; }
        public string menu_name { get; set; }
        public long parent_id { get; set; }
        public string menu_controller { get; set; }
        public string menu_action { get; set; }
        public string menu_icon { get; set; }
        public int menu_order { get; set; }
        public bool isactive { get; set; }
        public bool isadmin { get; set; }
    }

    public class CatConfigDTO
    {
        public List<CodeValues> avl_category { get; set; }
        public List<CodeValues> slt_category { get; set; }

        public CatConfigDTO()
        {
            this.avl_category = new List<CodeValues>();
            this.slt_category = new List<CodeValues>();
        }
    }
    public class RoleMenus
    {
        public List<Menus> avl_menus { get; set; }
        public List<Menus> slt_menus { get; set; }

        public RoleMenus()
        {
            this.avl_menus = new List<Menus>();
            this.slt_menus = new List<Menus>();
        }
    }

    public class Users
    {
        public long user_id { get; set; }
        public long role_id { get; set; }
        public long dept_id { get; set; }
        public long staff_id { get; set; }
        public long company_id { get; set; }
        public string username { get; set; }
        public string role_name { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public string photo { get; set; }
        public string department { get; set; }
        public string password { get; set; }
        public bool systemadmin { get; set; }
        public bool isadmin { get; set; }
        public bool isactive { get; set; }
        public string isStaff { get; set; }
        public HttpPostedFileBase upload { get; set; }
    }
    public class Config
    {
        public bool approve_shift { get; set; }
        public bool approve_leave { get; set; }
        public bool approve_trade { get; set; }
        public bool limit_depthrs { get; set; }
        public bool create_staffuser { get; set; }
        public bool allow_multipleshift { get; set; }
        public bool shift_deptbase { get; set; }
        public bool user_deptbase { get; set; }
        public bool log_shiftcalender { get; set; }
        public bool prevent_pastscheduler { get; set; }
        public string dateformat { get; set; }
        public string startday { get; set; }


        public bool show_emptyshift { get; set; }
        public string default_password { get; set; }
        public long default_role{ get; set; }
        public string limit_scheduler { get; set; }
        public bool QuickshiftonSchedule { get; set; }
        public bool AllowAdhocShift { get; set; }

        public long com_role { get; set; }
        public long man_role { get; set; }
    }
    public class Organisation
    {
        public string organisation_code { get; set; }
        public string organisation_name { get; set; }
        public string registration_number { get; set; }
        public string vat_number { get; set; }
        public string trading_name { get; set; }
        public string organisation_type { get; set; }
        public string telephone1 { get; set; }
        public string telephone2 { get; set; }
        public string fax_number1 { get; set; }
        public string fax_number2 { get; set; }
        public string email_address1 { get; set; }
        public string email_address2 { get; set; }

        public string physical1 { get; set; }
        public string physical2 { get; set; }
        public string physical3 { get; set; }
        public string physicalcode { get; set; }

        public string postal1 { get; set; }
        public string postal2 { get; set; }
        public string postal3 { get; set; }
        public string postalcode { get; set; }
    }


    public class MainTempo
    {
        public string title_text { get; set; }
        public string title_desc { get; set; }

        public List<MainDataTempo> maindata { get; set; }

        public MainTempo()
        {
            this.maindata = new List<MainDataTempo>();
        }
    }
    public class MainDataTempo
    {
        public string cat_text { get; set; }
        public string x_value { get; set; }
        public string y_value { get; set; }
    }
    public class StructureLevels
    {
        public long level_id { get; set; }
        public string level_name { get; set; }
        public string level_code { get; set; }
        public int level_order { get; set; }
    }
    public class CodeValues
    {
        public long id { get; set; }
        public long Order { get; set; }
        public string code { get; set; }
        public string value { get; set; }
        public bool isReadyonly { get; set; }
        public bool isActive { get; set; }
        public bool isVisible { get; set; }
        public string DefaultValue { get; set; }
        public bool isDeleted { get; set; }
        public string PrimaryVal { get; set; }
        public string SecondaryVal { get; set; }
    }
    public class LookupCode
    {
        public long code_id { get; set; }
        public string code_name { get; set; }
    }
    public class WeekInx
    {
        public int weekinx { get; set; }
    }
    public class QuickShift
    {
        public long quickshift_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string weekno { get; set; }
        public DateTime date_from { get; set; }
        public DateTime date_to { get; set; }
        public TimeSpan timefrom { get; set; }
        public TimeSpan timeto { get; set; }
        public string weekday { get; set; }
        public string color { get; set; }
        public string tothours { get; set; }
        public string totstaff { get; set; }
        public List<Staff> staff_list { get; set; }

        public QuickShift()
        {
            this.staff_list = new List<Staff>();
        }
    }
    public class StaffOnShift
    {
        public string shift_id { get; set; }
        public Shift shift { get; set; }
        public List<Staff> staff_list { get; set; }

        public StaffOnShift()
        {
            this.shift = new Shift();
            this.staff_list = new List<Staff>();
        }
    }
    public class QuickSchedule
    {
        public long quickshiftschedule_id { get; set; }
        public Nullable<long> staff_id { get; set; }
        public Nullable<long> shift_id { get; set; }
        public string scheduled_by { get; set; }
        public Nullable<System.DateTime> scheduled_date { get; set; }
        public Nullable<bool> isactive { get; set; }
    }
    public class EmailConfig
    {
        public long id { get; set; }
        public string email { get; set; }
        public string reply { get; set; }
        public string server { get; set; }
        public string port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool ssl { get; set; }
        public long type_id { get; set; }
    }
    public class Groups
    {
        public long structure_id { get; set; }
        public string structure_name { get; set; }
        public List<Staff> staff_list { get; set; }
        public List<Staff> groupstaff_list { get; set; }
        public List<StaffGroup> group_list { get; set; }
        public Groups()
        {
            this.groupstaff_list = new List<Staff>();
            this.staff_list = new List<Staff>();
            this.group_list = new List<StaffGroup>();
        }
    }
    public class StaffGroup
    {
        public long group_id { get; set; }
        public long dept_id { get; set; }
        public long noofstaff { get; set; }
        public string group_code { get; set; }
        public string group_name { get; set; }
        public string group_desc { get; set; }
    }

    public class GroupDTO
    {
        public long group_id { get; set; }
        public long dept_id { get; set; }
        public long noofstaff { get; set; }
        public string group_code { get; set; }
        public string group_name { get; set; }
        public string group_desc { get; set; }
        public List<Staff> staff_list { get; set; }
        public GroupDTO()
        {
            this.staff_list = new List<Staff>();
        }
    }


    public class Application
    {
      public long app_id { get; set; }
      public string version { get; set; }
      public DateTime release_date { get; set; }
      public DateTime valid_from { get; set; }
      public DateTime valid_to { get; set; }
      public string app_user { get; set; }
      public DateTime validate_date { get; set; }
      public string prod_key { get; set; }
      public string application { get; set; }
    }

}