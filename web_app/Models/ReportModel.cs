﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app.Models
{
    public class ReportsDTO
    {
        public List<ReportCategory> cat_list { get; set; }

        public ReportsDTO()
        {
            this.cat_list = new List<ReportCategory>();
        }
    }
    public class ReportCategory
    {
        public long reportcat_id { set; get; }
        public string report_cat { set; get; }

        public List<Reports> reports_list { get; set; }

        public ReportCategory()
        {
            this.reports_list = new List<Reports>();
        }
    }
    public class Reports
    {
        public long report_id { set; get; }
        public string reportcat_id { set; get; }
        public string report_name { set; get; }
        public string report_desc { set; get; }
        public string report_file { set; get; }
        public string report_url { set; get; }
        public string report_image { set; get; }
        public string report_parms { set; get; }
        public bool report_toggled { set; get; }
    }
    public class rptDeptStaffCount
    {
        public string label { set; get; }
        public int? data { set; get; }
    }
    public class rptDeptWithStaff
    {
        public string dept_name { set; get; }
        public string dept_id { set; get; }
    }
    public class DefaultDataDTO
    {
        public string code { set; get; }
        public string value { set; get; }
        public string isreadonly { set; get; }
        public string isvisible { set; get; }
        public string isactive { set; get; }
        public string ordervalue { set; get; }
    }

    public class RPTRolesDTO
    {
        public long role_id { get; set; }
        public string role_code { get; set; }
        public string role_name { get; set; }
        public string role_dept { get; set; }
        public string role_active { get; set; }
        public string role_deptid { get; set; }
        public string role_isactive { get; set; }
    }
    public class RptRecentDTO
    {
        public long id { get; set; }
        public string report_name { get; set; }
        public string report_cat { get; set; }
        public string report_date { get; set; }
        public string report_url { get; set; }
        public string report_image { get; set; }
        public string report_desc { get; set; }
        public string report_time { get; set; }
        public string username { get; set; }
    }
    public class RptShiftDTO
    {
        public string shift_code { get; set; }
        public string shift_name { get; set; }
        public string shift_from { get; set; }
        public string shift_to { get; set; }
        public string shift_dept { get; set; }
        public string split_shift { get; set; }
        public string split_from { get; set; }
        public string split_to { get; set; }
        public string non_billable { get; set; }
    }

    public class ArchiveDTO
    {
        public long id { get; set; }
        public string username { get; set; }
        public string description { get; set; }
        public string filename { get; set; }
        public string arch_date { get; set; }
        public string arch_time { get; set; }
    }


    public class ReportScheduleDTO
    {
        public long id { get; set; }
        public string report_name { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string date_from { get; set; }
        public string date_to { get; set; }
        public string repeat { get; set; }
        public string output { get; set; }
        public string createdon { get; set; }
    }
    public class ScheduleHistoryDTO
    {
        public long id { get; set; }
        public string title { get; set; }
        public string report_name { get; set; }
        public string output { get; set; }
        public string run_date { get; set; }
        public string run_time { get; set; }
        public string status { get; set; }
    }

    public class JobShiftNotify
    {
        public string staff_name { get; set; }
        public string staff_email { get; set; }
        public string shift_name { get; set; }
        public string shift_date { get; set; }
        public string shift_period { get; set; }
        public string shift_status { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        public string contact_email { get; set; }
        public string contact_tel { get; set; }
    }
    public class PassFailReportDTO
    {
        public long inspection_id { get; set; }
        public string datestampinspection_date { get; set; }
        public string inspection_status { get; set; }
        public string comments { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string pos_lat { get; set; }
        public string pos_lag { get; set; }
        public string inspection_image { get; set; }
        public string inspection_image_data { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string insp_fullname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string barcode { get; set; }
        public string asset_name { get; set; }
        public string asset_description { get; set; }
        public DateTime inspection_date { get; set; }
        public string insp_date { get; set; }
        public string item_name { get; set; }
        public string category_name { get; set; }
        public string certification { get; set; }
        public string load_test_cycle { get; set; }
        public string inspection_cycle { get; set; }
        public string site_name { get; set; }
        public string site_description { get; set; }
        public string address_street { get; set; }
        public string address_building { get; set; }
        public string address_pcode { get; set; }
        public string contact_email { get; set; }
        public string contact_tel { get; set; }
        public string company_name { get; set; }
        public string company_reg { get; set; }
        public string company_tel { get; set; }
        public string company_email { get; set; }
        public string company_street { get; set; }
        public string company_building { get; set; }
        public string company_city { get; set; }
        public string company_pcode { get; set; }
        public string inspector { get; set; }
        public string stdcode { get; set; }
        public string size_meter { get; set; }
        public string size_mm { get; set; }
        public string size_ton { get; set; }
        public string insp_lat { get; set; }
        public string insp_lon { get; set; }
        public string visual { get; set; }
        public string scrap { get; set; }
        public string loadtest { get; set; }
        public string recondition { get; set; }
        public byte[] company_logo { get; set; }
        public string company_logo_data { get; set; }
        public string next_inspection_date { get; set; }
        //public string inspection_image { get; set; }
        public byte[] item_image { get; set; }
        public string item_image_data { get; set; }
        public byte[] signature_img { get; set; }
        public string signature_data { get; set; }
        public string photo { get; set; }
        public string manufacture { get; set; }
        public string idnumber { get; set; }
        public string registerno { get; set; }
        public List<PassFailReportItemsDTO> line_items { get; set; }
        public PassFailReportDTO()
        {
            this.line_items = new List<PassFailReportItemsDTO>();
        }
    }

    public class PassFailReportItemsDTO
    {
        public string test_description { get; set; }
        public string test_info { get; set; }
        public string discard { get; set; }
        public string status { get; set; }
        public string comments { get; set; }
    }
}