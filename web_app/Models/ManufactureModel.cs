﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app.Models
{
    public class Certificate
    {
        public long certificate_id { get; set; }
        public long company_id { get; set; }
        public string code { get; set; }
        public string sn_no { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public byte[] file { get; set; }
    }
}