﻿using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;

namespace web_app.BLL
{

    public class DiaryEvent
    {

        public int ID;
        public string Title;
        public int SomeImportantKeyID;
        public string StartDateString;
        public string EndDateString;
        public string StatusString;
        public string StatusColor;
        public string ClassName;


        public static List<DiaryEvent> LoadAllAppointmentsInDateRange(double start, double end, string staff_id)
        {
            DateTime fromDate = ConvertFromUnixTimestamp(start);
            DateTime toDate = ConvertFromUnixTimestamp(end);
            using (lidEntities dbo = new lidEntities())
            {
                long _staff_id = staff_id.ToLongOrDefault();
                List<b_appointmentdiary> app_diary = dbo.b_appointmentdiary.Where(m => m.DateTimeScheduled >= fromDate && m.DateTimeScheduled <= toDate && m.staff_id == _staff_id).ToList();// && (DateTime)m.DateTimeScheduled.AddMinutes(m.AppointmentLength) <= toDate

                List<DiaryEvent> result = new List<DiaryEvent>();

                foreach (b_appointmentdiary item in app_diary)
                {
                    DiaryEvent rec = new DiaryEvent();
                    rec.ID = item.ID;
                    rec.SomeImportantKeyID = item.SomeImportantKey;
                    rec.StartDateString = item.DateTimeScheduled.ToString("s"); // "s" is a preset format that outputs as: "2009-02-27T12:12:22"
                    rec.EndDateString = item.DateTimeScheduled.AddMinutes(item.AppointmentLength).ToString("s"); // field AppointmentLength is in minutes
                    rec.Title = item.Title + " - " + item.AppointmentLength.ToString() + " mins";
                    rec.StatusString = Enums.GetName<AppointmentStatus>((AppointmentStatus)item.StatusENUM);
                    rec.StatusColor = Enums.GetEnumDescription<AppointmentStatus>(rec.StatusString);
                    string ColorCode = rec.StatusColor.Substring(0, rec.StatusColor.IndexOf(":"));
                    rec.ClassName = rec.StatusColor.Substring(rec.StatusColor.IndexOf(":") + 1, rec.StatusColor.Length - ColorCode.Length - 1);
                    rec.StatusColor = ColorCode;
                    result.Add(rec);
                }
                return result;
            }

        }


        public static List<DiaryEvent> LoadAppointmentSummaryInDateRange(double start, double end, string staff_id)
        {

            var fromDate = ConvertFromUnixTimestamp(start);
            var toDate = ConvertFromUnixTimestamp(end);
            using (lidEntities ent = new lidEntities())
            {
                var rslt = ent.b_appointmentdiary.Where(s => s.DateTimeScheduled >= fromDate && s.DateTimeScheduled.AddMinutes(s.AppointmentLength) <= toDate)
                                                        .GroupBy(s => s.DateTimeScheduled.ToShortTimeString())
                                                        .Select(x => new { DateTimeScheduled = x.Key, Count = x.Count() }
                                                        );

                List<DiaryEvent> result = new List<DiaryEvent>();
                int i = 0;
                try
                {
                    foreach (var item in rslt)
                    {
                        DiaryEvent rec = new DiaryEvent();
                        rec.ID = i; //we dont link this back to anything as its a group summary but the fullcalendar needs unique IDs for each event item (unless its a repeating event)
                        rec.SomeImportantKeyID = -1;
                        string StringDate = string.Format("{0:yyyy-MM-dd}", item.DateTimeScheduled);
                        rec.StartDateString = StringDate + "T00:00:00"; //ISO 8601 format
                        rec.EndDateString = StringDate + "T23:59:59";
                        rec.Title = "Booked: " + item.Count.ToString();
                        result.Add(rec);
                        i++;
                    }
                }
                catch (Exception)
                {
                }

                return result;
            }

        }

        public static bool UpdateDiaryEvent(int id, string NewEventStart, string NewEventEnd)
        {
            try
            {
                using (lidEntities ent = new lidEntities())
                {
                    var rec = ent.b_appointmentdiary.FirstOrDefault(s => s.ID == id);
                    if (rec != null)
                    {
                        DateTime DateTimeStart = DateTime.Parse(NewEventStart, null, DateTimeStyles.RoundtripKind).ToLocalTime(); // and convert offset to localtime
                        rec.DateTimeScheduled = DateTimeStart;
                        if (!String.IsNullOrEmpty(NewEventEnd))
                        {
                            TimeSpan span = DateTime.Parse(NewEventEnd, null, DateTimeStyles.RoundtripKind).ToLocalTime() - DateTimeStart;
                            rec.AppointmentLength = Convert.ToInt32(span.TotalMinutes);
                        }
                        ent.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }


        private static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }


        public static bool CreateNewEvent(string Title, string NewEventDate, string NewEventTime, string NewEventDuration)
        {
            try
            {
                lidEntities ent = new lidEntities();
                b_appointmentdiary rec = new b_appointmentdiary();
                rec.Title = Title;
                rec.DateTimeScheduled = DateTime.ParseExact(NewEventDate + " " + NewEventTime, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                rec.AppointmentLength = Int32.Parse(NewEventDuration);
                ent.b_appointmentdiary.Add(rec);
                ent.SaveChanges();

                updActiveEvent(Title);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private static void updActiveEvent(string Title)
        {
            using (lidEntities dbo = new lidEntities())
            {
                b_event _event = dbo.b_event.Where(m => m.event_desc == Title).FirstOrDefault();
                if (_event != null)
                {
                    _event.isActive = false;
                    dbo.Entry(_event).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }
            }
        }

        internal static bool DeleteEvent(string eventId)
        {
            try
            {
                using (lidEntities dbo = new lidEntities())
                {
                    var _events = dbo.b_appointmentdiary.FirstOrDefault(s => s.ID.ToString() == eventId);
                    if (_events != null)
                    {
                        dbo.Entry(_events).State = System.Data.Entity.EntityState.Deleted;
                        dbo.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}