﻿using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using Dapper;
using System.Collections;
using System.Reflection;
using System.Net.Mail;
using System.Web.Hosting;

namespace web_app.BLL
{
    public class Jobs
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }
        public static void jobShiftNotify(string shift_id)
        {
            // Prepare Postal classes to work outside of ASP.NET request
            //var viewsPath = Path.GetFullPath(HostingEnvironment.MapPath(@"~/Views/Emails"));
            //var engines = new ViewEngineCollection();
            //engines.Add(new FileSystemRazorViewEngine(viewsPath));

            //var emailService = new EmailService(engines);

            //// Get comment and send a notification.
            //using (var db = new MailerDbContext())
            //{
            //    var comment = db.Comments.Find(commentId);

            //    var email = new NewCommentEmail
            //    {
            //        To = "yourmail@example.com",
            //        UserName = comment.UserName,
            //        Comment = comment.Text
            //    };

            //    emailService.Send(email);
            //}

            JobShiftNotify notify = new JobShiftNotify();

            using (var cn = GetOpenConnection())
            {
                notify = cn.Query<JobShiftNotify>("job_shiftnotify", new { schedule_id = shift_id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (notify != null)
                {
                    string template = File.ReadAllText(HostingEnvironment.MapPath(@"~/Templates/email/shift_notification.html"));

                    template = template.Replace("##staff_name##", notify.staff_name);
                    template = template.Replace("##staff_email##", notify.staff_email);
                    template = template.Replace("##shift_name##", notify.shift_name);
                    template = template.Replace("##shift_date##", notify.shift_date);
                    template = template.Replace("##shift_period##", notify.shift_period);
                    template = template.Replace("##shift_status##", notify.shift_status);
                    template = template.Replace("##created_date##", notify.created_date);
                    template = template.Replace("##created_by##", notify.created_by);
                    template = template.Replace("##contact_email##", notify.contact_email);
                    template = template.Replace("##contact_tel##", notify.contact_tel);

                    string subject = ConfigurationManager.AppSettings["shiftnotify_subject"].ToString();

                    SendEmail(notify.staff_email, subject, template);
                }
            }
        }
        public static void SendEmail(string recepient, string subject, string body)
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.IsBodyHtml = true;
                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEMail"].ToString());
                mail.To.Add(recepient);
                mail.CC.Add(new MailAddress(ConfigurationManager.AppSettings["CcEMail"].ToString()));
                mail.Subject = subject;
                mail.Body = body;

                SmtpClient client = new SmtpClient();
                client.EnableSsl = true;
                client.Send(mail);
            }
            catch (Exception err)
            {
                string exp = err.Message;
            }
        }
    }
}