﻿using Dapper;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Collections;

namespace web_app.BLL
{
    public class ItemCore
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }


        internal static List<CodeValues> get_group_category(string code)
        {
            List<CodeValues> list = new List<CodeValues>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CodeValues>("get_codevalues", new { code = code }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<CodeValues> get_company_categories(string code, string username)
        {
            List<CodeValues> list = new List<CodeValues>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CodeValues>("get_company_categories", new { code = code, username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static TestModel get_test_byid(string id)
        {
            TestModel list = new TestModel();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<TestModel>("get_test_byid", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }

        internal static List<TestModel> get_linked_tests(long id)
        {
            List<TestModel> list = new List<TestModel>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<TestModel>("get_linked_tests", new { id = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<TestModel> get_unlinked_tests(long id, string keyword)
        {
            List<TestModel> list = new List<TestModel>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<TestModel>("get_unlinked_tests", new { id = id, keyword = keyword }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        //internal static List<ItemCategoryTree> get_category_tree()
        //{
        //    List<ItemCategoryTree> list = new List<ItemCategoryTree>();
        //    using (var cn = GetOpenConnection())
        //    {
        //        list = cn.Query<ItemCategoryTree>("get_category_tree", commandType: CommandType.StoredProcedure).ToList();
        //    }
        //    return list;
        //}

        internal static ItemCategory get_category_byid(long id)
        {
            ItemCategory list = new ItemCategory();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<ItemCategory>("get_category_byid", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }

        internal static List<CompanyAsset> get_manufacture_asset(string id)
        {
            List<CompanyAsset> list = new List<CompanyAsset>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyAsset>("get_manufacture_asset", new { id = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static List<CompanyModel> get_manufactures()
        {
            List<CompanyModel> list = new List<CompanyModel>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyModel>("get_manufactures", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<CompanyModel> get_suppliers()
        {
            List<CompanyModel> list = new List<CompanyModel>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyModel>("get_suppliers", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static List<SiteModel> get_sites_many(string username)
        {
            List<SiteModel> list = new List<SiteModel>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<SiteModel>("get_sites_many", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<CodeValues> get_codevalues(string code)
        {
            List<CodeValues> list = new List<CodeValues>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CodeValues>("get_codevalues", new { code = code }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static ToggleLinkModel toggle_link(ToggleLinkModel model)
        {
            ToggleLinkModel list = new ToggleLinkModel();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<ToggleLinkModel>("toggle_test_item_link", new { item_id = model.item_id, test_id = model.test_id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }
        internal static List<ItemCategory> get_category_many()
        {
            List<ItemCategory> list = new List<ItemCategory>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<ItemCategory>("get_category_many", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<ItemCategory> get_category_many(string id)
        {
            List<ItemCategory> list = new List<ItemCategory>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<ItemCategory>("get_category_many_byid", new { id = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<TestModel> get_tests_many()
        {
            List<TestModel> list = new List<TestModel>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<TestModel>("get_tests_many", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static ItemCategory crud_category(ItemCategoryTree payload)
        {
            ItemCategory list = new ItemCategory();
            using (var cn = GetOpenConnection())
            {

                long _id = payload.id.ToLongOrDefault(0);
                string parent_id = payload.parent == "#" ? null : payload.parent;
                list = cn.Query<ItemCategory>("crud_category", new
                {
                    id = _id,
                    parent = parent_id,
                    text = payload.text,
                    group_id = payload.group_id
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }
        internal static ItemCategory crud_item(ItemCategoryTree payload)
        {
            ItemCategory list = new ItemCategory();
            using (var cn = GetOpenConnection())
            {
                long _id = payload.id.ToLongOrDefault(0);
                string category_id = payload.category_id;
                string parent_id = payload.parent == "#" ? null : payload.parent;
                list = cn.Query<ItemCategory>("crud_item", new
                {
                    id = _id,
                    parent = parent_id,
                    text = payload.text,
                    category_id = category_id
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }
        internal static TestModel crud_test(TestModel payload)
        {
            TestModel list = new TestModel();
            using (var cn = GetOpenConnection())
            {
                long test_id = payload.test_id.ToLongOrDefault();
                list = cn.Query<TestModel>("crud_test", new
                {
                    id = payload.test_id,
                    desc = payload.test_description,
                    discard = payload.discard,
                    info = payload.test_info
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }

        internal static List<ItemCategoryTree> get_category_tree(string group_id)
        {
            List<ItemCategoryTree> hierarchy = new List<ItemCategoryTree>();
            using (lidEntities dbo = new lidEntities())
            {
                long _id = group_id.ToLongOrDefault();
                List<lid_category> tree = dbo.lid_category.Where(x => x.group_id == _id).ToList();

                hierarchy = tree.Select(c => new ItemCategoryTree()
                {
                    icon = "fa fa-folder icon-danger",
                    id = c.category_id.ToStringOrDefault(),
                    text = c.category_name,
                    parent = (c.parent_id == null) ? "#" : c.parent_id.ToStringOrDefault(),
                    state = get_items_selection(),
                    // children = GetChildren(tree, c.category_id.ToLongOrDefault())
                })
                            .ToList();
            }
            return hierarchy;
        }

        internal static List<ItemCategoryTree> get_category_tree_comp(string username)
        {
            List<ItemCategoryTree> hierarchy = new List<ItemCategoryTree>();
            using (lidEntities dbo = new lidEntities())
            {
                b_users _user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                //var _cats = dbo.lid_company_cat.Where(x => x.company_id == _user.company_id).Select(x => x.category_id).ToList();

                //List<lid_category> tree = dbo.lid_category.Where(x => _cats.Any(c => x.category_id == c)).ToList();
                
                List<lid_company_cat> _cats = dbo.lid_company_cat.Where(x => x.company_id == _user.company_id).ToList();
                foreach (lid_company_cat _cat in _cats)
                {
                    List<lid_category> _res = dbo.lid_category.Where(x => x.group_id == _cat.category_id).ToList();
                    foreach (lid_category c in _res)
                    {
                        hierarchy.Add(new ItemCategoryTree()
                        {
                            icon = "fa fa-folder icon-danger",
                            id = c.category_id.ToStringOrDefault(),
                            text = c.category_name,
                            parent = (c.parent_id == null) ? "#" : c.parent_id.ToStringOrDefault(),
                            state = get_items_selection(),
                            // children = GetChildren(tree, c.category_id.ToLongOrDefault())
                        });
                    }
                }

                //hierarchy = tree.Select(c => new ItemCategoryTree()
                //{
                //    icon = "fa fa-folder icon-danger",
                //    id = c.category_id.ToStringOrDefault(),
                //    text = c.category_name,
                //    parent = (c.parent_id == null) ? "#" : c.parent_id.ToStringOrDefault(),
                //    state = get_items_selection(),
                //    // children = GetChildren(tree, c.category_id.ToLongOrDefault())
                //})
                //            .ToList();
            }
            return hierarchy;
        }
        internal static List<ItemCategoryTree> get_items_tree(string id)
        {
            List<ItemCategoryTree> hierarchy = new List<ItemCategoryTree>();
            using (lidEntities dbo = new lidEntities())
            {
                long _id = id.ToLongOrDefault();

                List<Items> tree = new List<Items>();
                using (var cn = GetOpenConnection())
                {
                    tree = cn.Query<Items>("get_items_many", new { id = _id }, commandType: CommandType.StoredProcedure).ToList();
                }

                hierarchy = tree.Select(c => new ItemCategoryTree()
                {
                    icon = c.image != null
                        ? c.links > 0 ? "fa fa-file-image-o green_color" : "fa fa-file-image-o red_color"
                        : c.links > 0 ? "fa fa-file green_color" : "fa fa-file red_color",
                    id = "itm_" + c.item_id.ToStringOrDefault(),
                    text = c.item_name,
                    parent = (c.parent_id == 0) ? "#" : "itm_" + c.parent_id.ToStringOrDefault(),
                    state = get_items_selection(),
                    // children = GetChildren(tree, c.category_id.ToLongOrDefault())
                })
                            .ToList();
            }
            return hierarchy;
        }
        private static ItemCategorySelected get_items_selection()
        {
            ItemCategorySelected cat = new ItemCategorySelected();
            cat.selected = false;
            return cat;
        }
        public static Items get_item_byid(string id)
        {
            Items list = new Items();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<Items>("get_item_byid", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }

        public static List<ItemCategoryTree> GetChildren(List<lid_category> tree, long category_id)
        {
            return tree
                    .Where(c => c.parent_id == category_id)
                    .Select(c => new ItemCategoryTree
                    {
                        icon = "fa fa-folder icon-danger",
                        id = c.category_id.ToStringOrDefault(),
                        text = c.category_name,
                        parent = (c.parent_id == null) ? "#" : c.parent_id.ToStringOrDefault(),
                        children = GetChildren(tree, c.category_id.ToLongOrDefault())
                    })
                    .ToList();
        }

    }
}