﻿using Dapper;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace web_app.BLL
{
    public class CompanyCore
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        internal static List<CompanyAsset> get_assets_many(string username)
        {
            List<CompanyAsset> list = new List<CompanyAsset>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyAsset>("get_assets_many", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<CompanySites> get_sites_many(string username)
        {
            List<CompanySites> list = new List<CompanySites>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanySites>("get_sites_many", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static CompanySites get_site_byid(string id)
        {
            CompanySites list = new CompanySites();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanySites>("get_site_byid", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }
        internal static List<Tags> get_assetstags_many(long id)
        {
            List<Tags> list = new List<Tags>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<Tags>("get_assetstags_many", new { id = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static List<CompanyInspector> get_inspectors_many(string username)
        {
            List<CompanyInspector> list = new List<CompanyInspector>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyInspector>("get_inspectors_many", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        internal static List<MobiRecentInspections> get_upcoming_inspections(string username)
        {
            List<MobiRecentInspections> list = new List<MobiRecentInspections>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<MobiRecentInspections>("get_upcoming_inspections", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;

        }
        internal static List<Tags> get_company_tags(string username)
        {
            List<Tags> list = new List<Tags>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<Tags>("get_company_tags", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static CompanyInspector get_inspector_byid(string id)
        {
            CompanyInspector list = new CompanyInspector();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyInspector>("get_inspector_byid", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }
        public static List<PassFailReportDTO> get_company_inspections(string username)
        {
            List<PassFailReportDTO> res = new List<PassFailReportDTO>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<PassFailReportDTO>("rpt_get_passinspection", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }
        public static List<PassFailReportDTO> get_company_inspections_today(string username)
        {
            List<PassFailReportDTO> res = new List<PassFailReportDTO>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<PassFailReportDTO>("rpt_get_passinspection_today", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }
    }
}