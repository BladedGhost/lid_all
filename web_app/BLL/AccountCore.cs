﻿using Dapper;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace web_app.BLL
{
    public class AccountCore
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        internal static CompanyModel crud_company(CompanyModel model)
        {
            CompanyModel list = new CompanyModel();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyModel>("crud_company", new
                {
                    company_id = model.company_id.ToLongOrDefault(0),
                    company_type_id = model.company_type_id,
                    name = model.company_name,
                    tel = model.company_tel,
                    email = model.company_email,
                    group_id = model.group_id
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }
        internal static void link_cat_company(long comp_id, long cat_id)
        {
            try
            {
                using (lidEntities dbo = new lidEntities())
                {
                    lid_company_cat _link = dbo.lid_company_cat.Where(x => x.company_id == comp_id && x.category_id == cat_id).FirstOrDefault();
                    if (_link == null)
                    {
                        _link = new DAL.lid_company_cat();
                        _link.category_id = cat_id;
                        _link.company_id = comp_id;
                        dbo.lid_company_cat.Add(_link);
                        dbo.SaveChanges();
                    }
                }
            }
            catch (Exception err) { }
        }

        internal static object upd_user_company(CompanyModel model, long company_id)
        {
            CompanyModel list = new CompanyModel();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<CompanyModel>("upd_user_company", new
                {
                    company_id = company_id.ToLongOrDefault(0),
                    username = model.username
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }
    }
}