﻿using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using Dapper;
using System.Collections;
using System.Reflection;
using System.Web.Mvc;
using web_app.DAL;

namespace web_app.BLL
{
    [Authorize]
    public class AdminCore
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        internal static System.Collections.IEnumerable get_title()
        {
            throw new NotImplementedException();
        }

        public static Organisation get_organisation()
        {
            Organisation organisation = new Organisation();
            using (lidEntities dbo = new lidEntities())
            {
                b_organisation _organisation = dbo.b_organisation.FirstOrDefault();
                if (_organisation != null)
                {
                    organisation = (new Organisation
                    {
                        organisation_code = _organisation.organisation_code,
                        organisation_name = _organisation.organisation_name,
                        registration_number = _organisation.registration_number,
                        vat_number = _organisation.vat_number,
                        trading_name = _organisation.trading_name,
                        organisation_type = _organisation.organisation_type,
                        telephone1 = _organisation.telephone1,
                        telephone2 = _organisation.telephone2,
                        fax_number1 = _organisation.fax_number1,
                        fax_number2 = _organisation.fax_number2,
                        email_address1 = _organisation.email_address1,
                        email_address2 = _organisation.email_address2,

                        physical1 = _organisation.physical1,
                        physical2 = _organisation.physical2,
                        physical3 = _organisation.physical3,
                        physicalcode = _organisation.physicalcode,

                        postal1 = _organisation.postal1,
                        postal2 = _organisation.postal2,
                        postal3 = _organisation.postal3,
                        postalcode = _organisation.postalcode
                    });
                }
            }
            return organisation;
        }


        internal static List<ShiftLite> get_currentshifts()
        {
            List<ShiftLite> list = new List<ShiftLite>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<ShiftLite>("get_currentshifts", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static List<string> get_staffbydeptpost(string dept, string position)
        {
            List<string> list = new List<string>();
            using (lidEntities dbo = new lidEntities())
            {
                long pos = util.parselong(position);
                list = dbo.staffdept_link.Where(m => m.dept_id.ToString() == dept && m.post_id == pos).Select(m => m.staff_id.ToString()).ToList();
            }
            return list;
        }

        internal static bool check_shiftoverlap(string staff_id, string shift_id, string weekday, string schedule_type)
        {
            bool overlap = false;
            using (var cn = GetOpenConnection())
            {
                overlap = cn.Query<Boolean>("check_shiftoverlap", new { @staff_id = staff_id, shift_id = shift_id, weekday = weekday, schedule_type = schedule_type }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return overlap;
        }

        internal static Config get_sysconfig()
        {
            Config config = new Config();

            using (lidEntities dbo = new lidEntities())
            {
                b_config _config = dbo.b_config.FirstOrDefault();
                if (_config != null)
                {
                    config.allow_multipleshift = util.parsebool(_config.allow_multipleshift);
                    config.approve_leave = util.parsebool(_config.approve_leave);
                    config.approve_shift = util.parsebool(_config.approve_shift);
                    config.approve_trade = util.parsebool(_config.approve_trade);
                    config.create_staffuser = util.parsebool(_config.create_staffuser);
                    config.dateformat = _config.dateformat;
                    config.limit_depthrs = util.parsebool(_config.limit_depthrs);
                    config.shift_deptbase = util.parsebool(_config.shift_deptbase);
                    config.startday = _config.startday;
                    config.user_deptbase = util.parsebool(_config.user_deptbase);
                    config.default_password = _config.default_password;
                    config.show_emptyshift = _config.show_emptyshift.ToBooleanOrDefault();
                    config.default_role = _config.default_role.ToLongOrDefault();
                    config.log_shiftcalender = _config.log_shiftcalender.ToBooleanOrDefault();
                    config.prevent_pastscheduler = _config.prevent_pastscheduler.ToBooleanOrDefault();
                    config.QuickshiftonSchedule = _config.QuickshiftonSchedule.ToBooleanOrDefault();
                    config.AllowAdhocShift = _config.AllowAdhocShift.ToBooleanOrDefault();

                    config.com_role = _config.com_role.ToLongOrDefault();
                    config.man_role = _config.man_role.ToLongOrDefault();
                }
            }
            return config;
        }

        internal static List<Staff> get_deptstaff(long dept_id)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_staffstructure", new { id = dept_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }
        internal static List<Staff> get_deptstaff(long dept_id, string keyword)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_staffstructure_filter", new { id = dept_id, keyword = keyword }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }

        internal static int getDatediff(DateTime _from, DateTime _to)
        {
            int diff = 0;
            using (var cn = GetOpenConnection())
            {
                diff = cn.Query<Int32>("get_datediff", new { from = _from, to = _to }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return diff;
        }

        internal static List<Staff> get_deptgroupstaff(long dept_id, long group_id)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_deptgroupstaff", new { dept_id = dept_id, group_id = group_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }
        internal static List<Staff> get_deptnongroupstaff(long dept_id, long group_id)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_deptnongroupstaff", new { dept_id = dept_id, group_id = group_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }

        public static List<Staff> get_staffonthisshifttoday(string shift_id)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_staffonthisshifttoday", new { shift_id = shift_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }
        public static List<Staff> get_staffonshift(string shift_id, string position_id, string weekday)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                DateTime day = Convert.ToDateTime(weekday);
                staff = cn.Query<Staff>("get_staffonshift", new { shift_id = shift_id, position_id = position_id, weekday = day }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }

        public static List<LookupCode> get_lookupvalues(string _code)
        {
            List<LookupCode> list = new List<LookupCode>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<LookupCode>("get_lookupvalues", new { code = _code }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        public static LookupCode get_lookupvaluebyid(string _id)
        {
            LookupCode list = new LookupCode();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<LookupCode>("get_lookupvaluebyid", new { id = _id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return list;
        }

        public static List<Staff> get_dashStaffOnShiftByDay(string dept_id, string dayname)
        {
            List<Staff> list = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<Staff>("get_dashStaffOnShiftByDay", new { dept_id = dept_id, dayname = dayname }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        public static List<Dept> get_topstructure()
        {
            List<Dept> list = new List<Dept>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<Dept>("get_topstructure", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static List<b_structurelevels> get_strcturelevel()
        {
            List<b_structurelevels> _list = new List<b_structurelevels>();
            using (lidEntities dbo = new lidEntities())
            {
                _list = dbo.b_structurelevels.ToList();
            }
            return _list;
        }

        internal static List<OrgStructure> get_structurelevelbyid(long id)
        {
            List<OrgStructure> list = new List<OrgStructure>();
            using (var cn = GetOpenConnection())
            {
                List<OrgStructure> _list = cn.Query<OrgStructure>("get_structure", commandType: CommandType.StoredProcedure).ToList();

                //list = _list.Where(x => x..structure_level == id).ToList();
            }
            return list;
        }
        internal static List<Staff> get_allstaff()
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_allstaff", commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }
        internal static List<Staff> get_staffbirthdays()
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_staffbirthdays", commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }
        internal static int get_weekindex(string weekday)
        {
            int inx = 0;
            using (var cn = GetOpenConnection())
            {
                DateTime _weekday = weekday.ToDateTimeOrDefault(DateTime.Today);
                WeekInx model = cn.Query<WeekInx>("get_weekindex", new { weekday = _weekday }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                inx = model.weekinx;
            }
            return inx;
        }
        //internal static List<Staff> get_allstaff()
        //{
        //    List<Staff> staff = new List<Staff>();
        //    using (lidEntities dbo = new lidEntities())
        //    {
        //        List<b_staff> bstaff = null;
        //        bstaff = dbo.b_staff.ToList();

        //        foreach (b_staff _bstaff in bstaff)
        //        {
        //            Staff _staff = new Staff();
        //            _staff.staff_id = _bstaff.staff_id.ToString();
        //            _staff.staff_name = _bstaff.staff_name;
        //            _staff.staff_surname = _bstaff.staff_surname;
        //            _staff.staff_number = _bstaff.staff_number;
        //            _staff.staff_idnumber = _bstaff.staff_idnumber;
        //            _staff.staff_mobile = _bstaff.staff_mobile;
        //            _staff.staff_telephone = _bstaff.staff_telephone;
        //            _staff.staff_telephonehome = _bstaff.staff_hometelephone;
        //            _staff.staff_email = _bstaff.staff_email;
        //            _staff.photo = _bstaff.photo;
        //            _staff.emp_date = _bstaff.emp_date.ToDateTimeOrDefault();

        //            staffdept_link _staffdept = dbo.staffdept_link.Where(m => m.staff_id == _bstaff.staff_id).FirstOrDefault();
        //            if (_staffdept != null)
        //            {
        //                _staff.department_id = _staffdept.dept_id.ToString();
        //                _staff.position_id = _staffdept.post_id.ToString();
        //                _staff.title_id = _bstaff.staff_title.ToString();
        //                _staff.emp_type_id = _bstaff.emp_type.ToString();
        //                _staff.department = ((Dept)get_depatment(_staffdept.dept_id.ToString())).dept_name;
        //                _staff.dept_code = get_depatmentcode(_staffdept.dept_id.ToString());
        //                _staff.position = ((Position)get_position(_staffdept.post_id.ToString())).position_name;
        //                _staff.title = get_title(_bstaff.staff_title.ToString());
        //                _staff.emp_type = get_emptypecode(_bstaff.emp_type.ToString());

        //                staff.Add(_staff);
        //            }
        //        }
        //    }
        //    return staff;
        //}

        public static List<StaffLite> get_allstafflite()
        {
            List<StaffLite> results = new List<StaffLite>();
            using (var cn = GetOpenConnection())
            {
                results = cn.Query<StaffLite>("get_allstafflite", commandType: CommandType.StoredProcedure).ToList();
            }
            return results;
        }


        public static Staff get_stafflite(string id)
        {
            Staff staff = new Staff();
            using (lidEntities dbo = new lidEntities())
            {
                Staff _bstaff = get_staffById(id.ToLongOrDefault(0));
                if (_bstaff != null)
                {
                    staff.staff_id = _bstaff.staff_id.ToString();
                    staff.staff_name = _bstaff.staff_name;
                    staff.staff_surname = _bstaff.staff_surname;
                    staff.title = _bstaff.title;
                    staff.staff_shortname = _bstaff.staff_shortname;
                    staff.staff_shortdept = _bstaff.staff_shortdept;
                    ///staff.staff_number = _bstaff.staff_number;
                    staff.staff_idnumber = _bstaff.staff_idnumber;
                    staff.staff_mobile = _bstaff.staff_mobile;
                    //staff.staff_telephone = _bstaff.staff_telephone;
                    //staff.staff_telephonehome = _bstaff.staff_telephonehome;
                    staff.staff_email = _bstaff.staff_email;
                    staff.photo = _bstaff.photo;
                    //staff.emp_date = _bstaff.emp_date.ToString();

                    //staffdept_link _staffdept = dbo.staffdept_link.Where(m => m.staff_id == _bstaff.staff_id).FirstOrDefault();
                    //if (_staffdept != null)
                    //{
                    //    b_position _position = dbo.b_position.Where(m => m.position_id == _staffdept.post_id).FirstOrDefault();
                    //    staff.max_hours = _position.max_hours;
                    //}

                    //staff.department_id = _staffdept.dept_id.ToString();
                    //staff.position_id = _staffdept.post_id.ToString();
                    //staff.title_id = _bstaff.staff_title.ToString();
                    //staff.emp_type_id = _bstaff.emp_type.ToString();
                    //staff.department = ((Dept)get_depatment(_staffdept.dept_id.ToString())).dept_name;
                    //staff.dept_code = get_depatmentcode(_staffdept.dept_id.ToString());
                    //staff.position = ((Position)get_position(_staffdept.post_id.ToString())).position_name;
                    staff.title = get_title(_bstaff.staff_title.ToString());
                    //staff.emp_type = get_emptype(_bstaff.emp_type.ToString());
                }
            }
            return staff;
        }

        public static Staff get_staffshiftlite(string id, string weekday)
        {
            Staff staff = new Staff();

            return staff;
        }

        internal static List<QuickShift> get_quickshift(string quickshift_id)
        {
            List<QuickShift> shift = new List<QuickShift>();
            using (var cn = GetOpenConnection())
            {
                shift = cn.Query<QuickShift>("get_quickshift", new { shift_id = quickshift_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return shift;
        }

        public static List<Staff> get_quickschedulestaff(string quickshift_id)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_quickschedulestaff", new { shift_id = quickshift_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }

        internal static List<QuickShift> get_quickshiftsstaffPDF(string shifts_id)
        {
            List<QuickShift> shifts = new List<QuickShift>();
            using (var cn = GetOpenConnection())
            {
                shifts = cn.Query<QuickShift>("get_quickshiftbyids", new { shift_ids = shifts_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            foreach (QuickShift shift in shifts)
            {
                List<Staff> staff = new List<Staff>();
                using (var cn = GetOpenConnection())
                {
                    staff = cn.Query<Staff>("get_quickschedulestaff", new { shift_id = shift.quickshift_id }, commandType: CommandType.StoredProcedure).ToList();
                }
                shift.staff_list = staff;
            }
            return shifts;
        }

        //public static Staff get_staff(string id)
        //{
        //    Staff staff = new Staff();
        //    using (lidEntities dbo = new lidEntities())
        //    {
        //        b_staff _bstaff = dbo.b_staff.Where(m => m.staff_id.ToString() == id).FirstOrDefault();
        //        if (_bstaff != null)
        //        {
        //            staff.staff_id = _bstaff.staff_id.ToString();
        //            staff.staff_name = _bstaff.staff_name;
        //            staff.staff_surname = _bstaff.staff_surname;
        //            staff.staff_number = _bstaff.staff_number;
        //            staff.staff_idnumber = _bstaff.staff_idnumber;
        //            staff.staff_mobile = _bstaff.staff_mobile;
        //            staff.staff_telephone = _bstaff.staff_telephone;
        //            staff.staff_telephonehome = _bstaff.staff_hometelephone;
        //            staff.staff_email = _bstaff.staff_email;
        //            staff.photo = _bstaff.photo;
        //            staff.emp_date = _bstaff.emp_date.ToDateTimeOrDefault();

        //            staffdept_link _staffdept = dbo.staffdept_link.Where(m => m.staff_id == _bstaff.staff_id).FirstOrDefault();
        //            if (_staffdept != null) {
        //                b_position _position = dbo.b_position.Where(m => m.position_id == _staffdept.post_id).FirstOrDefault();
        //                staff.max_hours = _position.max_hours;
        //            }


        //            staff.department_id = _staffdept.dept_id.ToString();
        //            staff.position_id = _staffdept.post_id.ToString();
        //            staff.title_id = _bstaff.staff_title.ToString();
        //            staff.emp_type_id = _bstaff.emp_type.ToString();
        //            staff.department = ((Dept)get_depatment(_staffdept.dept_id.ToString())).dept_name;
        //            staff.dept_code = get_depatmentcode(_staffdept.dept_id.ToString());
        //            staff.position = ((Position)get_position(_staffdept.post_id.ToString())).position_name;
        //            staff.title = get_title(_bstaff.staff_title.ToString());
        //            staff.emp_type = get_emptype(_bstaff.emp_type.ToString());
        //        }
        //    }
        //    return staff;
        //}



        internal static List<b_title> get_titles()
        {
            List<b_title> _list = new List<b_title>();
            using (lidEntities dbo = new lidEntities())
            {
                _list = dbo.b_title.ToList();
            }
            return _list;
        }

        internal static string get_title(string id)
        {
            string _desc = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_title _list = dbo.b_title.Where(m => m.title_id.ToString() == id).FirstOrDefault();
                if (_list != null)
                    _desc = _list.title_name;
            }
            return _desc;
        }

       internal static List<b_roles> get_roles()
        {
            List<b_roles> list = new List<b_roles>();
            using (lidEntities dbo = new lidEntities())
            {
                list = dbo.b_roles.Where(x => x.isactive == true).ToList();
            }
            return list;
        }

        internal static Dept get_depatment(string id)
        {
            Dept list = new Dept();
            using (lidEntities dbo = new lidEntities())
            {
                b_structure _blist = dbo.b_structure.Where(m => m.structure_id.ToString() == id).FirstOrDefault();
                list.dept_id = _blist.structure_id.ToString();
                list.dept_name = _blist.structure_name;
                list.dept_code = _blist.structure_code;
            }
            return list;
        }
        internal static string get_depatmentcode(string id)
        {
            string _desc = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_structure _list = dbo.b_structure.Where(m => m.structure_id.ToString() == id).FirstOrDefault();
                _desc = _list.structure_code;
            }
            return _desc;
        }

        public static List<Position> get_positions()
        {
            List<Position> list = new List<Position>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_position> blist = dbo.b_position.ToList();
                foreach (b_position _blist in blist)
                {
                    Position _list = new Position();
                    _list.position_id = _blist.position_id.ToString();
                    _list.position_name = _blist.position_name;
                    _list.position_code = _blist.position_code;
                    _list.min_hours = _blist.min_hours;
                    _list.max_hours = _blist.max_hours;
                    list.Add(_list);
                }
            }
            return list;
        }

        public static List<Position> get_positionswithstaff()
        {
            List<Position> list = new List<Position>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_position> blist = dbo.b_position.Where(x => dbo.staffdept_link.Select(m => m.post_id).Any(b => b.Value == x.position_id)).ToList();
                foreach (b_position _blist in blist)
                {
                    Position _list = new Position();
                    _list.position_id = _blist.position_id.ToString();
                    _list.position_name = _blist.position_name;
                    _list.position_code = _blist.position_code;
                    _list.min_hours = _blist.min_hours;
                    _list.max_hours = _blist.max_hours;
                    list.Add(_list);
                }
            }
            return list;
        }

        public static string get_staffcountonshiftperdaybypos(string shift_id, string pos_id, string weekday)
        {
            string _count = "0";
            using (lidEntities dbo = new lidEntities())
            {
                var stafflink = dbo.staffdept_link.Where(m => m.post_id.ToString() == pos_id).Select(m => m.staff_id).ToList();
                if (stafflink != null)
                {
                    List<b_schedule> bschedule = dbo.b_schedule.Where(m => m.weekday == weekday && m.deleted == false && m.shift_id.ToString() == shift_id && stafflink.Any(x => m.staff_id == x)).ToList();
                    if (bschedule != null)
                    {
                        _count = bschedule.Count.ToString();
                    }
                }
            }
            return _count;
        }


        public static List<Staff> get_staffonshiftperdaybypos(string dept_id, string pos_id)
        {
            List<Staff> staff = new List<Staff>();
            using (lidEntities dbo = new lidEntities())
            {
                string weekday = DateTime.Today.ToString();
                var stafflink = dbo.staffdept_link.Where(m => m.post_id.ToString() == pos_id && m.dept_id.ToString() == dept_id).Select(m => m.staff_id).ToList();
                if (stafflink != null)
                {
                    List<b_schedule> bschedule = dbo.b_schedule.Where(m => m.weekday == weekday && m.deleted == false && stafflink.Any(x => m.staff_id == x)).ToList();
                    foreach (b_schedule schedule in bschedule)
                    {
                        Staff _staff = get_staffshiftlite(schedule.staff_id.ToString(), weekday);
                        staff.Add(_staff);
                    }
                }
            }
            return staff;
        }

        public static List<Staff> get_staffoffshiftperdaybypos(string dept_id, string pos_id)
        {
            List<Staff> staff = new List<Staff>();
            using (lidEntities dbo = new lidEntities())
            {
                string weekday = DateTime.Today.ToString();
                var stafflink = dbo.staffdept_link.Where(m => m.post_id.ToString() == pos_id && m.dept_id.ToString() == dept_id).Select(m => m.staff_id).ToList();
                if (stafflink != null)
                {
                    //List<b_schedule> bschedule = dbo.b_schedule.Where(m => m.weekday == weekday && m.deleted == false && stafflink.Any(x => m.staff_id == x)).ToList();
                    foreach (var schedule in stafflink)
                    {
                        Staff _staff = get_staffshiftlite(schedule.Value.ToString(), weekday);
                        if (_staff.curr_shift == null)
                        {
                            staff.Add(_staff);
                        }
                    }
                }
            }
            return staff;
        }

        internal static List<CompanyModel> get_companies()
        {
            List<CompanyModel> company = new List<CompanyModel>();
            using (lidEntities dbo = new lidEntities())
            {
                List<lid_company> _comp = dbo.lid_company.ToList();
                foreach (lid_company comp in _comp)
                {
                    company.Add(new CompanyModel
                    {
                        company_id = comp.company_id,
                        company_name = comp.company_name
                    });
                }
            }
            return company;
        }

        public static string get_dashdeptposcount(string dept_id, string pos_id, string weekday)
        {
            string _count = "0";
            using (lidEntities dbo = new lidEntities())
            {
                var stafflink = dbo.staffdept_link.Where(m => m.post_id.ToString() == pos_id && m.dept_id.ToString() == dept_id).Select(m => m.staff_id).ToList();
                if (stafflink != null)
                {
                    var weekdays = ((List<WeekDays>)get_weekdaysbydt(weekday)).Select(m => m.day_name);
                    List<b_schedule> bschedule = dbo.b_schedule.Where(m => weekdays.Any(x => m.weekday == x) && m.deleted == false && stafflink.Any(x => m.staff_id == x)).ToList();
                    if (bschedule != null)
                    {
                        _count = bschedule.Count.ToString();
                    }
                }
            }
            return _count;
        }
        public static string get_staffcountonbypos(string pos_id)
        {
            string _count = "0";
            using (lidEntities dbo = new lidEntities())
            {
                List<staffdept_link> stafflink = dbo.staffdept_link.Where(m => m.post_id.ToString() == pos_id).ToList();
                if (stafflink != null)
                {
                    _count = stafflink.Count.ToString();
                }
            }
            return _count;
        }

        internal static List<Users> get_userinroles(long role_id)
        {
            List<Users> users = new List<Users>();
            using (lidEntities dbo = new lidEntities())
            {
                long _role_id = role_id.ToLongOrDefault(0);
                List<b_users> _users = dbo.b_users.Where(m => m.role_id == _role_id).ToList();
                foreach (b_users _user in _users)
                {
                    users.Add(new Users
                    {
                        username = _user.email,
                        name = _user.name,
                        surname = _user.surname,
                        telephone = _user.telephone,
                        isStaff = (_user.staff_id != null) ? "Is Staff" : "Not Staff",
                    });
                }
            }
            return users;
        }

        internal static WorkArea get_workareabyid(long id)
        {
            WorkArea results = new WorkArea();
            using (var cn = GetOpenConnection())
            {
                results = cn.Query<WorkArea>("get_workareabyid", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return results;
        }
        internal static List<WorkArea> get_workareas()
        {
            List<WorkArea> results = new List<WorkArea>();
            using (var cn = GetOpenConnection())
            {
                results = cn.Query<WorkArea>("get_workareas", commandType: CommandType.StoredProcedure).ToList();
            }
            return results;
        }

        internal static Position get_position(string id)
        {
            Position list = new Position();
            using (lidEntities dbo = new lidEntities())
            {
                b_position _list = dbo.b_position.Where(m => m.position_id.ToString() == id).FirstOrDefault();
                list.position_id = _list.position_id.ToString();
                list.position_name = _list.position_name;
                list.position_code = _list.position_code;
                list.min_hours = _list.min_hours;
                list.max_hours = _list.max_hours;
            }
            return list;
        }

        public static string getDeptname(long dept_id)
        {
            string dept_name = "All Department";
            using (lidEntities dbo = new lidEntities())
            {
                b_structure department = dbo.b_structure.Where(m => m.structure_id == dept_id).FirstOrDefault();
                if (department != null)
                {
                    dept_name = department.structure_name;
                }
            }
            return dept_name;
        }
        internal static void del_userfromrole(string username, string role_id)
        {
            using (lidEntities dbo = new lidEntities())
            {
                b_users user = dbo.b_users.Where(m => m.email == username).FirstOrDefault();
                user.role_id = null;
                dbo.Entry(user).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();
            }
        }

        internal static List<b_employementtype> get_emptypes()
        {
            List<b_employementtype> _list = new List<b_employementtype>();
            using (lidEntities dbo = new lidEntities())
            {
                _list = dbo.b_employementtype.ToList();
            }
            return _list;
        }

        internal static string get_emptype(string id)
        {
            string _desc = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_employementtype _list = dbo.b_employementtype.Where(m => m.type_id.ToString() == id).FirstOrDefault();
                _desc = _list.type_name;
            }
            return _desc;
        }
        //internal static string get_emptypecode(string id)
        //{
        //    string _desc = "";
        //    using (lidEntities dbo = new lidEntities())
        //    {
        //        b_employementtype _list = dbo.b_employementtype.Where(m => m.type_id.ToString() == id).FirstOrDefault();
        //        _desc = _list.type_code;
        //    }
        //    return _desc;
        //}

        internal static List<OrganisationStructure> get_organisationstructure()
        {
            List<OrganisationStructure> res = new List<OrganisationStructure>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<OrganisationStructure>("get_structure", commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }

        internal static Structure get_structurebyid(string dept_id)
        {
            Structure res = new Structure();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<Structure>("get_structurebyid", new { dept_id = dept_id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return res;
        }
        internal static List<MonthWeek> get_weeks()
        {
            List<MonthWeek> list = new List<MonthWeek>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<MonthWeek>("get_MonthWeeks", commandType: CommandType.StoredProcedure).ToList();
            }
            //using (lidEntities db = new lidEntities())
            //{
            //    var weeks = db.get_MonthWeeks().ToList();

            //    foreach (var _weeks in weeks)
            //    {
            //        MonthWeek _list = new MonthWeek();
            //        _list.week_no = _weeks.WeekNo.ToString();
            //        _list.week_name = _weeks.WeekDays;
            //        list.Add(_list);
            //    }
            //}
            return list;
        }

        public static List<WeekDays> get_weekdays(int _weekinx)
        {
            List<WeekDays> weekDays = new List<WeekDays>();
            using (var cn = GetOpenConnection())
            {
                weekDays = cn.Query<WeekDays>("get_WeekDays", new { WeekNo = _weekinx }, commandType: CommandType.StoredProcedure).ToList();
            }
            //using (lidEntities dbo = new lidEntities())
            //{
            //    var list = dbo.get_WeekDays(_weekinx).ToList();
            //    foreach (var _list in list)
            //    {
            //        WeekDays _weekDays = new WeekDays();
            //        _weekDays.day_name = _list;
            //        weekDays.Add(_weekDays);
            //    }

            //}
            return weekDays;
        }

        internal static List<CodeTypes> get_defaultdatatypes()
        {
            List<CodeTypes> codeTypes = new List<CodeTypes>();
            using (lidEntities dbo = new lidEntities())
            {
                int inx = 1;
                var list = dbo.b_codevalues.Select(x => x.Code).Distinct().ToList();
                foreach (var _list in list)
                {
                    CodeTypes _codeTypes = new CodeTypes();
                    _codeTypes.code_id = inx.ToString();
                    _codeTypes.code_name = _list;
                    codeTypes.Add(_codeTypes);
                    inx = inx + 1;
                }
            }
            return codeTypes;
        }

        internal static StructureLevels get_structurelevel(string level_id)
        {
            StructureLevels level = new StructureLevels();
            using (lidEntities dbo = new lidEntities())
            {
                long id = level_id.ToLongOrDefault(0);
                b_structurelevels _level = dbo.b_structurelevels.Where(x => x.LevelId == id).FirstOrDefault();
                if (_level != null)
                {
                    level.level_id = _level.LevelId;
                    level.level_code = _level.LevelCode;
                    level.level_name = _level.LevelName;
                    level.level_order = _level.LevelOrder.ToInt16OrDefault(0);
                }
            }
            return level;
        }

        internal static List<StructureLevels> get_structurelevels()
        {
            List<StructureLevels> levels = new List<StructureLevels>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_structurelevels> _levels = dbo.b_structurelevels.ToList();
                foreach (b_structurelevels _level in _levels)
                {
                    levels.Add(new StructureLevels
                    {
                        level_id = _level.LevelId,
                        level_code = _level.LevelCode,
                        level_name = _level.LevelName,
                        level_order = _level.LevelOrder.ToInt16OrDefault(0),
                    });
                }
            }
            return levels;
        }

        public static List<WeekDays> get_weekdaysbydt(string _startdate)
        {
            List<WeekDays> weekDays = new List<WeekDays>();

            //DateTime startdate = new DateTime();

            //if (DateTime.TryParse(_startdate, out startdate))
            //{
            //    using (lidEntities dbo = new lidEntities())
            //    {
            //        var list = dbo.get_WeekDaysByDt(startdate).ToList();
            //        foreach (var _list in list)
            //        {
            //            WeekDays _weekDays = new WeekDays();
            //            _weekDays.day_name = _list;
            //            weekDays.Add(_weekDays);
            //        }

            //    }
            //}


            using (var cn = GetOpenConnection())
            {
                DateTime startdate = new DateTime();

                if (DateTime.TryParse(_startdate, out startdate))
                {
                    weekDays = cn.Query<WeekDays>("get_WeekDaysByDt", new { Date = startdate }, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            return weekDays;
        }

        public static List<WeekDays> get_Weekdaysbyinx(string startdate, string weekno)
        {
            List<WeekDays> weekDays = new List<WeekDays>();

            using (var cn = GetOpenConnection())
            {
                DateTime _startdate = new DateTime();

                if (DateTime.TryParse(startdate, out _startdate))
                {
                    weekDays = cn.Query<WeekDays>("get_Weekdaysbyinx", new { startdate = startdate, weekno = weekno }, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            return weekDays;
        }
        internal static object get_lookupd()
        {
            throw new NotImplementedException();
        }

        public static List<WeekDays> get_monthdaysbydt(string _startdate)
        {
            List<WeekDays> weekDays = new List<WeekDays>();
            using (var cn = GetOpenConnection())
            {
                weekDays = cn.Query<WeekDays>("get_MonthDaysByDt", new { month = _startdate }, commandType: CommandType.StoredProcedure).ToList();
            }
            //using (lidEntities dbo = new lidEntities())
            //{
            //    var list = dbo.get_MonthDaysByDt(_startdate).ToList();
            //    foreach (var _list in list)
            //    {
            //        WeekDays _weekDays = new WeekDays();
            //        _weekDays.day_name = _list.Date.ToString();
            //        _weekDays.day_code = _list.Day.ToString();
            //        weekDays.Add(_weekDays);
            //    }
            //}
            return weekDays;
        }

        public static List<quickStaffStats> get_staffquickstats(string staff_id, string startday)
        {
            List<quickStaffStats> stats = new List<quickStaffStats>();
            using (var cn = GetOpenConnection())
            {
                string _startday = Convert.ToDateTime(startday).ToString();
                stats = cn.Query<quickStaffStats>("get_staffquickstats", new { staff_id = staff_id, Date = _startday }, commandType: CommandType.StoredProcedure).ToList();
            }
            return stats;
        }

        public static StaffWeeklyStats get_staffweeklyhoursstats(string staff_id, string startday)
        {
            StaffWeeklyStats stats = new StaffWeeklyStats();
            using (var cn = GetOpenConnection())
            {
                string _startday = Convert.ToDateTime(startday).ToString();
                stats = cn.Query<StaffWeeklyStats>("get_staffweeklyhoursstats", new { staff_id = staff_id, Date = _startday }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return stats;
        }
        public static StaffHours get_staffweektothours(string staff_id, string startday)
        {
            StaffHours staffHours = new StaffHours();
            using (var cn = GetOpenConnection())
            {
                string _startday = Convert.ToDateTime(startday).ToString();
                StaffHours _staffHours = cn.Query<StaffHours>("get_staffweeklyhours", new { staff_id = staff_id, Date = _startday }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                staffHours.non_billable_hours = (_staffHours.non_billable_hours == null) ? "00:00" : _staffHours.non_billable_hours;
                staffHours.tot_hours = (_staffHours.tot_hours == null) ? "00:00" : _staffHours.tot_hours;
                staffHours.worked_hours = (_staffHours.worked_hours == null) ? "00:00" : _staffHours.worked_hours;
                staffHours.remaining_hours = get_timediff_incr(staffHours.tot_hours, staffHours.worked_hours);


            }
            return staffHours;
        }

        public static string get_stafftotalhours_ghost(string staff_id, string shift_id, string startday)
        {
            string tothours = "00:00";
            using (lidEntities dbo = new lidEntities())
            {
                List<WeekDays> weekdays = get_weekdaysbydt(startday);
                long _shift_id = util.parselong(shift_id);
                tothours = NewMethod(_shift_id, tothours);
                foreach (WeekDays _weekdays in weekdays)
                {
                    DateTime dt = Convert.ToDateTime(_weekdays.day_name);
                    string weekday = dt.ToString();
                    //string _tothours = "00:00";
                    b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false).FirstOrDefault();
                    if (schedule != null)
                    {
                        _shift_id = util.parselong(schedule.shift_id);
                        tothours = NewMethod(_shift_id, tothours);
                    }

                }
                //hrs = _hrs;
            }
            return tothours;
        }

        private static string NewMethod(long shift_id, string tothours)
        {
            string _tothours = "00:00";

            using (lidEntities dbo = new lidEntities())
            {
                b_shift _bshift = dbo.b_shift.Where(m => m.shift_id == shift_id).FirstOrDefault();
                if (_bshift != null)
                {
                    string _from = ((TimeSpan)_bshift.shift_from).ToString(@"hh\:mm");
                    string _to = ((TimeSpan)_bshift.shift_to).ToString(@"hh\:mm");
                    if ((Boolean)_bshift.is_split)
                    {
                        _to = ((TimeSpan)_bshift.split_to).ToString(@"hh\:mm");
                    }


                    if (_bshift.is_split == true)
                    {
                        _tothours = get_timediff(_from, _to, _bshift.non_billable.ToString());
                    }
                    else
                    {
                        _tothours = get_timediff(_from, _to, _bshift.non_billable.ToString());
                    }
                    _tothours = get_timediff_incr(tothours, _tothours);
                }
            }
            return _tothours;
        }

        internal static string get_staff_workingdays(string staff_id)
        {
            string workingdays = "";
            using (var cn = GetOpenConnection())
            {
                workingdays = cn.Query<string>("get_staff_workingdays", new { staff_id = staff_id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return workingdays;
        }

        internal static string get_staff_workarea(string staff_id)
        {
            string workarea = "";
            using (var cn = GetOpenConnection())
            {
                workarea = cn.Query<string>("get_staff_workarea", new { staff_id = staff_id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return workarea;
        }

        public static List<Shift> get_staffshiftbyday(string staff_id, string day, string schedule_type)
        {
            List<Shift> _shifts = new List<Shift>();
            using (lidEntities dbo = new lidEntities())
            {
                DateTime dt = Convert.ToDateTime(day);
                b_leave _leave = (schedule_type == "Staff") ? dbo.b_leave.Where(m => m.staff_id.ToString() == staff_id && m.leave_from <= dt && m.leave_to >= dt).FirstOrDefault() : null;
                if (_leave == null)
                {
                    Shift _shift = new Shift();
                    string weekday = dt.ToString();
                    List<b_schedule> schedules = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false && m.schedule_type == schedule_type).ToList();
                    foreach (b_schedule schedule in schedules)
                    {
                        bool adhoc = schedule.is_adhoc.ToBooleanOrDefault(false);
                        if (adhoc)
                            _shift = get_adhocshift(schedule.shift_id.ToString());
                        else
                            _shift = get_shift(schedule.shift_id.ToString());

                        _shift.shift_approved = schedule.shift_approved;
                        _shift.rej_reason = schedule.rej_reason;
                        _shift.rej_by = schedule.approved_by;
                        _shift.is_adhoc = schedule.is_adhoc.ToBooleanOrDefault(false);
                        _shift.schedule_id = schedule.schedule_id;
                        _shifts.Add(_shift);
                    }
                }
                else
                {
                    Shift _shift = new Shift();
                    _shift.leave_name = get_leavetype(_leave.leave_type.ToString());
                    _shift.staff_onleave = true;
                    _shifts.Add(_shift);
                }
            }
            return _shifts;
        }
        public static List<Shift> approved_staffshiftbyday(string staff_id, string day, string schedule_type)
        {
            List<Shift> _shifts = new List<Shift>();
            using (lidEntities dbo = new lidEntities())
            {
                DateTime dt = Convert.ToDateTime(day);
                b_leave _leave = (schedule_type == "Staff") ? dbo.b_leave.Where(m => m.staff_id.ToString() == staff_id && m.leave_from <= dt && m.leave_to >= dt).FirstOrDefault() : null;
                if (_leave == null)
                {
                    Shift _shift = new Shift();
                    string weekday = dt.ToString();
                    List<b_schedule> schedules = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false && m.schedule_type == schedule_type).ToList();
                    foreach (b_schedule schedule in schedules)
                    {
                        bool adhoc = schedule.is_adhoc.ToBooleanOrDefault(false);
                        if (adhoc)
                            _shift = get_adhocshift(schedule.shift_id.ToString());
                        else
                            _shift = get_shift(schedule.shift_id.ToString());

                        _shift.shift_approved = schedule.shift_approved;
                        _shift.rej_reason = schedule.rej_reason;
                        _shift.rej_by = schedule.approved_by;
                        _shift.is_adhoc = schedule.is_adhoc.ToBooleanOrDefault(false);
                        _shift.schedule_id = schedule.schedule_id;
                        _shifts.Add(_shift);
                    }
                }
                else
                {
                    Shift _shift = new Shift();
                    _shift.leave_name = get_leavetype(_leave.leave_type.ToString());
                    _shift.staff_onleave = true;
                    _shifts.Add(_shift);
                }
            }
            return _shifts;
        }

        public static Staff get_staffById(long id)
        {
            Staff staff = new Staff();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_staffById", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return staff;
        }
        public static List<Staff> get_staffListById(long id)
        {
            List<Staff> staff = new List<Staff>();
            using (var cn = GetOpenConnection())
            {
                staff = cn.Query<Staff>("get_staffById", new { id = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return staff;
        }
        public static Users get_userByUsername(string username)
        {
            Users user = new Users();
            using (var cn = GetOpenConnection())
            {
                Users _user = cn.Query<Users>("get_userByUsername", new { username = username }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_user != null)
                    user = _user;
            }
            return user;
        }
        public static List<ShiftComments> get_shiftcomments(string schedule_id)
        {
            List<ShiftComments> _comms = new List<ShiftComments>();
            using (var cn = GetOpenConnection())
            {
                _comms = cn.Query<ShiftComments>("get_staffshiftcomms", new { schedule_id = schedule_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return _comms;
        }

        public static Shift get_staffshiftbyday_appr(string staff_id, string day)
        {
            Shift _shift = new Shift();

            return _shift;
        }
        public static List<Shift> get_shifts(string username)
        {
            List<Shift> shift = new List<Shift>();
            using (lidEntities dbo = new lidEntities())
            {
                var _bool = get_config_shiftdeptbased();
                List<b_shift> bshifts = new List<b_shift>();
                if (_bool)
                {
                    long dept = get_userdept(username);
                    bshifts = dbo.b_shift.Where(x => x.dept_id == dept).ToList();
                }
                else
                {
                    bshifts = dbo.b_shift.ToList();
                }
                foreach (b_shift _bshift in bshifts)
                {
                    Shift _shift = new Shift();
                    _shift.shift_id = _bshift.shift_id;
                    _shift.shift_code = _bshift.shift_code;
                    _shift.shift_name = _bshift.shift_name;
                    _shift.shift_code = _bshift.shift_code;
                    _shift.is_split = _bshift.is_split.ToBooleanOrDefault();
                    _shift.shift_from = ((TimeSpan)_bshift.shift_from).ToString(@"hh\:mm");
                    _shift.shift_to = ((TimeSpan)_bshift.shift_to).ToString(@"hh\:mm");
                    _shift.minutes = _bshift.non_billable.ToString();
                    _shift.dept_name = getDeptname(_bshift.dept_id.ToLongOrDefault());
                    if ((Boolean)_bshift.is_split)
                    {
                        _shift.split_from = ((TimeSpan)_bshift.split_from).ToString(@"hh\:mm");
                        _shift.split_to = ((TimeSpan)_bshift.split_to).ToString(@"hh\:mm");
                    }

                    //_shift.billable = _bshift.non_billable.ToString();
                    string _billable = "", _period = "", _period_sm = "", _tothours = "";

                    _period = _bshift.shift_from.ToString() + " to " + _bshift.shift_to.ToString();
                    if (_bshift.is_split == true)
                    {
                        _billable = get_timediff(_bshift.shift_from.ToString(), _bshift.split_to.ToString(), _bshift.non_billable.ToString());
                        _tothours = get_timediff(_bshift.shift_from.ToString(), _bshift.split_to.ToString());
                        _period += "<br/>" + _shift.split_from + " to " + _shift.split_to;
                        _period_sm = _shift.shift_from + " to " + _shift.split_to;
                    }
                    else
                    {
                        _billable = get_timediff(_bshift.shift_from.ToString(), _bshift.shift_to.ToString(), _bshift.non_billable.ToString());
                        _tothours = get_timediff(_bshift.shift_from.ToString(), _bshift.shift_to.ToString());

                        _period_sm = _shift.shift_from + " to " + _shift.shift_to;
                    }
                    _shift.period = _period;
                    _shift.period_sm = _period_sm;
                    _shift.billable = _billable;
                    _shift.tothours = _tothours;

                    shift.Add(_shift);
                }
            }
            return shift;
        }

        public static string get_timediff(string _from, string _to, string min)
        {
            TimeSpan time_diff = DateTime.Parse(_to.ToString()).Subtract(DateTime.Parse(_from.ToString()));
            int tot_min = Convert.ToInt32(time_diff.TotalMinutes - Convert.ToInt32(min));

            TimeSpan time_span = TimeSpan.FromMinutes(tot_min);
            return time_span.ToString(@"hh\:mm");
        }


        public static string get_timediff(string _from, string _to)
        {
            TimeSpan time_diff = DateTime.Parse(_to.ToString()).Subtract(DateTime.Parse(_from.ToString()));
            int tot_min = Convert.ToInt32(time_diff.TotalMinutes);

            TimeSpan time_span = TimeSpan.FromMinutes(tot_min);
            return time_span.ToString(@"hh\:mm");
        }

        public static string get_timediff_incr(string _from, string _to)
        {
            _to = (_to == "") ? "00:00" : _to;
            int tot_min = Convert.ToInt32(_from.Split(':')[0]) * 60 + Convert.ToInt32(_from.Split(':')[1]) + Convert.ToInt32(_to.Split(':')[0]) * 60 + Convert.ToInt32(_to.Split(':')[1]);
            string _hrs = (tot_min / 60).ToString();
            string _min = (tot_min % 60).ToString();
            return _hrs + ":" + ((_min.Length == 1) ? "0" + _min : _min);
        }

        public static string get_mindiff_incr(string _from, string _to)
        {
            _to = (_to == "") ? "00:00" : "00:" + _to;
            int tot_min = Convert.ToInt32(_from.Split(':')[0]) * 60 + Convert.ToInt32(_from.Split(':')[1]) + Convert.ToInt32(_to.Split(':')[0]) * 60 + Convert.ToInt32(_to.Split(':')[1]);
            string _hrs = (tot_min / 60).ToString();
            string _min = (tot_min % 60).ToString();
            return _hrs + ":" + ((_min.Length == 1) ? "0" + _min : _min);
        }

        internal static Shift get_shift(string shift_id)
        {
            Shift _shift = new Shift();
            using (lidEntities dbo = new lidEntities())
            {
                b_shift _bshift = dbo.b_shift.Where(m => m.shift_id.ToString() == shift_id).FirstOrDefault();
                if (_bshift != null)
                {
                    _shift.shift_id = _bshift.shift_id;
                    _shift.shift_code = _bshift.shift_code;
                    _shift.shift_name = _bshift.shift_name;
                    _shift.shift_code = _bshift.shift_code;
                    _shift.dept_id = _bshift.dept_id.ToLongOrDefault(0);
                    _shift.is_split = _bshift.is_split.ToBooleanOrDefault();
                    _shift.non_billable = _bshift.non_billable.ToString();
                    _shift.shift_from = ((TimeSpan)_bshift.shift_from).ToString(@"hh\:mm");
                    _shift.shift_to = ((TimeSpan)_bshift.shift_to).ToString(@"hh\:mm");
                    _shift.minutes = _bshift.non_billable.ToString();
                    if ((Boolean)_bshift.is_split)
                    {
                        _shift.split_from = ((TimeSpan)_bshift.split_from).ToString(@"hh\:mm");
                        _shift.split_to = ((TimeSpan)_bshift.split_to).ToString(@"hh\:mm");
                    }

                    //_shift.billable = _bshift.non_billable.ToString();
                    string _billable = "", _period = "", _period_sm = "", _tothours = "";

                    _period = _bshift.shift_from.ToString() + " to " + _bshift.shift_to.ToString();
                    if (_bshift.is_split == true)
                    {
                        _billable = get_timediff(_bshift.shift_from.ToString(), _bshift.split_to.ToString(), _bshift.non_billable.ToString());
                        _tothours = get_timediff(_bshift.shift_from.ToString(), _bshift.split_to.ToString());
                        _period += "<br/>" + _shift.split_from + " to " + _shift.split_to;
                        _period_sm = _shift.shift_from + " to " + _shift.split_to;
                    }
                    else
                    {
                        _billable = get_timediff(_bshift.shift_from.ToString(), _bshift.shift_to.ToString(), _bshift.non_billable.ToString());
                        _tothours = get_timediff(_bshift.shift_from.ToString(), _bshift.shift_to.ToString());

                        _period_sm = _shift.shift_from + " to " + _shift.shift_to;
                    }
                    _shift.period = _period;
                    _shift.period_sm = _period_sm;
                    _shift.billable = _billable;
                    _shift.tothours = _tothours;
                }
            }
            return _shift;
        }
        internal static Shift get_adhocshift(string shift_id)
        {
            Shift _shift = new Shift();
            using (lidEntities dbo = new lidEntities())
            {
                b_adhocshift _bshift = dbo.b_adhocshift.Where(m => m.shift_id.ToString() == shift_id).FirstOrDefault();
                if (_bshift != null)
                {
                    _shift.shift_id = _bshift.shift_id;
                    _shift.shift_code = _bshift.shift_code.ToUpper();
                    _shift.shift_name = _bshift.shift_name;
                    _shift.is_split = false;
                    _shift.shift_from = ((TimeSpan)_bshift.shift_from).ToString(@"hh\:mm");
                    _shift.shift_to = ((TimeSpan)_bshift.shift_to).ToString(@"hh\:mm");
                    _shift.minutes = _bshift.non_billable.ToString();

                    string _billable = "", _period = "", _period_sm = "", _tothours = "";

                    _period = _bshift.shift_from.ToString() + " to " + _bshift.shift_to.ToString();

                    _billable = get_timediff(_bshift.shift_from.ToString(), _bshift.shift_to.ToString(), _bshift.non_billable.ToString());
                    _tothours = get_timediff(_bshift.shift_from.ToString(), _bshift.shift_to.ToString());

                    _period_sm = _shift.shift_from + " to " + _shift.shift_to;
                    _shift.period = _period;
                    _shift.period_sm = _period_sm;
                    _shift.billable = _billable;
                    _shift.tothours = _tothours;

                }
            }
            return _shift;
        }

        internal static List<Leave> get_activeleave(bool? flag)
        {
            List<Leave> leave = new List<Leave>();

            //using (lidEntities dbo = new lidEntities())
            //{
            //    List<b_leave> bleave = dbo.b_leave.Where(m => m.leave_approved == flag).ToList();
            //    foreach (b_leave _bleave in bleave)
            //    {
            //        Leave _leave = new Leave();
            //        Staff _staff = get_staff(_bleave.staff_id.ToString());
            //        _leave.leave_id = _bleave.leave_id.ToString();
            //        _leave.staff_fullname = _staff.staff_name + " " + _staff.staff_surname;
            //        _leave.staff_department = _staff.department;
            //        _leave.staff_photo = _staff.photo;
            //        _leave.leave_type = get_leavetype(_bleave.leave_type.ToString());
            //        _leave.leave_approved = Convert.ToBoolean(_bleave.leave_approved);
            //        _leave.leave_from = Convert.ToDateTime(_bleave.leave_from);
            //        _leave.leave_to = Convert.ToDateTime(_bleave.leave_to);
            //        _leave.leave_days = get_leavedays(Convert.ToDateTime(_bleave.leave_from), Convert.ToDateTime(_bleave.leave_to));
            //        _leave.staff_number = _staff.staff_number;
            //        leave.Add(_leave);
            //    }
            //}

            using (var cn = GetOpenConnection())
            {
                leave = cn.Query<Leave>("get_pendingleaverequest", commandType: CommandType.StoredProcedure).ToList();
            }
            return leave;
        }

        internal static List<Leave> get_activeleavebytype(string leavetype, bool? flag)
        {
            List<Leave> leave = new List<Leave>();

            using (lidEntities dbo = new lidEntities())
            {
                List<b_leave> bleave = dbo.b_leave.Where(m => m.leave_type.ToString() == leavetype && m.leave_approved == flag).ToList();
                foreach (b_leave _bleave in bleave)
                {
                    Leave _leave = new Leave();
                    Staff _staff = get_staffById(_bleave.staff_id.ToLongOrDefault(0));
                    _leave.leave_id = _bleave.leave_id.ToString();
                    _leave.staff_fullname = _staff.staff_name + " " + _staff.staff_surname;
                    _leave.staff_department = _staff.department;
                    _leave.leave_type = get_leavetype(_bleave.leave_type.ToString());
                    _leave.leave_approved = Convert.ToBoolean(_bleave.leave_approved);
                    _leave.leave_from = Convert.ToDateTime(_bleave.leave_from);
                    _leave.leave_to = Convert.ToDateTime(_bleave.leave_to);
                    _leave.leave_days = get_leavedays(Convert.ToDateTime(_bleave.leave_from), Convert.ToDateTime(_bleave.leave_to));
                    _leave.staff_number = _staff.staff_number;
                    leave.Add(_leave);
                }
            }
            return leave;
        }

        private static string get_leavedays(DateTime _from, DateTime _to)
        {
            return (_to - _from).TotalDays.ToString();
        }

        private static string get_leavetype(string leave_type)
        {
            string leave_name = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_leavetypes bleavetype = dbo.b_leavetypes.Where(m => m.leavetype_id.ToString() == leave_type).FirstOrDefault();
                if (bleavetype != null)
                    leave_name = bleavetype.leavetype_name;
            }
            return leave_name;
        }

        internal static List<LeaveTypes> get_leavetypes()
        {
            List<LeaveTypes> results = new List<LeaveTypes>();
            using (var cn = GetOpenConnection())
            {
                results = cn.Query<LeaveTypes>("select * from b_leavetypes ").ToList();
            }
            return results;
        }

        public static int get_countshiftstaffbydept(string dept_id, string shift_id, string weekday)
        {
            int tot = 0;

            return tot;
        }

        internal static List<ShiftTrade> get_tradeshift_appr()
        {
            List<ShiftTrade> trade = new List<ShiftTrade>();

            using (var cn = GetOpenConnection())
            {
                trade = cn.Query<ShiftTrade>("get_allshifttrade", commandType: CommandType.StoredProcedure).ToList();
            }
            return trade;
        }
        internal static List<Shift> get_staffshifthistory(long staff_id, long shift_id)
        {
            List<Shift> shift = new List<Shift>();

            using (var cn = GetOpenConnection())
            {
                shift = cn.Query<Shift>("get_staffshifthistory", new { staff_id = staff_id, shift_id = shift_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return shift;
        }

    


        internal static List<Events> get_events()
        {
            List<Events> events = new List<Events>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_event> bevents = dbo.b_event.Where(m => m.isActive == true).ToList();
                foreach (b_event _bevents in bevents)
                {
                    Events _events = new Events();
                    _events.event_id = _bevents.event_id.ToString();
                    _events.event_desc = _bevents.event_desc;
                    _events.isActive = _bevents.isActive;
                    events.Add(_events);
                }
            }
            return events;
        }

        internal static List<Menus> get_avlmenus(long role_id)
        {
            List<Menus> menus = new List<Menus>();
            using (lidEntities dbo = new lidEntities())
            {
                var menus_id = dbo.b_rolemenus.Where(x => x.role_id == role_id).Select(x => x.menu_id).Distinct().ToList();
                List<b_menus> bmenus = dbo.b_menus.Where(m => m.isactive == true).ToList();
                foreach (b_menus bmenu in bmenus)
                {
                    if (menus_id.Any(x => x == bmenu.menu_id)) { }
                    else
                    {
                        menus.Add(new Menus
                        {
                            menu_id = bmenu.menu_id,
                            menu_code = bmenu.menu_code,
                            menu_name = bmenu.menu_name
                        });
                    }
                }
            }
            return menus;
        }

        internal static List<CodeValues> get_avl_cat_group(long comp_id)
        {
            List<CodeValues> avl_cat = new List<CodeValues>();
            using (lidEntities dbo = new lidEntities())
            {
                List<CodeValues> _cats = ItemCore.get_group_category("Category_Group");

                List<lid_company_cat> comp_cats = dbo.lid_company_cat.Where(x => x.company_id == comp_id).ToList();
                foreach (CodeValues _cat in _cats)
                {
                    if (comp_cats.Any(x => x.category_id == _cat.id)) { }
                    else
                    {
                        avl_cat.Add(new CodeValues
                        {
                            id = _cat.id,
                            code = _cat.code,
                            value = _cat.value
                        });
                    }
                }
            }
            return avl_cat;
        }

        internal static List<CodeValues> get_slc_cat_group(long comp_id)
        {
            List<CodeValues> sel_cat = new List<CodeValues>();
            using (lidEntities dbo = new lidEntities())
            {
               

                List<lid_company_cat> comp_cats = dbo.lid_company_cat.Where(x => x.company_id == comp_id).ToList();
                foreach (lid_company_cat comp_cat in comp_cats)
                {
                    b_codevalues _cat = dbo.b_codevalues.Where(x => x.id == comp_cat.category_id).FirstOrDefault();
                    sel_cat.Add(new CodeValues
                        {
                            id = _cat.id,
                            code = _cat.Code,
                        value = _cat.Value
                    });
                }
            }
            return sel_cat;
        }
        internal static List<Menus> get_slctmenus(long role_id)
        {
            List<Menus> menus = new List<Menus>();
            using (lidEntities dbo = new lidEntities())
            {
                var menus_id = dbo.b_rolemenus.Where(x => x.role_id == role_id).Select(x => x.menu_id).Distinct().ToList();
                List<b_menus> bmenus = dbo.b_menus.Where(m => menus_id.Any(x => x.Value == m.menu_id) && m.isactive == true).ToList();
                foreach (b_menus bmenu in bmenus)
                {
                    menus.Add(new Menus
                    {
                        menu_id = bmenu.menu_id,
                        menu_code = bmenu.menu_code,
                        menu_name = bmenu.menu_name
                    });
                }
            }
            return menus;
        }
        internal static string get_deptname(long dept_id)
        {
            string dept_name = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_structure dept = dbo.b_structure.Where(m => m.structure_id == dept_id).FirstOrDefault();
                if (dept != null)
                {
                    dept_name = dept.structure_name;
                }
            }
            return dept_name;
        }
        internal static string get_rolename(long role_id)
        {
            string role_name = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_roles role = dbo.b_roles.Where(m => m.role_id == role_id).FirstOrDefault();
                if (role != null)
                {
                    role_name = role.role_name;
                }
            }
            return role_name;
        }
        internal static Roles get_rolebyid(long role_id)
        {
            Roles role = new Roles();
            using (lidEntities dbo = new lidEntities())
            {
                b_roles _role = dbo.b_roles.Where(m => m.role_id == role_id).FirstOrDefault();
                if (_role != null)
                {
                    role.role_id = _role.role_id;
                    role.role_code = _role.role_code;
                    role.role_name = _role.role_name;
                    role.role_isactive = true;
                    role.role_deptid = _role.role_dept.ToLongOrDefault(0);
                }
            }
            return role;
        }

        public static List<Menus> get_userwebmenus(string username)
        {

            List<Menus> menu = new List<Menus>();
            using (lidEntities dbo = new lidEntities())
            {
                var user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                if (user != null)
                {
                    List<b_menus> _menus = new List<b_menus>();
                    if (user.systemadmin == true)
                    {
                        _menus = dbo.b_menus.Where(x => x.isactive == true && x.parent_id == null).OrderBy(x => x.menu_order).ToList();
                    }
                    else
                    {
                        var menu_ids = dbo.b_rolemenus.Where(x => x.role_id == user.role_id).Select(x => x.menu_id).ToList();
                        _menus = dbo.b_menus.Where(x => menu_ids.Any(i => i == x.menu_id) && x.isactive == true && x.parent_id == null).OrderBy(x => x.menu_order).ToList();
                    }

                    foreach (b_menus _menu in _menus)
                {
                    menu.Add(new Menus
                    {
                        isadmin = user.systemadmin.ToBooleanOrDefault(),
                        menu_id = _menu.menu_id,
                        menu_code = _menu.menu_code,
                        menu_name = _menu.menu_name,
                        menu_action = _menu.menu_action,
                        menu_controller = _menu.menu_controller,
                        menu_icon = _menu.menu_icon,
                        menu_order = _menu.menu_order.ToInt16OrDefault(),
                    });
                }
                }
            }
            return menu;
        }

        public static List<Menus> get_userwebsubmenus(string username, string controller)
        {

            List<Menus> menu = new List<Menus>();
            using (lidEntities dbo = new lidEntities())
            {
                var user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                if (user != null)
                {
                    long parent_id = dbo.b_menus.Where(x => x.menu_controller == controller && x.parent_id != null).Select(x => x.parent_id).FirstOrDefault().ToLongOrDefault();
                    List<b_menus> _menus = new List<b_menus>();
                    if (user.systemadmin == true)
                    {
                        _menus = dbo.b_menus.Where(x => x.isactive == true && x.parent_id == parent_id).OrderBy(x => x.menu_order).ToList();
                    }
                    else
                    {
                        var menu_ids = dbo.b_rolemenus.Where(x => x.role_id == user.role_id).Select(x => x.menu_id).ToList();
                        _menus = dbo.b_menus.Where(x => menu_ids.Any(i => i == x.menu_id) && x.isactive == true && x.parent_id == parent_id).OrderBy(x => x.menu_order).ToList();
                    }

                    foreach (b_menus _menu in _menus)
                    {
                    menu.Add(new Menus
                    {
                        isadmin = user.systemadmin.ToBooleanOrDefault(),
                            menu_id = _menu.menu_id,
                            menu_code = _menu.menu_code,
                            menu_name = _menu.menu_name,
                            menu_action = _menu.menu_action,
                            menu_controller = _menu.menu_controller,
                            menu_icon = _menu.menu_icon,
                            menu_order = _menu.menu_order.ToInt16OrDefault(),
                        });
                    }
                }
            }
            return menu;
        }

        public static long get_userdept(string username)
        {
            long dept = 0;
            using (lidEntities dbo = new lidEntities())
            {
                var user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                if (user != null)
                {
                    dept = util.parselong(user.dept_id);
                }
            }
            return dept;
        }
        public static Users get_user(string username)
        {
            Users user = new Users();
            using (lidEntities dbo = new lidEntities())
            {
                b_users _user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                user.username = _user.email;
                user.isadmin = _user.systemadmin.ToBooleanOrDefault();
                user.company_id = _user.company_id.ToLongOrDefault();
            }
            return user;
        }
        public static Menus get_topuserwebmenu(string username)
        {
            Menus menu = new Menus();
            using (lidEntities dbo = new lidEntities())
            {
                var user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                if (user != null)
                {
                    b_menus _menu = new b_menus();
                    if (user.systemadmin == true)
                    {
                        _menu = dbo.b_menus.OrderBy(x => x.menu_order).FirstOrDefault();
                    }
                    else
                    {
                        var menu_ids = dbo.b_rolemenus.Where(x => x.role_id == user.role_id).Select(x => x.menu_id).ToList();
                        _menu = dbo.b_menus.Where(x => menu_ids.Any(i => i == x.menu_id) && x.isactive == true).OrderBy(x => x.menu_order).FirstOrDefault();
                    }
                    menu.isadmin = user.systemadmin.ToBooleanOrDefault();
                    if (_menu != null)
                    {
                        menu.menu_id = _menu.menu_id;
                        menu.menu_code = _menu.menu_code;
                        menu.menu_name = _menu.menu_name;
                        menu.menu_action = _menu.menu_action;
                        menu.menu_controller = _menu.menu_controller;
                        menu.menu_order = _menu.menu_order.ToInt16OrDefault();
                    }
                }
            }
            return menu;
        }
        public static List<b_codevalues> get_lookups(string keyword)
        {
            List<b_codevalues> lookups = new List<b_codevalues>();
            using (lidEntities dbo = new lidEntities())
            {
                lookups = dbo.b_codevalues.Where(x => x.Code == keyword && x.isActive == true && x.isVisible == true).OrderBy(x => x.OrderValue).ToList();
            }
            return lookups;
        }
        public static bool get_config_shiftdeptbased()
        {
            bool shiftbase = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                if (config != null) shiftbase = util.parsebool(config.shift_deptbase);
            }
            return shiftbase;
        }
        public static bool get_config_createstaff()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                if (config != null) _bool = util.parsebool(config.create_staffuser);
            }
            return _bool;
        }
        public static string get_config_defaultpassword()
        {
            string _pass = "";
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                _pass = config.default_password.ToString();
            }
            return _pass;
        }
        public static long get_config_defaultrole()
        {
            long _role = 0;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                _role = config.default_role.ToLongOrDefault(0);
            }
            return _role;
        }
        public static bool get_config_showemptyshift()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                if (config != null) _bool = util.parsebool(config.show_emptyshift);
            }
            return _bool;
        }
        public static bool get_config_deptauth()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                if (config != null) _bool = util.parsebool(config.user_deptbase);
            }
            return _bool;
        }
        public static bool get_config_depthours()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                if (config != null) _bool = util.parsebool(config.limit_depthrs);
            }
            return _bool;
        }

        public static bool get_config_pastshift()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                if (config != null) _bool = util.parsebool(config.prevent_pastscheduler);
            }
            return _bool;
        }

        public static bool get_AllowadhocShift()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                _bool = config.AllowAdhocShift.ToBooleanOrDefault(false);
            }
            return _bool;
        }
        public static bool get_quickshiftonschedule()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                _bool = config.QuickshiftonSchedule.ToBooleanOrDefault(false);
            }
            return _bool;
        }
        public static bool get_allowmultshifts()
        {
            bool _bool = false;
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                _bool = config.allow_multipleshift.ToBooleanOrDefault(false);
            }
            return _bool;
        }
        public static List<QuickShift> get_quickshifts()
        {
            List<QuickShift> res = new List<QuickShift>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<QuickShift>("get_quickshifts", commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }

        public static List<QuickShift> get_quickshiftbytag(string weekno, string weekday)
        {
            List<QuickShift> res = new List<QuickShift>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<QuickShift>("get_quickshiftbytag", new { weekno = weekno, weekday = weekday }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }

        public static int get_CheckStaffOnShift(string shift_id, string staff_id, string weekday)
        {
            int res = 0;
            using (lidEntities dbo = new lidEntities())
            {
                long _shift_id = shift_id.ToLongOrDefault(0);
                long _staff_id = staff_id.ToLongOrDefault(0);
                DateTime _weekday = weekday.ToDateTimeOrDefault();

                List<QuickSchedule> schedule = new List<QuickSchedule>();
                using (var cn = GetOpenConnection())
                {
                    schedule = cn.Query<QuickSchedule>("get_quickschedulerbyday", new { staff_id = _staff_id, weekday = _weekday }, commandType: CommandType.StoredProcedure).ToList();
                }
                if (schedule.Count > 0)
                {
                    int i = schedule.Where(x => x.shift_id == _shift_id).Count();
                    if (i > 0)
                        res = 1;
                    else
                        res = 2;
                }
            }
            return res;
        }

        public static long AddAdhocShift(Shift _form)
        {
            long shift_id = 0;
            using (lidEntities dbo = new lidEntities())
            {
                TimeSpan _timefrom, _timeto;
                TimeSpan.TryParse(_form.shift_from, out _timefrom);
                TimeSpan.TryParse(_form.shift_to, out _timeto);

                b_adhocshift shift = new b_adhocshift();
                shift.shift_code = _form.shift_code;
                shift.shift_name = _form.shift_name;
                shift.shift_from = _timefrom;
                shift.shift_to = _timeto;
                shift.non_billable = _form.non_billable.ToInt16OrDefault(0);
                shift.allday = _form.allday;
                shift.is_repeated = _form.repeated;

                dbo.b_adhocshift.Add(shift);
                dbo.SaveChanges();
                shift_id = shift.shift_id;
            }

            return shift_id;
        }

        public static long AddAdhocScheduler(Shift _form)
        {
            long scheduler_id = 0;

            using (lidEntities dbo = new lidEntities())
            {
                b_schedule schedule = new b_schedule();
                schedule.staff_id = _form.staff_id.ToLongOrDefault(0);
                schedule.shift_id = _form.shift_id.ToLongOrDefault(0);
                schedule.weekday = _form.weekday;
                schedule.is_adhoc = _form.is_adhoc;
                schedule.deleted = false;

                dbo.b_schedule.Add(schedule);
                dbo.SaveChanges();
                scheduler_id = schedule.schedule_id;
            }

            return scheduler_id;
        }

        public static AjaxResults ajaxSaveStaffShift(string shift_id, string staff_id, string weekday, string startday, string UserName, string dept_hours = "0")
        {
            AjaxResults results = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                b_config config = dbo.b_config.FirstOrDefault();


                if (config.limit_depthrs == true)
                {
                    string staff_hours = AdminCore.get_stafftotalhours_ghost(staff_id, shift_id, startday);

                    long _staff_hrs = util.parselong(staff_hours.Split(':')[0]);
                    long _dept_hrs = util.parselong(dept_hours.Split(':')[0]);

                    if (_staff_hrs >= _dept_hrs)
                    {
                        results.code = "0";
                        results.message = "Maximum department hours exceeded.";

                        return results; // JsonConvert.SerializeObject(results, Formatting.Indented);
                    }
                    else
                    {
                        b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false).FirstOrDefault();
                        if (schedule == null)
                        {
                            schedule = new b_schedule();
                            schedule.staff_id = Convert.ToInt32(staff_id);
                            schedule.shift_id = Convert.ToInt32(shift_id);
                            schedule.weekday = Convert.ToDateTime(weekday).ToString();
                            schedule.created_date = DateTime.Now;
                            schedule.schedule_type = "Staff";
                            schedule.created_by = UserName;
                            if (config.approve_shift == true)
                            {
                                schedule.date_approved = System.DateTime.Now;
                                schedule.shift_approved = true;
                                schedule.approved_by = "Auto Approved";
                            }
                            schedule.deleted = false;
                            dbo.b_schedule.Add(schedule);
                            dbo.SaveChanges();


                            results.code = "1";
                            results.message = "Shift was successfully saved.";
                        }
                        else
                        {

                            results.code = "0";
                            results.message = "Shift already exists on this day.";
                        }
                    }
                }
                else
                {

                    b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false).FirstOrDefault();
                    if (schedule == null)
                    {
                        schedule = new b_schedule();
                        schedule.staff_id = Convert.ToInt32(staff_id);
                        schedule.shift_id = Convert.ToInt32(shift_id);
                        schedule.weekday = Convert.ToDateTime(weekday).ToString();
                        schedule.created_date = DateTime.Now;
                        schedule.schedule_type = "Staff";
                        schedule.created_by = UserName;
                        if (config.approve_shift == true)
                        {
                            schedule.date_approved = System.DateTime.Now;
                            schedule.shift_approved = true;
                            schedule.approved_by = "Auto Approved";
                        }
                        schedule.deleted = false;
                        dbo.b_schedule.Add(schedule);
                        dbo.SaveChanges();

                        results.code = "1";
                        results.message = "Shift was successfully assigned to staff.";
                    }
                    else
                    {

                        results.code = "0";
                        results.message = "Shift already exists on this day";
                    }
                }
            }
            return results; // JsonConvert.SerializeObject(results, Formatting.Indented);
        }

        public static string ajaxAutoSaveStaffShift(string shift_id, string staff_id, string weekday, string UserName)
        {
            string new_day = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_config config = dbo.b_config.FirstOrDefault();
                Staff staff = AdminCore.get_staffById(staff_id.ToLongOrDefault());
                if (staff.no_schedule == true)
                {
                    new_day = "Staff " + staff.staff_shortname + " has been removed from schedule list.";

                    return new_day;
                }
                b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false).FirstOrDefault();
                if (schedule == null)
                {
                    schedule = new b_schedule();
                    schedule.staff_id = Convert.ToInt32(staff_id);
                    schedule.shift_id = Convert.ToInt32(shift_id);
                    schedule.weekday = weekday; //.ToString();

                    schedule.schedule_type = "Staff";
                    schedule.created_by = UserName;
                    schedule.created_date = DateTime.Now;
                    if (config.approve_shift == true)
                    {
                        schedule.date_approved = System.DateTime.Now;
                        schedule.shift_approved = true;
                        schedule.approved_by = "Auto Approved";
                    }
                    schedule.deleted = false;
                    dbo.b_schedule.Add(schedule);
                    dbo.SaveChanges();
                }
            }
            return new_day;
        }

        internal static Groups get_staffgroups(long dept_id)
        {
            Groups res = new Groups();
            using (var cn = GetOpenConnection())
            {
                res.structure_id = dept_id;
                res.group_list = cn.Query<StaffGroup>("get_staffgroups", new { dept_id = dept_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }
        internal static List<StaffGroup> get_staffgrouplist(long dept_id)
        {
            List<StaffGroup> res = new List<StaffGroup>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<StaffGroup>("get_staffgroups", new { dept_id = dept_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }
        internal static List<StaffGroup> get_staffgrouplist(long dept_id, string keyword)
        {
            List<StaffGroup> res = new List<StaffGroup>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<StaffGroup>("get_staffgroups_filter", new { dept_id = dept_id, keyword = keyword }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }


        internal static List<Structure> get_lowerstructurebydept(long dept_id)
        {
            List<Structure> res = new List<Structure>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<Structure>("get_lowerstructure", new { id = dept_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }
        internal static List<Structure> get_lowerstructurebydept(long dept_id, string keyword)
        {
            List<Structure> res = new List<Structure>();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<Structure>("get_lowerstructure_filter", new { id = dept_id, keyword = keyword }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }

        internal static List<string> get_staffbygroupid(string obj_id)
        {
            List<string> ids = new List<string>();
            long group_id = obj_id.ToLongOrDefault();
            using (lidEntities dbo = new lidEntities())
            {
                ids = dbo.b_staffgrouplink.Where(x => x.group_id == group_id).Select(x => x.staff_id.ToString()).ToList<string>();
            }
            return ids;
        }

        internal static List<string> get_staffbystructureid(string obj_id)
        {
            List<string> ids = new List<string>();
            long structure_id = obj_id.ToLongOrDefault();

            using (var cn = GetOpenConnection())
            {
                List<StaffLite> staff = cn.Query<StaffLite>("get_staffstructure", new { id = obj_id }, commandType: CommandType.StoredProcedure).ToList();
                ids = staff.Select(x => x.staff_id.ToString()).ToList();
            }
            return ids;
        }

        internal static GroupDTO get_groupandstafflist(string group_id)
        {
            GroupDTO res = new GroupDTO();
            using (var cn = GetOpenConnection())
            {
                res = cn.Query<GroupDTO>("get_staffgroupsbyid", new { group_id = group_id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                res.staff_list = cn.Query<Staff>("get_groupstafflist", new { group_id = res.group_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return res;
        }

        public static List<Shift> get_staffweeklyshiftsbystartday(long staff_id, DateTime startday)
        {
            List<Shift> shifts = new List<Shift>();
            using (var cn = GetOpenConnection())
            {
                shifts = cn.Query<Shift>("get_staffweeklyshiftsbystartday", new { staff_id = staff_id, startday = startday }, commandType: CommandType.StoredProcedure).ToList();
            }
            return shifts;
        }

        internal static bool validate_appversion()
        {
            Application app = new Application();
            using (var cn = GetOpenConnection())
            {
                string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                app = cn.Query<Application>("get_AppVersion", new { version = version }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return (app != null);
        }
        public static bool get_staffworkingday(string staff_id, string day)
        {
            bool block = false;
            using (var cn = GetOpenConnection())
            {
                block = cn.Query<bool>("get_staffworkingday", new { staff_id = staff_id, day = day.Split(" ")[0] }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return block;
        }
        public static StaffWeekDays get_staffweeklyworkingdays(string staff_id)
        {
            StaffWeekDays days = new StaffWeekDays();
            using (var cn = GetOpenConnection())
            {
                List<StaffWorkingDays> _days = cn.Query<StaffWorkingDays>("get_staffweekworkingday", new { staff_id = staff_id }, commandType: CommandType.StoredProcedure).ToList();
                foreach (StaffWorkingDays _day in _days)
                {
                    switch (_day.weekday.ToLower())
                    {
                        case "mon":
                            days.mon = true;
                            break;
                        case "true":
                            days.tue = true;
                            break;
                        case "wed":
                            days.wed = true;
                            break;
                        case "thu":
                            days.thu = true;
                            break;
                        case "fri":
                            days.fri = true;
                            break;
                        case "sat":
                            days.sat = true;
                            break;
                        case "sun":
                            days.sun = true;
                            break;
                    }
                }
            }
            return days;
        }
    }
}