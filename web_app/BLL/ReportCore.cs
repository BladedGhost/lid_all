﻿using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using Dapper;

namespace web_app.BLL
{
    public class ReportCore
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }
        internal static List<rptDeptStaffCount> rpt_deptstaffcount()
        {
            List<rptDeptStaffCount> reportData = new List<rptDeptStaffCount>();

            //using (lidEntities dbo = new lidEntities())
            //{
            //    var rptData = dbo.rpt_deptstaffcount().ToList();
            //    foreach (var _rptData in rptData)
            //    {
            //        rptDeptStaffCount _reportData = new rptDeptStaffCount();
            //        _reportData.label = _rptData.dept_name;
            //        _reportData.data = _rptData.records;
            //        reportData.Add(_reportData);
            //    }
            //}
            return reportData;
        }
        internal static List<rptDeptStaffCount> rpt_deptposstaffcount(int dept_id)
        {
            List<rptDeptStaffCount> reportData = new List<rptDeptStaffCount>();

            //using (lidEntities dbo = new lidEntities())
            //{
            //    var rptData = dbo.rpt_deptposstaffcount(dept_id).ToList();
            //    foreach (var _rptData in rptData)
            //    {
            //        rptDeptStaffCount _reportData = new rptDeptStaffCount();
            //        _reportData.label = _rptData.position_name;
            //        _reportData.data = _rptData.records;
            //        reportData.Add(_reportData);
            //    }
            //}
            return reportData;
        }

        internal static List<rptDeptWithStaff> rpt_deptwithstaff()
        {
            List<rptDeptWithStaff> reportData = new List<rptDeptWithStaff>();

            //using (lidEntities dbo = new lidEntities())
            //{
            //    var rptData = dbo.rpt_deptwithstaff().ToList();
            //    foreach (var _rptData in rptData)
            //    {
            //        rptDeptWithStaff _reportData = new rptDeptWithStaff();
            //        _reportData.dept_name = _rptData.dept_name;
            //        _reportData.dept_id = _rptData.dept_id.ToString();
            //        reportData.Add(_reportData);
            //    }
            //}
            return reportData;
        }

        internal static List<ReportCategory> get_reportcategory()
        {
            List<ReportCategory> reporscats = new List<ReportCategory>();

            using (lidEntities dbo = new lidEntities())
            {
                List<b_reportcat> _reporscats = dbo.b_reportcat.ToList();
                foreach (b_reportcat _reporscat in _reporscats)
                {
                    ReportCategory reporscat = new ReportCategory();
                    reporscat.reportcat_id = _reporscat.reportcat_id;
                    reporscat.report_cat = _reporscat.report_cat;
                    reporscat.reports_list = get_reportsbycat(_reporscat.reportcat_id);
                    reporscats.Add(reporscat);
                }
            }
            return reporscats;
        }

        internal static List<ReportCategory> get_allreportcategory(string username)
        {
            List<ReportCategory> reporscats = new List<ReportCategory>();

            using (lidEntities dbo = new lidEntities())
            {
                List<b_reportcat> _reporscats = dbo.b_reportcat.ToList();
                foreach (b_reportcat _reporscat in _reporscats)
                {
                    ReportCategory reporscat = new ReportCategory();
                    reporscat.reportcat_id = _reporscat.reportcat_id;
                    reporscat.report_cat = _reporscat.report_cat;
                    reporscat.reports_list = get_allreportsbyusername(_reporscat.reportcat_id,username);
                    reporscats.Add(reporscat);
                }
            }
            return reporscats;
        }
        internal static List<ReportCategory> get_reportcategory(string username)
        {
            List<ReportCategory> reporscats = new List<ReportCategory>();

            using (lidEntities dbo = new lidEntities())
            {
                List<b_reportcat> _reporscats = dbo.b_reportcat.ToList();
                foreach (b_reportcat _reporscat in _reporscats)
                {
                    ReportCategory reporscat = new ReportCategory();
                    reporscat.reportcat_id = _reporscat.reportcat_id;
                    reporscat.report_cat = _reporscat.report_cat;
                    reporscat.reports_list = get_reportsbycat(_reporscat.reportcat_id, username);
                    reporscats.Add(reporscat);
                }
            }
            return reporscats;
        }
        internal static List<Reports> get_reportsbycat(long reportcat_id)
        {
            List<Reports> reports = new List<Reports>();

            using (lidEntities dbo = new lidEntities())
            {
                List<b_reports> _reports = dbo.b_reports.Where(x => x.reportcat_id == reportcat_id).ToList();
                foreach (b_reports _report in _reports)
                {
                    Reports report = new Reports();
                    report.report_id = _report.report_id;
                    report.report_name = _report.report_name;
                    report.report_desc = _report.report_desc;
                    report.report_file = _report.report_file;
                    report.report_image = _report.report_image;
                    report.report_parms = _report.report_parms;
                    reports.Add(report);
                }
            }
            return reports;
        }

        internal static List<Reports> get_reportsbycat(long reportcat_id, string username)
        {
            List<Reports> reports = new List<Reports>();

            using (var cn = GetOpenConnection())
            {
                reports = cn.Query<Reports>("get_toggledreportbyusername", new { username = username, reportcat_id = reportcat_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return reports;
        }
        internal static List<Reports> get_allreportsbyusername(long reportcat_id, string username)
        {
            List<Reports> reports = new List<Reports>();

            using (var cn = GetOpenConnection())
            {
                reports = cn.Query<Reports>("get_reportsbyusername", new { username = username, reportcat_id = reportcat_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return reports;
        }
        internal static Reports get_report(long report_id)
        {
            Reports report = new Reports();
            using (lidEntities dbo = new lidEntities())
            {
                b_reports _report = dbo.b_reports.Where(x => x.report_id == report_id).FirstOrDefault();
                if (_report.report_id != null)
                {
                    report.report_id = _report.report_id;
                    report.report_name = _report.report_name;
                    report.report_desc = _report.report_desc;
                    report.report_file = _report.report_file;
                    report.report_image = _report.report_image;
                    report.report_parms = _report.report_parms;
                }
            }
            return report;
        }

        internal static List<Users> rpt_allsystemusers()
        {
            List<Users> users = new List<Users>();
            using (var cn = GetOpenConnection())
            {
                users = cn.Query<Users>("rpt_allsystemusers", commandType: CommandType.StoredProcedure).ToList();
            }
            return users;
        }
        internal static List<DefaultDataDTO> rpt_getdefaultdata()
        {
            List<DefaultDataDTO> list = new List<DefaultDataDTO>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<DefaultDataDTO>("rpt_getdefaultdata", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static List<RPTRolesDTO> rpt_getsystemroles()
        {
            List<RPTRolesDTO> list = new List<RPTRolesDTO>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<RPTRolesDTO>("rpt_getsystemroles", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        internal static List<RptShiftDTO> rpt_getsystemshifts()
        {
            List<RptShiftDTO> list = new List<RptShiftDTO>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<RptShiftDTO>("rpt_getsystemshifts", commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        public static List<RptRecentDTO> rpt_gettenresentreports(string username)
        {
            List<RptRecentDTO> list = new List<RptRecentDTO>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<RptRecentDTO>("rpt_gettenresentreports", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
        public static List<RptRecentDTO> get_RecentReports(string username)
        {
            List<RptRecentDTO> list = new List<RptRecentDTO>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<RptRecentDTO>("get_gettenresentreports", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        public static List<ArchiveDTO> get_archivereports(string username)
        {
            List<ArchiveDTO> list = new List<ArchiveDTO>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<ArchiveDTO>("get_archivereports", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }

        public static List<ReportScheduleDTO> get_reportschedule(string username)
        {
            List<ReportScheduleDTO> list = new List<ReportScheduleDTO>();
            using (var cn = GetOpenConnection())
            {
                list = cn.Query<ReportScheduleDTO>("get_reportschedulebyowner", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return list;
        }
    }
}