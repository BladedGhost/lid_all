﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace web_app.BLL
{
    public static class util
    {
        public static long parselong(string val = "0")
        {
            long _val = 0;
            long.TryParse(val, out _val);
            return _val;
        }
        public static long parselong(long? val)
        {
            long _val = 0;
            long.TryParse(val.ToString(), out _val);
            return _val;
        }
        public static bool parsebool(string val)
        {
            bool _val = false;
            bool.TryParse(val, out _val);
            return _val;
        }
        public static bool parsebool(bool? val)
        {
            bool _val = false;
            bool.TryParse(val.ToString(), out _val);
            return _val;
        }

        public static decimal parsedecimal(string val)
        {
            decimal _val = 0;
            Decimal.TryParse(val, out _val);
            return _val;
        }
    }
}
