//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace web_app.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class lid_company
    {
        public long company_id { get; set; }
        public string company_name { get; set; }
        public string company_reg { get; set; }
        public string company_trading { get; set; }
        public string company_tel { get; set; }
        public string company_email { get; set; }
        public string company_fax { get; set; }
        public string company_street { get; set; }
        public string company_building { get; set; }
        public string company_city { get; set; }
        public string company_pcode { get; set; }
        public string company_pobox { get; set; }
        public string company_pooffice { get; set; }
        public string company_pocity { get; set; }
        public string company_pocode { get; set; }
        public Nullable<long> company_type_id { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public byte[] company_logo { get; set; }
    }
}
