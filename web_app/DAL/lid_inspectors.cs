//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace web_app.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class lid_inspectors
    {
        public long inspector_id { get; set; }
        public Nullable<long> company_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public Nullable<long> certificate_id { get; set; }
        public Nullable<bool> isactive { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string idnumber { get; set; }
        public string registerno { get; set; }
        public byte[] signature_img { get; set; }
    }
}
