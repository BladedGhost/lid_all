//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace web_app.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class b_qualification
    {
        public long qualification_id { get; set; }
        public Nullable<long> user_id { get; set; }
        public string qualification_name { get; set; }
        public string institution { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> qualification_date { get; set; }
        public string qualification_file { get; set; }
    }
}
