﻿using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;

namespace web_app.Controllers
{
    public class AjaxController : Controller
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        public AjaxController()
        {
        }
        public AjaxController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        // GET: Ajax
        public ActionResult Index()
        {
            return View();
        }


        public AjaxResults ajaxSaveStaffShift(string shift_id, string staff_id, string schedule_type, string weekday, string startday, string dept_hours = "0", long link = 0)
        {
            AjaxResults results = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                b_config config = dbo.b_config.FirstOrDefault();
                if (schedule_type == "Staff")
                {
                    Staff staff = AdminCore.get_staffById(staff_id.ToLongOrDefault());
                    if (staff.no_schedule == true)
                    {
                        results.code = "0";
                        results.message = "Staff " + staff.staff_shortname + " has been removed from schedule list.";

                        return results;
                    }
                }
                schedule_type = string.IsNullOrWhiteSpace(schedule_type) ? "Staff" : schedule_type;

                if (config.limit_depthrs == true)
                {
                    string staff_hours = AdminCore.get_stafftotalhours_ghost(staff_id, shift_id, startday);

                    long _staff_hrs = util.parselong(staff_hours.Split(':')[0]);
                    long _dept_hrs = util.parselong(dept_hours.Split(':')[0]);

                    if (_staff_hrs >= _dept_hrs)
                    {
                        results.code = "0";
                        results.message = "Maximum department hours exceeded.";

                        return results; // JsonConvert.SerializeObject(results, Formatting.Indented);
                    }
                }
                //b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false && m.schedule_type == schedule_type).FirstOrDefault();
                bool _overlap = AdminCore.check_shiftoverlap(staff_id, shift_id, Convert.ToDateTime(weekday).ToString(), schedule_type);
                if (_overlap)
                {
                    results.code = "0";
                    results.message = "Shift overlap with already assigned shift";
                }
                else
                {
                    b_schedule schedule = new b_schedule();
                    schedule = new b_schedule();
                    schedule.staff_id = Convert.ToInt32(staff_id);
                    schedule.shift_id = Convert.ToInt32(shift_id);
                    schedule.schedule_type = schedule_type;
                    schedule.weekday = Convert.ToDateTime(weekday).ToString();
                    schedule.created_by = User.Identity.Name;
                    schedule.created_date = DateTime.Now;
                    schedule.link_id = link;
                    if (config.approve_shift == true)
                    {
                        schedule.date_approved = System.DateTime.Now;
                        schedule.shift_approved = true;
                        schedule.approved_by = "Auto Approved";
                    }
                    schedule.deleted = false;
                    dbo.b_schedule.Add(schedule);
                    dbo.SaveChanges();

                    results.id = schedule.schedule_id;
                    results.code = "1";
                    results.message = "Shift was successfully saved.";
                }
            }
            return results; // JsonConvert.SerializeObject(results, Formatting.Indented);
        }
        public string ajaxPasteStaffShift(string shift_id, string staff_id, string weekday, string startdate)
        {
            string new_day = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_config config = dbo.b_config.FirstOrDefault();
                List<WeekDays> weekdays = AdminCore.get_weekdaysbydt(startdate);
                foreach (WeekDays _day in weekdays)
                {
                    if (_day.day_name.Substring(0, 2) == weekday.Substring(0, 2))
                    {
                        new_day = _day.day_name;

                        b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false).FirstOrDefault();
                        if (schedule == null)
                        {
                            schedule = new b_schedule();
                            schedule.staff_id = Convert.ToInt32(staff_id);
                            schedule.shift_id = Convert.ToInt32(shift_id);
                            schedule.weekday = Convert.ToDateTime(_day.day_name).ToString();
                            schedule.schedule_type = "Staff";
                            schedule.created_by = User.Identity.Name;
                            schedule.created_date = DateTime.Now;
                            if (config.approve_shift == true)
                            {
                                schedule.date_approved = System.DateTime.Now;
                                schedule.shift_approved = true;
                                schedule.approved_by = "Auto Approved";
                            }
                            schedule.deleted = false;
                            dbo.b_schedule.Add(schedule);
                            dbo.SaveChanges();
                        }
                    }
                }
            }
            return new_day;
        }
        public string ajaxAutoSaveStaffShift(string shift_id, string staff_id, string weekday)
        {
            string new_day = "";
            using (lidEntities dbo = new lidEntities())
            {
                b_config config = dbo.b_config.FirstOrDefault();
                //List<WeekDays> weekdays = AdminCore.get_weekdaysbydt(startdate);
                //foreach (WeekDays _day in weekdays)
                //{
                //    if (_day.day_name.Substring(0, 2) == weekday.Substring(0, 2))
                //    {
                //        new_day = _day.day_name;

                b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false).FirstOrDefault();
                if (schedule == null)
                {
                    schedule = new b_schedule();
                    schedule.staff_id = Convert.ToInt32(staff_id);
                    schedule.shift_id = Convert.ToInt32(shift_id);
                    schedule.weekday = weekday;
                    schedule.schedule_type = "Staff";
                    schedule.created_by = User.Identity.Name;
                    schedule.created_date = DateTime.Now;
                    if (config.approve_shift == true)
                    {
                        schedule.date_approved = System.DateTime.Now;
                        schedule.shift_approved = true;
                        schedule.approved_by = "Auto Approved";
                    }
                    schedule.deleted = false;
                    dbo.b_schedule.Add(schedule);
                    dbo.SaveChanges();
                }
                //    }
                //}
            }
            return new_day;
        }


        public void ajaxDeleteStaffShift(string shift_id, string obj_id, string schedule_type, string weekday)
        {
            using (lidEntities dbo = new lidEntities())
            {
                DateTime dt = Convert.ToDateTime(weekday);
                weekday = dt.ToString();
                List<b_schedule> schedules = dbo.b_schedule.Where(m => m.shift_id.ToString() == shift_id && m.staff_id.ToString() == obj_id && m.weekday == weekday && m.deleted == false && m.schedule_type == schedule_type).ToList();
                foreach (b_schedule schedule in schedules)
                {
                    long schedule_id = schedule.schedule_id;
                    List<b_schedule> links = dbo.b_schedule.Where(m => m.link_id == schedule_id).ToList();
                    foreach (var _links in links)
                    {
                        _links.deleted = true;
                        dbo.Entry(_links).State = System.Data.Entity.EntityState.Modified;
                        dbo.SaveChanges();
                    }

                    schedule.deleted = true;
                    dbo.Entry(schedule).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }

                if (schedule_type != "Staff")
                {
                    List<string> staff_ids = get_staffbyobjectid(obj_id, schedule_type);
                    foreach (string staff_id in staff_ids)
                    {
                        List<b_schedule> _schedules = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false && m.schedule_type == schedule_type).ToList();
                        foreach (b_schedule schedule in _schedules)
                        {
                            schedule.deleted = true;
                            dbo.Entry(schedule).State = System.Data.Entity.EntityState.Modified;
                            dbo.SaveChanges();
                        }

                    }
                }
            }
        }
        public void deleteSeleteStaffShift(string shift_id, string staff_id, string schedule_type, string weekday, string is_adhoc)
        {
            using (lidEntities dbo = new lidEntities())
            {
                DateTime dt = Convert.ToDateTime(weekday);
                weekday = dt.ToString();
                List<b_schedule> schedules = dbo.b_schedule.Where(m => m.shift_id.ToString() == shift_id && m.staff_id.ToString() == staff_id && m.weekday == weekday && m.deleted == false).ToList();
                foreach (b_schedule schedule in schedules)
                {
                    schedule.deleted = true;
                    dbo.Entry(schedule).State = System.Data.Entity.EntityState.Deleted;
                    dbo.SaveChanges();
                }
            }
        }

        public void ajaxApproveStaffShift(string shift_id, string staff_id, string weekday)
        {
            using (lidEntities dbo = new lidEntities())
            {
                DateTime dt = Convert.ToDateTime(weekday);
                weekday = dt.ToString();
                b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.shift_id.ToString() == shift_id && m.deleted == false).FirstOrDefault();
                if (schedule != null)
                {
                    schedule.shift_approved = true;
                    schedule.approved_by = User.Identity.Name;
                    schedule.date_approved = DateTime.Now;
                    dbo.Entry(schedule).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }
            }
        }
        public void ajaxRejectStaffShift(string shift_id, string staff_id, string weekday, string reason)
        {
            using (lidEntities dbo = new lidEntities())
            {
                DateTime dt = Convert.ToDateTime(weekday);
                weekday = dt.ToString();
                b_schedule schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday && m.shift_id.ToString() == shift_id && m.deleted == false).FirstOrDefault();
                if (schedule != null)
                {
                    schedule.shift_approved = false;
                    schedule.approved_by = User.Identity.Name;
                    schedule.date_approved = DateTime.Now;
                    schedule.rej_reason = reason;
                    dbo.Entry(schedule).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }
            }
        }
        public void ajaxApproveLeave(string leave_id)
        {
            using (lidEntities dbo = new lidEntities())
            {
                b_leave leave = dbo.b_leave.Where(m => m.leave_id.ToString() == leave_id).FirstOrDefault();
                if (leave != null)
                {
                    leave.leave_approved = true;
                    dbo.Entry(leave).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }
            }
        }
        public void ajaxDeclineLeave(string leave_id)
        {
            using (lidEntities dbo = new lidEntities())
            {
                b_leave leave = dbo.b_leave.Where(m => m.leave_id.ToString() == leave_id).FirstOrDefault();
                if (leave != null)
                {
                    leave.leave_approved = false;
                    dbo.Entry(leave).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }
            }
        }
        public JsonResult loadStaffShiftTrade(string staff_id, string shift_dt)
        {
            var json = "";
            using (lidEntities dbo = new lidEntities())
            {
                DateTime dt = Convert.ToDateTime(shift_dt);
                string weekday = dt.ToString();
                //var weekday = dbo.get_WeekDayByDt(Convert.ToDateTime(shift_dt)).FirstOrDefault();
                b_schedule _schedule = dbo.b_schedule.Where(m => m.staff_id.ToString() == staff_id && m.weekday == weekday.ToString() && m.deleted == false).FirstOrDefault();
                if (_schedule != null)
                {
                    b_shift _shift = dbo.b_shift.Where(m => m.shift_id == _schedule.shift_id).FirstOrDefault();
                    Dictionary<string, string> obj = new Dictionary<string, string>();
                    obj.Add("shift_name", _shift.shift_name);
                    obj.Add("staff_id", staff_id);
                    obj.Add("shift_dt", shift_dt);
                    json = new JavaScriptSerializer().Serialize(obj);
                }
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StaffTimeSheetPDF()
        {
            return View();
        }
        public ActionResult StaffSchedulePDF()
        {
            return View();
        }

        public JsonResult ajaxGetChatData()
        {
            var json = "";
            using (var cn = GetOpenConnection())
            {
                List<DashData> res = cn.Query<DashData>("dash_getChartData", new { Date = DateTime.Now }, commandType: CommandType.StoredProcedure).ToList();
                json = new JavaScriptSerializer().Serialize(res);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ajaxPrintStaffTimesheet(string staff_id, string weekday)
        {
            ShiftManage shiftManage = new ShiftManage();

            Staff staff = AdminCore.get_staffById(staff_id.ToLongOrDefault(0));

            shiftManage.Staff_list.Add(staff);
            shiftManage.Week_days = AdminCore.get_weekdaysbydt(weekday);
            shiftManage.staff_id = staff_id;

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            //return new ViewAsPdf("StaffTimeSheetPDF", shiftManage)
            //{
            //    FileName = staff.staff_number + "_timesheet.pdf",
            //    CustomSwitches = footer,
            //    PageSize = Size.A4,
            //    PageOrientation = Orientation.Portrait,
            //};
            string json = new JavaScriptSerializer().Serialize("");
            return Json(json, JsonRequestBehavior.AllowGet);
    }
        public ActionResult ajaxPrintStaffSchedule(string staff_id, string weekday)
        {
            ShiftManage shiftManage = new ShiftManage();

            Staff staff = AdminCore.get_staffById(staff_id.ToLongOrDefault(0));

            shiftManage.Staff_list.Add(staff);
            shiftManage.Week_days = AdminCore.get_weekdaysbydt(weekday);
            shiftManage.staff_id = staff_id;

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            //return new ViewAsPdf("StaffSchedulePDF", shiftManage)
            //{
            //    FileName = staff.staff_number + "_weekschedule.pdf",
            //    CustomSwitches = footer,
            //    PageSize = Size.A4,
            //    PageOrientation = Orientation.Portrait,
            //};
            string json = new JavaScriptSerializer().Serialize("");
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxMgrRoleMenu(string role_id, string menu_id)
        {
            var json = "";
            Dictionary<string, string> obj = new Dictionary<string, string>();
            using (lidEntities dbo = new lidEntities())
            {
                b_rolemenus rolemenu = dbo.b_rolemenus.Where(m => m.role_id.ToString() == role_id && m.menu_id.ToString() == menu_id).FirstOrDefault();
                if (rolemenu == null)
                {
                    rolemenu = new b_rolemenus();
                    rolemenu.menu_id = util.parselong(menu_id);
                    rolemenu.role_id = util.parselong(role_id);
                    dbo.b_rolemenus.Add(rolemenu);
                    dbo.SaveChanges();

                    obj.Add("res", "Saved");
                    json = new JavaScriptSerializer().Serialize(obj);
                }
                else
                {

                    dbo.Entry(rolemenu).State = System.Data.Entity.EntityState.Deleted;
                    dbo.SaveChanges();

                    obj.Add("res", "Deleted");
                    json = new JavaScriptSerializer().Serialize(obj);
                }

                json = new JavaScriptSerializer().Serialize(obj);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ajaxMgrCompanyCat(string company_id, string category_id)
        {
            var json = "";
            Dictionary<string, string> obj = new Dictionary<string, string>();
            using (lidEntities dbo = new lidEntities())
            {
                lid_company_cat company_cat = dbo.lid_company_cat.Where(m => m.company_id.ToString() == company_id && m.category_id.ToString() == category_id).FirstOrDefault();
                if (company_cat == null)
                {
                    company_cat = new lid_company_cat();
                    company_cat.category_id = util.parselong(category_id);
                    company_cat.company_id = util.parselong(company_id);
                    dbo.lid_company_cat.Add(company_cat);
                    dbo.SaveChanges();

                    obj.Add("res", "Saved");
                    json = new JavaScriptSerializer().Serialize(obj);
                }
                else
                {

                    dbo.Entry(company_cat).State = System.Data.Entity.EntityState.Deleted;
                    dbo.SaveChanges();

                    obj.Add("res", "Deleted");
                    json = new JavaScriptSerializer().Serialize(obj);
                }

                json = new JavaScriptSerializer().Serialize(obj);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }


        public string get_config_startday()
        {
            string inx = "0";
            using (lidEntities dbo = new lidEntities())
            {
                var config = dbo.b_config.FirstOrDefault();
                if (config != null)
                {
                    long id = util.parselong(config.startday);
                    string start_inx = dbo.b_codevalues.Where(x => x.id == id).Select(x => x.SecondaryValue.ToString()).FirstOrDefault();
                    inx = start_inx;
                }
            }

            return JsonConvert.SerializeObject(inx, Formatting.Indented);
        }

        public string get_staffonshift()
        {
            string i = "0";
            using (lidEntities dbo = new lidEntities())
            {
                string dt = DateTime.Today.ToString();
                List<b_schedule> list = dbo.b_schedule.Where(x => x.weekday == dt && x.deleted == false).ToList();
                i = list.Count.ToString();
            }

            return JsonConvert.SerializeObject(i, Formatting.Indented);
        }
   
   
        public string get_countassets()
        {
            int i = 0;
            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                b_users _user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                i = dbo.lid_asset.Where(m => m.company_id == _user.company_id).Count();
            }
            return JsonConvert.SerializeObject(i.ToString(), Formatting.Indented);
        }

        public string get_countinspection()
        {
            int i = 0;
            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                b_users _user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                i = dbo.lid_inspection.Where(m => m.company_id == _user.company_id).Count();
            }
            return JsonConvert.SerializeObject(i.ToString(), Formatting.Indented);
        }
        public JsonResult tempoScatterChart()
        {
            var json = "";
            using (var cn = GetOpenConnection())
            {
                List<ScatterChart> res = cn.Query<ScatterChart>("temp_ScatterChart", new { Date = DateTime.Now }, commandType: CommandType.StoredProcedure).ToList();
                json = new JavaScriptSerializer().Serialize(res);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public JsonResult get_autoschedulerdata()
        {
            var json = "";
            using (var cn = GetOpenConnection())
            {
                List<AutoSchedulerData> data = cn.Query<AutoSchedulerData>("get_autoschedulerdata", commandType: CommandType.StoredProcedure).ToList();
                json = new JavaScriptSerializer().Serialize(data);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ShiftMonthlyScatterChart(string shift_id)
        {
            var json = "";
            using (var cn = GetOpenConnection())
            {
                List<ChartDataDTO> res = cn.Query<ChartDataDTO>("chart_ShiftMonthlyScatter", new { shift_id = shift_id }, commandType: CommandType.StoredProcedure).ToList();
                json = new JavaScriptSerializer().Serialize(res);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OrganisationStructure()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<OrgStructure> structure = cn.Query<OrgStructure>("get_structure", commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(structure);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        ///Ajax/OrganisationStructursearch=&key=0&parent=0&category=Department
        public JsonResult GetTreeStructure(string search, string parent, string category)
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<OrgStructure> structure = cn.Query<OrgStructure>("get_structurebyparent", new { parentId = parent }, commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(structure);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_TopStructure()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<Structure> res = cn.Query<Structure>("get_topstructure", commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_LowerStructure(string parent_id)
        {
            var json = "";
            using (var cn = GetOpenConnection())
            {
                List<Structure> res = cn.Query<Structure>("get_lowerstructure", new { id = parent_id }, commandType: CommandType.StoredProcedure).ToList();
                json = new JavaScriptSerializer().Serialize(res);
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public JsonResult get_allstaff()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<Staff> res = cn.Query<Staff>("get_allstaff", commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }



        public JsonResult get_allshifts()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<ShiftList> res = cn.Query<ShiftList>("select * from b_shift").ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserList()
        {
            List<Users> users = new List<Users>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_users> _users = dbo.b_users.Where(x => x.isactive == true).ToList();
                foreach (b_users _user in _users)
                {
                    users.Add(new Users
                    {
                        user_id = _user.user_id,
                        username = _user.email,
                        email = _user.email,
                        name = _user.name,
                        surname = _user.surname,
                        telephone = _user.telephone,
                        photo = _user.photo,
                        isadmin = _user.systemadmin.ToBooleanOrDefault(),//) ? "Yes" : "No",
                        isStaff = (string.IsNullOrWhiteSpace(_user.staff_id.ToString())) ? "No" : "Yes",
                        department = AdminCore.get_deptname(util.parselong(_user.dept_id)),
                        role_name = AdminCore.get_rolename(util.parselong(_user.role_id)),
                    });
                }
            }
            var json = new JavaScriptSerializer().Serialize(users);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetShiftList()
        {
            List<Shift> shifts = AdminCore.get_shifts(User.Identity.Name);

            var json = new JavaScriptSerializer().Serialize(shifts);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public List<Shift> Get_ShiftList()
        {
            List<Shift> shifts = AdminCore.get_shifts(User.Identity.Name);
            return shifts;
        }
        public JsonResult GetRoleList()
        {
            List<Roles> roles = new List<Roles>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_roles> _roles = dbo.b_roles.ToList();
                foreach (b_roles role in _roles)
                {
                    roles.Add(new Roles
                    {
                        role_id = role.role_id,
                        role_code = role.role_code,
                        role_name = role.role_name,
                        role_dept = AdminCore.getDeptname(role.role_dept.ToLongOrDefault(0)),
                        role_active = (util.parsebool(role.isactive)) ? "Active" : "Not Active"
                    });
                }
            }
            var json = new JavaScriptSerializer().Serialize(roles);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStructureLevels()
        {
            List<StructureLevels> levels = AdminCore.get_structurelevels();
            var results = new JavaScriptSerializer().Serialize(levels);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCodeValues(string datatype)
        {
            List<CodeValues> codevalues = new List<CodeValues>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_codevalues> _codevalues = dbo.b_codevalues.Where(x => x.Code == datatype).ToList();
                foreach (b_codevalues _codevalue in _codevalues)
                {
                    codevalues.Add(new CodeValues
                    {
                        id = _codevalue.id,
                        code = _codevalue.Code,
                        value = _codevalue.Value,
                        PrimaryVal = _codevalue.PrimaryValue,
                        SecondaryVal = _codevalue.SecondaryValue,
                        isActive = _codevalue.isActive.ToBooleanOrDefault(false),
                        isReadyonly = _codevalue.isReadonly.ToBooleanOrDefault(false),
                        isVisible = _codevalue.isVisible.ToBooleanOrDefault(false),
                        Order = _codevalue.OrderValue.ToInt32OrDefault(),
                        DefaultValue = _codevalue.DefaultValue
                        // role_active = (util.parsebool(role.isactive)) ? "Active" : "Not Active"
                    });
                }
            }
            var json = new JavaScriptSerializer().Serialize(codevalues);
            return Json(json, JsonRequestBehavior.AllowGet);
        }


        public JsonResult get_leaverequests()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<Leave> res = cn.Query<Leave>("get_allActiveLeaveRequests", new { username = User.Identity.Name }, commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWorkAreaList()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<WorkArea> res = cn.Query<WorkArea>("get_workarealist", commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult get_userexperience()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<Experience> res = cn.Query<Experience>("get_userexperience", new { username = User.Identity.Name }, commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult get_userqualification()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<Qualification> res = cn.Query<Qualification>("get_userqualification", new { username = User.Identity.Name }, commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult get_alltradeshift()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<ShiftTrade> res = cn.Query<ShiftTrade>("get_allshifttradebystaff", new { username = User.Identity.Name }, commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult get_shiftstrade()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<Leave> res = cn.Query<Leave>("get_allActiveLeaveRequests", new { username = User.Identity.Name }, commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult get_mystaffshift()
        {
            var results = "";
            using (var cn = GetOpenConnection())
            {
                List<Shift> res = cn.Query<Shift>("get_mystaffshift", new { username = User.Identity.Name }, commandType: CommandType.StoredProcedure).ToList();
                results = new JavaScriptSerializer().Serialize(res);
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult save_shift(string value, string obj_id, string schedule_type, string dept_id, string weekday, string dept_hours, string startday)
        {
            var results = "";
            AjaxResults _res = ajaxSaveStaffShift(value, obj_id, schedule_type, weekday, startday, dept_hours);
            if (schedule_type != "Staff")
            {
                List<string> staff_ids = get_staffbyobjectid(obj_id, schedule_type);
                foreach (string staff_id in staff_ids)
                {
                    ajaxSaveStaffShift(value, staff_id, "", weekday, startday, dept_hours, _res.id);
                    saveCalendarShiftEvent(value, staff_id, weekday);
                }
            }
            else
            {
                saveCalendarShiftEvent(value, obj_id, weekday);
            }
            //BackgroundJob.Enqueue(() => Jobs.jobShiftNotify(value));
            //StaffHours staffhours = AdminCore.get_staffweektothours((schedule_type == "Staff") ? obj_id : "0", startday);

            SaveShiftResults res = new SaveShiftResults();
            res.code = _res.code;
            res.message = _res.message;
            res.dayname = weekday;
            res.obj_id = obj_id;
            res.schedule_type = schedule_type;
            res.shift_id = value;
            res.dept_id = dept_id;
            res.startday = startday;
            res.dept_hours = dept_hours;
            //res.staff_hours = staffhours.tot_hours;
            results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        private List<string> get_staffbyobjectid(string obj_id, string schedule_type)
        {
            List<string> res = new List<string>();
            switch (schedule_type)
            {
                case "Group":
                    res = AdminCore.get_staffbygroupid(obj_id);
                    break;
                case "Structure":
                    res = AdminCore.get_staffbystructureid(obj_id);
                    break;
            }

            return res;
        }

        private void saveCalendarShiftEvent(string shift_id, string staff_id, string weekday)
        {
            Shift _shift = AdminCore.get_shift(shift_id);
            using (lidEntities dbo = new lidEntities())
            {

                b_appointmentdiary appointment = new b_appointmentdiary();
                appointment.Title = "Shift: " + _shift.shift_code + _shift.period;
                appointment.staff_id = staff_id.ToLongOrDefault(0);
                appointment.DateTimeScheduled = weekday.ToDateTimeOrDefault();
                appointment.SomeImportantKey = 1;
                appointment.StatusENUM = 1;
                appointment.AppointmentLength = _shift.minutes.ToInt32OrDefault(0);
                dbo.b_appointmentdiary.Add(appointment);
                dbo.SaveChanges();
            }
        }


        public JsonResult ChangePassword(string curpassword, string newpassword)
        {
            AjaxResults res = new AjaxResults();
            ManageUserViewModel model = new ManageUserViewModel();

            model.OldPassword = curpassword;
            model.NewPassword = newpassword;

            //AccountController acc = new AccountController();
            //acc.ChangePassword(model);

            IdentityResult response = UserManager.ChangePassword(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (response.Succeeded)
            {
                res.code = "1";
            }
            else
            {
                res.code = "0";
                res.message = ((string[])response.Errors)[0];
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

       
        public JsonResult RemoveUserFromRole(string username, string role_id)
        {
            AjaxResults res = new AjaxResults();


            AdminCore.del_userfromrole(username, role_id);

            res.code = "1";
            res.message = "User " + username + " successfully removed from role. ";

            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdReRunAutoScheduler(string scheduler_id)
        {
            AjaxResults res = new AjaxResults();

            using (lidEntities dbo = new lidEntities())
            {
                long autoschedulerdata_id = scheduler_id.ToLongOrDefault(0);
                b_autoschedulerdata data = dbo.b_autoschedulerdata.Where(x => x.Autoschedulerdata_id == autoschedulerdata_id).FirstOrDefault();
                data.re_run = (data.re_run ?? 0) + 1;
                dbo.Entry(data).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();
                res.code = "1";
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteAutoScheduler(string scheduler_id)
        {
            AjaxResults res = new AjaxResults();

            using (lidEntities dbo = new lidEntities())
            {
                long autoschedulerdata_id = scheduler_id.ToLongOrDefault(0);
                b_autoschedulerdata data = dbo.b_autoschedulerdata.Where(x => x.Autoschedulerdata_id == autoschedulerdata_id).FirstOrDefault();
                data.deleted = true;
                dbo.Entry(data).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();
                res.code = "1";
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ChangeUserPassword(string username, string password)
        {
            AjaxResults res = new AjaxResults();
            ResetPasswordViewModel model = new ResetPasswordViewModel();

            model.Email = username;
            model.Password = password;

            var user = UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                res.code = "0";
                res.message = "No user found";
            }
            string token = UserManager.GeneratePasswordResetToken(user.Result.Id); //.ResetPassword(user.Result.Id, user.Result.PasswordHash, model.Password);
            IdentityResult response = UserManager.ResetPassword(user.Result.Id, token, model.Password);
            if (response.Succeeded)
            {
                res.code = "1";
            }
            else
            {
                res.code = "0";
                res.message = ((string[])response.Errors)[0];
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddQualification(string qname, string qinstitution, string qualificationdata, HttpPostedFileBase upload)
        {
            AjaxResults res = new AjaxResults();

            using (lidEntities dbo = new lidEntities())
            {
                b_users user = dbo.b_users.Where(x => x.email == User.Identity.Name).FirstOrDefault();
                b_qualification exp = new b_qualification();
                exp.qualification_name = qname;
                exp.institution = qinstitution;
                exp.qualification_date = qualificationdata.ToDateTimeOrDefault();
                exp.user_id = user.user_id;
                dbo.b_qualification.Add(exp);
                dbo.SaveChanges();
                res.code = "1";
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddExperience(string position, string institution, string from, string to, string responsibilities, string skills)
        {
            AjaxResults res = new AjaxResults();

            using (lidEntities dbo = new lidEntities())
            {
                b_users user = dbo.b_users.Where(x => x.email == User.Identity.Name).FirstOrDefault();
                b_experience exp = new b_experience();
                exp.position = position;
                exp.institution = institution;
                exp.date_from = from.ToDateTimeOrDefault();
                exp.date_to = to.ToDateTimeOrDefault();
                exp.responsibilities = responsibilities;
                exp.skills = skills;
                exp.user_id = user.user_id;

                dbo.b_experience.Add(exp);
                dbo.SaveChanges();
                res.code = "1";
            }


            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }


        public JsonResult get_quickshiftprintselected(string shifts)
        {
            AjaxResults res = new AjaxResults();

            res.code = "1";


            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getStaffWeeklyDetails(string staff_id, string startday)
        {
            StaffWeeklyStats stats = new StaffWeeklyStats();
            ViewBag.staff_id = staff_id;
            ViewBag.startday = startday;
            stats = AdminCore.get_staffweeklyhoursstats(staff_id, startday);

            stats.quick_stats = AdminCore.get_staffquickstats(staff_id, startday);
            stats.workarea = AdminCore.get_staff_workarea(staff_id);
            stats.workingdays = AdminCore.get_staff_workingdays(staff_id);

            var results = new JavaScriptSerializer().Serialize(stats);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult refresh_staffweeklyhours(string staff_id, string startday)
        {
            StaffHours staffHours = new StaffHours();
            if (!string.IsNullOrWhiteSpace(staff_id))
            {
                using (var cn = GetOpenConnection())
                {
                    string _startday = Convert.ToDateTime(startday).ToString();
                    StaffHours _staffHours = cn.Query<StaffHours>("get_staffweeklyhours", new { staff_id = staff_id, Date = _startday }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    staffHours.non_billable_hours = (_staffHours.non_billable_hours == null) ? "00:00" : _staffHours.non_billable_hours;
                    staffHours.tot_hours = (_staffHours.tot_hours == null) ? "00:00" : _staffHours.tot_hours;
                    staffHours.worked_hours = (_staffHours.worked_hours == null) ? "00:00" : _staffHours.worked_hours;
                }
            }
            var results = new JavaScriptSerializer().Serialize(staffHours);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveFromList(string staff_id, bool remove)
        {
            string _count = "0";
            using (var cn = GetOpenConnection())
            {
                _count = cn.Query<string>("set_togglestafffrolist", new { staff_id = staff_id, remove = remove }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            var results = new JavaScriptSerializer().Serialize(_count);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ajaxGetWebMenus()
        {
            List<Menus> menus = new List<Menus>();
            using (var cn = GetOpenConnection())
            {
                menus = cn.Query<Menus>("get_webmenus", commandType: CommandType.StoredProcedure).ToList();
            }
            var results = new JavaScriptSerializer().Serialize(menus);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ToggleStaffWorkingDays(string staff_id, string weekday, bool toggle)
        {
            AjaxResults res = new AjaxResults();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name;
               int _count = cn.Query<int>("set_togglestafffworkingday", new { staff_id = staff_id, weekday = weekday, toggle = toggle, user = username }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_count > 0)
                {
                    res.code = "1";
                }else
                {
                    res.code = "0";    
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ToggleStaffWorkArea(string staff_id, string workarea_id)
        {
            AjaxResults res = new AjaxResults();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name;
               int _count = cn.Query<int>("set_togglestafffworkarea", new { staff_id = staff_id, workarea_id = workarea_id, username = username }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_count > 0)
                {
                    res.code = "1";
                }else
                {
                    res.code = "0";    
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}