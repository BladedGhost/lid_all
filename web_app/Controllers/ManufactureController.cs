﻿using Dapper;
using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace web_app.Controllers
{
    [Authorize]
    public class ManufactureController : Controller
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        // GET: Manufacture
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Certificate()
        {
            List<Certificate> list = new List<Certificate>();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                list = cn.Query<Certificate>("get_certificates_many", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
            }
            return View(list);
        }
        public ActionResult win_add_certificate()
        {
            return View();
        }

        public JsonResult CrudCertificate(Certificate model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                b_users user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                lid_certificate cert = dbo.lid_certificate.Where(m => m.certificate_id == model.certificate_id).FirstOrDefault();
                if (cert == null)
                {
                    cert = new lid_certificate();
                    cert.code = model.code;
                    cert.sn_no = model.sn_no;
                    cert.name = model.name;
                    cert.description = model.description;
                    cert.file = model.file;
                    cert.company_id = user.company_id;
                    cert.datestamp = DateTime.Now;
                    dbo.lid_certificate.Add(cert);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Certificate for " + model.name + " successfully saved";
                }
                else
                {
                    cert.code = model.code;
                    cert.sn_no = model.sn_no;
                    cert.name = model.name;
                    cert.description = model.description;
                    cert.file = model.file;
                    cert.company_id = user.company_id;
                    dbo.Entry(cert).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Certificate for " + model.name + " successfully update";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManufactureDetails()
        {
            CompanyModel list = new CompanyModel();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                list = cn.Query<CompanyModel>("get_company_byid", new { username = username }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return View(list);
        }
    }
}