﻿using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web_app.Controllers
{
    public class OrganisationController : Controller
    {
        // GET: Organisation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Structure()
        {
            return View();
        }
        public ActionResult Structure_View()
        {
            return View();
        }
        public ActionResult StaffStructure()
        {
            return View();
        }
public ActionResult StaffHours()
        {
            return View();
        }
public ActionResult OrganisationStaff()
        {
            return View();
        }
public ActionResult DisplayFormats()
        {
            return View();
        }
public ActionResult Settings()
        {
            Config config = AdminCore.get_sysconfig();

            ViewBag.list_dateformat = new SelectList(AdminCore.get_lookups("date-format"), "id", "value", config.dateformat);
            ViewBag.list_startday = new SelectList(AdminCore.get_lookups("week-startday"), "id", "value", config.startday);
            ViewBag.list_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", config.default_role);
            return View(config);
        }
public ActionResult StaffSettings()
        {
            Config config = AdminCore.get_sysconfig();

            ViewBag.list_dateformat = new SelectList(AdminCore.get_lookups("date-format"), "id", "value", config.dateformat);
            ViewBag.list_startday = new SelectList(AdminCore.get_lookups("week-startday"), "id", "value", config.startday);
            ViewBag.list_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", config.default_role);
            return View(config);
        }
public ActionResult ShiftSettings()
        {
            Config config = AdminCore.get_sysconfig();

            ViewBag.list_dateformat = new SelectList(AdminCore.get_lookups("date-format"), "id", "value", config.dateformat);
            ViewBag.list_startday = new SelectList(AdminCore.get_lookups("week-startday"), "id", "value", config.startday);
            ViewBag.list_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", config.default_role);
            return View(config);
        }
public ActionResult UploadDocument()
        {
            return View();
        }
public ActionResult SettingDocument()
        {
            return View();
        }
        public ActionResult Staff(string _dept_id)
        {
            long dept_id = _dept_id.ToLongOrDefault();
            List<Staff> staff = AdminCore.get_deptstaff(dept_id);
            return View(staff);
        }

        public ActionResult Organisation()
        {
            Organisation organisation = AdminCore.get_organisation();
            return View(organisation);
        }
        [HttpPost]
        public ActionResult Organisation(FormCollection _form)
        {
            string code = _form["code"].ToString();
            string name = _form["name"].ToString();
            string reg = _form["reg"].ToString();
            string vat = _form["vat"].ToString();
            string trading = _form["trading"].ToString();
            string type = _form["type"].ToString();
            string tel1 = _form["tel1"].ToString();
            string tel2 = _form["tel2"].ToString();
            string fax1 = _form["fax1"].ToString();
            string fax2 = _form["fax2"].ToString();
            string email1 = _form["email1"].ToString();
            string email2 = _form["email2"].ToString();

            string physical1 = _form["physical1"].ToString();
            string physical2 = _form["physical2"].ToString();
            string physical3 = _form["physical3"].ToString();
            string physicalcode = _form["physicalcode"].ToString();

            string postal1 = _form["postal1"].ToString();
            string postal2 = _form["postal2"].ToString();
            string postal3 = _form["postal3"].ToString();
            string postalcode = _form["postalcode"].ToString();


            using (lidEntities dbo = new lidEntities())
            {
                b_organisation organisation = dbo.b_organisation.FirstOrDefault();
                if (organisation == null)
                {
                    organisation = new b_organisation();
                    organisation.organisation_code = code;
                    organisation.organisation_name = name;
                    organisation.registration_number = reg;
                    organisation.vat_number = vat;
                    organisation.trading_name = trading;
                    organisation.organisation_type = type;
                    organisation.telephone1 = tel1;
                    organisation.telephone2 = tel2;
                    organisation.fax_number1 = fax1;
                    organisation.fax_number2 = fax2;
                    organisation.email_address1 = email1;
                    organisation.email_address2 = email2;

                    organisation.physical1 = physical1;
                    organisation.physical2 = physical2;
                    organisation.physical3 = physical3;
                    organisation.physicalcode = physicalcode;

                    organisation.postal1 = postal1;
                    organisation.postal2 = postal2;
                    organisation.postal3 = postal3;
                    organisation.postalcode = postalcode;
                    dbo.b_organisation.Add(organisation);
                    dbo.SaveChanges();
                }
                else
                {
                    organisation.organisation_code = code;
                    organisation.organisation_name = name;
                    organisation.registration_number = reg;
                    organisation.vat_number = vat;
                    organisation.trading_name = trading;
                    organisation.organisation_type = type;
                    organisation.telephone1 = tel1;
                    organisation.telephone2 = tel2;
                    organisation.fax_number1 = fax1;
                    organisation.fax_number2 = fax2;
                    organisation.email_address1 = email1;
                    organisation.email_address2 = email2;

                    organisation.physical1 = physical1;
                    organisation.physical2 = physical2;
                    organisation.physical3 = physical3;
                    organisation.physicalcode = physicalcode;

                    organisation.postal1 = postal1;
                    organisation.postal2 = postal2;
                    organisation.postal3 = postal3;
                    organisation.postalcode = postalcode;
                    dbo.Entry(organisation).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }

            }

            return RedirectToAction("Organisation");
        }
    }
}