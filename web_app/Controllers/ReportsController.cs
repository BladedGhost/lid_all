﻿using web_app.BLL;
using web_app.DAL;
using web_app.Models;
//using Rotativa;0
//using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using Dapper;
using System.Configuration;
using System.IO;

namespace web_app.Controllers
{
    public class ReportsController : Controller
    {

        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        public ActionResult Index()
        {
            string username = User.Identity.Name;
            List<ReportCategory> cats = ReportCore.get_reportcategory(username);
            ReportsDTO reports = new ReportsDTO();
            reports.cat_list = cats;
            return View(reports);
        }

        public ActionResult ReportParameters(long report_id)
        {
            Reports report = ReportCore.get_report(report_id);
            return View(report);
        }

        public ActionResult DepartmentStaff()
        {
            List<rptDeptWithStaff> data = ReportCore.rpt_deptwithstaff();
            return View(data);
        }

        public ActionResult RecentReports()
        {
            string username = User.Identity.Name;
            List<RptRecentDTO> list = ReportCore.get_RecentReports(username);
            return View(list);
        }

        public ActionResult ManageRecent()
        {
            return View();
        }
        public JsonResult GetRecentReports()
        {
            string username = User.Identity.Name;
            List<RptRecentDTO> list = ReportCore.get_RecentReports(username);
            var json = new JavaScriptSerializer().Serialize(list);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DelRecentReport(string id)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long _id = id.ToLongOrDefault(0);
                b_recentreports recent = dbo.b_recentreports.Where(x => x.id == _id).FirstOrDefault();
                if (recent != null)
                {
                    dbo.Entry(recent).State = System.Data.Entity.EntityState.Deleted;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Recent report successfully deleted.";
                }
                else
                {
                    res.code = "0";
                    res.message = "Recent report not found, Please refresh list and try again.";
                }
            }

            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPassReport()
        {
            List<PassFailReportDTO> res = new List<PassFailReportDTO>();
            using (lidEntities dbo = new lidEntities())
            {
                using (var cn = GetOpenConnection())
                {
                    string username = User.Identity.Name.ToString();
                    res = cn.Query<PassFailReportDTO>("rpt_get_passinspection", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
                }
                //if (recent != null)
                //{
                //    dbo.Entry(recent).State = System.Data.Entity.EntityState.Deleted;
                //    dbo.SaveChanges();
                //    res.code = "1";
                //    res.message = "Recent report successfully deleted.";
                //}
                //else
                //{
                //    res.code = "0";
                //    res.message = "Recent report not found, Please refresh list and try again.";
                //}
            }

            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RecentSettings()
        {
            return View();
        }
        public ActionResult ArchivedReports()
        {
            List<ArchiveDTO> archive = ReportCore.get_archivereports(User.Identity.Name);
            return View(archive);
        }
        public ActionResult ManageArchives()
        {
            return View();
        }
        public JsonResult GetManageArchives()
        {
            string username = User.Identity.Name;
            List<ArchiveDTO> list = ReportCore.get_archivereports(username);
            var json = new JavaScriptSerializer().Serialize(list);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DelArchivesFiles(string id)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long _id = id.ToLongOrDefault(0);
                b_reportsarchive recent = dbo.b_reportsarchive.Where(x => x.id == _id).FirstOrDefault();
                if (recent != null)
                {
                    dbo.Entry(recent).State = System.Data.Entity.EntityState.Deleted;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Archive file was successfully deleted.";
                }
                else
                {
                    res.code = "0";
                    res.message = "Archive file was not found, Please refresh list and try again.";
                }
            }

            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ArchiveSettings()
        {
            return View();
        }
        public ActionResult ScheduleReport()
        {
            string username = User.Identity.Name;
            List<ReportCategory> cats = ReportCore.get_reportcategory(username);
            ReportsDTO reports = new ReportsDTO();
            reports.cat_list = cats;
            return View(reports);
        }
        public ActionResult ScheduleReport_win(string report_id)
        {
            ViewBag.report_id = report_id;
            return View();
        }

        public JsonResult SaveReportScheduler(string report_id, string title, string desc, string from, string to, string repeatevery, string email, string increamental, string mo, string tu, string we, string th, string fr, string sa, string su, string output, string repeat)
        {
            AjaxResults res = new AjaxResults();

            DateTime _from = Convert.ToDateTime(from);
            DateTime _to = Convert.ToDateTime(to);

            using (lidEntities dbo = new lidEntities())
            {
                b_reportschedule schedule = new b_reportschedule();
                schedule.report_id = report_id.ToLongOrDefault(0);
                schedule.title = title;
                schedule.desc = desc;
                schedule.date_from = _from;
                schedule.date_to = _to;
                schedule.repeatevery = repeatevery.ToInt16OrDefault();
                schedule.email = email;
                schedule.increamental = increamental.ToBooleanOrDefault();
                schedule.mo = mo.ToBooleanOrDefault();
                schedule.tu = tu.ToBooleanOrDefault();
                schedule.we = we.ToBooleanOrDefault();
                schedule.th = th.ToBooleanOrDefault();
                schedule.fr = fr.ToBooleanOrDefault();
                schedule.sa = sa.ToBooleanOrDefault();
                schedule.su = su.ToBooleanOrDefault();
                schedule.output = output;
                schedule.repeat = repeat;
                schedule.date_created = DateTime.Now;
                schedule.deleted = false;
                schedule.owner = User.Identity.Name;

                dbo.b_reportschedule.Add(schedule);
                dbo.SaveChanges();
                res.code = "1";
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageSchedule()
        {
            //List<ReportScheduleDTO> archive = ReportCore.get_reportschedule(User.Identity.Name);
            return View();
        }

        public JsonResult GetManageSchedule()
        {
            string username = User.Identity.Name;
            List<ReportScheduleDTO> list = ReportCore.get_reportschedule(username);
            var json = new JavaScriptSerializer().Serialize(list);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DelReportSchedule(string id)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long _id = id.ToLongOrDefault(0);
                b_reportschedule recent = dbo.b_reportschedule.Where(x => x.id == _id).FirstOrDefault();
                if (recent != null)
                {
                    dbo.Entry(recent).State = System.Data.Entity.EntityState.Deleted;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Report scheduler was successfully deleted.";
                }
                else
                {
                    res.code = "0";
                    res.message = "Report scheduler was not found, Please refresh list and try again.";
                }
            }

            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ScheduleHistory()
        {
            return View();
        }
        public JsonResult GetScheduleHistory()
        {
            string username = User.Identity.Name;
            List<ScheduleHistoryDTO> list = new List<ScheduleHistoryDTO>();
            var json = new JavaScriptSerializer().Serialize(list);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ManageReports()
        {
            string username = User.Identity.Name;
            List<ReportCategory> cats = ReportCore.get_allreportcategory(username);
            ReportsDTO reports = new ReportsDTO();
            reports.cat_list = cats;
            return View(reports);
        }

        public JsonResult ToggleReport(string id)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                long report_id = id.ToLongOrDefault(0);
                List<b_reporthide> reports = dbo.b_reporthide.Where(x => x.username == username && x.report_id == report_id).ToList();
                if (reports.Count > 0)
                {
                    foreach (b_reporthide report in reports)
                    {
                        dbo.Entry(report).State = System.Data.Entity.EntityState.Deleted;
                        dbo.SaveChanges();
                    }
                    res.code = "1";
                    res.message = "Report Is Showing On The List.";
                }
                else
                {
                    b_reporthide report = new b_reporthide();
                    report.report_id = report_id;
                    report.date_created = DateTime.Now;
                    report.username = username;

                    dbo.b_reporthide.Add(report);
                    dbo.SaveChanges();

                    res.code = "2";
                    res.message = "Report Is Hidden From The List.";
                }
            }
            var json = new JavaScriptSerializer().Serialize(res);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult rptDepartmentStaff(int dept_id)
        {
            List<rptDeptStaffCount> data = ReportCore.rpt_deptposstaffcount(dept_id);
            var json = new JavaScriptSerializer().Serialize(data);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LogRecentReport(string report_id, string report_url)
        {
            var res = "";
            long _report_id = report_id.ToLongOrDefault();
            using (lidEntities dbo = new lidEntities())
            {
                b_recentreports recent = new b_recentreports();
                recent.report_id = report_id.ToLongOrDefault();
                recent.report_url = report_url;
                recent.username = User.Identity.Name;
                recent.report_date = DateTime.Now;

                dbo.b_recentreports.Add(recent);
                dbo.SaveChanges();

                res = "saved";
            }
            var json = new JavaScriptSerializer().Serialize(res);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public void LogArchiveReport(string fileName, string desc)
        {
            string username = User.Identity.Name;
            using (lidEntities dbo = new lidEntities())
            {
                b_reportsarchive archive = new b_reportsarchive();
                archive.filename = fileName;
                archive.description = desc;
                archive.username = User.Identity.Name;
                archive.datecreated = DateTime.Now;

                dbo.b_reportsarchive.Add(archive);
                dbo.SaveChanges();
            }
        }

        public ActionResult MonthlySchedule(string _dept_id, string monthdate)
        {
            string month = string.IsNullOrWhiteSpace(monthdate) ? DateTime.Today.ToString("MMMM yyyy") : monthdate;
            string weekday = ("01 " + month).ToDateTimeOrDefault().ToString("dd/MM/yyyy");
            ShiftManage shiftManage = new ShiftManage();

            shiftManage.dept_id = Convert.ToInt32((_dept_id == "") ? "0" : _dept_id);
            shiftManage.Staff_list = AdminCore.get_deptstaff(shiftManage.dept_id);// : AdminCore.get_deptstaff(shiftManage.dept_id, shiftManage.pos_id);
            shiftManage.Week_days = AdminCore.get_monthdaysbydt(weekday);

           // ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", shiftManage.dept_id);
            ViewBag.list_position = new SelectList(AdminCore.get_positions(), "position_id", "position_name", shiftManage.pos_id);

            shiftManage.weekday = weekday;
            ViewBag.monthdate = month;
            return View(shiftManage);
        }
        public ActionResult AdvMonthlySchedule(string _dept_id, string monthdate)
        {
            string month = string.IsNullOrWhiteSpace(monthdate) ? DateTime.Today.ToString("MMMM yyyy") : monthdate;
            string weekday = ("01 " + month).ToDateTimeOrDefault().ToString("dd/MM/yyyy");
            ShiftManage shiftManage = new ShiftManage();

            shiftManage.dept_id = Convert.ToInt32((_dept_id == "") ? "0" : _dept_id);
            shiftManage.Staff_list = AdminCore.get_deptstaff(shiftManage.dept_id);// : AdminCore.get_deptstaff(shiftManage.dept_id, shiftManage.pos_id);
            shiftManage.Week_days = AdminCore.get_monthdaysbydt(weekday);

           // ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", shiftManage.dept_id);
            ViewBag.list_position = new SelectList(AdminCore.get_positions(), "position_id", "position_name", shiftManage.pos_id);

            shiftManage.weekday = weekday;
            ViewBag.monthdate = month;
            return View(shiftManage);
        }

        [HttpPost]
        public ActionResult PostMonthlySchedule(string _dept_id, string pos)
        {
            //string month = _form["month"].ToString();
            //string _dept_id = _form["ddlDept"].ToString();
            //string _pos_id = _form["ddlPosition"].ToString();
            string month = DateTime.Today.ToShortDateString();

            ShiftManage shiftManage = new ShiftManage();

            shiftManage.dept_id = Convert.ToInt32((_dept_id == "") ? "0" : _dept_id);
            //shiftManage.pos_id = Convert.ToInt32((_pos_id == "") ? "0" : _pos_id);

            //string weekday = (string.IsNullOrWhiteSpace(_weekday)) ? DateTime.Today.ToShortDateString() : _weekday;
            shiftManage.Staff_list = AdminCore.get_deptstaff(shiftManage.dept_id);// : AdminCore.get_deptstaff(shiftManage.dept_id, shiftManage.pos_id);
            shiftManage.Week_days = AdminCore.get_monthdaysbydt(month);

           // ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", shiftManage.dept_id);
            ViewBag.list_position = new SelectList(AdminCore.get_positions(), "position_id", "position_name", shiftManage.pos_id);

            shiftManage.weekday = month;
            return View(shiftManage);
        }

        public ActionResult MonthlySchedulePDF(string month, string _dept_id, string _pos_id)
        {
            ShiftManage shiftManage = new ShiftManage();

            shiftManage.dept_id = Convert.ToInt32((_dept_id == "") ? "0" : _dept_id);
            shiftManage.pos_id = Convert.ToInt32((_pos_id == "") ? "0" : _pos_id);

            //string weekday = (string.IsNullOrWhiteSpace(_weekday)) ? DateTime.Today.ToShortDateString() : _weekday;
            shiftManage.Staff_list = AdminCore.get_deptstaff(shiftManage.dept_id);// (shiftManage.pos_id == 0) ? AdminCore.get_deptstaff(shiftManage.dept_id) : AdminCore.get_deptstaff(shiftManage.dept_id, shiftManage.pos_id);
            shiftManage.Week_days = AdminCore.get_monthdaysbydt(month);

            //ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", shiftManage.dept_id);
            ViewBag.list_position = new SelectList(AdminCore.get_positions(), "position_id", "position_name", shiftManage.pos_id);

            shiftManage.weekday = month;

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            return View();
            //return new ViewAsPdf("MonthlySchedulePDF", shiftManage)
            //{
            //    FileName = "MonthlySchedule.pdf",
            //    CustomSwitches = footer,
            //    PageSize = Size.A4,
            //    PageOrientation = Orientation.Landscape,
            //};

        }

        public ActionResult GetReport(string report_id, string datefrom, string dateto)
        {
            ShiftManage shiftManage = new ShiftManage();


            shiftManage.Staff_list = AdminCore.get_deptstaff(report_id.ToLongOrDefault());
            shiftManage.Week_days = AdminCore.get_monthdaysbydt(datefrom);

            //ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", shiftManage.dept_id);
            //ViewBag.list_position = new SelectList(AdminCore.get_positions(), "position_id", "position_name", shiftManage.pos_id);

            //shiftManage.weekday = datefrom;

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            string filePath;
            SetupFilePath("System Report", out filePath);

            return View();
            //return new ViewAsPdf("GetReportPDF", shiftManage)
            //{
            //    FileName = "MonthlySchedule.pdf",
            //    CustomSwitches = footer,
            //    PageSize = Size.A4,
            //    PageOrientation = Orientation.Landscape,
            //};

        }

        public ActionResult SystemUsers(bool archive)
        {
            List<Users> users = ReportCore.rpt_allsystemusers();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("System Users Report", out filePath);
                return View();
                //return new ViewAsPdf("rptSystemUsers", users)
                //{
                //    FileName = "ReportSystemUsers.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptSystemUsers", users)
                //{
                //    FileName = "ReportSystemUsers.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult DefaultData(bool archive)
        {
            List<DefaultDataDTO> data = ReportCore.rpt_getdefaultdata();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Default Data Report", out filePath);

                return View();
                //return new ViewAsPdf("rptDefaultData", data)
                //{
                //    FileName = "ReportDefaultData.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptDefaultData", data)
                //{
                //    FileName = "ReportDefaultData.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult SystemRoles(bool archive)
        {
            List<RPTRolesDTO> data = ReportCore.rpt_getsystemroles();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("System Roles Report", out filePath);
                return View();
                //return new ViewAsPdf("rptSystemRoles", data)
                //{
                //    FileName = "ReportSystemRoles.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptSystemRoles", data)
                //{
                //    FileName = "ReportSystemRoles.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult SystemShifts(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("System Shifts Report", out filePath);
                return View();
                //return new ViewAsPdf("rptSystemShifts", data)
                //{
                //    FileName = "SystemShiftsReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptSystemShifts", data)
                //{
                //    FileName = "SystemShiftsReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult OrganisationStructure(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Organisation Structure Report", out filePath);
                return View();
                //return new ViewAsPdf("rptOrganisationStructure", data)
                //{
                //    FileName = "OrganisationStructureReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptOrganisationStructure", data)
                //{
                //    FileName = "OrganisationStructureReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult OrganisationStaff(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Organisation Staff Report", out filePath);
                return View();
                //return new ViewAsPdf("rptOrganisationStaff", data)
                //{
                //    FileName = "OrganisationStaffReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptOrganisationStaff", data)
                //{
                //    FileName = "OrganisationStaffReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult OrganisationDepartment(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Organisation Department Report", out filePath);
                return View();
                //return new ViewAsPdf("rptOrganisationDepartment", data)
                //{
                //    FileName = "OrganisationDepartmentReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptOrganisationDepartment", data)
                //{
                //    FileName = "OrganisationDepartmentReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult DepartmentHours(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            //if (archive)
            //{
            //    string filePath;
            //    SetupFilePath("Department Hours Report", out filePath);
            //    return new ViewAsPdf("rptDepartmentHours", data)
            //    {
            //        FileName = "DepartmentHoursReport.pdf",
            //        CustomSwitches = footer,
            //        PageSize = Size.A4,
            //        PageOrientation = Orientation.Landscape,
            //        SaveOnServerPath = filePath,
            //    };
            //}
            //else
            //{

            return View();
            //return new ViewAsPdf("rptDepartmentHours", data)
            //{
            //    FileName = "DepartmentHoursReport.pdf",
            //    CustomSwitches = footer,
            //    PageSize = Size.A4,
            //    PageOrientation = Orientation.Landscape,
            //};
            //}

        }

        public ActionResult StaffMembers(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Staff Members Report", out filePath);
                return View();
                //return new ViewAsPdf("rptStaffMembers", data)
                //{
                //    FileName = "StaffMembersReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptStaffMembers", data)
                //{
                //    FileName = "StaffMembersReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult BirthDays(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Staff BirthDays Report", out filePath);
                return View();
                //return new ViewAsPdf("rptBirthDays", data)
                //{
                //    FileName = "BirthDaysReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptBirthDays", data)
                //{
                //    FileName = "BirthDaysReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult StaffHours(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Staff Hours Report", out filePath);
                return View();
                //return new ViewAsPdf("rptStaffHours", data)
                //{
                //    FileName = "StaffHoursReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //   /// SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptStaffHours", data)
                //{
                //    FileName = "StaffHoursReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult StaffLeave(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Staff Leave Report", out filePath);
                return View();
                //return new ViewAsPdf("rptStaffLeave", data)
                //{
                //    FileName = "rptStaffLeave.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptSystemShifts", data)
                //{
                //    FileName = "StaffLeaveReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult MonthlyScheduler(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Monthly Scheduler Report", out filePath);
                return View();
                //return new ViewAsPdf("rptMonthlyScheduler", data)
                //{
                //    FileName = "MonthlySchedulerReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptMonthlyScheduler", data)
                //{
                //    FileName = "MonthlySchedulerReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult AdvMonthlyScheduler(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath = "";
                SetupFilePath("Adv Monthly Scheduler Report", out filePath);
                return View();
                //return new ViewAsPdf("rptAdvMonthlyScheduler", data)
                //{
                //    FileName = "AdvMonthlySchedulerReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = BuildPdf filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptAdvMonthlyScheduler", data)
                //{
                //    FileName = "AdvMonthlySchedulerReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }

        public ActionResult StaffSchedule(bool archive)
        {
            List<RptShiftDTO> data = ReportCore.rpt_getsystemshifts();

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            if (archive)
            {
                string filePath;
                SetupFilePath("Staff Schedule Report", out filePath);
                return View();
                //return new ViewAsPdf("rptStaffSchedule", data)
                //{
                //    FileName = "StaffScheduleReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //    //SaveOnServerPath = filePath,
                //};
            }
            else
            {
                return View();
                //return new ViewAsPdf("rptStaffSchedule", data)
                //{
                //    FileName = "StaffScheduleReport.pdf",
                //    CustomSwitches = footer,
                //    PageSize = Size.A4,
                //    PageOrientation = Orientation.Landscape,
                //};
            }

        }


        public ActionResult PassFailReport()
        {
            PassFailReportDTO inspections = new PassFailReportDTO();

            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                long id = 1;
                inspections = cn.Query<PassFailReportDTO>("rpt_get_passinspection_byid", new { inspection_id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

                List<PassFailReportItemsDTO> lines = new List<PassFailReportItemsDTO>();
                using (var cn = GetOpenConnection())
                {
                    string username = User.Identity.Name.ToString();
                    lines = cn.Query<PassFailReportItemsDTO>("rpt_get_passinspection_items", new { inspection_id = inspections.inspection_id }, commandType: CommandType.StoredProcedure).ToList();
                }
            inspections.line_items = lines;
            

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            return View(inspections);
            //return new ViewAsPdf("PassFailReport", inspections)
            //{
            //    FileName = "PassReport.pdf",
            //    CustomSwitches = footer,
            //    PageSize = Size.A4,
            //    PageOrientation = Orientation.Landscape,
            //};
        }

        private void SetupFilePath(string Desc, out string filePath)
        {
            string unique_name = Desc.Replace(" ", "") + DateTime.Now.ToString("_yyMMdd_hhmmss") + ".pdf";

            filePath = Path.Combine(Server.MapPath(@"~/Files/ReportsArchivers/"), unique_name);
            LogArchiveReport(unique_name, Desc);
        }
    }
}