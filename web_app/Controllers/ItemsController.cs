﻿using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace web_app.Controllers
{
    [Authorize]
    public class ItemsController : Controller
    {
        // GET: Items
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Category(string id)
        {
            string username = User.Identity.Name;
            ViewBag.group_category = new SelectList(ItemCore.get_company_categories("Category_Group", username), "id", "value", id);
            Users user = AdminCore.get_user(User.Identity.Name);
            ViewBag.id = id;
            //List<ItemCategory> res = ItemCore.get_category_many(id);
            return View(user);
        }

        public ActionResult flt_group_category(FormCollection _form)
        {
            string id = _form["ddlGroupCat"].ToString();
            return RedirectToAction("Category", new { id = id });
        }
        public ActionResult Tests()
        {
            return View();
        }
        public ActionResult DelCat()
        {
            using (lidEntities dbo = new lidEntities())
            {
                List<lid_category> cat = dbo.lid_category.Where(m => m.parent_id != null).ToList();
                foreach (lid_category item in cat)
                {
                    dbo.lid_category.Remove(item);
                    dbo.SaveChanges();
                }
            }
                return View();
        }

        public ActionResult LinkTest(string cat_id, string itm_id, string keyword)
        {
            Users user = AdminCore.get_user(User.Identity.Name);
            long category_id = cat_id == null ? 0 : cat_id.ToLongOrDefault();
            long item_id = itm_id == null ? 0 : itm_id.Replace("itm_","").ToLongOrDefault();
            ViewBag.list_category = new SelectList(ItemCore.get_category_tree_comp(User.Identity.Name), "id", "text", cat_id);
            ViewBag.list_items = new SelectList(ItemCore.get_items_tree(category_id.ToStringOrDefault()), "id", "text", itm_id);
            LinkTests linkTest = new LinkTests();
            linkTest.cat_id = cat_id;
            linkTest.itm_id = itm_id;
            linkTest.keyword = keyword;
            linkTest.isadmin = user.isadmin;
            linkTest.cat_list = ItemCore.get_category_tree("");
            linkTest.tests_list = ItemCore.get_unlinked_tests(item_id, keyword == null ? "" : keyword);
            linkTest.linked_tests = ItemCore.get_linked_tests(item_id);

            return View(linkTest);
        }
        public JsonResult PostUpdateItemImage(Items model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                lid_items item = dbo.lid_items.Where(m => m.item_id == model.item_id).FirstOrDefault();
                item.image = model.image != null ? model.image : item.image;
                item.std_code = model.std_code;
                dbo.Entry(item).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();
                res.code = "1";
                res.message = "Image successfully update";
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult flt_category(FormCollection _form)
        {
            string id = _form["ddlCat"].ToString();
            return RedirectToAction("LinkTest", new { cat_id = id });
        }
        public ActionResult flt_items(FormCollection _form, string cat_id, string keyword)
        {
            string id = _form["ddlItems"].ToString();
            return RedirectToAction("LinkTest", new { cat_id = cat_id, itm_id = id, keyword = keyword });
        }
        public ActionResult search_tests(FormCollection _form, string cat_id, string itm_id)
        {
            string keyword = _form["txt_search"].ToString();
            return RedirectToAction("LinkTest", new { cat_id = cat_id, itm_id = itm_id, keyword = keyword });
        }
        

        public ActionResult win_add_test(string id)
        {
            TestModel res = ItemCore.get_test_byid(id);
            TestModel model = res != null ? res : new TestModel();
            return View(model);
        }
        public ActionResult win_import(string id)
        {
            return View();
        }
        public ActionResult win_item_tests(string id)
        {
            List<TestModel> model = ItemCore.get_linked_tests(id.ToLongOrDefault());
            return View(model);
        }
        public ActionResult win_item_image(string id)
        {
            Items model = ItemCore.get_item_byid(id);

            if (model.image != null)
            {
                string imreBase64Data = Convert.ToBase64String(model.image);
                string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                model.imgDataURL = imgDataURL;
            }
            return View(model);
        }
        public JsonResult get_category_json(string group_id)
        {
            List<ItemCategoryTree> res = ItemCore.get_category_tree(group_id);


            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CrudCategory(ItemCategoryTree payload)
        {
            ItemCategory res = ItemCore.crud_category(payload);


            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CrudItem(ItemCategoryTree payload)
        {
            ItemCategory res = ItemCore.crud_item(payload);


            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CrudTest(TestModel payload)
        {
            TestModel res = ItemCore.crud_test(payload);


            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ToggleLink(ToggleLinkModel model)
        {
            ToggleLinkModel res = ItemCore.toggle_link(model);


            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult get_category_items(string id)
        {
            List<ItemCategoryTree> res = ItemCore.get_items_tree(id);


            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteTestById(TestModel model)
        {
            lid_tests test = new lid_tests();
            using (lidEntities dbo = new lidEntities())
            {
                lid_test_item link = dbo.lid_test_item.Where(m => m.test_id == model.test_id).FirstOrDefault();
                if (link == null)
                {
                    test = dbo.lid_tests.Where(m => m.test_id == model.test_id).FirstOrDefault();
                    if (test != null)
                    {
                        dbo.lid_tests.Remove(test);
                        dbo.SaveChanges();
                    }
                }
            }
            string results = new JavaScriptSerializer().Serialize(test);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult get_tests_many()
        {
            List<TestModel> res = ItemCore.get_tests_many();
            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }

}