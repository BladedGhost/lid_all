﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using System.Web.Script.Serialization;

namespace web_app.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult lnkDepartment(FormCollection _form)
        {
            string dept_id = _form["ddlDept"].ToString();
            return RedirectToAction("Index","Home", new { dept_id = dept_id });
        }

        public ActionResult lnkPosition(FormCollection _form, string _weekday = "", int _dept_id = 0, string _uri = "")
        {
            string pos_id = _form["ddlPosition"].ToString();
            return RedirectToAction(_uri, new { _weekday = _weekday, _dept_id = _dept_id, _pos_id = pos_id });
        }

        public ActionResult flt_calendar(FormCollection _form, int _dept_id = 0, int _pos_id = 0, string _uri = "")
        {
            string startday = _form["startDate"].ToString();
            return RedirectToAction(_uri, new { _weekday = startday, _dept_id = _dept_id, _pos_id = _pos_id });
        }

        public ActionResult ManageShiftByDay_win(string _weekday)
        {
            ShiftManage shiftManage = new ShiftManage();

            shiftManage.weekday = _weekday;

            shiftManage.Staff_list = AdminCore.get_allstaff();
            shiftManage.shift_list = AdminCore.get_shifts(User.Identity.Name);
            shiftManage.position_list = AdminCore.get_positions();

            return View(shiftManage);
        }
        public ActionResult StaffViewSchedule_win(string staff_id, string weekday)
        {
            ShiftManage shiftManage = new ShiftManage();

            shiftManage.Staff_list.Add(AdminCore.get_staffById(staff_id.ToLongOrDefault(0)));
            //shiftManage.shift_list = AdminCore.get_shifts(User.Identity.Name);
            //shiftManage.Week_days = AdminCore.get_weekdaysbydt(weekday);

            shiftManage.weekday = weekday;
            return View(shiftManage);
        }
        public ActionResult Index(string dept_id)
        {

            DashBoard dash = new DashBoard();


            dash.list_inspection = CompanyCore.get_company_inspections_today(User.Identity.Name);
            dash.list_sites = CompanyCore.get_sites_many(User.Identity.Name);
            dash.list_inspectors = CompanyCore.get_inspectors_many(User.Identity.Name);
            dash.list_upcoming = CompanyCore.get_upcoming_inspections(User.Identity.Name);
            //ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name");
            //dash.position_list = AdminCore.get_positions();
            //if(string.IsNullOrWhiteSpace(dept_id))
            //dash.department_list = AdminCore.get_depatments(User.Identity.Name);
            //else
            //dash.department_list.Add( AdminCore.get_depatment(dept_id));
            ////dash.leave_list = AdminCore.get_activeleave(null);
            //dash.Staff_list = AdminCore.get_staffbirthdays();
            //dash.shifts = AdminCore.get_currentshifts();
            //dash.shift_list = AdminCore.get_shifts(User.Identity.Name);
            //dash.weekday = DateTime.Today.ToShortDateString();

            //var _menu = AdminCore.get_topuserwebmenu(User.Identity.Name);
            //if (_menu == null || _menu.menu_name == "Dashboard")
            //{
            //   // return RedirectToAction("NoRoles", "Account");
            //}
            //else
            //{
            //    ViewBag.IsAdmin = _menu.isadmin;
            //    return RedirectToAction(_menu.menu_action.ToString(), _menu.menu_controller.ToString());
            //}

            return View(dash);
        }
        public ActionResult _ViewDeptPostStaff_win(string dept_id, string pos_id, string weekday)
        {
            List<Staff> staff = new List<Staff>();
            ViewBag.Dept = ((Dept)AdminCore.get_depatment(dept_id)).dept_name;
            ViewBag.Pos = ((Position)AdminCore.get_position(pos_id)).position_name;

            ViewBag.weekdays = AdminCore.get_weekdaysbydt(weekday);
            using (lidEntities dbo = new lidEntities())
            {
                var stafflink = dbo.staffdept_link.Where(m => m.post_id.ToString() == pos_id && m.dept_id.ToString() == dept_id).Select(m => m.staff_id).ToList();
                if (stafflink != null)
                {
                    var weekdays = ((List<WeekDays>)AdminCore.get_weekdaysbydt(weekday)).Select(m => m.day_name);
                    List<b_schedule> bschedule = dbo.b_schedule.Where(m => weekdays.Any(x => m.weekday == x) && stafflink.Any(x => m.staff_id == x)).ToList();
                    foreach (b_schedule _bschedule in bschedule)
                    {
                        Staff _staff = AdminCore.get_staffById(_bschedule.staff_id.ToLongOrDefault(0));
                        _staff.weekday = _bschedule.weekday;
                        staff.Add(_staff);
                    }
                }
            }
            return View(staff);
        }
        public ActionResult Calendar()
        {
            DashBoard dash = new DashBoard();
            //dash.events_list = AdminCore.get_events();
            return View(dash);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            ViewBag.list_structurelevel = new SelectList(AdminCore.get_strcturelevel(), "LevelId", "LevelName");
            return View();
        }

        public ActionResult Queue()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Tempo()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Stats()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ApproveShift(string _weekday = "", int _dept_id = 0)
        {
            ShiftManage shiftManage = new ShiftManage();


            shiftManage.dept_id = _dept_id;

            string weekday = (string.IsNullOrWhiteSpace(_weekday)) ? DateTime.Today.ToShortDateString() : _weekday;
            shiftManage.Staff_list = AdminCore.get_deptstaff(_dept_id);//: AdminCore.get_deptstaff(_dept_id, _pos_id);
            shiftManage.Week_days = AdminCore.get_weekdaysbydt(weekday);
            shiftManage.shift_list = AdminCore.get_shifts(User.Identity.Name);
            shiftManage.position_list = AdminCore.get_positions();

            //ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", shiftManage.dept_id);
            //ViewBag.list_shifts = new SelectList(AdminCore.get_shifts(User.Identity.Name), "shift_id", "shift_name", 0);
            ViewBag.list_position = new SelectList(AdminCore.get_positions(), "position_id", "position_name", shiftManage.pos_id);

            shiftManage.weekday = weekday;
            return View(shiftManage);
        }

        public ActionResult _approveshift_block(string staff_id, string dayname)
        {
            ViewData["staff_id"] = staff_id;
            ViewData["dayname"] = dayname;

            return PartialView();
        }

        public ActionResult ApproveLeave()
        {
            DashBoard dash = new DashBoard();

            ViewBag.list_leavetypes = new SelectList(AdminCore.get_leavetypes(), "leavetype_id", "leavetype_name");
            //dash.leave_list = AdminCore.get_activeleave(false);
            return View(dash);
        }
        public ActionResult ShiftDashboard_win(string shift_id)
        {
            string month =  DateTime.Today.ToString("MMMM yyyy");
            string weekday = ("01 " + month).ToDateTimeOrDefault().ToString("dd/MM/yyyy");
            ShiftManage shiftManage = new ShiftManage();

            //shiftManage.Staff_list = AdminCore.get_deptstaff(shiftManage.dept_id);// : AdminCore.get_deptstaff(shiftManage.dept_id, shiftManage.pos_id);
            shiftManage.Week_days = AdminCore.get_monthdaysbydt(weekday);
            shiftManage.shift_list.Add(AdminCore.get_shift(shift_id));
            //shiftManage.dept = AdminCore.get_depatments(User.Identity.Name);
            shiftManage.shift_id = shift_id;
            return View(shiftManage);
        }
        public ActionResult CurrentShiftStaff_win(string shift_id)
        {
            StaffOnShift dash = new StaffOnShift();
            dash.staff_list = AdminCore.get_staffonthisshifttoday(shift_id);
            dash.shift = AdminCore.get_shift(shift_id);
            dash.shift_id = shift_id;
            return View(dash);
        }
        public ActionResult ViewAllCurrentShift_win()
        {
            List<ShiftLite> shift = AdminCore.get_currentshifts();
            return View(shift);
        }
        public ActionResult StaffOnShift_win()
        {
            ViewBag.list_shifts = new SelectList(AdminCore.get_shifts(User.Identity.Name), "shift_id", "shift_name", 0);
            return View();
        }
        public ActionResult StaffOffShift_win()
        {
            ViewBag.list_shifts = new SelectList(AdminCore.get_shifts(User.Identity.Name), "shift_id", "shift_name", 0);
            return View();
        }

        public ActionResult DayShiftDashPerDeptPDF(string dept_id, string dayname, string dept)
        {
            ViewBag.dayname = dayname;
            ViewBag.department = dept;
            List<Staff> staff = AdminCore.get_dashStaffOnShiftByDay(dept_id, dayname);
            
            //return View(shiftManage);

            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
                DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            //return new ViewAsPdf("DayShiftDashPerDeptPDF", staff)
            //{
            //    FileName = dayname + "_Schedule.pdf",
            //    CustomSwitches = footer,
            //    PageSize = Size.A4,
            //    PageOrientation = Orientation.Landscape,
            //};

            string json = new JavaScriptSerializer().Serialize("");
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public string Init()
        {
            bool rslt = Utils.InitialiseDiary();
            return rslt.ToString();
        }

        public bool UpdateEvent(int id, string NewEventStart, string NewEventEnd)
        {
            return DiaryEvent.UpdateDiaryEvent(id, NewEventStart, NewEventEnd);
        }
        
        public bool SaveEvent(string Title, string NewEventDate, string NewEventTime, string NewEventDuration)
        {
            return DiaryEvent.CreateNewEvent(Title, NewEventDate, NewEventTime, NewEventDuration);
        }
        public bool DeleteEvent(string eventId)
        {
            return DiaryEvent.DeleteEvent(eventId);
        }
        
    }
}