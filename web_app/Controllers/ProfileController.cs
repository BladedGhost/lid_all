﻿using web_app.Models;
using web_app.BLL;
using web_app.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web_app.Controllers
{
    public class ProfileController : Controller
    {
        public ProfileController()
        {
        }
        public ProfileController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MyCalendar()
        {
            DashBoard dash = new DashBoard();
            dash.events_list = AdminCore.get_events();
            return View(dash);
        }
        // GET: Profile
    
        public ActionResult ChangePassword()
        {
            return View();
        }
        public ActionResult Settings()
        {
            return View();
        }
        public ActionResult EditStaff_win(string staff_id)
        {
            long id = staff_id.ToLongOrDefault();
            Staff _staff = AdminCore.get_staffById(id);


            //ViewBag.list_title = new SelectList(AdminCore.get_titles(), "title_id", "title_name");
           // ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", _staff.department_id);
            ViewBag.list_position = new SelectList(AdminCore.get_positions(), "position_id", "position_name", _staff.position_id);
            ViewBag.list_emptype = new SelectList(AdminCore.get_emptypes(), "type_id", "type_name", _staff.emp_type_id);
            ViewBag.list_structurelevel = new SelectList(AdminCore.get_strcturelevel(), "LevelId", "LevelName", _staff.structure_level);

            ViewBag.list_title = new SelectList(AdminCore.get_lookupvalues("Title"), "code_id", "code_name", _staff.staff_title);
            ViewBag.list_idtype = new SelectList(AdminCore.get_lookupvalues("ID Type"), "code_id", "code_name", _staff.idType_id);


            ViewBag.list_countryofissue = new SelectList(AdminCore.get_lookupvalues("Country Of Issue"), "code_id", "code_name", _staff.counntyOfIssue_id);
            ViewBag.list_race = new SelectList(AdminCore.get_lookupvalues("Race"), "code_id", "code_name", _staff.race_id);
            ViewBag.list_nationality = new SelectList(AdminCore.get_lookupvalues("Nationality"), "code_id", "code_name", _staff.nationality_id);
            ViewBag.list_position = new SelectList(AdminCore.get_lookupvalues("Positions"), "code_id", "code_name", _staff.position_id);
            ViewBag.list_qualification = new SelectList(AdminCore.get_lookupvalues("Qualification"), "code_id", "code_name", _staff.qualification_id);


            ViewBag.list_relationship = new SelectList(AdminCore.get_lookupvalues("Relationship"), "code_id", "code_name");
            ViewBag.list_employementtype = new SelectList(AdminCore.get_lookupvalues("Employement Types"), "code_id", "code_name", _staff.emp_type);

            ViewBag.list_gender = new SelectList(AdminCore.get_lookupvalues("Gender"), "code_id", "code_name", _staff.gender_id);
            ViewBag.list_maritalstatus = new SelectList(AdminCore.get_lookupvalues("Marital status"), "code_id", "code_name", _staff.maritalstatus_id);
            return View(_staff);
        }
        public ActionResult TimeSheet_win(string staff_id)
        {
            Staff staff = AdminCore.get_stafflite(staff_id);
            return View(staff);
        }
        public ActionResult ManageShift_win(string staff_id)
        {
            Staff staff = AdminCore.get_stafflite(staff_id);
            return View(staff);
        }
        public ActionResult LeaveRequest_win(string staff_id)
        {
            Staff staff = AdminCore.get_stafflite(staff_id);
            ViewBag.list_staff = new SelectList(AdminCore.get_allstafflite(), "staff_id", "staff_desc");
            ViewBag.list_leaveType = new SelectList(AdminCore.get_leavetypes(), "leavetype_id", "leavetype_name");
            return View(staff);
        }

    }
}