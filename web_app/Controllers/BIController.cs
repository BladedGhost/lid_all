﻿using Dapper;
using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace web_app.Controllers
{
    [Authorize]
    public class BIController : Controller
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        public ActionResult Index()
        {
            string username = User.Identity.Name;
            List<ReportCategory> cats = ReportCore.get_reportcategory(username);
            ReportsDTO reports = new ReportsDTO();
            reports.cat_list = cats;
            return View(reports);
        }
        public ActionResult ReportParameters(long report_id)
        {
            Reports report = ReportCore.get_report(report_id);
            return View(report);
        }
        public ActionResult passreport()
        {
            List<PassFailReportDTO> res = new List<PassFailReportDTO>();
            using (lidEntities dbo = new lidEntities())
            {
                using (var cn = GetOpenConnection())
                {
                    string username = User.Identity.Name.ToString();
                    res = cn.Query<PassFailReportDTO>("rpt_get_passinspection", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            return View(res);
        }
        public ActionResult failreport()
        {
            return View();
        }
        public ActionResult assetlist()
        {
            return View();
        }

        public JsonResult GetPassReport()
        {
            List<PassFailReportDTO> res = new List<PassFailReportDTO>();
            using (lidEntities dbo = new lidEntities())
            {
                using (var cn = GetOpenConnection())
                {
                    string username = User.Identity.Name.ToString();
                    res = cn.Query<PassFailReportDTO>("rpt_get_passinspection", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
                }
            }

            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PassFailReport(string id)
        {
            PassFailReportDTO inspections = new PassFailReportDTO();

            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                //long id = 1;
                inspections = cn.Query<PassFailReportDTO>("rpt_get_passinspection_byid", new { inspection_id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (inspections.company_logo != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.company_logo);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.company_logo_data = imgDataURL;
                }
                if (inspections.inspection_image != null)
                {
                    //string imreBase64Data = Convert.ToBase64String(inspections.inspection_image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", inspections.inspection_image);
                    inspections.inspection_image_data = imgDataURL;
                }
                if (inspections.item_image != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.item_image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.item_image_data = imgDataURL;
                }
                if (inspections.signature_img != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.signature_img);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.signature_data = imgDataURL;
                }

            }

            List<PassFailReportItemsDTO> lines = new List<PassFailReportItemsDTO>();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                lines = cn.Query<PassFailReportItemsDTO>("rpt_get_passinspection_items", new { inspection_id = inspections.inspection_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            inspections.line_items = lines;


            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            //return View(inspections);

            return new ViewAsPdf("PassFailReport", inspections)
            {
                FileName = "InpectionReport" + inspections.inspection_id + ".pdf",
                CustomSwitches = footer,
                PageSize = Size.A4,
                //PageOrientation = Orientation.Landscape,
            };
        }
        public ActionResult PassFailCert(string id)
        {
            PassFailReportDTO inspections = new PassFailReportDTO();

            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                //long id = 1;
                inspections = cn.Query<PassFailReportDTO>("rpt_get_passinspection_byid", new { inspection_id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (inspections.company_logo != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.company_logo);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.company_logo_data = imgDataURL;
                }
                if (inspections.inspection_image != null)
                {
                    //string imreBase64Data = Convert.ToBase64String(inspections.inspection_image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", inspections.inspection_image);
                    inspections.inspection_image_data = imgDataURL;
                }
                if (inspections.item_image != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.item_image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.item_image_data = imgDataURL;
                }
                if (inspections.signature_img != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.signature_img);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.signature_data = imgDataURL;
                }
            }

            List<PassFailReportItemsDTO> lines = new List<PassFailReportItemsDTO>();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                lines = cn.Query<PassFailReportItemsDTO>("rpt_get_passinspection_items", new { inspection_id = inspections.inspection_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            inspections.line_items = lines;


            string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
               DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            //return View(inspections);

            return new ViewAsPdf("PassFailCert", inspections)
            {
                FileName = "InpectionCertificate" + inspections.inspection_id + ".pdf",
                CustomSwitches = footer,
                PageSize = Size.A4,
                //PageOrientation = Orientation.Landscape,

            };
        }
       public ActionResult PassFailView(string id)
        {
            PassFailReportDTO inspections = new PassFailReportDTO();

            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                //long id = 1;
                inspections = cn.Query<PassFailReportDTO>("rpt_get_passinspection_byid", new { inspection_id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (inspections.company_logo != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.company_logo);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.company_logo_data = imgDataURL;
                }
                if (inspections.inspection_image != null)
                {
                    //string imreBase64Data = Convert.ToBase64String(inspections.inspection_image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", inspections.inspection_image);
                    inspections.inspection_image_data = imgDataURL;
                }
                if (inspections.item_image != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.item_image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.item_image_data = imgDataURL;
                }
                if (inspections.signature_img != null)
                {
                    string imreBase64Data = Convert.ToBase64String(inspections.signature_img);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    inspections.signature_data = imgDataURL;
                }
            }

            List<PassFailReportItemsDTO> lines = new List<PassFailReportItemsDTO>();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                lines = cn.Query<PassFailReportItemsDTO>("rpt_get_passinspection_items", new { inspection_id = inspections.inspection_id }, commandType: CommandType.StoredProcedure).ToList();
            }
            inspections.line_items = lines;


            //string footer = string.Format("--print-media-type --footer-line --header-line --header-font-size 8 --footer-font-size 8 --footer-center \"{0}\" --footer-left \"{1}\" --footer-right \"Page [page] of [toPage]\"",
            //   DateTime.Now.ToShortDateString() + "   " + DateTime.Now.ToShortTimeString(), "Printed By:" + User.Identity.Name);

            return View(inspections);
        }
        public JsonResult GetPassFailedGrid()
        {
            List<PassFailReportDTO> res = new List<PassFailReportDTO>();
            using (lidEntities dbo = new lidEntities())
            {
                using (var cn = GetOpenConnection())
                {
                    string username = User.Identity.Name.ToString();
                    res = cn.Query<PassFailReportDTO>("rpt_get_passinspection", new { username = username }, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

    }
}