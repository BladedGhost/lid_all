﻿using web_app.Models;
using web_app.BLL;
using web_app.DAL;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;

namespace web_app.Controllers
{
        [Authorize]
    public class AdminController : Controller
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        public AdminController()
        {
        }
        public AdminController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Admin
        public ActionResult Organisation()
        {
            return View();
        }
        // GET: Admin
        public ActionResult spHelper()
        {
            return View();
        }
        public ActionResult OrganisationDetails()
        {
            Organisation organisation = AdminCore.get_organisation();
            return View(organisation);
        }
        public ActionResult WorkArea()
        {
            return View();
        }
      

        public ActionResult ChangePassword_win(string username)
        {
            ViewBag.username = username;
            return View();
        }

        public ActionResult Sysconfig()
        {
            Config config = AdminCore.get_sysconfig();

            ViewBag.list_dateformat = new SelectList(AdminCore.get_lookups("date-format"), "id", "value", config.dateformat);
            ViewBag.list_startday = new SelectList(AdminCore.get_lookups("week-startday"), "id", "value", config.startday);
            ViewBag.list_com_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", config.com_role);
            ViewBag.list_man_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", config.man_role);
            return View(config);
        }
        [HttpPost]
        public ActionResult Sysconfig(FormCollection _form)
        {
            string dateformat = _form["ddlDateFormat"].ToString();
            string startday = _form["ddlStartDay"].ToString();
            string com_role = _form["ddlCompanyRole"].ToString();
            string man_role = _form["ddlManufactureRole"].ToString();


            using (lidEntities dbo = new lidEntities())
            {
                b_config _config = dbo.b_config.FirstOrDefault();

                if (_config == null)
                {
                    _config = new b_config();
                    
                    _config.dateformat = dateformat;
                    _config.startday = startday;
                    _config.com_role = com_role.ToLongOrDefault();
                    _config.man_role = man_role.ToLongOrDefault();

                    dbo.b_config.Add(_config);
                    dbo.SaveChanges();
                }
                else
                {
                    _config.dateformat = dateformat;
                    _config.startday = startday;
                    _config.com_role = com_role.ToLongOrDefault();
                    _config.man_role = man_role.ToLongOrDefault();

                    dbo.Entry(_config).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                }
            }

            return RedirectToAction("Sysconfig");
        }
        //public ActionResult schedule_ view(string staff_id, string staff_surname, string weekday, string dept_id = "0")
        //{
        //    ViewData["_staff_id"] = staff_id;
        //    ViewData["_dept_id"] = dept_id;
        //    ViewData["_staff_surname"] = staff_surname;
        //    ViewData["dayname"] = weekday;
        //    return PartialView();
        //}
        public ActionResult schedule_view(string staff_id, string weekday, string dept_id = "0")
        {
            ViewData["_staff_id"] = staff_id;
            ViewData["_dept_id"] = dept_id;
            ViewData["_staff_surname"] = "";
            ViewData["dayname"] = weekday;
            return PartialView();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult lnkDepartment(FormCollection _form, string _weekday = "", int _pos_id = 0, string _uri = "")
        {
            string dept_id = _form["ddlDept"].ToString();
            return RedirectToAction(_uri, new { _weekday = _weekday, _dept_id = dept_id, _pos_id = _pos_id });
        }

        public ActionResult lnkPosition(FormCollection _form, string _weekday = "", int _dept_id = 0, string _uri = "")
        {
            string pos_id = _form["ddlPosition"].ToString();
            return RedirectToAction(_uri, new { _weekday = _weekday, _dept_id = _dept_id, _pos_id = pos_id });
        }

        public ActionResult flt_calendar(FormCollection _form, int _dept_id = 0, int _pos_id = 0, string _uri = "")
        {
            string startday = _form["startDate"].ToString();
            return RedirectToAction(_uri, new { _weekday = startday, _dept_id = _dept_id, _pos_id = _pos_id });
        }
   
        [HttpPost]
        public ActionResult AddStaff(FormCollection _form, HttpPostedFileBase upload)
        {
            long staff_id = 0;
            string ddlTitle = _form["ddlTitle"].ToString();
            string initials = _form["initials"].ToString();
            string name = _form["name"].ToString();
            string surname = _form["surname"].ToString();
            string ddlIDType = _form["ddlIDType"].ToString();
            string ddlCounntyOfIssue = _form["ddlCountryOfIssue"].ToString();
            string idnumber = _form["idnumber"].ToString();
            string dob = _form["dob"].ToString();
            string ddlGender = _form["ddlGender"].ToString();
            string ddlRace = _form["ddlRace"].ToString();
            string ddlMaritalstatus = _form["ddlMaritalstatus"].ToString();
            string ddlNationality = _form["ddlNationality"].ToString();
            string structure_id = _form["org_hierarchy"].ToString();
            //string upload = _form["upload"].ToString();
            string chkActive = _form["chkActive"].ToString();
            string empno = _form["empno"].ToString();
            string startdate = _form["startdate"].ToString();
            string emptype = _form["ddlEmployementType"].ToString();
            string ddlPositionHeld = _form["ddlPositionHeld"].ToString();
            string ddlQualification = _form["ddlQualification"].ToString();
            string qualificationdate = _form["qualificationdate"].ToString();
            string ddlDepartment = _form["ddlDepartment"].ToString();
            //string ddlStructureLevel = _form["ddlStructureLevel"].ToString();
            string telw = _form["telw"].ToString();
            string telh = _form["telh"].ToString();
            string email = _form["email"].ToString();
            string mobile = _form["mobile"].ToString();
            string spousename = _form["spousename"].ToString();
            string spousenumber = _form["spousenumber"].ToString();
            string nextkinname = _form["nextkinname"].ToString();
            string nextkinnumber = _form["nextkinnumber"].ToString();
            string houseno = _form["houseno"].ToString();
            string pobox = _form["pobox"].ToString();
            string streetname = _form["streetname"].ToString();
            string postoffice = _form["postoffice"].ToString();
            string addresssurburd = _form["addresssurburd"].ToString();
            string postsurburd = _form["postsurburd"].ToString();
            string addresstown = _form["addresstown"].ToString();
            string posttown = _form["posttown"].ToString();
            string addresscode = _form["addresscode"].ToString();
            string postcode = _form["postcode"].ToString();


            string filename = "";
            if (upload != null && upload.ContentLength > 0)
            {
                string ext = Path.GetFileName(upload.FileName);
                filename = ext;
                if (ext.Split('.').Length > 1)
                {
                    filename = empno + "." + ext.Split('.')[ext.Split('.').Length - 1].ToString();
                }
                var path = Path.Combine(Server.MapPath("~/Photo/Staff"), filename);
                upload.SaveAs(path);
            }

            using (lidEntities dbo = new lidEntities())
            {
                b_staff _staff = new b_staff();
                _staff.staff_name = name;
                _staff.staff_surname = surname;
                _staff.emp_type = Convert.ToInt32(emptype);
                _staff.staff_title = ddlTitle.ToLongOrDefault();
                _staff.staff_number = empno;
                _staff.staff_idnumber = idnumber;
                _staff.staff_mobile = mobile;
                _staff.staff_telephone = telw;
                _staff.staff_email = email;
                _staff.photo = filename;
                _staff.isActive = true;
                _staff.isDeleted = false;
                _staff.emp_date = Convert.ToDateTime(startdate);
                _staff.date_created = DateTime.Now;



                _staff.staff_initials = initials;
                _staff.idType_id = ddlIDType.ToLongOrDefault();
                _staff.counntyOfIssue_id = ddlCounntyOfIssue.ToLongOrDefault();
                _staff.dob = dob.ToDateTimeOrDefault();
                _staff.gender_id = ddlGender.ToLongOrDefault();
                _staff.race_id = ddlRace.ToLongOrDefault();
                _staff.maritalstatus_id = ddlMaritalstatus.ToLongOrDefault();
                _staff.nationality_id = ddlNationality.ToLongOrDefault();
                _staff.position_id = ddlPositionHeld.ToLongOrDefault();
                _staff.qualification_id = ddlQualification.ToLongOrDefault();
                _staff.qualification_date = qualificationdate.ToDateTimeOrDefault();
                _staff.department_id = ddlDepartment.ToLongOrDefault();
                _staff.structure_id = structure_id.ToLongOrDefault();

                _staff.staff_hometelephone = telh;
                _staff.staff_spousename = spousename;
                _staff.staff_spousenumber = spousenumber;
                _staff.staff_nextofkinname = nextkinname;
                _staff.staff_nextofkinnumber = nextkinnumber;

                _staff.staff_houseno = houseno;
                _staff.staff_pobox = pobox;
                _staff.staff_streetname = streetname;
                _staff.staff_postoffice = postoffice;
                _staff.staff_addresssurburd = addresssurburd;
                _staff.staff_postsurburd = postsurburd;
                _staff.staff_addresstown = addresstown;
                _staff.staff_posttown = posttown;
                _staff.staff_addresscode = addresscode;
                _staff.staff_postcode = postcode;

                dbo.b_staff.Add(_staff);
                dbo.SaveChanges();
                staff_id = _staff.staff_id;
            }

            if (AdminCore.get_config_createstaff())
            {
                //Create user
                using (lidEntities dbo1 = new lidEntities())
                {
                    var user = new ApplicationUser
                    {
                        UserName = email,
                        Email = email,
                    };
                    string password = AdminCore.get_config_defaultpassword();
                    var result = UserManager.CreateAsync(user, password);
                    //if (result)
                    {
                        long role_id = AdminCore.get_config_defaultrole();
                        //AspNetUsers aspuser = dbo.AspNetUsers.Where(x => x.Email == email).FirstOrDefault();
                        b_users _users = new b_users();
                        _users.name = name;
                        //_users.id = aspuser.Id;
                        _users.surname = surname;
                        _users.dept_id = ddlDepartment.ToLongOrDefault(0);
                        _users.role_id = role_id;
                        _users.email = email;
                        _users.telephone = mobile;
                        _users.photo = filename;
                        _users.isactive = true;
                        _users.staff_id = staff_id;

                        dbo1.b_users.Add(_users);
                        dbo1.SaveChanges();
                    }
                }
            }
            return RedirectToAction("AddStaff", new { saved = true });
        }


        [HttpPost]
        public ActionResult EditStaff(FormCollection _form, HttpPostedFileBase upload)
        {
            string staff_id = _form["staff_id"].ToString();
            string ddlTitle = _form["ddlTitle"].ToString();
            string initials = _form["initials"].ToString();
            string name = _form["name"].ToString();
            string surname = _form["surname"].ToString();
            string ddlIDType = _form["ddlIDType"].ToString();
            string ddlCounntyOfIssue = _form["ddlCountryOfIssue"].ToString();
            string idnumber = _form["idnumber"].ToString();
            string dob = _form["dob"].ToString();
            string ddlGender = _form["ddlGender"].ToString();
            string ddlRace = _form["ddlRace"].ToString();
            string ddlMaritalstatus = _form["ddlMaritalstatus"].ToString();
            string ddlNationality = _form["ddlNationality"].ToString();
            //string upload = _form["upload"].ToString();
            string chkActive = _form["chkActive"].ToString();
            string empno = _form["empno"].ToString();
            string startdate = _form["startdate"].ToString();
            string emptype = _form["ddlEmployementType"].ToString();
            string ddlPositionHeld = _form["ddlPositionHeld"].ToString();
            string ddlQualification = _form["ddlQualification"].ToString();
            string qualificationdate = _form["qualificationdate"].ToString();
            string ddlDepartment = _form["ddlDepartment"].ToString();
            //string ddlStructureLevel = _form["ddlStructureLevel"].ToString();
            string telw = _form["telw"].ToString();
            string telh = _form["telh"].ToString();
            string email = _form["email"].ToString();
            string mobile = _form["mobile"].ToString();
            string spousename = _form["spousename"].ToString();
            string spousenumber = _form["spousenumber"].ToString();
            string nextkinname = _form["nextkinname"].ToString();
            string nextkinnumber = _form["nextkinnumber"].ToString();
            string houseno = _form["houseno"].ToString();
            string pobox = _form["pobox"].ToString();
            string streetname = _form["streetname"].ToString();
            string postoffice = _form["postoffice"].ToString();
            string addresssurburd = _form["addresssurburd"].ToString();
            string postsurburd = _form["postsurburd"].ToString();
            string addresstown = _form["addresstown"].ToString();
            string posttown = _form["posttown"].ToString();
            string addresscode = _form["addresscode"].ToString();
            string postcode = _form["postcode"].ToString();
            string structure_id = "0";
            try
            {
                structure_id = _form["org_hierarchy"].ToString();
            }
            catch (Exception)
            {
            }

            string filename = "";
            if (upload != null && upload.ContentLength > 0)
            {
                string ext = Path.GetFileName(upload.FileName);
                filename = ext;
                if (ext.Split('.').Length > 1)
                {
                    filename = empno + "." + ext.Split('.')[ext.Split('.').Length - 1].ToString();
                }
                var path = Path.Combine(Server.MapPath("~/Photo/Staff"), filename);
                upload.SaveAs(path);
            }
            long _staff_id = staff_id.ToLongOrDefault(0);

            using (lidEntities dbo = new lidEntities())
            {
                b_staff _staff = dbo.b_staff.Where(x => x.staff_id == _staff_id).FirstOrDefault();
                if (_staff != null)
                {
                    _staff.staff_name = name;
                    _staff.staff_surname = surname;
                    _staff.emp_type = Convert.ToInt32(emptype);
                    _staff.staff_title = ddlTitle.ToLongOrDefault();
                    _staff.staff_number = empno;
                    _staff.staff_idnumber = idnumber;
                    _staff.staff_mobile = mobile;
                    _staff.staff_telephone = telw;
                    _staff.staff_email = email;
                    _staff.isActive = true;
                    _staff.isDeleted = false;
                    _staff.emp_date = Convert.ToDateTime(startdate);
                    _staff.date_created = DateTime.Now;

                    if(!string.IsNullOrWhiteSpace(filename))
                        _staff.photo = filename;

                    _staff.staff_initials = initials;
                    _staff.idType_id = ddlIDType.ToLongOrDefault();
                    _staff.counntyOfIssue_id = ddlCounntyOfIssue.ToLongOrDefault();
                    _staff.dob = dob.ToDateTimeOrDefault();
                    _staff.gender_id = ddlGender.ToLongOrDefault();
                    _staff.race_id = ddlRace.ToLongOrDefault();
                    _staff.maritalstatus_id = ddlMaritalstatus.ToLongOrDefault();
                    _staff.nationality_id = ddlNationality.ToLongOrDefault();
                    _staff.position_id = ddlPositionHeld.ToLongOrDefault();
                    _staff.qualification_id = ddlQualification.ToLongOrDefault();
                    _staff.qualification_date = qualificationdate.ToDateTimeOrDefault();
                    _staff.department_id = ddlDepartment.ToLongOrDefault();
                    _staff.structure_id = (structure_id == "0") ? _staff.structure_id : structure_id.ToLongOrDefault();

                    _staff.staff_hometelephone = telh;
                    _staff.staff_spousename = spousename;
                    _staff.staff_spousenumber = spousenumber;
                    _staff.staff_nextofkinname = nextkinname;
                    _staff.staff_nextofkinnumber = nextkinnumber;

                    _staff.staff_houseno = houseno;
                    _staff.staff_pobox = pobox;
                    _staff.staff_streetname = streetname;
                    _staff.staff_postoffice = postoffice;
                    _staff.staff_addresssurburd = addresssurburd;
                    _staff.staff_postsurburd = postsurburd;
                    _staff.staff_addresstown = addresstown;
                    _staff.staff_posttown = posttown;
                    _staff.staff_addresscode = addresscode;
                    _staff.staff_postcode = postcode;

                    dbo.Entry(_staff).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    long results = _staff.staff_id;
                }
            }

            return RedirectToAction("ViewStaff");
        }
        public ActionResult ViewStaff()
        {
            return View();
        }
        public ActionResult StaffView_win(string staff_id)
        {
            long id = staff_id.ToLongOrDefault(0);
            Staff list = AdminCore.get_staffById(id);
            return View(list);
        }



        [HttpPost]
        public JsonResult SaveOrUpdateShift(Shift _shift)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long id = _shift.shift_id.ToLongOrDefault(0);
                b_shift shift = dbo.b_shift.Where(x => x.shift_id == id).FirstOrDefault();
                if (shift == null)
                {
                    shift = new b_shift();
                    shift.shift_code = _shift.shift_code;
                    shift.shift_name = _shift.shift_name;
                    shift.shift_from = Convert.ToDateTime(_shift.shift_from).TimeOfDay;
                    shift.shift_to = Convert.ToDateTime(_shift.shift_to).TimeOfDay;
                    shift.non_billable = _shift.non_billable.ToInt32OrDefault(0);
                    shift.is_split = _shift.is_split.ToBooleanOrDefault(false);
                    shift.dept_id = _shift.dept_id;
                    if (_shift.is_split.ToBooleanOrDefault(false))
                    {
                        shift.split_from = Convert.ToDateTime(_shift.split_from).TimeOfDay;
                        shift.split_to = Convert.ToDateTime(_shift.split_to).TimeOfDay;
                    }
                    dbo.b_shift.Add(shift);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Shift " + _shift.shift_name + " successfully saved. ";
                }
                else
                {
                    shift.shift_code = _shift.shift_code;
                    shift.shift_name = _shift.shift_name;
                    shift.shift_from = Convert.ToDateTime(_shift.shift_from).TimeOfDay;
                    shift.shift_to = Convert.ToDateTime(_shift.shift_to).TimeOfDay;
                    shift.non_billable = _shift.non_billable.ToInt32OrDefault(0);
                    shift.is_split = _shift.is_split.ToBooleanOrDefault(false);
                    shift.dept_id = _shift.dept_id;
                    if (_shift.is_split.ToBooleanOrDefault(false))
                    {
                        shift.split_from = Convert.ToDateTime(_shift.split_from).TimeOfDay;
                        shift.split_to = Convert.ToDateTime(_shift.split_to).TimeOfDay;
                    }
                    dbo.Entry(shift).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Shift " + _shift.shift_name + " successfully updated. ";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteShift(string shift_id)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long _shift_id = shift_id.ToLongOrDefault(0);
                b_shift shift = dbo.b_shift.Where(x => x.shift_id == _shift_id).FirstOrDefault();

                dbo.Entry(shift).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();
                res.code = "1";
                res.message = "Shift " + shift.shift_name + " successfully deleted. ";
            }

            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewShifts()
        {
            List<Shift> shifts = AdminCore.get_shifts(User.Identity.Name);
            return View(shifts);
        }
        public ActionResult ShiftView_win(string shift_id)
        {
            Shift shifts = AdminCore.get_shift(shift_id);
            return View(shifts);
        }
        public ActionResult StaffAddHocShift_win(string staff_id, string weekday)
        {
            ShiftManage shiftManage = new ShiftManage();

            shiftManage.Staff_list.Add(AdminCore.get_staffById(staff_id.ToLongOrDefault(0)));
            shiftManage.shift_list = AdminCore.get_shifts(User.Identity.Name);

            shiftManage.staff_id = staff_id;
            shiftManage.weekday = weekday;
            return View(shiftManage);
        }
        public ActionResult StaffViewSchedule_win(string staff_id, string weekday)
        {
            ShiftManage shiftManage = new ShiftManage();

            shiftManage.Staff_list.Add(AdminCore.get_staffById(staff_id.ToLongOrDefault(0)));
            //shiftManage.shift_list = AdminCore.get_shifts();
            //shiftManage.Week_days = AdminCore.get_weekdaysbydt(weekday);

            shiftManage.weekday = weekday;
            return View(shiftManage);
        }
        public ActionResult StaffViewShift_win(string shift_id, string staff_id, string weekday, string dept_id)
        {
            ShiftManage shiftManage = new ShiftManage();

            Staff staff = AdminCore.get_staffById(staff_id.ToLongOrDefault(0));
            Shift shift = AdminCore.get_shift(shift_id);

            shiftManage.shift_id = shift_id;
            shiftManage.staff_id = staff_id;
            shiftManage.shift_name = shift.shift_name;
            shiftManage.shift_period = shift.period;
            shiftManage.weekday = weekday;

            shiftManage.Staff_list = AdminCore.get_deptstaff(dept_id.ToLongOrDefault()); ;//
            shiftManage.Week_days = AdminCore.get_weekdaysbydt(weekday);
            shiftManage.shift_list = AdminCore.get_shifts(User.Identity.Name);
            shiftManage.position_list = AdminCore.get_positions();

            ViewBag.weekday = weekday;
            return View(shiftManage);
        }



        public JsonResult SaveOrUpdateRole(Roles model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                b_roles role = dbo.b_roles.Where(m => m.role_code == model.role_code).FirstOrDefault();
                if (role == null)
                {
                    role = new b_roles();
                    role.role_code = model.role_code;
                    role.role_name = model.role_name;
                    role.isactive = model.role_isactive;
                    role.role_dept = model.role_deptid;
                    dbo.b_roles.Add(role);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Role " + model.role_name + " successfully saved";
                }
                else {
                    role.role_code = model.role_code;
                    role.role_name = model.role_name;
                    role.isactive = model.role_isactive;
                    role.role_dept = model.role_deptid;

                    dbo.Entry(role).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Role " + model.role_name + " successfully update";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
    }
        public ActionResult ViewRoles()
        {
           
            return View();
        }

        public ActionResult Roleconfig(string _role_id = "")
        {
            long role_id = util.parselong(_role_id);
            ViewBag.list_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", role_id);
            RoleMenus roleMenus = new RoleMenus();
            if (_role_id != "")
            {
                roleMenus.avl_menus = AdminCore.get_avlmenus(role_id);
                roleMenus.slt_menus = AdminCore.get_slctmenus(role_id);
            }
            return View(roleMenus);
        }

        public ActionResult CategoryConfig(string _company_id = "")
        {
            long comp_id = _company_id.ToLongOrDefault();
            ViewBag.list_companies = new SelectList(AdminCore.get_companies(), "company_id", "company_name", _company_id);
            CatConfigDTO compCat = new CatConfigDTO();
            if (_company_id != "")
            {
                compCat.avl_category = AdminCore.get_avl_cat_group(comp_id);
                compCat.slt_category = AdminCore.get_slc_cat_group(comp_id);
            }
            return View(compCat);
        }

        public ActionResult flt_roles(FormCollection _form)
        {
            string role_id = _form["ddlRoles"].ToString();
            return RedirectToAction("Roleconfig", new { _role_id = role_id });
        }
        public ActionResult flt_company(FormCollection _form)
        {
            string _id = _form["ddlCompany"].ToString();
            return RedirectToAction("CategoryConfig", new { _company_id = _id });
        }
        public ActionResult AddDepartment()
        {
            return View();
        }


        public ActionResult DepartmentView_win(string dept_id)
        {
            Dept list = AdminCore.get_depatment(dept_id);
            return View(list);
        }

        [HttpPost]
        public ActionResult AddDepartment(FormCollection _form)
        {
            string name = _form["name"].ToString();
            string code = _form["code"].ToString();
            using (lidEntities dbo = new lidEntities())
            {
                b_dept department = new b_dept();
                department.dept_code = code;
                department.dept_name = name;
                dbo.b_dept.Add(department);
                dbo.SaveChanges();
            }
            return RedirectToAction("Departments");
        }

        public ActionResult UpdateDepartment(FormCollection _form)
        {
            string dept_id = _form["id"].ToString();
            string name = _form["name"].ToString();
            string code = _form["code"].ToString();
            using (lidEntities dbo = new lidEntities())
            {
                b_dept department = dbo.b_dept.Where(m => m.dept_id.ToString() == dept_id).FirstOrDefault();
                department.dept_code = code;
                department.dept_name = name;
                dbo.Entry(department).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();
            }
            return RedirectToAction("Departments");
        }

        public ActionResult AddPosition()
        {
            return View();
        }
   
        public ActionResult ViewPosition()
        {
            List<Position> list = AdminCore.get_positions();
            return View(list);
        }

        public ActionResult PositionView_win(string position_id)
        {
            Position list = AdminCore.get_position(position_id);
            return View(list);
        }

        [HttpPost]
        public ActionResult AddPosition(FormCollection _form)
        {
            string name = _form["name"].ToString();
            string code = _form["code"].ToString();
            string min = _form["min"].ToString();
            string max = _form["max"].ToString();
            using (lidEntities dbo = new lidEntities())
            {
                b_position position = new b_position();
                position.position_code = code;
                position.position_name = name;
                position.min_hours = min;
                position.max_hours = max;
                dbo.b_position.Add(position);
                dbo.SaveChanges();
            }
            return RedirectToAction("Positions");
        }

        public ActionResult UpdatePostion(FormCollection _form)
        {
            string pos_id = _form["id"].ToString();
            string name = _form["name"].ToString();
            string code = _form["code"].ToString();
            string min = _form["min"].ToString();
            string max = _form["max"].ToString();
            using (lidEntities dbo = new lidEntities())
            {
                b_position position = dbo.b_position.Where(m => m.position_id.ToString() == pos_id).FirstOrDefault();
                position.position_code = code;
                position.position_name = name;
                position.min_hours = min;
                position.max_hours = max;
                dbo.Entry(position).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();
            }
            return RedirectToAction("Positions");
        }

        public ActionResult AddUser(string username = "")
        {
            Users user = AdminCore.get_userByUsername(username);
            if (user == null)
            {
                ViewBag.list_staff = new SelectList(AdminCore.get_allstaff(), "staff_id", "staff_shortname");
                ViewBag.list_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name");
                //ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name");
            }
            else
            {
                ViewBag.list_staff = new SelectList(AdminCore.get_allstaff(), "staff_id", "staff_shortname", user.staff_id.ToLongOrDefault(0));
                ViewBag.list_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", user.role_id.ToLongOrDefault(0));
                //ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", user.dept_id.ToLongOrDefault(0));
            }
            return View(user);
        }
        public ActionResult EditUser_win(string username)
        {
            Users user = AdminCore.get_userByUsername(username);
            ViewBag.list_staff = new SelectList(AdminCore.get_allstaff(), "staff_id", "staff_shortname", user.staff_id.ToLongOrDefault(0));
            ViewBag.list_roles = new SelectList(AdminCore.get_roles(), "role_id", "role_name", user.role_id.ToLongOrDefault(0));
           // ViewBag.list_dept = new SelectList(AdminCore.get_depatments(User.Identity.Name), "dept_id", "dept_name", user.dept_id.ToLongOrDefault(0));

            user.department = AdminCore.get_deptname(util.parselong(user.dept_id));
            user.role_name = AdminCore.get_rolename(util.parselong(user.role_id));
            return View(user);
        }

        [HttpPost]
        public JsonResult SaveOrUpdateUsers(Users user)
        {
            AjaxResults res = new AjaxResults();
            string filename = "";
            user.password = AdminCore.get_config_defaultpassword();
            HttpPostedFileBase upload = user.upload;
            if (upload != null && upload.ContentLength > 0)
            {
                string ext = Path.GetFileName(upload.FileName);
                filename = ext;
                if (ext.Split('.').Length > 1)
                {
                    filename = user.email.Split('@')[0] + "." + ext.Split('.')[ext.Split('.').Length - 1].ToString();
                }
                var path = Path.Combine(Server.MapPath("~/Photo/Users"), filename);
                upload.SaveAs(path);
            }

            using (lidEntities dbo = new lidEntities())
            {
                b_users _users = dbo.b_users.Where(x => x.user_id == user.user_id).FirstOrDefault();
                if (_users == null)
                {
                    var _user = new ApplicationUser
                    {
                        UserName = user.email,
                        Email = user.email,
                    };
                    var result = UserManager.CreateAsync(_user, user.password);
                    //if (result)
                    {
                        _users = new b_users();
                        _users.name = user.name;
                        _users.surname = user.surname;
                        _users.dept_id = user.dept_id;
                        _users.role_id = user.role_id;
                        _users.email = user.email;
                        _users.telephone = user.telephone;
                        _users.photo = filename;
                        _users.isactive = user.isactive;
                        _users.systemadmin = user.systemadmin;
                        _users.staff_id = user.staff_id.ToLongOrDefault(0);

                        dbo.b_users.Add(_users);
                        dbo.SaveChanges();
                        res.code = "1";
                        res.message = "User " + user.email + " was sucessfully saved";
                    }
                }
                else
                {
                    _users.name = user.name;
                    _users.surname = user.surname;
                    _users.dept_id = user.dept_id;
                    _users.role_id = user.role_id;
                    _users.email = user.email;
                    _users.telephone = user.telephone;
                    _users.photo = filename;
                    _users.isactive = user.isactive;
                    _users.systemadmin = user.systemadmin;
                    _users.staff_id = user.staff_id.ToLongOrDefault(0);
                    if (!string.IsNullOrWhiteSpace(filename))
                        _users.photo = filename;

                    dbo.Entry(_users).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "User " + user.email + " was sucessfully updated";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult ViewUsers()
        {
            List<Users> users = new List<Users>();
            using (lidEntities dbo = new lidEntities())
            {
                List<b_users> _users = dbo.b_users.Where(x => x.isactive == true).ToList();
                foreach (b_users _user in _users)
                {
                    users.Add(new Users
                    {
                        user_id = _user.user_id,
                        username = _user.email,
                        email = _user.email,
                        name = _user.name,
                        surname = _user.surname,
                        telephone = _user.telephone,
                        photo = _user.photo,
                        department = AdminCore.get_deptname(util.parselong(_user.dept_id)),
                        role_name = AdminCore.get_rolename(util.parselong(_user.role_id)),
                    });
                }
            }
            return View(users);
        }
        public ActionResult Emailconfig()
        {
            EmailConfig smtp = new EmailConfig();
            using (lidEntities dbo = new lidEntities())
            {
                b_smtp _smtp = dbo.b_smtp.FirstOrDefault();
                if (_smtp != null)
                {
                    smtp.email = _smtp.email;
                    smtp.reply = _smtp.reply;
                    smtp.server = _smtp.server;
                    smtp.port = _smtp.port;
                    smtp.username = _smtp.username;
                    smtp.password = _smtp.password;
                    smtp.ssl = _smtp.ssl.ToBooleanOrDefault(false);
                    smtp.type_id = _smtp.type_id.ToLongOrDefault(0);
                }
            }
            ViewBag.list_emailtypes = new SelectList(AdminCore.get_lookups("smtp-types"), "id", "value", smtp.type_id.ToLongOrDefault(0));
            return View(smtp);
        }
        [HttpPost]
        public JsonResult SaveOrUpdateEmailconfig(EmailConfig config)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                b_smtp smtp = dbo.b_smtp.FirstOrDefault();
                if (smtp == null)
                {
                    smtp = new b_smtp();
                    smtp.email = config.email;
                    smtp.reply = config.reply;
                    smtp.server = config.server;
                    smtp.port = config.port;
                    smtp.username = config.username;
                    smtp.password = config.password;
                    smtp.ssl = config.ssl;
                    smtp.type_id = config.type_id;
                    dbo.b_smtp.Add(smtp);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Email SMTP settings successfully saved";
                }
                else
                {
                    smtp.email = config.email;
                    smtp.reply = config.reply;
                    smtp.server = config.server;
                    smtp.port = config.port;
                    smtp.username = config.username;
                    smtp.password = config.password;
                    smtp.ssl = config.ssl;
                    smtp.type_id = config.type_id;

                    dbo.Entry(smtp).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Email SMTP settings successfully updated";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BioMetrixconfig()
        {
            return View();
        }
        public ActionResult Structure()
        {
            List<OrganisationStructure> structure = AdminCore.get_organisationstructure();

            return View(structure);
        }
        public ActionResult StructureLevels()
        {
            return View();
        }
        public ActionResult StructureLevel_win(string level_id)
        {
            StructureLevels level = AdminCore.get_structurelevel(level_id);
            return View(level);
        }
        public JsonResult SaveOrUpdateLevels(StructureLevels level)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long id = level.level_id.ToLongOrDefault(0);
                b_structurelevels _level = dbo.b_structurelevels.Where(x => x.LevelId == id).FirstOrDefault();
                if (_level == null)
                {
                    _level = new b_structurelevels();
                    _level.LevelCode = level.level_code;
                    _level.LevelName = level.level_name;
                    _level.LevelOrder = level.level_order;

                    dbo.b_structurelevels.Add(_level);
                    dbo.SaveChanges();

                    res.code = "1";
                    res.message = "Structure Level " + level.level_name + " was successfully saved";
                }
                else
                {
                    _level.LevelCode = level.level_code;
                    _level.LevelName = level.level_name;
                    _level.LevelOrder = level.level_order;

                    dbo.Entry(_level).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Structure Level " + level.level_name + " was successfully updated";
                }
            }
            var json = new JavaScriptSerializer().Serialize(res);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveOrUpdateStructure(Structure model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long id = model.structure_id.ToLongOrDefault(0);
                b_structure _structure = dbo.b_structure.Where(x => x.structure_id == id).FirstOrDefault();
                if (_structure == null)
                {
                    _structure = new b_structure();
                    _structure.structure_code = model.structure_code;
                    _structure.structure_name = model.structure_name;
                    _structure.max_hours = model.max_hours.ToInt32OrDefault();
                    _structure.min_hours = model.min_hours.ToInt32OrDefault();

                    dbo.b_structure.Add(_structure);
                    dbo.SaveChanges();

                    res.code = "1";
                    res.message = "Structure " + _structure.structure_name + " was successfully saved";
                }
                else
                {
                    _structure.structure_code = model.structure_code;
                    _structure.structure_name = model.structure_name;
                    _structure.max_hours = model.max_hours.ToInt32OrDefault();
                    _structure.min_hours = model.min_hours.ToInt32OrDefault();

                    dbo.Entry(_structure).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Structure " + _structure.structure_name + " was successfully updated";
                }
            }
            var json = new JavaScriptSerializer().Serialize(res);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteLevel(long level_id)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                long id = level_id.ToLongOrDefault(0);
                b_structurelevels _level = dbo.b_structurelevels.Where(x => x.LevelId == id).FirstOrDefault();
                if (_level != null)
                {

                    dbo.Entry(_level).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Structure Level was successfully deleted";
                }
                else {

                    res.code = "0";
                    res.message = "Structure Level was not found ";
                }
            }
            var json = new JavaScriptSerializer().Serialize(res);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult structure_win(string id)
        {
            using (lidEntities dbo = new lidEntities())
            {
                long structure_id = id.ToLongOrDefault(0);
                b_structure structure = dbo.b_structure.Where(b => b.structure_id == structure_id).FirstOrDefault();
                if (structure != null)
                {
                    string _name = structure.structure_name;
                    string _id = structure.structure_id.ToString();
                    ViewBag._name = _name;
                    ViewBag._id = _id;
                }
            }
            return View();
        }

        public ActionResult structure_edit(string id)
        {
            Structure _structure = new Structure();
            using (lidEntities dbo = new lidEntities())
            {
                long structure_id = id.ToLongOrDefault(0);
                b_structure structure = dbo.b_structure.Where(b => b.structure_id == structure_id).FirstOrDefault();
                if (structure != null)
                {
                    _structure.structure_id = structure.structure_id;
                    _structure.structure_code = structure.structure_code;
                    _structure.structure_name = structure.structure_name;
                    _structure.min_hours = structure.min_hours.ToString();
                    _structure.max_hours = structure.max_hours.ToString();
                }
            }
            return View(_structure);
        }

        [HttpPost]
        public ActionResult structure_win(FormCollection _form)
        {
            string code = _form["code"].ToString();
            string name = _form["name"].ToString();
            string min = _form["min"].ToString();
            string max = _form["max"].ToString();
            string id = _form["id"].ToString();
            long parent_id = id.ToLongOrDefault(0);

            using (lidEntities dbo = new lidEntities())
            {
                b_structure structure = new b_structure();
                structure.structure_code = code;
                structure.structure_name = name;
                structure.max_hours = max.ToInt32OrDefault(0);
                structure.min_hours = min.ToInt32OrDefault(0);
                structure.isActive = true;
                structure.isDeleted = false;
                structure.date_created = DateTime.Now;
                if (parent_id != 0)
                    structure.parent_id = parent_id;

                dbo.b_structure.Add(structure);
                dbo.SaveChanges();
            }
            return RedirectToAction("structure");
        }
        [HttpPost]
        public ActionResult structure_update(FormCollection _form)
        {
            string code = _form["code"].ToString();
            string name = _form["name"].ToString();
            string min = _form["min"].ToString();
            string max = _form["max"].ToString();
            string id = _form["id"].ToString();
            long structure_id = id.ToLongOrDefault(0);

            using (lidEntities dbo = new lidEntities())
            {
                b_structure structure = dbo.b_structure.Where(x => x.structure_id == structure_id).FirstOrDefault();
                structure.structure_code = code;
                structure.structure_name = name;
                structure.max_hours = max.ToInt32OrDefault(0);
                structure.min_hours = min.ToInt32OrDefault(0);

                dbo.Entry(structure).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();

            }
            return RedirectToAction("structure");
        }


        public ActionResult DataConfig(string id)
        {

            ViewBag.list_datatypes = new SelectList(AdminCore.get_defaultdatatypes(), "code_name", "code_name", id);
            return View();
        }


        public ActionResult flt_data_config(FormCollection _form)
        {
            string id = _form["ddlData"].ToString();
            return RedirectToAction("DataConfig", new { id = id });
        }
        public ActionResult DataConfig_win(string datatype)
        {

            ViewBag.datatype = datatype;
            return View();
        }
        public ActionResult DataConfig_add(string id)
        {

            ViewBag.datatype = id;
            return View();
        }
        [HttpPost]
        public ActionResult DataConfig_crud(FormCollection _form)
        {
            string name = _form["name"].ToString();
            string datatype = _form["datatype"].ToString();
            ViewBag.datatype = datatype;
            using (lidEntities dbo = new lidEntities())
            {
                b_codevalues codevalue = dbo.b_codevalues.Where(x => x.Code == datatype && x.Value == name).FirstOrDefault();
                if (codevalue == null)
                {
                    codevalue = new b_codevalues();
                    codevalue.Code = datatype;
                    codevalue.Value = name;
                    codevalue.isActive = true;
                    codevalue.isReadonly = true;
                    codevalue.isVisible = true;
                    codevalue.OrderValue = 1;

                    dbo.b_codevalues.Add(codevalue);
                    dbo.SaveChanges();
                }
            }
            return RedirectToAction("DataConfig");
        }

        public ActionResult DataConfig_edit(string id)
        {
            CodeValues codevalues = new CodeValues();
            using (lidEntities dbo = new lidEntities())
            {
                long codevalue_id = id.ToLongOrDefault(0);
                b_codevalues _codevalues = dbo.b_codevalues.Where(b => b.id == codevalue_id).FirstOrDefault();
                if (_codevalues != null)
                {
                    codevalues.id = _codevalues.id;
                    codevalues.code = _codevalues.Value;
                    codevalues.isActive = _codevalues.isActive.ToBooleanOrDefault(false);
                }
            }
            return View(codevalues);
        }

        [HttpPost]
        public ActionResult DataConfig_update(FormCollection _form)
        {
            string code = _form["name"].ToString();
            string id = _form["id"].ToString();
            long codevalue_id = id.ToLongOrDefault(0);

            using (lidEntities dbo = new lidEntities())
            {
                b_codevalues codevalues = dbo.b_codevalues.Where(x => x.id == codevalue_id).FirstOrDefault();
                codevalues.Value = code;

                dbo.Entry(codevalues).State = System.Data.Entity.EntityState.Modified;
                dbo.SaveChanges();

            }
            return RedirectToAction("DataConfig");
        }
        
        
        [HttpPost]
        public JsonResult PostHelpData(AjaxResults model)
        {
            AjaxResults res = new AjaxResults();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand(model.message, connection);
                connection.Open();
                string table = "";
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        table = "<table>";
                        while (reader.Read())
                        {
                            table += "<tr>";
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                table += "<td>" + reader.GetValue(i) + "<td/>";
                            }
                            table += "<tr/>";
                        }
                        table += "<table/>";
                        reader.Close();
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                }
                res.message = table;
            }
            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}