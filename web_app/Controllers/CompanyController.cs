﻿using Dapper;
using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace web_app.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        // GET: Company
        public ActionResult Index()
        {
            return View();
        }
        // GET: Company
        public ActionResult Inspectors()
        {
            List<CompanyInspector> res = new List<CompanyInspector>();
            string username = User.Identity.Name.ToString();
            List<CompanyInspector> _res = CompanyCore.get_inspectors_many(username);
            foreach (CompanyInspector rec in _res)
            {
                if (rec.signature_img != null)
                {
                    string imreBase64Data = Convert.ToBase64String(rec.signature_img);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    rec.imgDataURL = imgDataURL;
                }
                res.Add(rec);
            }
            return View(res);
        }
        public ActionResult Sites()
        {
            string username = User.Identity.Name.ToString();
            List<CompanySites> res = CompanyCore.get_sites_many(username);
            return View(res);
        }
        public ActionResult CompanyDetails()
        {
            CompanyModel list = new CompanyModel();
            using (var cn = GetOpenConnection())
            {
                string username = User.Identity.Name.ToString();
                list = cn.Query<CompanyModel>("get_company_byid", new { username = username } , commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (list != null)
                {
                    string imreBase64Data = list.company_logo == null ? "" : Convert.ToBase64String(list.company_logo);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    list.imgDataURL = imgDataURL;
                }
            }
            return View(list);
        }
        public ActionResult AddAssetTag()
        {
            string username = User.Identity.Name.ToString();
            List<CompanyAsset> res = CompanyCore.get_assets_many(username);

            foreach (CompanyAsset rec in res)
            {
                if (rec.image != null)
                {
                    string imreBase64Data = Convert.ToBase64String(rec.image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    rec.imgDataURL = imgDataURL;
                }
            }

            return View(res);
        }
        public ActionResult AddAsset(string cat_id, string itm_id, string cert_id, string test_id, string insp_id, string man_id, string ass_id)
        {
            CompanyAsset asset = new CompanyAsset();
            string username = User.Identity.Name.ToString();
            long category_id = cat_id == null ? 0 : cat_id.Replace("cat_", "").ToLongOrDefault();
            long item_id = itm_id == null ? 0 : itm_id.Replace("itm_", "").ToLongOrDefault();
            ViewBag.list_category = new SelectList(ItemCore.get_category_tree_comp(User.Identity.Name), "id", "text", cat_id);
            ViewBag.list_items = new SelectList(ItemCore.get_items_tree(category_id.ToString()), "id", "text", "itm_" + itm_id);
            //ViewBag.list_sites = new SelectList(ItemCore.get_sites_many(username), "site_id", "site_name", site_id);
            ViewBag.list_certification = new SelectList(ItemCore.get_codevalues("Certification"), "id", "value", cert_id);
            ViewBag.list_testcycle = new SelectList(ItemCore.get_codevalues("Test Cycle"), "id", "value", test_id);
            ViewBag.list_inpectioncycle = new SelectList(ItemCore.get_codevalues("Inspection Cycle"), "id", "value", insp_id);

            ViewBag.list_man = new SelectList(ItemCore.get_manufactures(), "company_id", "company_name", man_id);
            ViewBag.list_supplier = new SelectList(ItemCore.get_suppliers(), "company_id", "company_name");
            ViewBag.list_asset = new SelectList(ItemCore.get_manufacture_asset(man_id), "asset_id", "asset_name", ass_id);
            asset.category_id = cat_id;
            asset.item_id = itm_id;
            asset.cert_id = cert_id.ToLongOrDefault();
            asset.test_id = test_id.ToLongOrDefault();
            asset.insp_id = insp_id.ToLongOrDefault();
            asset.man_id = man_id.ToLongOrDefault();
            asset.ass_id = ass_id.ToLongOrDefault();

            Items model = ItemCore.get_item_byid(itm_id);

            if (model != null)
            {
                asset.std_code = model.std_code;
                string imreBase64Data = model.image == null ? "" : Convert.ToBase64String(model.image);
                string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                asset.imgDataURL = imgDataURL;
            }
            return View(asset);
        }
        public ActionResult filter_category(FormCollection _form, string itm_id, string cert_id, string test_id, string insp_id, string man_id, string ass_id)
        {
            string id = _form["ddlCat"].ToString();
            return RedirectToAction("AddAsset", new { cat_id = id, itm_id = id, cert_id = cert_id, test_id = test_id, insp_id = insp_id, man_id = man_id, ass_id = ass_id });
        }
        public ActionResult filter_item(FormCollection _form, string cat_id, string cert_id, string test_id, string insp_id, string man_id, string ass_id)
        {
            string id = _form["ddlItems"].ToString().Replace("itm_","");
            return RedirectToAction("AddAsset", new { cat_id = cat_id, itm_id = id, cert_id = cert_id, test_id = test_id, insp_id = insp_id, man_id = man_id, ass_id = ass_id });
        }
        public ActionResult filter_certification(FormCollection _form, string cat_id, string itm_id, string test_id, string insp_id, string man_id, string ass_id)
        {
            string cert_id = _form["ddlCertificate"].ToString();
            return RedirectToAction("AddAsset", new { cat_id = cat_id, itm_id = itm_id, cert_id = cert_id, test_id = test_id, insp_id = insp_id, man_id = man_id, ass_id = ass_id });
        }
        public ActionResult filter_inpection(FormCollection _form, string cat_id, string itm_id, string cert_id, string test_id, string man_id, string ass_id)
        {
            string insp_id = _form["ddlInpectionCycle"].ToString();
            return RedirectToAction("AddAsset", new { cat_id = cat_id, itm_id = itm_id, cert_id = cert_id, test_id = test_id, insp_id = insp_id, man_id = man_id, ass_id = ass_id });
        }
        public ActionResult filter_loadtest(FormCollection _form, string cat_id, string itm_id, string cert_id, string insp_id, string man_id, string ass_id)
        {
            string test_id = _form["ddlTestCycle"].ToString();
            return RedirectToAction("AddAsset", new { cat_id = cat_id, itm_id = itm_id, cert_id = cert_id, test_id = test_id, insp_id = insp_id, man_id = man_id, ass_id = ass_id });
        }


        public ActionResult filter_man(FormCollection _form, string cat_id, string itm_id, string cert_id, string test_id, string insp_id, string ass_id)
        {
            string man_id = _form["ddlMan"].ToString();
            return RedirectToAction("AddAsset", new { cat_id = cat_id, itm_id = itm_id, cert_id = cert_id, test_id = test_id, insp_id = insp_id, man_id = man_id, ass_id = ass_id });
        }
        public ActionResult filter_asset(FormCollection _form, string cat_id, string itm_id, string cert_id, string test_id, string insp_id, string man_id)
        {
            string ass_id = _form["ddlAssets"].ToString();
            return RedirectToAction("AddAsset", new { cat_id = cat_id, itm_id = itm_id, cert_id = cert_id, test_id = test_id, insp_id = insp_id, man_id = man_id, ass_id = ass_id });
        }
        public ActionResult ViewAssets()
        {
            string username = User.Identity.Name.ToString();
            List<Tags> res = CompanyCore.get_company_tags(username);

            return View(res);
        }
        public ActionResult TimeCycle()
        {
            return View();
        }
        public ActionResult Maps()
        {
            return View();
        }
        public ActionResult ViewAllAssets()
        {
            return View();
        }

        public JsonResult SaveOrUpdateCompany(CompanyModel model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                lid_company company = dbo.lid_company.Where(m => m.company_id == model.company_id).FirstOrDefault();
                if (company == null)
                {
                    company = new lid_company();
                    company.company_name = model.company_name;
                    company.company_reg = model.company_reg;
                    company.company_trading = model.company_trading;
                    company.company_email = model.company_email;
                    company.company_tel = model.company_tel;
                    company.company_fax = model.company_fax;
                    company.company_street = model.company_street;
                    company.company_building = model.company_building;
                    company.company_city = model.company_city;
                    company.company_pcode = model.company_pcode;
                    company.company_pobox = model.company_pobox;
                    company.company_pooffice = model.company_pooffice;
                    company.company_pocity = model.company_pocity;
                    company.company_pocode = model.company_pocode;
                    company.company_logo = model.company_logo;
                     dbo.lid_company.Add(company);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Company " + model.company_name + " successfully saved";
                }
                else
                {
                    company.company_name = model.company_name;
                    company.company_reg = model.company_reg;
                    company.company_trading = model.company_trading;
                    company.company_email = model.company_email;
                    company.company_tel = model.company_tel;
                    company.company_fax = model.company_fax;
                    company.company_street = model.company_street;
                    company.company_building = model.company_building;
                    company.company_city = model.company_city;
                    company.company_pcode = model.company_pcode;
                    company.company_pobox = model.company_pobox;
                    company.company_pooffice = model.company_pooffice;
                    company.company_pocity = model.company_pocity;
                    company.company_pocode = model.company_pocode;
                    company.company_logo = model.company_logo;

                    dbo.Entry(company).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Company " + model.company_name + " successfully update";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveOrUpdateAsset(CompanyAsset model)
        {
            AjaxResults res = new AjaxResults();
            try
            {

            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                b_users user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                lid_asset asset = new lid_asset(); 
                // dbo.lid_asset.Where(m => m.asset_id == model.asset_id).FirstOrDefault();
                                                   //if (asset == null)
                                                   //{
                //asset = new lid_asset();
                asset.asset_name = model.asset_name;
                asset.asset_description = model.asset_description;
                //asset.image = model.image;
                asset.category_id = model.category_id.ToLongOrDefault();
            asset.item_id = model.item_id.ToLongOrDefault();
                asset.cert_id = model.cert_id;
                asset.manufacture_id = model.man_id;
                asset.man_asset_id = model.man_id;
                asset.load_test_cycle_id = model.load_test_cycle_id;
                asset.inspection_cycle_id = model.inspection_cycle_id;
               // asset.manufacture_id = model.man_id;
               // asset.man_asset_id = model.ass_id;
                asset.size_ton = model.size_ton;
                asset.size_meter = model.size_meter;
                asset.size_mm = model.size_mm;
                asset.company_id = user.company_id;
                dbo.lid_asset.Add(asset);
                dbo.SaveChanges();
                res.code = "1";
                res.message = "Asset " + model.asset_name + " successfully saved";
                //}
                //else
                //{
                //    asset.asset_name = model.asset_name;
                //    asset.asset_description = model.asset_description;
                //    //asset.image = model.image;
                //    asset.category_id = model.category_id.ToLongOrDefault();
                //    asset.item_id = model.item_id.ToLongOrDefault();
                //    asset.cert_id = model.cert_id;
                //    asset.load_test_cycle_id = model.load_test_cycle_id;
                //    asset.inspection_cycle_id = model.inspection_cycle_id;
                //    asset.manufacture_id = model.man_id;
                //    asset.man_asset_id = model.ass_id;
                //    //asset.ops_times = model.ops_times;
                //    //asset.time_from = model.time_from;
                //    //asset.time_to = model.time_to;

                //    dbo.Entry(asset).State = System.Data.Entity.EntityState.Modified;
                //    dbo.SaveChanges();
                //    res.code = "1";
                //    res.message = "Asset " + model.asset_name + " successfully update";
                ////}
            }

            }
            catch (Exception er)
            {
                res.code = "0";
                res.message = er.Message;
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CrudTag(Tags model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                b_users user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                lid_tags tag = dbo.lid_tags.Where(m => m.tag_id == model.tag_id).FirstOrDefault();
                if (tag == null)
                {
                    tag = new lid_tags();
                    tag.item_id = model.item_id;
                    tag.sn_no = model.sn_no;
                    tag.barcode = model.barcode;
                    tag.tag_description = model.tag_description;
                    tag.site_description = model.site_description;
                    tag.site_id = model.site_id;
                    tag.company_id = user.company_id;
                    dbo.lid_tags.Add(tag);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Tag " + model.sn_no + " successfully saved";
                }
                else
                {
                    tag.item_id = model.item_id;
                    tag.sn_no = model.sn_no;
                    tag.tag_description = model.tag_description;
                    tag.site_description = model.site_description;
                    tag.site_id = model.site_id;
                    tag.company_id = user.company_id;
                    dbo.Entry(tag).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Tag " + model.sn_no + " successfully update";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CrudInspector(CompanyInspector model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                b_users user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                lid_inspectors inspector = dbo.lid_inspectors.Where(m => m.inspector_id == model.inspector_id).FirstOrDefault();
                if (inspector == null)
                {
                    inspector = new lid_inspectors();
                    inspector.firstname = model.firstname;
                    inspector.lastname = model.lastname;
                    inspector.phone = model.phone;
                    inspector.email = model.email;
                    inspector.username = model.username;
                    inspector.password = model.password;
                    inspector.certificate_id = model.certificate_id;
                    inspector.company_id = user.company_id;
                    inspector.idnumber = model.idnumber;
                    inspector.registerno = model.registerno;
                    inspector.signature_img = model.signature_img;
                    dbo.lid_inspectors.Add(inspector);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Inspector " + model.firstname + " successfully saved";
                }
                else
                {
                    inspector.firstname = model.firstname;
                    inspector.lastname = model.lastname;
                    inspector.phone = model.phone;
                    inspector.email = model.email;
                    inspector.username = model.username;
                    inspector.password = model.password;
                    inspector.certificate_id = model.certificate_id;
                    inspector.company_id = user.company_id;
                    inspector.idnumber = model.idnumber;
                    inspector.registerno = model.registerno;
                    inspector.signature_img = model.signature_img;
                    dbo.Entry(inspector).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Inspector " + model.firstname + " successfully update";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CrudSite(CompanySites model)
        {
            AjaxResults res = new AjaxResults();
            using (lidEntities dbo = new lidEntities())
            {
                string username = User.Identity.Name;
                b_users user = dbo.b_users.Where(x => x.email == username).FirstOrDefault();
                lid_sites site = dbo.lid_sites.Where(m => m.site_id == model.site_id).FirstOrDefault();
                if (site == null)
                {
                    site = new lid_sites();
                    site.site_name = model.site_name;
                    site.site_description = model.site_description;
                    site.contact_person = model.contact_person;
                    site.contact_email = model.contact_email;
                    site.contact_tel = model.contact_tel;
                    site.address_street = model.address_street;
                    site.address_building = model.address_building;
                    site.address_city = model.address_city;
                    site.address_pcode = model.address_pcode;
                    site.company_id = user.company_id;
                    dbo.lid_sites.Add(site);
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Site " + model.site_name + " successfully saved";
                }
                else
                {
                    site.site_name = model.site_name;
                    site.site_description = model.site_description;
                    site.contact_person = model.contact_person;
                    site.contact_email = model.contact_email;
                    site.contact_tel = model.contact_tel;
                    site.address_street = model.address_street;
                    site.address_building = model.address_building;
                    site.address_city = model.address_city;
                    site.address_pcode = model.address_pcode;
                    site.company_id = user.company_id;
                    dbo.Entry(site).State = System.Data.Entity.EntityState.Modified;
                    dbo.SaveChanges();
                    res.code = "1";
                    res.message = "Site " + model.site_name + " successfully update";
                }
            }
            var results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult win_add_asettag(string id)
        {
            string username = User.Identity.Name.ToString();
            ViewBag.list_sites = new SelectList(ItemCore.get_sites_many(username), "site_id", "site_name");
            ViewBag.asset_id = id;
            return View();
        }
        public ActionResult win_add_inspector(string id)
        {
            CompanyInspector res = CompanyCore.get_inspector_byid(id);
            CompanyInspector model = res != null ? res : new CompanyInspector();
            ViewBag.list_certification = new SelectList(ItemCore.get_codevalues("Certification"), "id", "value", model.certificate_id);
            return View(model);
        }
        public ActionResult win_add_site(string id)
        {
            CompanySites res = CompanyCore.get_site_byid(id);
            CompanySites model = res != null ? res : new CompanySites();
            return View(model);
        }
        public ActionResult win_view_asettag(string id)
        {
            List<Tags> res = CompanyCore.get_assetstags_many(id.ToLongOrDefault());
            ViewBag.asset_id = id;
            return View(res);
        }
        public JsonResult GetCompanyAssets()
        {
            string username = User.Identity.Name.ToString();
            List<CompanyAsset> res = CompanyCore.get_assets_many(username);

            foreach (CompanyAsset rec in res)
            {
                if (rec.image != null)
                {
                    string imreBase64Data = Convert.ToBase64String(rec.image);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                    rec.imgDataURL = imgDataURL;
                }
            }

            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCompanySites()
        {
            string username = User.Identity.Name.ToString();
            List<CompanySites> res = CompanyCore.get_sites_many(username);

            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetCompanyAssetTags(long id)
        {
            List<Tags> res = CompanyCore.get_assetstags_many(id);

            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCompanyInspectors()
        {
            string username = User.Identity.Name.ToString();
            List<CompanyInspector> res = CompanyCore.get_inspectors_many(username);

            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCompanyTag()
        {
            string username = User.Identity.Name.ToString();
            List<Tags> res = CompanyCore.get_company_tags(username);

            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteInspectorById(CompanyInspector model)
        {
            lid_inspectors res = new lid_inspectors();
            using (lidEntities dbo = new lidEntities())
            {
                lid_inspectors link = dbo.lid_inspectors.Where(m => m.inspector_id == model.inspector_id).FirstOrDefault();
                if (link.inspector_id > 0)
                {
                    res = dbo.lid_inspectors.Where(m => m.inspector_id == model.inspector_id).FirstOrDefault();
                    if (res != null)
                    {
                        res.isactive = false;
                        dbo.Entry(res).State = System.Data.Entity.EntityState.Modified;
                        dbo.SaveChanges();
                    }
                }
            }
            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSiteById(CompanySites model)
        {
            lid_sites res = new lid_sites();
            using (lidEntities dbo = new lidEntities())
            {
                lid_sites link = dbo.lid_sites.Where(m => m.site_id == model.site_id).FirstOrDefault();
                if (link.site_id > 0)
                {
                    res = dbo.lid_sites.Where(m => m.site_id == model.site_id).FirstOrDefault();
                    if (res != null)
                    {
                        res.isactive = false;
                        dbo.Entry(res).State = System.Data.Entity.EntityState.Modified;
                        dbo.SaveChanges();
                    }
                }
            }
            string results = new JavaScriptSerializer().Serialize(res);
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}