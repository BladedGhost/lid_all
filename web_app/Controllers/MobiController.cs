﻿using Dapper;
using web_app.BLL;
using web_app.DAL;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace web_app.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    [AllowAnonymous]
    public class MobiController : ApiController
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        public IHttpActionResult Get()
        {
            return Json("My Get Method Implemeted");
        }


        [Route("mobi/auth")]
        public HttpResponseMessage PostUser(MobiUser model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            MobiUser user = new MobiUser();
            using (var dbo = GetOpenConnection())
            {
                user = dbo.Query<MobiUser>("mobi_auth_inspectors", new
                {
                    username = model.username,
                    password = model.password
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            if (user != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, user);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, user);
            }

            return resp;
        }

        [Route("mobi/inspection")]
        public HttpResponseMessage PostInspection(MobiAsset model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<MobiAsset> user = new List<MobiAsset>();
            using (var dbo = GetOpenConnection())
            {
                user = dbo.Query<MobiAsset>("mobi_get_asset_bybarcode", new
                {
                    barcode = model.barcode
                }, commandType: CommandType.StoredProcedure).ToList();
            }

            if (user != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, user);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, user);
            }

            return resp;
        }

        [Route("mobi/recent_inspections")]
        public HttpResponseMessage PostRecentInspections(MobiUser model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<MobiRecentInspections> res = new List<MobiRecentInspections>();
            using (var dbo = GetOpenConnection())
            {
                res = dbo.Query<MobiRecentInspections>("get_recent_inspections", new
                {
                    username = model.username
                }, commandType: CommandType.StoredProcedure).ToList();


                foreach (MobiRecentInspections rec in res)
                {
                    if (rec.image != null)
                    {
                        string imreBase64Data = Convert.ToBase64String(rec.image);
                        string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                        rec.imgDataURL = imgDataURL;
                    }
                }
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }


        [Route("mobi/upcoming_inspections")]
        public HttpResponseMessage PostUpcomingInspections(MobiUser model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<MobiRecentInspections> res = new List<MobiRecentInspections>();
            using (var dbo = GetOpenConnection())
            {
                res = dbo.Query<MobiRecentInspections>("get_upcoming_inspections", new
                {
                    username = model.username
                }, commandType: CommandType.StoredProcedure).ToList();


                foreach (MobiRecentInspections rec in res)
                {
                    if (rec.image != null)
                    {
                        string imreBase64Data = Convert.ToBase64String(rec.image);
                        string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                        rec.imgDataURL = imgDataURL;
                    }
                }
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }

        [Route("mobi/upd_asset")]
        public HttpResponseMessage PostUpdateAsset(MobiTag model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            using (lidEntities dbo = new lidEntities())
            {
                //long id = model.asset_id.ToLongOrDefault();
                lid_tags tag = dbo.lid_tags.Where(x => x.barcode == model.barcode).FirstOrDefault();
                //foreach (lid_tags _tag in is_tag)
                //{
                //    _tag.barcode = null;
                //    dbo.Entry(_tag).State = System.Data.Entity.EntityState.Modified;
                //    dbo.SaveChanges();
                //}
                //lid_tags tag = dbo.lid_tags.Where(x => x.tag_id == id).FirstOrDefault();

                //if (tag == null)
                //{
                tag = new lid_tags();
                tag.barcode = model.barcode;
                tag.site_id = model.site_id;
                tag.sn_no = model.serial_no;
                tag.item_id = model.asset_id;
                tag.datecreated = DateTime.Now;
                tag.company_id = 1;
                dbo.lid_tags.Add(tag);
                dbo.SaveChanges();

                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Asset successfuly updated");
                resp = Request.CreateResponse(HttpStatusCode.OK, model);
                //}
                //else
                /// {
                //     resp.Headers.Add("Err", "Error");
                //    resp.Content = new StringContent("Invalid Asset");
                //    resp = Request.CreateResponse(HttpStatusCode.NotFound, model);
                // }
            }

            return resp;
        }

        [Route("mobi/add_tag")]
        public HttpResponseMessage PostAddTag(MobiTag model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            using (lidEntities dbo = new lidEntities())
            {
                //long id = model.asset_id.ToLongOrDefault();
                lid_tags tag = dbo.lid_tags.Where(x => x.barcode == model.barcode).FirstOrDefault();
                //foreach (lid_tags _tag in is_tag)
                //{
                //    _tag.barcode = null;
                //    dbo.Entry(_tag).State = System.Data.Entity.EntityState.Modified;
                //    dbo.SaveChanges();
                //}
                lid_asset _ass = dbo.lid_asset.Where(x => x.asset_id == model.asset_id).FirstOrDefault();

                if (tag == null)
                {
                    tag = new lid_tags();
                    tag.barcode = model.barcode;
                    tag.site_id = model.site_id;
                    tag.sn_no = model.serial_no;
                    tag.item_id = model.asset_id;

                    tag.tag_latitude = model.tag_latitude;
                    tag.tag_longitude = model.tag_longitude;
                    tag.datecreated = DateTime.Now;
                    tag.company_id = _ass.company_id;
                    dbo.lid_tags.Add(tag);
                    dbo.SaveChanges();

                    resp.Headers.Add("Ok", "Ok");
                    resp.Content = new StringContent("Asset successfuly updated");
                    resp = Request.CreateResponse(HttpStatusCode.OK, model);
                }
                //else
                //{
                //    tag.barcode = model.barcode;
                //    tag.site_id = model.site_id;
                //    tag.sn_no = model.serial_no;
                //    tag.item_id = model.asset_id;

                //    tag.tag_latitude = model.tag_latitude;
                //    tag.tag_longitude = model.tag_longitude;
                //    tag.datecreated = DateTime.Now;
                //    tag.company_id = _ass.company_id;

                //    dbo.Entry(tag).State = System.Data.Entity.EntityState.Modified;
                //    dbo.SaveChanges();

                //    resp.Headers.Add("Err", "Error");
                //    resp.Content = new StringContent("Asset Successfully Updated");
                //    resp = Request.CreateResponse(HttpStatusCode.NotFound, model);
                //}
            }

            return resp;
        }


        [Route("mobi/get_assets")]
        public HttpResponseMessage GetAssets()
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<MobiAssets> res = new List<MobiAssets>();
            using (lidEntities dbo = new lidEntities())
            {
                List<lid_items> tags = dbo.lid_items.ToList();
                foreach (lid_items item in tags)
                {
                    res.Add(new MobiAssets
                    {
                        asset_id = item.item_id,
                        asset_name = item.item_name
                    });
                }
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }


        [Route("mobi/get_sites")]
        public HttpResponseMessage GetSites(string company_id)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<MobiSites> res = new List<MobiSites>();
            using (lidEntities dbo = new lidEntities())
            {
                long id = company_id.ToLongOrDefault();
                List<lid_sites> tags = dbo.lid_sites.Where(x => x.company_id == id).ToList();
                foreach (lid_sites item in tags)
                {
                    res.Add(new MobiSites
                    {
                        site_id = item.site_id,
                        site_name = item.site_name,
                        tag_count = dbo.lid_tags.Where(x => x.site_id == item.site_id).Count()
                });
                }
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }



        [Route("mobi/get_category")]
        public HttpResponseMessage GetCategories(string company_id)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<lid_category> res = new List<lid_category>();
            using (lidEntities dbo = new lidEntities())
            {
                long id = company_id.ToLongOrDefault();

                List<lid_company_cat> _cats = dbo.lid_company_cat.Where(x => x.company_id == id).ToList();
                foreach (lid_company_cat _cat in _cats)
                {
                    List<lid_category> _res = dbo.lid_category.Where(x => x.group_id == _cat.category_id).ToList();
                    foreach (lid_category _i in _res)
                    {
                        res.Add(_i);
                    }
                }
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }

        [Route("mobi/get_items")]
        public HttpResponseMessage GetItems(string cat_id)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<lid_items> res = new List<lid_items>();
            using (lidEntities dbo = new lidEntities())
            {
                long id = cat_id.ToLongOrDefault();
                res = dbo.lid_items.Where(x => x.category_id == id).ToList();
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }


        [Route("mobi/get_item_assets")]
        public HttpResponseMessage GetItemsAssets(string itm_id)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            List<lid_asset> res = new List<lid_asset>();
            using (lidEntities dbo = new lidEntities())
            {
                long id = itm_id.ToLongOrDefault();
                res = dbo.lid_asset.Where(x => x.item_id == id).ToList();
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }

        [Route("mobi/get_tag")]
        public HttpResponseMessage PostGetTag(MobiTag model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            MobiTag res = new MobiTag();
            using (lidEntities dbo = new lidEntities())
            {
                lid_tags tag = dbo.lid_tags.Where(x => x.barcode == model.barcode).FirstOrDefault();
                if (tag != null)
                {

                    lid_asset asset = dbo.lid_asset.Where(x => x.asset_id == tag.item_id && x.company_id == tag.company_id).FirstOrDefault();
                    //&& x.company_id == tag.company_id remove since tags and assets does have same company ids ids
                    Items itm = ItemCore.get_item_byid(tag.item_id.ToString());
                    if (itm != null)
                    {
                        string imreBase64Data = itm.image == null ? "" : Convert.ToBase64String(itm.image);
                        string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);
                        res.image = imgDataURL;
                    }

                    res.barcode = tag.barcode;
                    res.asset_name = asset.asset_name;
                    res.asset_description = asset.asset_description;
                }
            }

            if (res != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, res);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, res);
            }

            return resp;
        }

        [Route("mobi/save_inspection")]
        public HttpResponseMessage PostSaveInspection(MobiInspection model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };
                long inspection_id = 0;
            try
            {

                using (var dbo = GetOpenConnection())
                {
                    inspection_id = dbo.Query<long>("mobi_save_inspection", new
                    {
                        barcode = model.barcode,
                        inspector_id = model.inspector_id,
                        company_id = model.company_id,
                        comments = model.comments,
                        status = model.status,
                        image = model.image,
                        latitude = model.latitude,
                        longitude = model.longitude,
                        visual = model.visual,
                        loadtest = model.loadtest,
                        scrap = model.scrap,
                        recondition = model.recondition
                    }, commandType: CommandType.StoredProcedure).Single();

                    foreach (MobiInspectionItems item in model.inspection_items)
                    {
                        dbo.Query<long>("mobi_save_inspection_item", new
                        {
                            inspection_id = inspection_id,
                            test_id = item.test_id,
                            status = item.status,
                            comments = item.comments
                        }, commandType: CommandType.StoredProcedure).Single();
                    }
                }

                if (inspection_id != 0)
                {
                    resp.Headers.Add("Ok", "Ok");
                    resp.Content = new StringContent("Invalid token");
                    resp = Request.CreateResponse(HttpStatusCode.OK, inspection_id);
                }
                else
                {
                    resp.Headers.Add("Err", "Tag Not Registered");
                    resp.Content = new StringContent("Invalid token");
                    resp = Request.CreateResponse(HttpStatusCode.NotFound, inspection_id);
                }

            }
            catch (Exception err)
            {
                resp.Headers.Add("Error", "");
                resp.Content = new StringContent(err.Message);
                resp = Request.CreateResponse(HttpStatusCode.NotFound, inspection_id);
            }
            return resp;
        }
    }
}
