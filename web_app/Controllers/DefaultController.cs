﻿using Dapper;
using web_app.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace web_app.Controllers
{
    public class DefaultController : ApiController
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        private static SqlConnection GetOpenConnection(bool mars = false)
        {
            var cs = ConnectionString;
            if (mars)
            {
                var scsb = new SqlConnectionStringBuilder(cs)
                {
                    MultipleActiveResultSets = true
                };
                cs = scsb.ConnectionString;
            }
            var connection = new SqlConnection(cs);
            connection.Open();
            return connection;
        }

        public IHttpActionResult Get()
        {
            return Json("My Get Method Implemeted");
        }

        public HttpResponseMessage PostUser(MobiUser model)
        {
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent("GET: Test message")
            };

            MobiUser user = new MobiUser();
            using (var dbo = GetOpenConnection())
            {
                user = dbo.Query<MobiUser>("mobi_auth_inspectors", new
                {
                    username = model.username,
                    password = model.password
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            if (user != null)
            {
                resp.Headers.Add("Ok", "Ok");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.OK, user);
            }
            else
            {
                resp.Headers.Add("Err", "Tag Not Registered");
                resp.Content = new StringContent("Invalid token");
                resp = Request.CreateResponse(HttpStatusCode.NotFound, user);
            }

            return resp;
        }
    }
}
