var app = {
    dev: 'http://192.168.1.54:8999/mobi/',
    local: 'http://localhost:54965//mobi/',
    dev_off: 'http://www.petnfc.co.za/mobi/',
    initialize: function () {
        document.addEventListener('deviceready', function () {
            app.bindEvents();
        })
    },
    bindEvents: function () {
        app.onDeviceReady();
    },

    onDeviceReady: function () {

        var intent = function () {
            window.plugins.intent.getCordovaIntent(function (Intent) {
                // alert('intent:::'+JSON.stringify(Intent));
            }, function () {
                // alert('Error');
            });
        };

        var success = function (event) {
            //alert("addNdefListener success");
            //alert("event: " + JSON.stringify(event));
        };

        var failure = function (reason) {
            alert("addNdefListener failed " + reason);
        };

        // var setsuccess = function () {
        //     alert("showSettings success");
        // };

        // var setfailure = function (reason) {
        //     alert("showSettings failed " + reason);
        // };

        //intent();
        // //nfc.showSettings(setsuccess, setfailure);
        // // The NDEF listener runs in the foreground
        //nfc.addNdefListener(app.onNfc, success, failure);
        // MufuCaCa.addTagDiscoveredListener(app.onNfc, success, failure);
        nfc.addTagDiscoveredListener(app.onNfc, success, failure);

        // The Mime-Type listener is required to handle NDEF messages that
        // launch the app from an intent filters in AndroidManifest.xml.
        // For messages from intents, the mime type defined here doesn't matter.
        // Note the same event handler is used for NDEF and Mime
        nfc.addMimeTypeListener('text/json', app.onNfc, success, failure);
    },

    onNfc: function (nfcEvent) {
        //jQuery(".preloader").fadeIn();

        var tag = nfcEvent.tag;
        var tag_id = (tag.id).toString();
        //alert('tag::'+JSON.stringify(tag));
        // alert('tag id::'+ (tag.id).toString());
        //var ndefMessage = tag.ndefMessage;
        app.addTag(tag_id);

        //window.location.href = 'loading.html';
        // dump the raw json of the message
        //note: real code will need to decode
        //the payload from each record
        //alert(JSON.stringify(ndefMessage));

        // show the payload of the first record as a string
        // might produce junk depending on the record type
        //alert(nfc.bytesToString(ndefMessage[0].payload).substring(1));

    },
    addTag: function (tag) {
        tag = tag.replace(/,/g, '').replace(/-/g, '')
        app.getTag(tag);
    },
    getTag: function (sn) {
        var data = {
            barcode: sn
        };
        alert(sn);
        jQuery.ajax({
            url: config.env.uri + 'get_tag',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                //alert(JSON.stringify(response));
                app.insTag(response);
            },
            error: function (xhr, thrown, Error) {
                console.log(xhr);
                console.log(thrown);
                jQuery(".preloader").fadeOut();
            }
        });
    },
    insTag: function (tag) {
        //alert('ins');
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("INSERT INTO app_tag (id, sn, desc, name) VALUES (?,?,?,?)", [1, tag.barcode, tag.asset_description, tag.asset_name], function (tx, res) {
                    window.location.href = 'maint_home.html';
                },
                    function (e) {
                        alert("ERROR: " + e.message);
                    });
            });
        });
    },


};

app.initialize();
