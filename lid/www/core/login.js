"use strict"

var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
            jQuery(".preloader").fadeIn();
            app.bindEvents();
            jQuery(".preloader").fadeOut();
        })
    },
    bindEvents: function () {
        //app.autoLogin();
        jQuery('#btn_login').click(function () {
            jQuery(".preloader").fadeIn();
            app.Login();
        });
        jQuery(".input_keypress").keypress(function (e) {
            if (e.which == 13) {
                jQuery(".preloader").fadeIn();
                app.Login();
                return false;
            }
        })
    },
    Login: function () {
        var data = {
            password: jQuery('#password').val(),
            username: jQuery('#username').val(),
        };
        if (data.username && data.password) {
            jQuery.ajax({
                url: config.env.uri + 'auth',
                headers: {
                    'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                    'Content-Type': 'application/json'
                },
                dataType: "json",
                type: "POST",
                data: JSON.stringify(data),
                crossDomain: true,
                contentType: 'application/json; charset=utf-8',
                async: false,
                success: function (response) {
                    if (response.inspector_id) {
                        app.deleteUser(response, data.password);
                    } else {
                        alert('Invalid username and password.')
                    }
                },
                error: function (xhr, thrown, Error) {
                    $(".preloader").fadeOut();
                    alert('ERROR: ' + JSON.stringify(xhr.responseText))
                }
            });
        } else {
            $(".preloader").fadeOut();
            alert('Please fill in username and password.')
        }
    },
    deleteUser: function (data, password) {
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("delete from app_db;", [], function (tx, res) {
                    app.creatUser(data, password)
                });
            });
        });
    },
    creatUser: function (data, password) {
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                try {

                    tx.executeSql("INSERT INTO app_db(inspector_id, company_id, username, password, firstname, lastname) VALUES (?,?,?,?,?,?)",
                        [data.inspector_id, data.company_id, data.username, password, data.firstname, data.lastname],
                        function (tx, res) {
                            $(".preloader").fadeOut();
                             window.location.href = 'dash.html';
                        },
                        function (e) {
                            $(".preloader").fadeOut();
                            alert("ERROR: " + JSON.stringify(e));
                        });

                } catch (exp) {
                    $(".preloader").fadeOut();
                    alert(JSON.stringify(exp))
                }
            });
        });
    }
}

app.initialize()