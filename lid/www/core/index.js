"use strict"

var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
            app.bindEvents();
        })
    },
    bindEvents: function (position) {
        app.dbInit();
    },
    dbInit: function () {
        //document.writeln('app init.......<br/>');
        try {

            window.sqlitePlugin.selfTest(function () {
                ////document.writeln('app init.......<br/>');
                //document.writeln('app plugins .......<br/>');

                var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
                db.transaction(function (tx) {
                    //document.writeln('drop existing sql lite tables.......<br/>');
                    tx.executeSql('DROP TABLE IF EXISTS app_db');
                    tx.executeSql('DROP TABLE IF EXISTS app_tag');
                    //document.writeln('creat sql lite tables.......<br/>');
                    tx.executeSql('CREATE TABLE IF NOT EXISTS app_db (inspector_id integer primary key,company_id integer, username text, password text, firstname text, lastname text)');
                    tx.executeSql('CREATE TABLE IF NOT EXISTS app_tag (id integer primary key,sn text,desc text,name text)');

                    db.executeSql("pragma table_info (app_db);", [], function (res) {
                        //document.writeln("PRAGMA res: " + JSON.stringify(res));
                    });

                    db.executeSql("pragma table_info (app_tag);", [], function (res) {
                        //document.writeln("PRAGMA res: " + JSON.stringify(res));
                    });

                    setTimeout(
                        function () { window.location.href = 'login.html'; }, 1000
                    );
                },
                    function (e) {
                        alert("ERROR: " + e.message);
                    });
            });

        } catch (error) {

            //document.writeln('Error Init App.......<br/>');
            document.writeln(error);
        }
    }
}

app.initialize()