"use strict"

var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
            app.bindEvents();
        })
    },
    bindEvents: function () {
        app.getUser();
        //app.getToday();
        //app.getRecent();
        //app.getPostion();

        jQuery('#li_tag').click(function () {
            window.location.href = 'scan.html'
        });

        jQuery('#li_bcode').click(function () {
            window.location.href = 'bcode.html'
        });
    },
    getPostion: function () {
        //alert('get position');
        try {
            navigator.geolocation.getCurrentPosition(app.getCoodinates, app.positionError, { enableHighAccuracy: true });
        } catch (error) {
            alert(error)
        }
    },
    positionError: function () {
        //alert('position error');
    },
    getCoodinates: function (position) {
        //alert('get coords');
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;

        //alert(latitude + ' ' + longitude)
        app.getWeather(latitude, longitude);
    },
    getWeather(latitude, longitude) {

        //alert('getWeather')
        // Get a free key at http://openweathermap.org/. Replace the "Your_Key_Here" string with that key.
        var OpenWeatherAppKey = "2e0ac7d946811e7ca777e3ca66eed210";

        var queryString =
            'http://api.openweathermap.org/data/2.5/weather?lat='
            + latitude + '&lon=' + longitude + '&appid=' + OpenWeatherAppKey + '&units=imperial';

        jQuery.getJSON(queryString, function (results) {

            if (results.weather.length) {

                jQuery.getJSON(queryString, function (results) {

                    //alert('results')
                    if (results.weather.length) {
                        jQuery('#w_city').text(results.name);
                        jQuery('#w_temp').text(results.main.temp);
                        //$('#w_wind').text(results.wind.speed + ' mph');
                        // $('#humidity').text(results.main.humidity);
                        // $('#visibility').text(results.weather[0].main);

                        // var sunriseDate = new Date(results.sys.sunrise);
                        // $('#sunrise').text(sunriseDate.toLocaleTimeString());

                        // var sunsetDate = new Date(results.sys.sunrise);
                        // $('#sunset').text(sunsetDate.toLocaleTimeString());
                    }

                });
            }
        }).fail(function () {
            //console.log("error getting location");
            //alert("error getting location");
        });
    },
    getToday: function () {
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        jQuery('#date_name').text(days[new Date().getDay()]);
    },
    getCharts: function () {
        var chart = new Chartist.Line('.campaign', {
            labels: [1, 2, 3, 4, 5, 6, 7, 8],
            series: [
                [0, 5, 6, 8, 25, 9, 8, 24],
                [0, 3, 1, 2, 8, 1, 5, 1]
            ]
        }, {
            low: 0,
            high: 28,

            showArea: true,
            fullWidth: true,
            plugins: [
                Chartist.plugins.tooltip()
            ],
            axisY: {
                onlyInteger: true,
                scaleMinSpace: 40,
                offset: 20,
                labelInterpolationFnc: function (value) {
                    return (value / 1) + 'k';
                }
            },

        });

        // Offset x1 a tiny amount so that the straight stroke gets a bounding box
        // Straight lines don't get a bounding box 
        // Last remark on -> http://www.w3.org/TR/SVG11/coords.html#ObjectBoundingBox
        chart.on('draw', function (ctx) {
            if (ctx.type === 'area') {
                ctx.element.attr({
                    x1: ctx.x1 + 0.001
                });
            }
        });

        // Create the gradient definition on created event (always after chart re-render)
        chart.on('created', function (ctx) {
            var defs = ctx.svg.elem('defs');
            defs.elem('linearGradient', {
                id: 'gradient',
                x1: 0,
                y1: 1,
                x2: 0,
                y2: 0
            }).elem('stop', {
                offset: 0,
                'stop-color': 'rgba(255, 255, 255, 1)'
            }).parent().elem('stop', {
                offset: 1,
                'stop-color': 'rgba(64, 196, 255, 1)'
            });
        });

        var chart = [chart];
    },
    getUser: function () {
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("select inspector_id,company_id,username,firstname,lastname from app_db;", [], function (tx, res) {
                    jQuery('#profile_name').html(res.rows.item(0).firstname);
                    jQuery('#profile_fullname').html(res.rows.item(0).firstname + ' ' + res.rows.item(0).lastname);
                    jQuery('#dash_fullname').html('Welcome ' + res.rows.item(0).firstname + ' ' + res.rows.item(0).lastname);
                    //jQuery('#profile_notification').html(res.rows.item(0).firstname + ', you do not have any notification.');
                    app.getRecent(res.rows.item(0).username);
                    app.getComming(res.rows.item(0).username);
                    app.getSites(res.rows.item(0).company_id);
                });
            });
        });
    },
    getRecent: function (user) {
        var data = {
            username: user
        };
        jQuery.ajax({
            url: config.env.uri + 'recent_inspections',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                console.log(response);
                app.renderRecent(response);
                // //alert('Inpection Send');
                // setTimeout(
                //     function () { $(".preloader").fadeOut(); window.location.href = 'dash.html'; }, 3000
                // );
            },
            error: function (xhr, thrown, Error) {
                alert('err')
                $(".preloader").fadeOut();
            }
        });
    },
    getComming: function (user) {
        var data = {
            username: user
        };
        jQuery.ajax({
            url: config.env.uri + 'upcoming_inspections',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                console.log(response);
                app.renderUpcoming(response);
                // //alert('Inpection Send');
                // setTimeout(
                //     function () { $(".preloader").fadeOut(); window.location.href = 'dash.html'; }, 3000
                // );
            },
            error: function (xhr, thrown, Error) {
                alert('err')
                $(".preloader").fadeOut();
            }
        });
    },
    renderUpcoming: function (rows) {
        var template = '', _day = '', _week = '', _month = '', _expired = '';
        for (let x of rows) {
            try {
                template = '<div class="m-list-timeline__item">' +
                    '<span' +
                    '    class="m-list-timeline__badge m-list-timeline__badge--success"></span>' +
                    '<span class="m-list-timeline__text">' + x.asset_name + ': ' + x.site_name + '<span' +
                    '        class="m-badge m-badge--success m-badge--wide">Pending</span></span>' +
                    '<span class="m-list-timeline__time">' + x.inspection_date + '</span>' +
                    '</div><hr/>'
            }
            catch (err) {
                //template = '<div>'+ err.message + '</div><hr/>'
            }
            switch (x.time_cycle) {
                case 'day':
                    _day += template;
                    break;
                case 'week':
                    _week += template;
                    break;
                case 'month':
                    _month += template;
                    break;
                case 'expired':
                    _expired += template;
                    break;
                    default:
                            _day += '';
                        break;
            }
        }
        jQuery('#upcoming_insp_day').html(_day);
        jQuery('#upcoming_insp_week').html(_week);
        jQuery('#upcoming_insp_monthly').html(_month);
        jQuery('#upcoming_insp_expired').html(_expired);
    },
    renderRecent: function (rows) {
        var template = ''
        for (let x of rows) {
            var txt = x.inspection_status === 'fail' ? 'Failed' : 'Passed'
            var css = x.inspection_status === 'fail' ? 'text-danger' : 'text-success'
            try {
                template += '<div class="d-flex flex-row comment-row">' +
                    '<div class="p-2"><img src="' + x.imgDataURL + '"  width="50" class="rounded-circle"></div>' +
                    ' <div class="comment-text w-100">' +
                    '    <h6 class="font-medium">' + x.asset_name + '</h6>' +
                    '    <span class="m-b-15 d-block">' + x.site_name + '</span>' +
                    '    <div class="comment-footer ">' +
                    '        <span class="text-muted float-right">' + x.inspection_date + '</span>' +
                    '        <span class="label label-rounded' + css + '"><b>' + txt + '</b></span>' +
                    '        <br>' +
                    '        <span class="label"><i class="fa fa-pencil"></i>' + x.comments + '</span>' +
                    '        <br>' +
                    '        <span class="action-icons text-success"><i class="fa fa-user"></i>' + x.fullname + '</span>' +
                    '    </div>' +
                    ' </div><hr/>' +
                    '</div><hr/>'
            }
            catch (err) {
                template += '<div>' + err.message + '</div><hr/>'
            }
        }
        jQuery('#recent_insp').html(template)
    },
    getSites: function (id) {
        jQuery.ajax({
            url: config.env.uri + 'get_sites?company_id=' + id,
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "GET",
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                app.renderSites(response)
            },
            error: function (xhr, thrown, Error) {
                //$(".preloader").fadeOut();
                alert(JSON.stringify(xhr.responseText))
            }
        });
    },
    renderSites: function (rows) {
        var template = ''
        for (let x of rows) {
            template += '<div class="m-timeline-3__item m-timeline-3__item--success">' +
                '    <span class="m-timeline-3__item-time">' + x.tag_count + ' <small style="font-size:6px">Tags</small></span>' +
                '    <div class="m-timeline-3__item-desc">' +
                '        <span class="m-timeline-3__item-text">' + x.site_name + '</span>' +
                '        <br />' +
                '        <span class="m-timeline-3__item-user-name">' +
                '            <a href="#"' +
                '                class="m-link m-link--metal m-timeline-3__item-link">' +
                '                ' + x.site_name + '' +
                '            </a>' +
                '        </span>' +
                '    </div>' +
                '</div>'
        }
        jQuery('#site_list').html(template)
    }
    // addTag: function (sn) {
    //     window.sqlitePlugin.selfTest(function () {
    //         var db = window.sqlitePlugin.openDatabase({ name: 'nfc.db', location: 'default' });
    //         db.transaction(function (tx) {

    //             tx.executeSql("INSERT INTO app_tag (sn) VALUES (?)", [sn], function (tx, res) {

    //                  window.location.href = 'loading.html';
    //             },
    //                 function (e) {
    //                     //alert("ERROR: " + e.message);
    //                 });
    //         });
    //     });
    // }
}

app.initialize()
