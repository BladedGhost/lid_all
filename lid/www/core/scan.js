var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
            app.bindEvents();
        })
    },
    bindEvents: function () {
        app.onDeviceReady();
    },
    onDeviceReady: function () {

        var intent = function () {
            window.plugins.intent.getCordovaIntent(function (Intent) {
                // alert('intent:::'+JSON.stringify(Intent));
            }, function () {
                // alert('Error');
            });
        };

        var success = function (event) {
            //alert("addNdefListener success");
            //alert("event: " + JSON.stringify(event));
        };

        var failure = function (reason) {
            alert("addNdefListener failed " + reason);
        };

        nfc.addTagDiscoveredListener(app.onNfc, success, failure);
        nfc.addMimeTypeListener('text/json', app.onNfc, success, failure);
    },

    onNfc: function (nfcEvent) {
        var tag = nfcEvent.tag;
        var tag_id = (tag.id).toString();
       // alert(tag_id)
        app.addTag(tag_id);
    },
    addTag: function (tag) {
        tag = tag.replace(/,/g, '').replace(/-/g, '')
        app.getUser(tag);
    },
    getUser: function (tag) {
        //alert('get user')
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("select inspector_id,company_id,username,firstname,lastname from app_db;", [], function (tx, res) {
                    jQuery('#profile_firstname').html(res.rows.item(0).firstname);
                    jQuery('#profile_username').html(res.rows.item(0).username);
                    app.getTag(tag,res.rows.item(0).company_id);

                });
            });
        });
    },
    getTag: function (sn,company_id) {
        //alert('get tag')
        var data = {
            barcode: sn,
            company_id: company_id
        };
        //alert(JSON.stringify(data, null, 4))
        jQuery.ajax({
            url: config.env.uri + 'get_tag',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                //alert('response')
                console.log(response)
                app.deleteTag(response);
            },
            error: function (xhr, thrown, error) {
                alert('response error')
                console.log(xhr)
                console.log(thrown)
                console.log(error)
                jQuery(".preloader").fadeOut();
            }
        });
    },
    deleteTag: function (tag) {
        //alert('del')
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("delete from app_tag;", [], function (tx, res) {
                    //alert('del.')
                    app.insTag(tag)
                });
            });
        });
    },
    insTag: function (tag) {
        //alert('ins')
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("INSERT INTO app_tag (id, sn, desc, name) VALUES (?,?,?,?)", [1, tag.barcode, tag.asset_description, tag.asset_name], function (tx, res) {
                   
        //alert('ins.') 
        window.location.href = 'form.html';
                },
                    function (e) {
                        alert("ERROR: " + e.message);
                    });
            });
        });
    },


};

app.initialize();
