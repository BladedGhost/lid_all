"use strict"

var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
           app.bindEvents();
        })
    },
    bindEvents: function () {
        jQuery(".preloader").fadeIn();
        app.getUsers();
        jQuery('#cat_list').change(function() {
            jQuery(".preloader").fadeIn();
            app.getItems(this.value);
            jQuery(".preloader").fadeOut();
        });
        jQuery('#itm_list').change(function() {
            jQuery(".preloader").fadeIn();
            app.getAssets(this.value)
            jQuery(".preloader").fadeOut();
        });
        app.initTagListener();
        
        navigator.geolocation.getCurrentPosition(app.onSuccess, app.onError, { enableHighAccuracy: true });
        jQuery(".preloader").fadeOut();
    }, 
    onSuccess: function(position) {
        //alert('position.coords.latitude')
        //alert(position.coords.latitude)
        jQuery('#pos_lat').html(position.coords.latitude);
        jQuery('#pos_lon').html(position.coords.longitude);
    },
    onError: function(error) {
        alert('Position Error Code: '  + error.code + ', Message: ' + error.message);
    },
    getUsers: function () {
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("select inspector_id,company_id,username,password,firstname,lastname from app_db;", [], function (tx, res) {
                    jQuery('#txt_company_id').val(res.rows.item(0).company_id);
                    jQuery('#txt_inspector_id').val(res.rows.item(0).inspector_id);
                    app.getCategory(res.rows.item(0).company_id);
                app.getSites(res.rows.item(0).company_id);
                });
            },
                function (e) {
                    jQuery(".preloader").fadeOut();
                    window.location.href = 'login.html';
                });
        });
    },
    initTagListener: function () {

        var intent = function () {
            window.plugins.intent.getCordovaIntent(function (Intent) {
            }, function () {
            });
        };

        var success = function (event) {
        };

        var failure = function (reason) {
            alert("addNdefListener failed " + reason);
        };
        nfc.addTagDiscoveredListener(app.onNfc,success, failure);
        nfc.addMimeTypeListener('text/json', app.onNfc, success, failure);
    },
    onNfc: function (nfcEvent) {
        var tag = nfcEvent.tag;
        var tag_id = (tag.id).toString();
        app.prepTag(tag_id);
    },
    prepTag: function (tag) {
        tag = tag.replace(/,/g, '').replace(/-/g, '')
        app.saveTag(tag);
    },
    saveTag: function (tag) {
        var data = {
            category_id: jQuery('#cat_list').find("option:selected").val(),
            item_id: jQuery('#itm_list').find("option:selected").val(),
            asset_id: jQuery('#asset_list').find("option:selected").val(),
            site_id: jQuery('#site_list').find("option:selected").val(),
            serial_no: jQuery('#txt_serial').val(),
            tag_latitude: jQuery('#pos_lat').html(),
            tag_longitude: jQuery('#pos_lon').html(),
            company_id: jQuery('#txt_company_id').val(),
            inspector_id: jQuery('#txt_inspector_id').val(),
            barcode: tag,
        };
        jQuery.ajax({
            url: config.env.uri + 'add_tag',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                alert('Tag saved successfully');
                window.location.href = 'dash.html';
                
            },
            error: function (xhr, thrown, Error) {
                $(".preloader").fadeOut();
            }
        });
    },
    getCategory: function (id) {
        jQuery.ajax({
            url: config.env.uri + 'get_category?company_id='+ id,
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "GET",
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                //alert('Authed')
                console.log('Cat')
                console.log(response)
                app.renderList(response)
            },
            error: function (xhr, thrown, Error) {
                //$(".preloader").fadeOut();
                //alert(JSON.stringify(xhr.responseText))
            }
        });
    },
    renderList: function (rows) {
        var template = '<option value="0">Select Category...</option>'  
        for (let x of rows) {
            template += x ? '<option value="' + x.category_id + '">' + x.category_name + '</option>' : ''
        }
        if(template == ''){
            template = '<option value="0">0 Category Found</option>'  
        }
        jQuery('#cat_list').html(template)
    },
    getItems: function (cat_id) {
        jQuery.ajax({
            url: config.env.uri + 'get_items?cat_id='+cat_id,
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "GET",
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                //alert('Authed')
                console.log('Item')
                console.log(response)
                app.renderItems(response)
            },
            error: function (xhr, thrown, Error) {
                //$(".preloader").fadeOut();
                //alert(JSON.stringify(xhr.responseText))
            }
        });
    },
    renderItems: function (rows) {
        var template = '<option value="0">Please select item...</option>'    
        for (let x of rows) {
            template += '<option value="' + x.item_id + '">' + x.item_name + '</option>'
        }
        if(template == ''){
            template = '<option value="0">0 Items Found</option>'  
        }
        jQuery('#itm_list').html(template)
    },
    getSites: function (id) {
        jQuery.ajax({
            url: config.env.uri + 'get_sites?company_id='+ id,
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "GET",
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                //alert('Authed')
                console.log('Site')
                console.log(response)
                app.renderSites(response)
            },
            error: function (xhr, thrown, Error) {
                //$(".preloader").fadeOut();
                //alert(JSON.stringify(xhr.responseText))
            }
        });
    },
    renderSites: function (rows) {
        var template = ''
        for (let x of rows) {
            template += '<option value="' + x.site_id + '">' + x.site_name + '</option>'
        }
        if(template == ''){
            template += '<option value="0">0 Sites Found</option>'  
        }
        jQuery('#site_list').html(template)
    },
    getAssets: function (itm_id) {
        jQuery.ajax({
            url: config.env.uri + 'get_item_assets?itm_id='+itm_id,
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "GET",
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                //alert('Authed')
                console.log('Asset')
                console.log(response)
                app.renderAssets(response)
            },
            error: function (xhr, thrown, Error) {
                //$(".preloader").fadeOut();
                //alert(JSON.stringify(xhr.responseText))
            }
        });
    },
    renderAssets: function (rows) {
        var template = '<option value="0">Please Select Asset...</option>'
        for (let x of rows) {
            template += '<option value="' + x.asset_id + '">' + x.asset_description + '</option>'
        }
        if(template == ''){
            template = '<option value="0">0 Assets Found</option>'  
        }
        jQuery('#asset_list').html(template)
    }
}

app.initialize()