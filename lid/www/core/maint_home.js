"use strict"

var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
            app.bindEvents();
        })
    },
    bindEvents: function (position) {
        app.getUser();
        app.getTag();
        jQuery('#btn_save').click(function () {
            alert('init save')
            app.getPosition();
        });
        jQuery('#btn-capture').click(function () {
            app.capturePhoto();
        });
    },
    capturePhoto: function () {
        var cameraOptions = {
            // Some common settings are 20, 50, and 100
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,
            // In this app, dynamically set the picture source, Camera or photo gallery
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.JPEG,
            mediaType: Camera.MediaType.PICTURE,
            saveToPhotoAlbum: true,
            allowEdit: false,
            correctOrientation: true  //Corrects Android orientation quirks
        };

        navigator.camera.getPicture(function cameraSuccess(imageUri) {

            app.displayImage(imageUri);

        }, function cameraError(error) {
            alert("Unable to obtain picture: " + error, "app");

        }, cameraOptions);
    },
    displayImage: function (imageData) {
        //alert(imageData)
        var image = document.getElementById('img-photo');
        image.style.display = 'block';
        image.src = imageData;
    },
    getUser: function () {
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("select inspector_id,company_id,username,firstname,lastname from app_db;", [], function (tx, res) {
                    jQuery('#profile_firstname').html(res.rows.item(0).firstname);
                    jQuery('#profile_username').html(res.rows.item(0).username);
                });
            });
        });
    },
    getPosition: function () {
        
        //app.getInputValues('');
        // navigator.geolocation.getCurrentPosition(positionSuccess);
        // var positionSuccess = function (position) {
        //     alert('positionSuccess');
        //     app.getInputValues(position);
        // };
        
        alert('positionSuccess');
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("select inspector_id,company_id,username,firstname,lastname from app_db;", [], function (tx, res) {
                    //jQuery('#profile_firstname').text(res.rows.item(0).firstname);
                    app.getInputValues('', res.rows.item(0).username);
                });
            });
        });
    },
    getInputValues: function (position, username) {

        alert('init data')
        var data = [];
        jQuery('#insp_form *').filter('.form-group').each(function () {
            var id = jQuery(this).find('#hdn_id').val();
            alert(id);
            data.push({
                test_id: id,
                comments: jQuery(this).find('#txt_' + id).val(),
                status: jQuery('input[name=rad_' + id + ']:checked').val()
            });
        })

        alert('get pos');
        alert(username);
        var json = {
            barcode: jQuery('#item_barcode').text(),
            inspector: username,
            comments: jQuery('#txt_inspection_comments').val(),
            status: jQuery('input[name=rad_pass_inspection_status]:checked').val(),
            image: document.getElementById('img-photo').src,
            latitude: '', // position.coords.latitude,
            longitude: '', //position.coords.longitude,
            inspection_items: data
        }

        app.postData(json);
    },
    postData: function (data) {
        alert('init post');
        try {
         
        jQuery.ajax({
            url: config.env.uri + 'save_inspection',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                alert('Inpection Send');
                setTimeout(
                    function () { jQuery(".preloader").fadeOut(); window.location.href = 'dash.html'; }, 3000
                );
            },
            error: function (xhr, thrown, error) {
                alert('xhr');
                alert(JSON.stringify(xhr));
                alert(JSON.stringify(thrown));
                alert(error);
                jQuery(".preloader").fadeOut();
            }
        });
           
    } catch (error) {
            
        alert('error');
        alert(error);
    }
    },
    getTag: function () {
        //app.getForm('44231028674128');
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                // alert('elect sn from app_tag')
                tx.executeSql("select sn,desc,name from app_tag;", [], function (tx, res) {
                    var tag = res.rows.item(0).sn;
                    jQuery('#item_name').text(res.rows.item(0).name);
                    jQuery('#item_desc').text(res.rows.item(0).desc);
                    jQuery('#item_barcode').text(res.rows.item(0).sn);
                    jQuery('#item_img').attr('src', res.rows.item(0).image);
                    app.getForm(tag);
                    app.deleteTag();
                });
            });
        });
    },
    deleteTag: function () {
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("delete from app_tag;", [], function (tx, res) {
                });
            });
        });
    },
    getForm: function (sn) {
        app.sn_number = sn;
        var data = {
            barcode: sn
        };

        jQuery.ajax({
            url: config.env.uri + 'inspection',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                console.log(response);
                app.renderForm(response);
            },
            error: function (xhr, thrown, Error) {
                console.log(xhr);
                console.log(thrown);
                jQuery(".preloader").fadeOut();
            }
        });
    },
    renderForm: function (rows) {
        var template = ''
        for (let x of rows) {
            template += '<div class="form-group">' +
                '    <label class="col-md-12">' + x.test_description + '</label>' +
                '    <div class="col-md-12">' +
                '        <label class="radio-inline" style="width: 45%;"><input type="radio" value="pass" id="rad_pass_' + x.test_id + '" name="rad_' + x.test_id + '"' +
                '                checked> Pass</label>' +
                '        <label class="radio-inline" style="width: 45%;"><input type="radio" value="fail" id="rad_fail_' + x.test_id + '" name="rad_' + x.test_id + '">' +
                '            Fail</label>' +
                '    </div>' +
                '    <div class="col-md-12">' +
                '        <input type="hidden" name="hdn_id" id="hdn_id" value="' + x.test_id + '">' +
                '        <input type="text" name="txt_' + x.test_id + '" id="txt_' + x.test_id + '" placeholder="Comments" class="form-control form-control-line">' +
                '    </div>' +
                '</div>'
        }
        jQuery('#insp_form').html(template)
    }
}

app.initialize()