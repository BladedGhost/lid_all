"use strict"

var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
            app.bindEvents();
        })
    },
    bindEvents: function () {
        app.dbKill();
    },
    dbKill: function () {
            try {
                window.sqlitePlugin.selfTest(function () {
                    var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
                    db.transaction(function (tx) {
                        tx.executeSql('DROP TABLE IF EXISTS app_db');
                        tx.executeSql('DROP TABLE IF EXISTS app_tag');
    
                        tx.executeSql('CREATE TABLE IF NOT EXISTS app_db (inspector_id integer primary key,company_id integer, username text, password text, firstname text, lastname text)');
                        tx.executeSql('CREATE TABLE IF NOT EXISTS app_tag (id integer primary key,sn text,desc text,name text)');
    
                        db.executeSql("pragma table_info (app_db);", [], function (res) {
                            //document.writeln("PRAGMA res: " + JSON.stringify(res));
                        });
    
                        db.executeSql("pragma table_info (app_tag);", [], function (res) {
                            //document.writeln("PRAGMA res: " + JSON.stringify(res));
                        });
                    },
                        function (e) {
                            alert("ERROR: failed creating local tables,  ||" + e.message);
                        });
                });
                window.location.href = 'login.html';
            } catch (error) {
                alert("ERROR: failed creating local storage || " + e.message);
                window.location.href = 'login.html';
            }
        }
}

app.initialize()