var app = {
    initialize: function () {
        document.addEventListener('deviceready', function () {
            app.bindEvents();
        })
    },
    bindEvents: function () {
        jQuery('#btn_go').click(function () {
            var bcode = jQuery('#txt_bcode').val();
            console.log(bcode);
            app.getTag(bcode);
        });
    },
    getTag: function (sn) {
        var data = {
            barcode: sn
        };
        jQuery.ajax({
            url: config.env.uri + 'get_tag',
            headers: {
                'token': '0Um1EB58as5We6quii5XGnmsbtBsb5La',
                'Content-Type': 'application/json'
            },
            dataType: "json",
            type: "POST",
            data: JSON.stringify(data),
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (response) {
                //alert(JSON.stringify(response));
                app.insTag(response);
            },
            error: function (xhr, thrown, Error) {
                console.log(xhr);
                console.log(thrown);
                jQuery(".preloader").fadeOut();
            }
        });
    },
    insTag: function (tag) {
        //alert('ins');
        window.sqlitePlugin.selfTest(function () {
            var db = window.sqlitePlugin.openDatabase({ name: 'lid.db', location: 'default' });
            db.transaction(function (tx) {
                tx.executeSql("INSERT INTO app_tag (id, sn, desc, name) VALUES (?,?,?,?)", [1, tag.barcode, tag.asset_description, tag.asset_name], function (tx, res) {
                    window.location.href = 'form.html';
                },
                    function (e) {
                        alert("ERROR: " + e.message);
                    });
            });
        });
    },


};

app.initialize();
